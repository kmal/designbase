jQuery(function() {
  jQuery('.drupaledit').each(function() {
    var elm = jQuery(this),
        className = this.className.match(/edit-(\w+)-(\d+)-?(\d+)?/),
        type = className[1] || '',
        id = className[2] || 0,
        addon = (className[3]) ? '/'+ className[3] : '',
        link,
        path;
        
    switch(type) {
      case 'node':
        path = '/node/'+ id +'/edit'+ addon;
        break;
      case 'term':
        path = '/admin/content/taxonomy/edit/term/'+ id;
        break;
    }
    
    if(path) {
      link = jQuery('<a href="'+ path +'" class="editbutton"></a>');
      elm.prepend(link.click(function(e) { e.stopPropagation(); }));
    }

  });
});