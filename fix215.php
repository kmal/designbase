#!/pack/php/bin/php -q
<?php
define('DRUPAL_ROOT', getcwd());
require_once 'includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
$sql = "SELECT * FROM {field_data_field_logo_print}";
$result = db_query($sql, array());
foreach ($result as $row) {
  $node = node_load($row->entity_id);
  $directory = 'public://suppliers/print/';
  $file = new stdClass();
  $file->uid = $node->uid;
  $file->filename = $node->field_logo_print['und'][0]['filename'];
  $file->uri = $node->field_logo_print['und'][0]['uri'];
  $file->filemime = $node->field_logo_print['und'][0]['filemime'];
  $file->filesize = $node->field_logo_print['und'][0]['filesize'];
  $image_print_file = $file;
  switch ($image_print_file->filemime) {
    case 'application/pdf':
      $fileinfo['extension'] = 'pdf';
    break;
    case 'application/postscript':
      $fileinfo['extension'] = 'eps';
    break;
    case 'application/illustrator':
      $fileinfo['extension'] = 'ai';
    break;
    case 'application/photoshop':
      $fileinfo['extension'] = 'psd';
    break;
    case 'application/tiff':
      $fileinfo['extension'] = 'tif';
    break;
    default:
      $fileinfo = image_get_info($image_print_file->uri);
    break;
  }
  
  $destination = file_stream_wrapper_uri_normalize($directory . $node->nid . '_1.' . $fileinfo['extension']);
  if ($final_file = file_copy($image_print_file, $destination, FILE_EXISTS_REPLACE)) {
    $saved_file = file_save($final_file);
    $node->field_file_print[LANGUAGE_NONE][0] = (array)$saved_file;
    $node->field_file_print[LANGUAGE_NONE][0]['display'] = 1;
    node_save($node);
  }
}
