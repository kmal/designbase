﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
  config.toolbar = [ [ 'Source', '-', 'Bold',  'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ] ];
//  config.uiColor = '#FFFFFF';
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};
