<?php if (!empty($content)) : ?>
<div class="mod box further-reading">
	<div class="inner special">
		<div class="hd">
			<h4><?php print variable_get('admin_related_article_header', 'L&aelig;s ogs&aring;'); ?></h4>
		</div>
		<div class="bd">
			<div class="custom-list link-overlay">
				<ul>
          <?php foreach ($content as $node): ?>
          <?php if (!isset($node->field_headline["und"][0]["value"]) || !isset( $node->field_teaser["und"][0]["value"])) { continue; } ?>
					<li>            
            <h5><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
            <?php if (isset($node->category)): ?>
              <span class="tag"><?php print $node->category; ?></span>
            <?php endif; ?>
            <span class="date">| <?php print get_date($node->created); ?></span>
						<p><?php print $node->field_teaser["und"][0]["value"]; ?></p>
					</li>
          <?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
