<div class="c100 article-carousel">

	<div class="box-slideshow">
		<div class="box-slideshow-nav-left"></div>
		<div class="box-slideshow-nav-right"></div>
		<div class="box-slideshow-content">
			<ul class="ss clearfix">
				<?php foreach($content as $term) { ?> 
				<li class="box">
          <div class="term-name"><?php print ucfirst(mb_strtolower($term['term'])); ?></div>
          <div class="content"> 
            <?php 
            $nodes = $term['nodes']; 
            for ( $j=0; $j < count($nodes); $j++) { ?>
              <div class="row">  
                <div class="date"><?php print date('d. M Y', $nodes[$j]['date']);?></div>
                <div class="title">
                    <a href="<?php print $nodes[$j]['path']; ?>" title="<?php print $nodes[$j]['title']; ?>"><?php print $nodes[$j]['title']; ?></a>
                </div>
              </div>
            <?php } ?>  
          </div>    
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>
