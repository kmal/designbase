<?php

  // Load preprocess functions
require "preprocess/preprocess_node.php";

/**
 * Override or insert variables into the node template.
 */
function designbase_preprocess_node(&$variables) {
  $node_wrapper = entity_metadata_wrapper('node', $variables['node']);

  $node = $variables['node'];
  //Preprocess function
  $preprocess_function = "_preprocess_" . $node->type;
  //If preprocess function exists, call it
  if (function_exists($preprocess_function)) {
    $preprocess_function($node);
  }
  
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
  if ($variables['type'] == 'event_post' && $variables['view_mode'] == 'full') {
    $node_wrapper = entity_metadata_wrapper('node', $variables['node']);
    $variables['page'] = FALSE;
    $variables['display_submitted'] = FALSE;
    $variables['title_prefix'] = $node_wrapper->field_event_type->name->value();
    $variables['title'] = $node_wrapper->field_headline->value();
    $variables['share_block'] = TRUE;
    if (!isset($variables['content']['flag_my_designbase'])) {
      $variables['flag_my_designbase'] = flag_create_link('my_designbase', $node_wrapper->nid->value());
    }
  }
  
  if ($variables['type'] == 'job' && $variables['view_mode'] == 'full') {
    $node_wrapper = entity_metadata_wrapper('node', $variables['node']);
    $variables['display_submitted'] = FALSE;
    $variables['title'] = $node_wrapper->field_headline->value();
    $variables['content']['#suffix'] = '<div class="automatic_text">Skriv venligst at du har set stillingsopslaget på designbase.dk</div>';
    $protocol = (strpos('http', $node_wrapper->field_www->value()) === FALSE) ? 'http://' : '';
    $attributes = array(
			'attributes' => array(
        'class' => array('url', 'link-plus'), 
        'target' => '_blank'
      )
    );
    $variables['content']['field_www'][0]['#markup'] = l('SØG JOB', $protocol . $variables['content']['field_www']['#items'][0]['value'], $attributes);
  }
  
  if ($variables['type'] == 'product_news' && $variables['view_mode'] == 'full') {
    $node_wrapper = entity_metadata_wrapper('node', $variables['node']);
    $variables['page'] = FALSE;
    $variables['display_submitted'] = FALSE;
    $variables['title_prefix'] = $node_wrapper->field_category->name->value();
    $variables['title'] = $node_wrapper->field_headline->value();
    $variables['content']['field_byline'][0]['#markup'] .= ' | ' . l('See showroom', 'taxonomy/term/' . $node_wrapper->field_company[0]->tid->value());  
    $variables['share_block'] = TRUE;
    if (!isset($variables['content']['flag_my_designbase'])) {
      $variables['flag_my_designbase'] = flag_create_link('my_designbase', $node_wrapper->nid->value());
    }
  }
  
  if ($variables['type'] == 'name_article' && $variables['view_mode'] == 'full') {
    $node_wrapper = entity_metadata_wrapper('node', $variables['node']);
    $variables['page'] = FALSE;
    $variables['title_prefix'] = node_type_get_name('name_article');
    $variables['title'] = $node_wrapper->field_headline->value();

    $company = taxonomy_term_load($node_wrapper->field_company[0]->tid->value());
    $supplier = _get_supplier_by_company_tid($node_wrapper->field_company[0]->tid->value());
    $variables['title_suffix'] = $company->name;
    if (is_object($supplier)) {
      $variables['title_suffix'] .= ' | ' . l('See showroom', 'node/' . $supplier->nid);
    }
  }
  
  if ($variables['type'] == 'partner_profile' && $variables['view_mode'] == 'full') {
    $protocol = (strpos('http', $node->field_www['und'][0]['value']) === FALSE)?'http://':'';
    $attributes = array(
			'attributes' => array(
        'class' => array('url'), 
        'target' => '_blank'
      )
    );
    $variables['content']['field_www'][0]['#markup'] = l($variables['content']['field_www']['#items'][0]['value'], $protocol . $variables['content']['field_www']['#items'][0]['value'], $attributes);
  }
}



/**
 * Theme callback for jQeury carousel field formatter.
 */
function designbase_jquery_carousel_field_formatter($vars) {

  _jquery_carousel_settings_format($vars['settings']);
  _jquery_carousel_include_css_js($vars['settings']);

  $output = '';

  if (is_array($vars['element']) && (count($vars['element']) > 1)) {
    $selector_class = drupal_attributes(array('class' => $vars['settings']['selector']));
    $output .= '<div ' . $selector_class . '">';
    $output .= '<ul>';
    foreach ($vars['element'] as $elem) {
      $output .= '<li>';
      $output .= theme('image_style', array('path' => $elem['uri'], 'style_name' => $vars['settings']['style_name']));
      $output .= '<div class="image-description">
        <div class="show-title"></div>
        <div class="image-title hidden">
          '. $elem['title'] .'
        </div>
      </div>';
      $output .= '</li>';
    }
    $output .= '</ul>';
    $output .= '</div>';
  }
  else {
    $output .= theme('image_style', array('path' => $vars['element'][0]['uri'], 'style_name' => $vars['settings']['style_name']));
  }

  return $output;
}

/**
 * Implements hook_form_alter().
 * Adds values to the company and email field of the registration form, if present in the querystring.
 *
 * @param type $form
 * @param type $form_state
 * @param type $form_id
 */
function designbase_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id === 'user_register_form') {
    $form['field_company']['und']['#default_value'] = isset($_GET['company']) ? ($_GET['company'] === 'FIRMA' ? '' : $_GET['company']) : '';
    $form['account']['mail']['#default_value'] = isset($_GET['email']) ? ($_GET['email'] === 'EMAIL' ? '' : $_GET['email']) : '';
  }
}

/**
 * A preprocess function for views.
 * This limits the number of supplier news set in the page settings.
 */
function designbase_preprocess_views_view(&$variables) {
  // Only do stuff if the view is the right one.
  if ($variables['view']->name === 'frontpage_primary_supplier_artic') {
    // Get the variable from the page settings.
    $limit = variable_get('admin_number_of_primary_supplier_news', '');

    // If the variables is set in the page settings, do stuff.
    // Otherwise, leave it to the view to decide how many news to display.
    if ($limit !== '') {
      $i = 0;
      // A temp array to hold each renderable node.
      $rows = array();

      while($i < $limit) {
        $row = $variables['view']->result[$i];
        $node = node_load($row->nid);
        $rows[] = node_view($node);

        $i++;
      }

      // Render the temp array, and put it into the rows variable, used in the view template.
      $variables['rows'] = render($rows);
    }
  }
}

/**
 * designbase_html_head_alter
 * adds facebook open graph metatags for sharing articles on fb
 *
 * @param mixed $elements
 * @access public
 * @return void
 */
function designbase_html_head_alter(&$elements) {
  $elements = array();
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));
    if ($node->type == 'article') {
      $elements['og_meta_title'] = array(
        '#type' => 'html_tag',
        '#tag' => 'meta',
        '#attributes' => array(
          'property' => 'og:title',
          'content' => $node->field_headline['und'][0]['value'],
        ),
      );
      if (isset($node->field_image['und'][0])) {
        $elements['og_meta_image'] = array(
          '#type' => 'html_tag',
          '#tag' => 'meta',
          '#attributes' => array(
            'property' => 'og:image',
            'content' => file_create_url($node->field_image['und'][0]['uri']),
          ),
        );
      }
      if (isset($node->field_teaser['und'][0])) {
        $elements['og_meta_description'] = array(
          '#type' => 'html_tag',
          '#tag' => 'meta',
          '#attributes' => array(
            'property' => 'og:description',
            'content' => $node->field_teaser['und'][0]['value'],
          ),
        );
      }
    }
  }
}

function _get_top_messages() {
  $out = "";
  $msg = drupal_get_messages("fagportaler_top");
  if (count($msg) > 0) {
    $out.= "<ul>";
    foreach ($msg['fagportaler_top'] as $m) {
      $out.= "<li>".$m."</li>";
    }
    $out.= "</ul>";
  }
  return $out;
}

function _get_status_messages() {
  $out = "";
  $msg = drupal_get_messages("fagportaler_status");
  if (count($msg) > 0) {
    $out.= "<ul>";
    foreach ($msg['fagportaler_status'] as $m) {
      $out.= "<li>".$m."</li>";
    }
    $out.= "</ul>";
  }
  return $out;
}

function _get_error_messages() {
  $out = "";
  $msg = drupal_get_messages("fagportaler_error");
  if (count($msg) > 0) {
    $out.= "<ul>";
    foreach ($msg['fagportaler_error'] as $m) {
      foreach ($m as $error) {
        $out.= "<li>".$error."</li>";
      }
    }
    $out.= "</ul>";
  }
  return $out;
}


function _get_job_error_messages() {
  $out = "";
  $msg = drupal_get_messages("job_error");
  if (count($msg) > 0) {
    $out.= "<ul>";
    foreach ($msg['job_error'] as $m) {
      foreach ($m as $error) {
        $out.= "<li>".$error."</li>";
      }
    }
    $out.= "</ul>";
  }
  return $out;
}

//function designbase_status_messages($variables) {
//  $display = $variables['display'];
//  $output = '';
//
//  $status_heading = array(
//    'status' => t('Status message'),
//    'error' => t('Error message'),
//    'warning' => t('Warning message'),
//  );
//  $types = array("status", "error", "warning");
//  $elms = array();
//  foreach ($types as $type) {
//    $elms[$type] = drupal_get_messages($type);
//  }
//  foreach ($types as $type => $messages) {
//    $output .= "<div class=\"messages $type\">\n";
//    if (!empty($status_heading[$type])) {
//      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
//    }
//    if (count($messages) > 1) {
//      $output .= " <ul>\n";
//      foreach ($messages as $message) {
//        $output .= '  <li>' . $message . "</li>\n";
//      }
//      $output .= " </ul>\n";
//    }
//    else {
//      $output .= $messages[0];
//    }
//    $output .= "</div>\n";
//  }
//  return $output;
//}

function designbase_fieldset($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper'));

  $output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';

  if (isset($variables['element']['#id']) && $variables['element']['#id'] == "user_user_form_group_userinfo" && !empty($element['#title'])) {
    $element['#title'] = "<h1>".$element['#title']."</h1>";
  }

  if (!empty($element['#title'])) {
    // Always wrap fieldset legends in a SPAN for CSS positioning.
    $output .= '<legend><span class="fieldset-legend">' . $element['#title'] . '</span></legend>';
  }

  if (isset($variables['element']['#id'])) {
    if ($variables['element']['#id'] == "user_user_form_group_userinfo") {
      $output .= "<p>".variable_get("admin_register_description")."</p>";
    }

    if ($variables['element']['#id'] == "edit-login-set") {
      $output .= "<p>".variable_get("admin_login_description")."</p>";
    }
  }
    $output .= '<div class="fieldset-wrapper">';
    if (!empty($element['#description'])) {
      $output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
    }
    $output .= $element['#children'];
    if (isset($element['#value'])) {
      $output .= $element['#value'];
    }
    $output .= '</div>';
    $output .= "</fieldset>\n";
    return $output;

}

/**
 * Implements theme_pager().
 */
function designbase_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this dotpager jsshowpiece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this dotpager jsshowpiece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this dotpager jsshowpiece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];

  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }

  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  if ($pager_total[0] == 1) {
    return "";
  }
  // End of generation loop preparation.
  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('første')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (t('Forrige')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('Næste')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('sidste')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] >= 1) {
    if ($li_previous) {
      $items[] = array(
        'class' => array('back'),
        'data' => $li_previous,
      );
    }
    // When there is more than one page, create the dotpager jsshowlist.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual dotpager jsshowpiece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('active'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('next'),
        'data' => $li_next,
      );
    }

    if (!empty($items)) {
      return theme('item_list', array(
          'items' => $items,
          'attributes' => array('class' => array('pager')),
        ));
    }
  }
}

function designbase_pager_list($items = array(), $title = NULL, $type = "ul", $attributes = NULL) {
  $a = "";
  foreach ($items['items'] as $i) {
    if (isset($i['data'])) {
      $a.= $i['data'];
    }
  }
  return $a;
}


function designbase_pager_previous($variables) {

  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  global $pager_page_array;
  $output = '';

  if ($pager_page_array[$element] == 0) {
    $output = theme('pager_link', array('text' => $text, 'page_new' => array(0), 'element' => $element, 'parameters' => $parameters));
  }
  else {
    $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);

    // If the previous page is the first page, mark the link as such.
    if ($page_new[$element] == 0) {
      $output = theme('pager_first', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
    }
    // The previous page is not the first page.
    else {
      $output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
    }

  }
  return $output;

}



function designbase_pager_next($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
    }
    // The next page is not the last page.
    else {
      $output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
    }
  }
  else {
    $output = theme('pager_link', array('text' => $text, 'page_new' => array($pager_total[0]-1), 'element' => $element, 'parameters' => $parameters));
  }

  return $output;

}


/**
 * Implementation of theme_breadcrumb
 */
function designbase_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  $arg = arg(1);
  $type = arg(0);
  //If this page got argument
  if (isset($arg)) {
    //If its a node loaded, build the breadcrumb from that
    if ($type == "node") {
      $node = node_load($arg);
      if (isset($node->nid) && isset($node->field_category['und'])) {
        if ($node->field_category['und'][0]['tid'] > 0) {
          $term = taxonomy_term_load($node->field_category['und'][0]['tid']);
          $breadcrumb[] = l($term->name, "taxonomy/term/".$term->tid);
        }
        $breadcrumb[] = l($node->field_headline['und'][0]['value'], "node/".$node->nid);
      }
    }
    if ($type == "taxonomy") {
      $tid = arg(2);
      $term = taxonomy_term_load_multiple(array($tid));
      $term = $term[$tid];
      $breadcrumb[] = l($term->name, "taxonomy/term/".$term->tid);
    }
  }
  else if (isset($type)) {

  }

  $crumbs = "";
  if (!empty($breadcrumb)) {
    $crumbs = '<ul>';
    foreach($breadcrumb as $value) {
      $crumbs .= '<li>'.$value.'</li>';
    }
    $crumbs .= '</ul>';
  }
  return $crumbs;
}

/**
 * Get possible edit classes for frontend editing
 *
 * @return string with classes
 */
function get_edit_classes($node) {
  return " drupaledit edit-node-" . $node->nid;
}


/**
 * Implmementation of theme_menu_link().
 */
function designbase_menu_link($variables) {
  $arg = arg(1);
  $type = arg(0);
  $nodetype = "";
  //If this page got argument
  if (isset($arg)) {
    if ($type == "node") {
      $node = node_load($arg);
      if (isset($node->field_type))
        $nodetype = $node->field_type['und'][0]['value'];

      if (isset($node->nid) && isset($node->field_category['und'])) {
        if ($node->field_category['und'][0]['tid'] > 0) {
          $term = taxonomy_term_load($node->field_category['und'][0]['tid']);
          $tpath = "taxonomy/term/" . $term->tid;
        }
      }
    }
  }
  $class_add = $class = $addspan = NULL;
  $e = $variables["element"];
  if ($nodetype == 1) {
    if (!empty($tpath)) {
      if (!empty($e['#below'])) {
        foreach ($e['#below'] as $q) {
          if (isset($q['#href'])) {
            if ($q['#href'] == $tpath) {
              $e['#attributes']['class'][] = "active-trail";
              $e['#attributes']['class'][] = "open";
            }
          }
        }
      }
      if ($e['#href'] == $tpath) {
        $e['#attributes']['class'][] = "active";
        $e['#attributes']['class'][] = "open";
      }
    }
  }
  else if ($nodetype == 3) {
    if ($e['#href'] == "blog") {
      $e['#attributes']['class'][] = "active";
    }
  }

  // add a span for main menu
  if ($e["#original_link"]["menu_name"] == "menu-menu") {
    $addspan = true;
  }

  // add standard drupal classes
  if (isset($e["#localized_options"]["attributes"]["title"])) {
    $class = $e["#localized_options"]["attributes"]["title"];
  }

  // Add icons - description is used for that
  if (isset($e["description"])) {
    $class .= $e["description"];
  }

  if ($class) {
    $class_add = " class=\"". trim($class) ."\"";
  }

  // Render submenu
  $sub = "";
  if ($e["#below"]) {
    $sub = drupal_render($e["#below"]);

  }

  $out  = '<li'. drupal_attributes($e["#attributes"]) .'>';

  // Render name
  $name = "";
  if ($addspan) {
    $name = "<span". $class_add .">";
  }
  $name .= $e["#title"];

  if ($addspan) {
    $name .= "</span>";
  }

  // Finalize link
  $out .= l($name, $e["#href"], array("html" => true));
  $out .= $sub;
  $out .= '</li>';

  return $out;
}

/**
 * Implements theme_menu_tree__MENU().
 * Wraps the menu in the "Bliv Business Partner" sections into a div with class navigation.
 *
 * @param type $variables
 * @return string
 */
function designbase_menu_tree__menu_bliv_business_partner($variables) {
  $out = '<div class="navigation">';
  $out .= '<ul>';
  $out .= $variables['tree'];
  $out .= '</ul>';
  $out .= '</div>';

  return $out;
}


/**
 * Implementation of hook_css_alter().
 *
 * Remove unneeded css files from frontend to speed up load
 */
//function designbase_css_alter(&$css) {
//
//  // Imagecrop uses frontend - bail out on impact
//  if (arg(0) == "imagecrop") {
//    return;
//  }
//
//  $paths = _designbase_get_allowed_modules();
//
//  // Loop through all css'es
//  foreach ($css as $key => $value) {
//    $unset = TRUE;
//    foreach ($paths as $path) {
//      if (strpos($key, $path) !== FALSE) {
//        $unset = FALSE;
//      }
//    }
//
//    if ($unset) {
//      unset($css[$key]);
//    }
//  }
//}


/**
 * Get the url of a styled image
 *
 * @param $field the field to get the file from
 * @param $style name of the style to use
 *
 * @return url of the styled field
 */
function image_cache_for_apsis($style, $field) {
  $add = "";

  if (is_object($field)) {
    $uri = $field->uri;
  }
  else {
    $uri = $field["uri"];
    $sql = "SELECT entity_id as nid FROM {field_data_field_image} WHERE field_image_fid = :fid";

    $result = db_query($sql, array(":fid" => $field["fid"]));
    foreach ($result as $r) {
      if (is_object($r) > 0) {
        $node = node_load($r->nid);
        if (isset($node->nid)) {
          $add = "?". $node->changed;
        }
      }
      else {
        $add = "";
      }
    }
  }

  $out = image_style_url_for_apsis($style, $uri);
  $out .= $add;

  return $out;
}


function image_style_url_for_apsis($style_name, $path) {
  $uri = image_style_path($style_name, $path);

  // If not using clean URLs, the image derivative callback is only available
  // with the query string. If the file does not exist, use url() to ensure
  // that it is included. Once the file exists it's fine to fall back to the
  // actual file path, this avoids bootstrapping PHP once the files are built.
  if (!variable_get('clean_url') && file_uri_scheme($uri) == 'public' && !file_exists($uri)) {
    $directory_path = file_stream_wrapper_get_instance_by_uri($uri)->getDirectoryPath();
    //  return url($directory_path . '/' . file_uri_target($uri), array('absolute' => TRUE));
  }

  return file_create_url_apsis($uri);
}

function file_create_url_apsis($uri) {
  // Allow the URI to be altered, e.g. to serve a file from a CDN or static
  // file server.
  drupal_alter('file_url', $uri);

  $scheme = file_uri_scheme($uri);

  if (!$scheme) {

    // Allow for:
    // - root-relative URIs (e.g. /foo.jpg in http://example.com/foo.jpg)
    // - protocol-relative URIs (e.g. //bar.jpg, which is expanded to
    //   http://example.com/bar.jpg by the browser when viewing a page over
    //   HTTP and to https://example.com/bar.jpg when viewing a HTTPS page)
    // Both types of relative URIs are characterized by a leading slash, hence
    // we can use a single check.
    if (drupal_substr($uri, 0, 1) == '/') {
      return $uri;
    }
    else {
      // If this is not a properly formatted stream, then it is a shipped file.
      // Therefore, return the urlencoded URI with the base URL prepended.
      return $GLOBALS['base_url'] . '/' . drupal_encode_path($uri);
    }
  }
  elseif ($scheme == 'http' || $scheme == 'https') {
    // Check for http so that we don't have to implement getExternalUrl() for
    // the http wrapper.
    return $uri;
  }
  else {
    // Attempt to return an external URL using the appropriate wrapper.
    if ($wrapper = file_stream_wrapper_get_instance_by_uri($uri)) {
      list($scheme, $target) = explode('://', $uri, 2);
      $target = trim($target, '\/');
      $path = str_replace('\\', '/', $target);

      $host = $_SERVER['HTTP_HOST'];
      if (strpos($host, 'www.') !== 0) {
        $host = 'www.' . $host;
      }
      return "http://" . $host . "/" . variable_get('file_public_path', conf_path() . '/files') . '/' . drupal_encode_path($path);
    }
    else {
      return FALSE;
    }
  }
}


/**
 * Get the url of a styled image
 *
 * @param $field the field to get the file from
 * @param $style name of the style to use
 *
 * @return url of the styled field
 */
function image_cache($style, $field) {
  $add = "";

  if (is_object($field)) {
    $uri = $field->uri;
  }
  else {
    $uri = $field["uri"];
    $sql = "SELECT entity_id as nid FROM {field_data_field_image} WHERE field_image_fid = :fid";

    $result = db_query($sql, array(":fid" => $field["fid"]));
    foreach ($result as $r) {
      if (is_object($r) > 0) {
        $node = node_load($r->nid);
        if (isset($node->nid)) {
          $add = "?". $node->changed;
        }
      }
      else {
        $add = "";
      }
    }
  }

  $out = image_style_url($style, $uri);
  $out .= $add;

  return $out;
}

/**
 * Get a list of all modules that can enable css and js files
 *
 * @return array containing paths to modules
 */
function _designbase_get_allowed_modules() {

  $a[] = "admin_menu";
  $a[] = "toolbar";

  // Find paths for modules
  foreach ($a as $module) {
    $paths[] = drupal_get_path("module", $module);
  }

  return $paths;
}

/**
 * Dateformat
 *
 * @param $timestamp
 *   Unix timestamp
 * @return
 *   Formatted date
 */
function get_date($timestamp) {

  // Check for MySQL timestamp
  if (substr($timestamp, 4, 1) == "-") {
    return get_date_mysql($timestamp);
  }

  $day = date("d", $timestamp);
  $month = date("m", $timestamp);
  $year = date("y", $timestamp);

  $out = $day .".". $month .".". $year;

  return $out;
}
/**
 * Dateformat
 *
 * @param $timestamp
 *   Mysql timestamp
 * @return
 *   Formatted date
 */
function get_date_mysql($timestamp) {
  $year = substr($timestamp, 2, 2);
  $month = substr($timestamp, 5, 2);
  $day = substr($timestamp, 8, 2);

  $out = $day .".". $month .".". $year;

  return $out;
}



function designbase_theme() {
  return array(
    "comment_form" => array(
      'template' => "comment-form",
      "render element" => "",
    ),
    "comment_wrapper" => array(
      "arguments" => array('form' => NULL),
      'template' => "comment-wrapper",
    ),
    "pager_list" => array(
      "arguments" => array('form' => NULL),
    ),
  );
}

function get_day($day) {
  $ret = "";
  switch($day) {
    case "Monday":
      $ret = "Mandag";
      break;
    case "Tuesday":
      $ret = "Tirsdag";
      break;
    case "Wednesday":
      $ret = "Onsdag";
      break;
    case "Thursday":
      $ret = "Torsdag";
      break;
    case "Friday":
      $ret = "Fredag";
      break;
    case "Saturday":
      $ret = "L&oslash;rdag";
      break;
    case "Sunday":
      $ret = "S&oslash;ndag";
      break;
  }
  return $ret;
}


function designbase_user_register_form(){

}

function designbase_user_register() {

}
function designbase_select_as_radios($vars) {
  $element = &$vars['element'];

  if (!empty($element['#bef_nested'])) {
    return theme('select_as_tree', $vars);
  }
  $types = array(
    'edit-field-job-type-tid', 
    'edit-field-event-type-tid'
  );
  $output = (in_array($element['#id'], $types)) ? '<div class="date">' : '';
  $i = 1;
  foreach (element_children($element) as $key) {
    $element[$key]['#default_value'] = NULL;
    $element[$key]['#children'] = theme('radio', array('element' => $element[$key]));
    $output .= theme('form_element', array('element' => $element[$key]));
    if ($i == 1 && in_array($element['#id'], $types)) {
      $output .= '</div><div class="type-filter"><div>Type</div><div class="options hidden">';
    }
    $i++;
  }
  if (in_array($element['#id'], $types)) {
    $output .= '</div>';
  }

  return $output;
}

/**
 * Implements theme_views_pre_render().
 */
function designbase_views_pre_render(&$view) {
  if (!empty($view->result)) {
//    $view->style_options['row_class'] .= $view->style_plugin->options['row_class'] .= 'page-' . $view->query->pager->current_page;
  }
}

/**
 * Implements theme_menu_tree().
 */
function designbase_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

//function designbase_views_load_more_pager($vars) {
//  global $pager_page_array, $pager_total;
////  dsm($vars);
//  $tags = $vars['tags'];
//  $element = $vars['element'];
//  $parameters = $vars['parameters'];
//
//  $pager_classes = array('pager', 'pager-load-more');
//
//  $li_next = theme('pager_next',
//    array(
//      'text' => (isset($tags[3]) ? $tags[3] : t($vars['more_button_text'])),
//      'element' => $element,
//      'interval' => 1,
//      'parameters' => $parameters,
//    )
//  );
//  if (empty($li_next)) {
//    $li_next = empty($vars['more_button_empty_text']) ? '&nbsp;' : t($vars['more_button_empty_text']);
//    $pager_classes[] = 'pager-load-more-empty';
//  }
//  // Compatibility with tao theme's pager
//  elseif (is_array($li_next) && isset($li_next['title'], $li_next['href'], $li_next['attributes'], $li_next['query'])) {
//    $li_next = l($li_next['title'], $li_next['href'], array('attributes' => $li_next['attributes'], 'query' => $li_next['query']));
//  }
//  
//  $parameters['type_pager'] = 'prev';
////  $page1 = ($pager_page_array[0] - 1 > 0) ? $pager_page_array[0] - 1 : 0;
//  $page1 = $pager_page_array[0];
//  $li_prev = l('less', '<front>', array('query' => array('pager_prev' => 'Y', 'page' => $page1)));
//
//  if ($pager_total[$element] > 1) {
//    $items[] = array(
//      'class' => array('pager-next'),
//      'data' => $li_next,
//    );
//    $items[] = array(
//      'class' => array('pager-prev'),
//      'data' => $li_prev,
//    );
//    return theme('item_list',
//      array(
//        'items' => $items,
//        'title' => NULL,
//        'type' => 'ul',
//        'attributes' => array('class' => $pager_classes),
//      )
//    );
//  }
//}
