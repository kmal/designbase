<?php

// Plugin definition
$plugin = array(
  'title' => t('Supplier specific layout'),
  'category' => t('Installator.dk'),
  'icon' => 'installator_onecol.png',
  'theme' => 'panels_installator_supplier',
  'css' => 'installator_twocol.css',
  'regions' => array(
    'left' => t('left'),
    'left_bottom_left' => t('left_bottom_left'),
    'left_bottom_right' => t('left_bottom_right'),
    'right' => t('right'),
    ),
  );
