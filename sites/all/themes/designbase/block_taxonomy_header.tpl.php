<div class="expert-blog-content">
  <div class="inner">
    <?php if (user_is_logged_in()) : ?>
    <div class="article-action">
      <?php print l('Opret nyhed', "opretnyhed"); ?>
    </div>
    <?php else :  ?>
    <div class="article-action">
      <?php print l('Opret nyhed', "bliv-business-partner"); ?>
    </div>
    <?php endif; ?>
    <!-- SOCIAL MENU -->
    <div class="social-menu">
    	<ul>
		<?php print theme("share_block"); ?>
    	</ul>
    </div>
    <!-- END SOCIAL MENU -->     
    <?php if (isset($headline)): ?>
      <h1><?php print $headline; ?></h1>
    <?php endif; ?>
    <?php if (empty($content)) : ?>

    <?php else: ?>
    <?php print $content; ?>
    <?php endif; ?>
  </div>
  
</div>

