<?php  global $pager_total_items; ?>
<div class="c100 content subpage fullpage">
  <div class="fakerWrapper" style="width:621px;">
    <div class="searchResults">
      <h1><?php print variable_get("admin_search_results_headline"); ?></h1>
      <?php if (count($results) > 0) : ?>
	<h3>Søgeresultater for: <strong><?php print $s; ?></strong></h3>
	<h3><?php if ($num_all_results > $pager_total_items[0]) : ?>
          <?php $txt = str_replace("!x", "<strong>" . $pager_total_items[0] . "</strong>", variable_get("admin_number_and_showed_search_results_text")); ?>
          <?php print str_replace("!y", "<strong>" . $num_all_results . "</strong>", $txt); ?>
	<?php else: ?>
          <?php print str_replace("!x", "<strong>" . $pager_total_items[0] . "</strong>", variable_get("admin_number_search_results_text")); ?>
	<?php endif; ?></h3>
        
	<span class="shows">Viser: <?php print $start; ?>-<?php print $end; ?> af <?php print $pager_total_items[0]; ?></span>
	<div class="pagination">  
          <div class="inner">
            <?php print $pager; ?>
          </div>
	</div>

	<ul class="aList">
	  <?php foreach ($results as $node) : ?>
            <li>
              <a href="<?php print url("node/" . $node->nid); ?>">
		<?php if (in_array($node->type, array('article','event'))) : ?>
		<h3><?php print $node->field_headline['und'][0]['value']; ?></h3>
		<?php if (isset($node->field_category['und'][0]['tid'])): ?>
		  <span class="tag"><?php $term = taxonomy_term_load($node->field_category['und'][0]['tid']); print $term->name; ?></span>
		<?php endif; ?>
		<span class="date">| <?php print date('d.m.y', $node->created); ?></span>
<?php if (isset($node->field_teaser['und'][0]['safe_value'])): ?>
		<p><?php print $node->field_teaser['und'][0]['safe_value']; ?></p>
<?php endif; ?>
      <?php endif; ?>
      <?php if ($node->type == 'supplier') : ?>
        <h3><?php print $node->field_supplier_company['und'][0]['value']; ?></h3>
        <span class="date"><?php print date('d.m.y', $node->created); ?></span>
        <p><?php print truncate_utf8($node->field_summary['und'][0]['value'], 300, TRUE, TRUE); ?></p>
      <?php endif; ?>
              </a>
              <a class="nextLink" href="<?php print url("node/" . $node->nid); ?>"><?php print variable_get("admin_search_results_read_more"); ?></a>
            </li>
	  <?php endforeach; ?>
	</ul>
	<div class="pagination">  
          <div class="inner">
            <?php print $pager; ?>
          </div>
	</div>
	<?php else : ?>
	<p><?php print $no_results; ?></p>
	<?php endif; ?>
    </div>
  </div>
</div>
