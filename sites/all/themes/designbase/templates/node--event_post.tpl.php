<?php 
hide($content['field_link']); 
hide($content['field_date']);
hide($content['field_location']);
?>  
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h1 <?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h1>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <div class="article-info">
    <?php if (isset($node->supplier_company['und'][0]['value'])): ?>
      <span class="seperator">|</span>
      <span class="author">Arrang&oslash;r: <?php print $node->supplier_company['und'][0]['value']; ?></span>
    <?php endif; ?>
    <?php if (isset($node->field_company['und'][0]['tid'])): ?>
          <span class="seperator">|</span>
          <span class="link">
          <?php $supplier = _get_supplier_by_company_tid($node->field_company['und'][0]['tid']); ?>
              <?php if (isset($supplier->nid)): ?>
                <?php print l('Se virksomhedsprofil', 'node/' . $supplier->nid); ?>
        <?php endif; ?>
          </span>
    <?php endif; ?>
  </div>
  <div class="eventWhenWhere">
    <ul>
      <li>
      <?php
      $time =  strtotime($node->field_date['und'][0]['value']);
      $date = '<span class="date">' . date('d.m.Y', $time) . '</span>' . ' <span class="time">' . date('H.i', $time) . '</span>';
      print $date;
      ?> 
      </li>
      <?php if (isset($node->field_location['und'])) : ?>
      <li><?php print $node->field_location['und'][0]['value']; ?> </li>
      <?php endif; ?>
      <li>
      <?php if ($node->field_link && !empty($node->field_link['und'][0]['value'])): ?>
        <?php if (strpos($node->field_link['und'][0]['value'], 'mailto:') > -1): ?>
          <a class="event-mailto" data-mail="<?php print $node->field_link['und'][0]['value']; ?>" data-eid="<?php print $node->nid; ?>"><span>Tilmeld</span></a>
        <?php else: ?>
          <a href="/event/tilmeld?eid=<?php echo $node->nid; ?>&&href=<?php echo $node->field_link['und'][0]['value']; ?>" target="_blank"><span>Tilmeld</span></a>
        <?php endif; ?>
      <?php endif; ?>
      </li>    
    </ul>
  </div>  

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
      
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>
  
  <?php if (isset($share_block) && $share_block) :?>
    <div class="left-share-block">
      <?php 
      if (isset($flag_my_designbase)) {
        print $flag_my_designbase;
      }
      else {
        print render($content['flag_my_designbase']);
      }
      ?>
      <div class="comment-count"><?php print $node->comment_count;?></div>
    </div>
  <?php endif; ?>  

</div>
