<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<?php $node_wrapper = entity_metadata_wrapper('node', $node);?>    
  <?php hide($content['field_image']);
  hide($content['field_headline']);
  hide($content['field_category']);
  hide($content['flag_my_designbase']);
  ?>
    

    
    <div class="field-name-field-top-image">
      <?php 
      $image = $node_wrapper->field_image[0]->value();
      $params = array(
        'style_name' => '720px',
        'path' => $image['uri'],
        'alt' => $image['alt'],
        'title' => $image['title'],
        'width' => $image['width'],
        'height' => $image['height'],
        'attributes' => array('class' => array('image')),
        'getsize' => FALSE,
      );
      print theme('image_style', $params); ?>
    </div>  
  <div class="title-prefix"><?php print $node_wrapper->field_category->name->value();?></div>  
  <h1 <?php print $title_attributes; ?>>
    <?php print $node_wrapper->field_headline->value(); ?>
  </h1>
  <div class="filed-teaser"><?php print $node_wrapper->field_teaser->value();?></div>

  <div class="title-suffix">
  <!-- BYLINE -->	  
  <?php if (!isset($node->field_show_byline['und'][0]) || $node->field_show_byline['und'][0]['value'] == 1) : ?>
    <span class="author">Af 
      <?php if (isset($node->field_byline['und'])) :?>
        <?php if ($node->field_byline['und'][0]['value'] != '') : ?>
          <?php print $node->field_byline['und'][0]['value']; ?>
        <?php else : ?>
          <?php if ($node->field_type['und'][0]['value'] != 2) : ?>
            <?php print $node->author->field_name['und'][0]['value']; ?>
          <?php endif; ?>
        <?php endif; ?>
      <?php else : ?>
        <?php if ($node->field_type['und'][0]['value'] != 2 && isset($node->author->field_name['und'][0]['value'])) : ?>
          <?php print $node->author->field_name['und'][0]['value']; ?>
        <?php endif; ?>
      <?php endif; ?>
      <?php if ($node->field_type['und'][0]['value'] != 1) : ?>
        <?php if (isset($node->company) && isset($node->company->name) && ($node->company->name != '')) : ?> 
          <?php print $node->company->name;?>
        <?php endif; ?>
      <?php endif; ?>
    </span>
    <?php if ($node->field_type['und'][0]['value'] != 2) : ?>
      <span class="email"><a href="mailto: <?php print $node->author->mail; ?>" title=""><?php print $node->author->mail; ?></a></span>
    <?php endif; ?>
  <?php endif; ?>
  <!-- END BYLINE -->	 
  </div>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']); 
      
      print render($content);
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
//    if ($teaser || !empty($content['comments']['comment_form'])) {
//      unset($content['links']['comment']['#links']['comment-add']);
//    }
    // Only display the wrapper div if there are links. ?>
  <?php  
  $links = render($content['links']);
  if ($links): ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
  <div class="left-share-block">
    <?php print render($content['flag_my_designbase']); ?>
    <div class="comment-count"><?php print $node->comment_count;?></div>
  </div>
</div>
