<div class="quote mod <?php print $node->field_addon_position['und'][0]['value']; ?> <?php //print get_edit_classes($node); ?>">
<div class="inner">
  <div class="bd"><p><?php print $node->field_content['und'][0]['value']; ?></p></div>
  <?php if (isset($node->field_quote_source['und'][0])) : ?>
  <div class="source"><?php print $node->field_quote_source['und'][0]['value']; ?></div>
  <?php endif; ?>
</div>
</div>
