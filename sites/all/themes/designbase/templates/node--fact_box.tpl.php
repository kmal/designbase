<div class="mod box facts <?php if (isset($node->field_addon_position['und'][0]['value'])): print $node->field_addon_position['und'][0]['value']; endif; ?> <?php //print get_edit_classes($node); ?>">
<div class="inner">
     <?php if (isset($node->field_headline['und'][0]['value'])): ?>
  <div class="hd dark">
    <h4><?php print $node->field_headline['und'][0]['value']; ?></h4>
  </div>
<?php endif; ?>
  <div class="bd">
    <?php print $node->field_content['und'][0]['value']; ?>
  </div>
</div>
</div>
