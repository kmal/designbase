<?php if (!empty($content)) : ?>
  <div class="block-company-fact-box">
    <div class="pane-title">
    <h3><?php print $content['title'] ?></h3>
    <?php if (isset($content['description'])) : ?>
      <?php print $content['link']?>
    <?php endif; ?>
    </div>  
    <div class="content clearfix">
      <?php if (isset($content['logo'])) : ?>  
        <div class="company-logo"><?php print $content['logo']?></div>
      <?php endif; ?>
      <?php if (isset($content['description'])) : ?>  
        <div class="company-description"><?php print $content['description']?></div>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
