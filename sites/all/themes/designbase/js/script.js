/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
  Drupal.behaviors.themeDesignbase = {
    attach: function () {
      
//      $(".scrollable").scrollable({
//        circular: true
//      }).autoscroll({
//            autoplay: false,
//            autopause: true
//        });  
//        $(".scrollable").scrollable({
//        circular: true
//      }); 
    
//      gridLatestNews();
      gridGallery();       
      
      /* more content from comapny */
      $('.more-content-from-company .views-row').hover(
        function(){
          $(this).find('.views-field-field-headline').stop().animate({
            opacity: 1
          });
          $(this).find('.views-field-field-image').stop().animate({
            opacity: 0.5
          });
        }, function(){
          $('.more-content-from-company .views-field-field-headline').stop().animate({
            opacity: 0
          });
          $('.more-content-from-company .views-field-field-image').stop().animate({
            opacity: 1
          });
        }
      );
      /* end more content from comapny */
      
      $('.event-page-view .type-filter').hover(
        function(){
          $(this).find('.options').slideDown();
        },
        function(){
          $(this).find('.options').slideUp();
        }
      );
     
      /* slider for articles */
      
      if ($('#page').find('.rs-carousel').length > 0) {
        var $slider = $('.block-node-images-slider1 .slide').packery({
          itemSelector: ".image-cont1"
        });
        $slider.packery();
        $('.rs-carousel').carousel({
          pagination: false
        });
      }
      
      /* end slider */
      
      $('.image-description').hover(
        function(){
          $(this).find('.image-title').fadeIn();
        },
        function(){
          $(this).find('.image-title').fadeOut();
        }
        );
      
      customizeWebFormModal();
        $('#top_nav_bar #block-db-common-login-button').hover(
        function(){
          var height = $('#login-form-button-content .content').outerHeight();
          height = 288;
          $('#top_nav_bar #login-form-button-content').stop().animate({
            height: height
          });
          $(this).addClass('hover');
        },
        function(){
          $(this).removeClass('hover');
          $('#top_nav_bar #login-form-button-content').stop().animate({
            height: 0
          });
        }
        );
      
      $('#top_nav_bar #block-db-common-custom-search-form').hover(
        function(){
          $(this).addClass('hover');
          $(this).find('.search-item').slideDown();
        }, function(){
          $(this).removeClass('hover');
          $('#top_nav_bar #block-db-common-custom-search-form .search-item').slideUp();
        }
      );
      $('.field_indicate_sponsor').hover(
        function(){
          $(this).find('.field_indicate_sponsor_content').fadeIn();
        }, function(){
          $('.field_indicate_sponsor_content').fadeOut();
        }
      );
      
      $('.view-showroom #edit-field-products-tid-wrapper').hover(
        function(){
          $('.view-showroom #edit-field-products-tid-wrapper .views-widget').slideDown();
        }, function(){
          $('.view-showroom #edit-field-products-tid-wrapper .views-widget').slideUp();
        }
      );
    
      $('.scroll_to_top').click(function () {
        $('body,html').animate({
          scrollTop: 0
        }, 800);
        return false;
      });
      
      $('#left-menu-link').click(function(){
        $('#wrapper-white-opacity').fadeIn('fast');
        $("#main-navigation").animate({width:'350'},350);
      });
      $('#wrapper-white-opacity, #main-navigation .close').click(function(){
        $('#wrapper-white-opacity').fadeOut('fast');
        $("#main-navigation").animate({width:'0'},350);
      });
      
      function customizeWebFormModal() {
        var modalContent = $('#modalContent');
//        var top = $('#top_nav_bar').height();
        var top = 86;
        $('#top_nav_bar').css({
          'z-index' : '100005'
        });
        modalContent.css({
          'top': top + 'px'
        });
      }
      
      function gridLatestNews(){
        var $container = $('.view-latest-news .view-content').packery({
          itemSelector: ".views-row"
        });
        $container.packery('destroy');
        $container.packery();
      }
      
      function gridGallery(){
        if ($('#page').find('.view-gallery').length > 0) {
          $('.view-gallery .view-content a').hover(
            function(){
              $(this).addClass('opacity');
              $(this).find('.title').fadeIn();
            },
            function(){
              $(this).removeClass('opacity');
              $(this).find('.title').fadeOut();
            }
          );
          var $container = $('.view-gallery .view-content').packery({
            itemSelector: ".views-row"
          });
          $container.packery('destroy');
          $container.packery({
            gutter: 23
          });
        }
      }
      
      $('.menu-block-3 li.expanded').hover(
        function(){
//          var height = $(this).find('.menu').attr('rel');
//          $(this).find('.menu').stop().animate({
//            height: height
//          });
          $(this).find('.menu').slideDown();
          $(this).addClass('hover');
        },
        function(){
//          $(this).find('.menu').stop().animate({
//            height: 0
//          });
          $(this).find('.menu').slideUp();
          $(this).removeClass('hover');
        }
      );
    
    /******************************************************
   * DATEPICKER
  ******************************************************/

    $( "#edit-date" ).datepicker({ 
      dateFormat: 'yy-mm-dd',
      minDate: 0
    });

    $( "#publish-date" ).datepicker({
      dateFormat: 'yy-mm-dd',
      minDate: 0
    });


    $("#job_applicationdeadline_fake").datepicker({
      dateFormat: 'yy-mm-dd',
      minDate: 0
    });

    $("#job_deadline_wish_fake").datepicker({
      dateFormat: 'yy-mm-dd',
      minDate: 0
    });
    
    $( "#job_startdate_fake" ).datepicker({
      dateFormat: 'yy-mm-dd',
      minDate: 0
    });
    
    $( "#unpublish-date" ).datepicker({ 
      dateFormat: 'yy-mm-dd',
      minDate: 0
    });

    $("#-jobs-create-nobusiness-job-form").bind("submit", function() {
	var startdate = $("input[name=job_startdate_fake]").val();
	if (startdate.length > 0) {
	    $("input[name=job_startdate]").val(startdate);
	}	

	var appdate = $("input[name=job_applicationdeadline_fake]").val();
	if (appdate.length > 0) {
	    $("input[name=job_applicationdeadline]").val(appdate);
	}	
	
	var wishdate = $("input[name=job_deadline_wish_fake]").val();
	if (wishdate.length > 0) {
	    $("input[name=job_deadline_wish]").val(wishdate);
	}

    });
    $("#-jobs-create-job-form").bind("submit", function() {
	
	var startdate = $("input[name=job_startdate_fake]").val();
	if (startdate.length > 0) {
	    $("input[name=job_startdate]").val(startdate);
	}	

	var appdate = $("input[name=job_applicationdeadline_fake]").val();
	if (appdate.length > 0) {
	    $("input[name=job_applicationdeadline]").val(appdate);
	}	
	
	

    });

      
    
    /******************************************************
 * CAROUSEL
 ******************************************************/
		var  carousel = {
		
			init: function(sel, slideDis, noElem, styling) {
				$cs = this;
				$cs.selected = $(sel);
				$cs.numOfElem = $cs.selected.find('ul.ss>li').length; 
				$cs.slideDistance = slideDis;
				$cs.currentPage = 1;
				$cs.numOfPages = 1;
				$cs.elemPerPage = noElem;
				
				if ($cs.numOfElem > noElem) {
					$('.box-slideshow-nav-right').css('display', 'block');
					
					$cs.numOfPages = Math.ceil($cs.numOfElem/$cs.elemPerPage);
					
					//If any custom styling needed
					if(styling) {$cs.customStyling();}
					
					$cs.navigationVisibility();
					$cs.leftNavigation(); 
					$cs.rightNavigation();
				}			
			},
			
			//Custom styles/function
			customStyling: function() {
				
			},
			
			//Left navigation
			leftNavigation: function() {
				$('.box-slideshow-nav-left').click(function() {
					$cs.selected.find('ul.ss').animate({  marginLeft: -(($cs.currentPage-2)*$cs.slideDistance) }, 750);
					$cs.currentPage--;
					$cs.navigationVisibility();
				});
			},
			
			//Right navigation
			rightNavigation: function() {
				$('.box-slideshow-nav-right').click(function() {
					$cs.selected.find('ul.ss').animate({  marginLeft: -($cs.currentPage*$cs.slideDistance) }, 750);
					$cs.currentPage++;
					$cs.navigationVisibility();
				});			
			},
			
			// Show or hide navigation
			navigationVisibility: function() {

				if ($cs.currentPage == 1) {
					$('.box-slideshow-nav-left').css('display', 'none');
					$('.box-slideshow-nav-right').css('display', 'block');
				} else if ($cs.currentPage == $cs.numOfPages) {
					$('.box-slideshow-nav-right').css('display', 'none');
					$('.box-slideshow-nav-left').css('display', 'block');
				} else {
					$('.box-slideshow-nav-left').css('display', 'block');
					$('.box-slideshow-nav-right').css('display', 'block');
				}
			}
		};
		//carousel.init(parent element, slide distance, number of elements visible at a time, custom styling)
		if ($('.article-carousel').length) { carousel.init($('.article-carousel'), 1022, 4, true); }
	
    
    }
  };
  
  
  
  Drupal.ajax.prototype.commands.LoadLess = function(ajax, response, status) {

    var wrapper = response.selector ? $(response.selector) : $(ajax.wrapper);
    wrapper.find('.views-row.page-1').remove();
    wrapper.find('.view-content').children()
      .removeClass('views-row-first views-row-last views-row-odd views-row-even')
      .filter(':first')
        .addClass('views-row-first')
        .end()
      .filter(':last')
        .addClass('views-row-last')
        .end()
      .filter(':even')
        .addClass('views-row-odd')
        .end()
      .filter(':odd')
        .addClass('views-row-even')
        .end();
//    $('.view-latest-news .pager-next a').attr('href');
//    str = $('.view-latest-news .pager-next a').attr('href');
//    console.log(str);
//    str.replace('All', 'hhhhhhh');
//    console.log(str);
//    $('.view-latest-news .pager-next a').attr('href', str);
//    
//    console.log($('.view-latest-news .pager-next a'));
//    var of = $('.view-latest-news  .views-row-last').offset();
//    $('body,html').animate({
//        scrollTop: of.top
//      }, 800);
      
    $('.view-search-result').once().ajaxSuccess(function() {
       Drupal.attachBehaviors();
     });  
  };
  
})(jQuery);