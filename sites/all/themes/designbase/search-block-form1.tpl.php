<div class="searchWrap">
    <form action="/search_result" method="get">
      <input type="text" class="search-input form-item form-item-search-block-form clear-input" value="<?php if (isset($_GET['s'])) { print check_plain($_GET['s']); } else { print "Søg.."; } ?>" name="s" />
      <input type="hidden" name="" value="Søg.." />
      <input type="submit" class="search-submit" />
    </form>
  </div>