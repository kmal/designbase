<?php

// Load preprocess functions
require "preprocess/preprocess_node.php";

function at_designbase_preprocess_page(&$variables) {
//  dsm($variables);
  if (arg(0) == 'redigerjob' || arg(0) == 'joboversigt' || arg(0) == 'newsletter' || arg(0) == 'opretjob') {
    drupal_set_title('');
  }
}

/**
 * Override or insert variables into the node template.
 */
function at_designbase_preprocess_node(&$variables) {
  $node_wrapper = entity_metadata_wrapper('node', $variables['node']);
  if (preg_match('#pagepreview#', request_uri())) {
    $preview_id = at_designbase_get_preview_id();
    $node_preview = pagepreview_cache_get($preview_id); 
    if (isset($node_preview->nid)) {      
      $node_wrapper = entity_metadata_wrapper('node', $node_preview);
      $variables['node'] = $node_wrapper->value();
      if (isset($variables['content']['field_description'])) {
        $descrip = $node_wrapper->field_description->value();
        if (is_array($descrip)) {
          $variables['content']['field_description'][0]['#markup'] = $descrip['value'];
        }
        else {
          $variables['content']['field_description'][0]['#markup'] = $descrip;
        }
      }
      if (isset($variables['content']['field_content'])) {
        $content_value = $node_wrapper->field_content->value();
        $variables['content']['field_content'][0]['#markup'] = (isset($content_value['value'])) ? $content_value['value'] : $content_value;
      }
      if (isset($variables['content']['field_teaser'])) {
        if ($node_wrapper->__isset('field_teaser')) {
          $variables['content']['field_teaser'][0]['#markup'] = $node_wrapper->field_teaser->value();
        }
      }
      $variables['title'] = $node_wrapper->title->value();
    }
    if (isset($variables['content']['field_applicationdeadline'])) {
      if ($node_wrapper->__isset('field_applicationdeadline')) {
        $applicationdeadline = $node_wrapper->field_applicationdeadline->value();
      }
      if (isset($applicationdeadline)) {
        $variables['content']['field_applicationdeadline'][0]['#markup'] = '<span class="date-display-single">' . date('d.m.sY', $applicationdeadline) . '</span>';
      }
    }
  }
  $node = $variables['node'];
  //Preprocess function
  $preprocess_function = "_preprocess_" . $node->type;
  //If preprocess function exists, call it
  if (function_exists($preprocess_function)) {
    $preprocess_function($node);
  }

  
  if (isset($variables['view']) && $variables['view']->name == 'latest_news' && $variables['view']->current_display == 'block_1') {
    if (isset($variables['content']['field_image'])) {
      unset($variables['content']['field_teaser']);
    }
  }
  
  // Teasers
  if ($variables['view_mode'] == 'teaser') {    
    if (isset($variables['view']) && ($variables['view']->name == 'jobs' || $variables['view']->name == 'frontpage_events')) {
      if (isset($variables['content']['field_teaser'])) {
        $variables['content']['field_teaser'][0]['#markup'] = l($variables['content']['field_teaser'][0]['#markup'], 'node/' . $variables['nid']);
      }
      if (isset($variables['content']['field_headline'])) {
        $variables['content']['field_headline'][0]['#markup'] = l($variables['content']['field_headline'][0]['#markup'], 'node/' . $variables['nid']);
      }

      if (isset($variables['content']['field_jobteaser'])) {
        $variables['content']['field_jobteaser'][0]['#markup'] = l($variables['content']['field_jobteaser'][0]['#markup'], 'node/' . $variables['nid']);
      }
    }
    
    // Job
    if ($variables['type'] == 'job') {
      $company = $node_wrapper->field_company->value();
      $node = $variables['node'];
      if (!empty($company)) {
        $supplier_node = _get_supplier_by_company_tid($company[0]->tid);
        if (isset($supplier_node->field_logo['und'])) {
          $variables['content']['field_logo'][0]['#item'] = $supplier_node->field_logo['und'][0];
        }
        else if (!empty($node->field_logo['und'])) {
          $variables['content']['field_logo'][0]['#item'] = $node->field_logo['und'][0];
        }
        else {
          $variables['content']['field_logo'][0]['#item'] = array();
        }
      }
      
      if (isset($variables['content']['field_applicationdeadline'])) {
        $variables['content']['field_applicationdeadline'][0]['#markup'] = '<span class="label"><i class="fa fa-calendar"></i>Ansøgningsfrist:</span>' . $variables['content']['field_applicationdeadline'][0]['#markup'];
      }
      $variables['content']['field_job_geo'][0]['#markup'] = '<i class="fa fa-map-marker"></i>' . $variables['content']['field_job_geo'][0]['#markup'];
    }
    
    // Events
    if ($variables['type'] == 'event_post') {
      $variables['content']['field_date'][0]['#markup'] = '<i class="fa fa-calendar"></i>' . $variables['content']['field_date'][0]['#markup'];
      if (isset($variables['content']['field_location'][0]['#markup'])) {
        $variables['content']['field_location'][0]['#markup'] = '<i class="fa fa-map-marker"></i>' . $variables['content']['field_location'][0]['#markup'];
      }
    }
  }
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
  
  if ($variables['view_mode'] == 'full') {
    if (isset($variables['field_hide_teaser'])) {
      $field_hide_teaser = $node_wrapper->field_hide_teaser->value();
      if ($field_hide_teaser) {
        unset($variables['content']['field_teaser']);
      }
    }
    if ($variables['type'] == 'standard_page') {
      drupal_set_title('');
      $variables['content']['field_headline'][0]['#markup'] = '<h1>' . $variables['content']['field_headline'][0]['#markup'] . '</h1>';
      $images = $node_wrapper->field_image->value();
      if (!empty($images)) {
        $image = $images[0];
        $params = array(
          'style_name' => 'top_image',
          'path' => $image['uri'],
          'alt' => (isset($image['alt'])) ? $image['alt'] : NULL,
          'title' => $image['title'],
          'width' => (isset($image['width'])) ? $image['width'] : NULL,
          'height' => (isset($image['height'])) ? $image['height'] : NULL,
          'attributes' => array('class' => array('image')),
          'getsize' => FALSE,
        );
        $class = ($image['width'] < $image['height']) ? 'vertical' : 'horizont';
        $top_image = '<div class="top-image ' . $class . '"><div class="top-image-content">';
        $top_image .= theme('image_style', $params);
        if (strlen($image['title']) > 0) {
          $top_image .= '<div class="top-image-description">
              ' . $image['title'] . '
          </div>';
        }
        $top_image .= '</div></div>';
//          $variables['title_prefix'] = $top_image . $variables['title_prefix'];
          $variables['content']['field_headline']['#suffix'] = $top_image;

      }
    }
    
    // Event_post
    if ($variables['type'] == 'event_post') {
      $variables['page'] = FALSE;
      $variables['display_submitted'] = FALSE;
      $type_value = $node_wrapper->field_event_type->value();
      if ($type_value) {
        $variables['title_prefix'] = $node_wrapper->field_event_type->name->value();
      }
      $variables['title'] = $node_wrapper->field_headline->value();
      $variables['share_block'] = TRUE;
      if (!isset($variables['content']['flag_my_designbase'])) {
        $variables['flag_my_designbase'] = flag_create_link('my_designbase', $node_wrapper->nid->value());
      }
    }
    
    // Job
    if ($variables['type'] == 'job') {
      $variables['display_submitted'] = FALSE;
      $variables['title'] = $node_wrapper->field_headline->value();
      $variables['page'] = FALSE;
      $variables['content']['#suffix'] = '<div class="automatic_text">Skriv venligst at du har set stillingsopslaget på designbase.dk</div>';
      $protocol = (strpos('http', $node_wrapper->field_www->value()) === FALSE) ? 'http://' : '';
      $attributes = array(
        'attributes' => array(
          'class' => array('url', 'gray-plus'),
          'target' => '_blank'
        )
      );
      if ($node_wrapper->__isset('field_applicationdeadline_soon')) {
        $appl_soon = $node_wrapper->field_applicationdeadline_soon->value();
      }
      if ($node_wrapper->__isset('field_startdate_soon')) {
        $start_soon = $node_wrapper->field_startdate_soon->value();
      }
      if (isset($variables['content']['field_applicationdeadline']) && strlen($variables['content']['field_applicationdeadline'][0]['#markup']) > 0) {
        $variables['content']['field_applicationdeadline'][0]['#markup'] = '<i class="fa fa-calendar"></i><span class="label">Ansøgningsfrist:</span>' . $variables['content']['field_applicationdeadline'][0]['#markup'];
      }
      if (isset($appl_soon) && $appl_soon == 1) {
        if (isset($variables['content']['field_applicationdeadline'])) {
          $variables['content']['field_applicationdeadline'][0]['#markup'] = '<i class="fa fa-calendar"></i><span class="label">Ansøgningsfrist:</span>' . 'Hurtigst muligt';
        }
        else {
          $variables['content']['field_applicationdeadline'][0]['#markup'] = '<div class="field field-name-field-applicationdeadline"><i class="fa fa-calendar"></i><span class="label">Ansøgningsfrist:</span>' . 'Hurtigst muligt' . '</div>';
        }
        $variables['content']['field_applicationdeadline']['#weight'] = 2;
      }
      $variables['content']['field_jobtitle'][0]['#markup'] = '<i class="fa fa-users"></i><span class="label">Stilling:</span>' . $variables['content']['field_jobtitle'][0]['#markup'];
      if (isset($variables['content']['field_www'])) {
        $variables['content']['field_www'][0]['#markup'] = l(t('Search jobs'), $protocol . $variables['content']['field_www']['#items'][0]['value'], $attributes);
      }
      
      $company = $node_wrapper->field_company->value();
      if (!empty($company)) {
        $supplier_node = _get_supplier_by_company_tid($company[0]->tid);
        if (!empty($supplier_node->field_logo['und'][0])) {
          $variables['content']['field_logo'][0]['#item'] = $supplier_node->field_logo['und'][0];  
        }
      }
    }
    
    // Product_news or industry_news
    if ($variables['type'] == 'product_news' || $variables['type'] == 'industry_news') {
      $variables['page'] = FALSE;
      $variables['display_submitted'] = FALSE;
      $variables['title'] = $node_wrapper->field_headline->value();

      $variables['share_block'] = TRUE;

      if ($variables['type'] == 'product_news') {
        $category = $node_wrapper->field_category->value();
        if ($category) {
          $variables['title_prefix'] = $node_wrapper->field_category->name->value();
        }
        if (!isset($variables['content']['flag_my_designbase'])) {
          $variables['flag_my_designbase'] = flag_create_link('my_designbase', $node_wrapper->nid->value());
        }
      }
      if ($variables['type'] == 'industry_news') {
        $variables['title_prefix'] = node_type_get_name('industry_news');
        if (!isset($variables['content']['flag_my_designbase'])) {
          $variables['flag_my_designbase'] = flag_create_link('my_designbase', $node_wrapper->nid->value());
        }
      }
    }
    
    // Byline
    if (($variables['type'] == 'product_news' || $variables['type'] == 'name_article' || $variables['type'] == 'industry_news')) {
      $byline = $node_wrapper->field_byline->value();
      $field_byline = '';
      $date_create = date('d. ', $node_wrapper->created->value());
      $date_create .= t(date('M', $node_wrapper->created->value()));
      $date_create .= date(' Y', $node_wrapper->created->value());
      if (isset($byline)) {
        $field_byline .= 'AF ' . $byline;
      }
      else {
        $field_byline .= '<div class="field field-name-field-byline view-mode-full">';
      }
      $company_field = $node_wrapper->field_company->value();
      if (!empty($company_field)) {
        $supplier = _get_supplier_by_company_tid($node_wrapper->field_company[0]->tid->value()); 
        if ($supplier) {
          $field_byline .= (isset($byline)) ? ' | ' . $supplier->title : 'AF ' . $supplier->title;
        }
        $field_byline .= (strlen($field_byline) > 0) ? ' | ' . $date_create : $date_create;
        if ($supplier) {
          $field_byline .= ' | ' . l('Se Showroom', 'node/' . $supplier->nid);
        }
      }
      else {
        $name_author = $node_wrapper->author->field_name->value();
        $email_author = $node_wrapper->author->mail->value();
        $field_byline .= (!isset($byline) && isset($name_author)) ? 'AF ' . $name_author : '';
        $field_byline .= ((isset($byline) || isset($name_author)) && isset($email_author)) ? ' | ' : '';
        $field_byline .= (isset($email_author)) ? '<a href="mailto:' .$email_author . ' " title="">' . $email_author . '</a>' : '';
        $field_byline .= (strlen($field_byline) > 0) ? ' | ' . $date_create : $date_create;
      }
      if (!isset($byline)) {
        $field_byline .= '</div>';
      }
      $variables['content']['field_byline'][0]['#markup'] = $field_byline;
    }
    
    // Name_article
    if ($variables['type'] == 'name_article') {
      $variables['share_block'] = TRUE;
      $variables['page'] = FALSE;
      $variables['title_prefix'] = node_type_get_name('name_article');
      $variables['title'] = $node_wrapper->field_headline->value();
      if (!isset($variables['content']['flag_my_designbase'])) {
        $variables['flag_my_designbase'] = flag_create_link('my_designbase', $node_wrapper->nid->value());
      }
    }
    
    // Partner_profile
    if ($variables['type'] == 'partner_profile') {
      if (isset($variables['content']['field_phone']) && strlen($variables['content']['field_phone'][0]['#markup']) > 0) {
        $variables['content']['field_phone'][0]['#markup'] = 'Tel.: ' . $variables['content']['field_phone'][0]['#markup'];
      }
      if (isset($variables['content']['field_www']) && strlen($variables['content']['field_www'][0]['#markup']) > 0) {
        $protocol = (strpos('http', $node->field_www['und'][0]['value']) === FALSE) ? 'http://' : '';
        $attributes = array(
          'attributes' => array(
            'class' => array('url'),
            'target' => '_blank'
          )
        );
        $variables['content']['field_www'][0]['#markup'] = l($variables['content']['field_www']['#items'][0]['value'], $protocol . $variables['content']['field_www']['#items'][0]['value'], $attributes);
      }
    }
    
    // Brands and Tags  
    if (isset($variables['content']['field_brands']) || isset($variables['content']['field_tags'])) {
      if (isset($variables['content']['field_brands'])) {
        $field_start = 'field_brands';
        $field_end = 'field_brands';
      }
      if (isset($variables['content']['field_tags'])) {  
        if (!isset($variables['content']['field_brands'])) {
          $field_start = 'field_tags';
        }
        $field_end = 'field_tags';
      }
      $variables['content'][$field_start]['#prefix'] = '<div class="tags-brands-cont"><div class="icon"><i class="fa fa-tags"></i></div>';
      $variables['content'][$field_end]['#suffix'] = '</div>';
      if (isset($variables['content']['field_brands'])) {      
        $tags = element_children($variables['content']['field_brands']);
        $i = 1;
        foreach ($tags as $tag) {
          if ($i == 1) {
            $variables['content']['field_brands'][$tag]['#prefix'] = '<span class="label">Tags:</span> ';
          }
          if ($i < count($tags) || isset($variables['content']['field_tags'])) {
            $variables['content']['field_brands'][$tag]['#title'] .= ', ';
          }
          $i++;
        }
      }
      if (isset($variables['content']['field_tags'])) {      
        $tags = element_children($variables['content']['field_tags']);
        $i = 1;
        foreach ($tags as $tag) {
          if ($i == 1 && !isset($variables['content']['field_brands'])) {
            $variables['content']['field_tags'][$tag]['#prefix'] = '<span class="label">Tags:</span> ';
          }
          if ($i < count($tags)) {
            $variables['content']['field_tags'][$tag]['#title'] .= ', ';
          }
          $i++;
        }
      }
    }
    
  }
  


  $comments_content = array(
    'article',
    'name_article',
    'product_news',
    'industry_news',
    'event_post'
  );
  if (in_array($variables['type'], $comments_content) && $variables['view_mode'] == 'full') {
    if (user_access('view disqus comments') && isset($node->disqus)) {
      $disqus = $variables['disqus'];
      $comments = array(
        '#theme' => 'link',
        '#text' => t('0'),
        '#path' => $disqus['identifier'],
        '#options' => array(
          'fragment' => 'disqus_thread',
          'attributes' => array(
            'data-disqus-identifier' => $disqus['identifier'],
          ),
          'html' => FALSE,
        ),
      );
      $comments['#attached'] = array(
        'js' => array(
          array('data' => drupal_get_path('module', 'disqus') . '/disqus.js'),
          array(
            'data' => array('disqusComments' => $disqus['domain']),
            'type' => 'setting',
          ),
        ),
      );
      $variables['disqus'] = drupal_render($comments);
    }
  }
  
  if ($variables['view_mode'] == 'latest_news') {
    $detect = mobile_detect_get_object();
    $is_mobile = $detect->isMobile();
    $is_tablet = $detect->isTablet();
    if (!$is_mobile || $is_tablet) {
      if (isset($variables['content']['field_category'])) {
        $field = 'field_category';
        $variables['content'][$field]['#suffix'] = '';
        $variables['content'][$field]['#prefix'] = '<div class="counters-wrap">';
      }
      else {
        $field = 'field_teaser';
        $variables['content'][$field]['#suffix'] = '';
        $variables['content'][$field]['#suffix'] .= '<div class="counters-wrap">';
      }
      $variables['content'][$field]['#suffix'] .= '<div class="comments-count">' . db_common_disqus_comments_count($variables['node']) . '</div>';
      $variables['content'][$field]['#suffix'] .= '</div>';
    }
    
    
    if (isset($variables['content']['field_teaser'][0]['#markup'])) {
      $variables['content']['field_teaser'][0]['#markup'] = l($variables['content']['field_teaser'][0]['#markup'], 'node/' . $variables['nid']);      
    }
    global $user;
    if ($user->uid == 0) {
      $variables['content']['flag_my_designbase'][0]['#markup'] = '<div class="flag-outer flag-outer-my-designbase"><span class="flag-wrapper flag-my-designbase">';
      $variables['content']['flag_my_designbase'][0]['#markup'] .= l('<i class="fa fa-heart-o"></i>', 'modal/user_create', array('html' => TRUE));
      $variables['content']['flag_my_designbase'][0]['#markup'] .= '</span></div>';
    }
    
    if ($variables['type'] == 'article') {  
      $byline = $node_wrapper->field_byline->value();
      $name_author = $node_wrapper->author->field_name->value();
      $company = $node_wrapper->field_company->value();
      if (!empty($company)) {
        $company_tid = $company[0]->tid;
        $sup = _get_supplier_by_company_tid($company_tid);
      }
      $field_byline = '';
      if (!isset($byline)) {
        $name_author = 'AF ' . $name_author;
        if (isset($sup) && is_object($sup)) {
          $name_author = l($name_author, 'node/' . $sup->nid, array('html' => TRUE));
        }
        $field_byline = '';
        $field_byline .= '<div class="field field-name-field-byline view-mode-full">';
        $field_byline .= $name_author;
        $field_byline .= '</div>';
        $variables['content']['field_byline'][0]['#markup'] = $field_byline;
        $variables['content']['field_byline']['#weight'] = 2;
      }
      else {
        if (isset($sup) && is_object($sup)) {
          $variables['content']['field_byline'][0]['#markup'] = l($variables['content']['field_byline'][0]['#markup'], 'node/' . $sup->nid, array('html' => TRUE));
        }
      }
    }
    
    if (in_array($variables['type'], array('name_article', 'product_news', 'industry_news'))) { 
      $company = $node_wrapper->field_company->value();
      if (!empty($company) && isset($variables['content']['field_company'])) {
        $company_tid = $company[0]->tid;
        $sup = _get_supplier_by_company_tid($company_tid);
        if (is_object($sup)) {
          $variables['content']['field_company'][0]['#markup'] = l($variables['content']['field_company'][0]['#markup'], 'node/' . $sup->nid, array('html' => TRUE));
        }
      }
    } 
    if (in_array($variables['type'], array('name_article', 'product_news', 'industry_news', 'article'))) { 
      if (in_array('post_date', $variables['elements']['#groups']['group_byline']->children)) {
        if (isset($variables['content']['field_company'])) {
          $variables['content']['field_company'][0]['#markup'] = '<div class="border-right">' . $variables['content']['field_company'][0]['#markup'] . '</div>';
        }
      }
    } 
    
    
  }
  
  // For top image
    drupal_add_js(libraries_get_path('fancybox') . '/jquery.fancybox.js');
    drupal_add_css(libraries_get_path('fancybox') . '/jquery.fancybox.css');
    drupal_add_js(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-thumbs.js');
    drupal_add_css(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-thumbs.css');
    drupal_add_js(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-buttons.js');
    drupal_add_css(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-buttons.css');
    drupal_add_js(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-media.js');
  if ($node_wrapper->__isset('field_top_carousel_template') && $variables['view_mode'] == 'full' && $variables['type'] != 'article') {
    $type = $node_wrapper->field_top_carousel_template->value();
    $images = $node_wrapper->field_image->value();
    if (!empty($images) && ($type == 'top_image' || count($images) == 1)) {
      $image = $images[0];
      $params = array(
        'style_name' => 'top_image',
        'path' => $image['uri'],
        'alt' => (isset($image['alt'])) ? $image['alt'] : NULL,
        'title' => $image['title'],
        'width' => (isset($image['width'])) ? $image['width'] : NULL,
        'height' => (isset($image['height'])) ? $image['height'] : NULL,
        'attributes' => array('class' => array('image')),
        'getsize' => FALSE,
      );
      $class = ($image['width'] < $image['height']) ? 'vertical' : 'horizont';
      $top_image = '<div class="top-image ' . $class . ' "><div class="top-image-content"><a class="fancybox-gallery-group">';
      $top_image .= theme('image_style', $params);
      if (strlen($image['title']) > 0) {
        $top_image .= '<div class="top-image-description">
            ' . $image['title'] . '
        </div>';
      }
      $top_image .= '</a></div></div>';
      if ($variables['type'] == 'event_post') {
        $variables['title_prefix'] = $top_image . $variables['title_prefix'];
      }
      else {
        $variables['content']['field_byline']['#suffix'] = $top_image;
      }
      
    }
  }
  
  if ($variables['view_mode'] == 'quote') {
    $variables['content']['field_quote_text'][0]['#markup'] = l($variables['content']['field_quote_text'][0]['#markup'], 'node/' . $variables['nid']);
  }
}

/**
 * Theme callback for jQeury carousel field formatter.
 */
function at_designbase_jquery_carousel_field_formatter($vars) {

  _jquery_carousel_settings_format($vars['settings']);
  _jquery_carousel_include_css_js($vars['settings']);

  $output = '';

  if (is_array($vars['element']) && (count($vars['element']) > 1)) {
    $selector_class = drupal_attributes(array('class' => $vars['settings']['selector']));
    $output .= '<div ' . $selector_class . '">';
    $output .= '<ul>';
    foreach ($vars['element'] as $elem) {
      $output .= '<li>';
      $output .= theme('image_style', array('path' => $elem['uri'], 'style_name' => $vars['settings']['style_name']));
      if (strlen($elem['title']) > 0) {
        $output .= '<div class="image-description">
          <div class="show-title"><i class="fa fa-info"></i></div>
          <div class="image-title hidden">
            ' . $elem['title'] . '
          </div>
        </div>';
      }
      $output .= '</li>';
    }
    $output .= '</ul>';
    $output .= '</div>';
  }
  else {
    $output .= theme('image_style', array('path' => $vars['element'][0]['uri'], 'style_name' => $vars['settings']['style_name']));
  }

  return $output;
}

/**
 * Implements hook_form_alter().
 * Adds values to the company and email field of the registration form, if present in the querystring.
 *
 * @param type $form
 * @param type $form_state
 * @param type $form_id
 */
function at_designbase_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id === 'user_register_form') {
    $form['field_company']['und']['#default_value'] = isset($_GET['company']) ? ($_GET['company'] === 'FIRMA' ? '' : $_GET['company']) : '';
    $form['account']['mail']['#default_value'] = isset($_GET['email']) ? ($_GET['email'] === 'EMAIL' ? '' : $_GET['email']) : '';
  }
}

/**
 * A preprocess function for views.
 * This limits the number of supplier news set in the page settings.
 */
function at_designbase_preprocess_views_view(&$variables) {
  // Only do stuff if the view is the right one.
  if ($variables['view']->name === 'frontpage_primary_supplier_artic') {
    // Get the variable from the page settings.
    $limit = variable_get('admin_number_of_primary_supplier_news', '');

    // If the variables is set in the page settings, do stuff.
    // Otherwise, leave it to the view to decide how many news to display.
    if ($limit !== '') {
      $i = 0;
      // A temp array to hold each renderable node.
      $rows = array();

      while ($i < $limit) {
        $row = $variables['view']->result[$i];
        $node = node_load($row->nid);
        $rows[] = node_view($node);

        $i++;
      }

      // Render the temp array, and put it into the rows variable, used in the view template.
      $variables['rows'] = render($rows);
    }
  }
}

/**
 * at_designbase_html_head_alter
 * adds facebook open graph metatags for sharing articles on fb
 *
 * @param mixed $elements
 * @access public
 * @return void
 */
function at_designbase_html_head_alter(&$elements) {
  $elements = array();
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));
    if ($node->type == 'article') {
      $elements['og_meta_title'] = array(
        '#type' => 'html_tag',
        '#tag' => 'meta',
        '#attributes' => array(
          'property' => 'og:title',
          'content' => $node->field_headline['und'][0]['value'],
        ),
      );
      if (isset($node->field_image['und'][0])) {
        $elements['og_meta_image'] = array(
          '#type' => 'html_tag',
          '#tag' => 'meta',
          '#attributes' => array(
            'property' => 'og:image',
            'content' => file_create_url($node->field_image['und'][0]['uri']),
          ),
        );
      }
      if (isset($node->field_teaser['und'][0])) {
        $elements['og_meta_description'] = array(
          '#type' => 'html_tag',
          '#tag' => 'meta',
          '#attributes' => array(
            'property' => 'og:description',
            'content' => $node->field_teaser['und'][0]['value'],
          ),
        );
      }
    }
  }
}

function _get_top_messages() {
  $out = "";
  $msg = drupal_get_messages("fagportaler_top");
  if (count($msg) > 0) {
    $out.= "<ul>";
    foreach ($msg['fagportaler_top'] as $m) {
      $out.= "<li>" . $m . "</li>";
    }
    $out.= "</ul>";
  }
  return $out;
}

function _get_status_messages() {
  $out = "";
  $msg = drupal_get_messages("fagportaler_status");
  if (count($msg) > 0) {
    $out.= "<ul>";
    foreach ($msg['fagportaler_status'] as $m) {
      $out.= "<li>" . $m . "</li>";
    }
    $out.= "</ul>";
  }
  return $out;
}

function _get_error_messages() {
  $out = "";
  $msg = drupal_get_messages("fagportaler_error");
  if (count($msg) > 0) {
    $out.= "<ul>";
    foreach ($msg['fagportaler_error'] as $m) {
      foreach ($m as $error) {
        $out.= "<li>" . $error . "</li>";
      }
    }
    $out.= "</ul>";
  }
  return $out;
}

function _get_job_error_messages() {
  $out = "";
  $msg = drupal_get_messages("job_error");
  if (count($msg) > 0) {
    $out.= "<ul>";
    foreach ($msg['job_error'] as $m) {
      foreach ($m as $error) {
           $out.= "<li>" . $error . "</li>";
      }
    }
    $out.= "</ul>";
  }
  return $out;
}

//function at_designbase_status_messages($variables) {
//  $display = $variables['display'];
//  $output = '';
//
//  $status_heading = array(
//    'status' => t('Status message'),
//    'error' => t('Error message'),
//    'warning' => t('Warning message'),
//  );
//  $types = array("status", "error", "warning");
//  $elms = array();
//  foreach ($types as $type) {
//    $elms[$type] = drupal_get_messages($type);
//  }
//  foreach ($types as $type => $messages) {
//    $output .= "<div class=\"messages $type\">\n";
//    if (!empty($status_heading[$type])) {
//      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
//    }
//    if (count($messages) > 1) {
//      $output .= " <ul>\n";
//      foreach ($messages as $message) {
//        $output .= '  <li>' . $message . "</li>\n";
//      }
//      $output .= " </ul>\n";
//    }
//    else {
//      $output .= $messages[0];
//    }
//    $output .= "</div>\n";
//  }
//  return $output;
//}

function at_designbase_fieldset($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper'));

  $output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';

  if (isset($variables['element']['#id']) && $variables['element']['#id'] == "user_user_form_group_userinfo" && !empty($element['#title'])) {
    $element['#title'] = "<h1>" . $element['#title'] . "</h1>";
  }

  if (!empty($element['#title'])) {
    // Always wrap fieldset legends in a SPAN for CSS positioning.
    $output .= '<legend><span class="fieldset-legend">' . $element['#title'] . '</span></legend>';
  }

  if (isset($variables['element']['#id'])) {
    if ($variables['element']['#id'] == "user_user_form_group_userinfo") {
      $output .= "<p>" . variable_get("admin_register_description") . "</p>";
    }

    if ($variables['element']['#id'] == "edit-login-set") {
      $output .= "<p>" . variable_get("admin_login_description") . "</p>";
    }
  }
  $output .= '<div class="fieldset-wrapper">';
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
  }
  $output .= $element['#children'];
  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= '</div>';
  $output .= "</fieldset>\n";
  return $output;
}


/**
 * Implementation of theme_breadcrumb
 */
function at_designbase_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  $arg = arg(1);
  $type = arg(0);
  //If this page got argument
  if (isset($arg)) {
    //If its a node loaded, build the breadcrumb from that
    if ($type == "node") {
      $node = node_load($arg);
      if (isset($node->nid) && isset($node->field_category['und'])) {
        if ($node->field_category['und'][0]['tid'] > 0) {
          $term = taxonomy_term_load($node->field_category['und'][0]['tid']);
          $breadcrumb[] = l($term->name, "taxonomy/term/" . $term->tid);
        }
        $breadcrumb[] = l($node->field_headline['und'][0]['value'], "node/" . $node->nid);
      }
    }
    if ($type == "taxonomy") {
      $tid = arg(2);
      $term = taxonomy_term_load_multiple(array($tid));
      $term = $term[$tid];
      $breadcrumb[] = l($term->name, "taxonomy/term/" . $term->tid);
    }
  }
  else if (isset($type)) {
    
  }

  $crumbs = "";
  if (!empty($breadcrumb)) {
    $crumbs = '<ul>';
    foreach ($breadcrumb as $value) {
      $crumbs .= '<li>' . $value . '</li>';
    }
    $crumbs .= '</ul>';
  }
  return $crumbs;
}

/**
 * Get possible edit classes for frontend editing
 *
 * @return string with classes
 */
function get_edit_classes($node) {
  return " drupaledit edit-node-" . $node->nid;
}

/**
 * Implmementation of theme_menu_link().
 */
function at_designbase_menu_link($variables) {
  $arg = arg(1);
  $type = arg(0);
  $nodetype = "";
  //If this page got argument
  if (isset($arg)) {
    if ($type == "node") {
      $node = node_load($arg);
      if (isset($node->field_type))
        $nodetype = $node->field_type['und'][0]['value'];

      if (isset($node->nid) && isset($node->field_category['und'])) {
        if ($node->field_category['und'][0]['tid'] > 0) {
          $term = taxonomy_term_load($node->field_category['und'][0]['tid']);
          $tpath = "taxonomy/term/" . $term->tid;
        }
      }
    }
  }
  $class_add = $class = $addspan = NULL;
  $e = $variables["element"];
  if ($nodetype == 1) {
    if (!empty($tpath)) {
      if (!empty($e['#below'])) {
        foreach ($e['#below'] as $q) {
          if (isset($q['#href'])) {
            if ($q['#href'] == $tpath) {
              $e['#attributes']['class'][] = "active-trail";
              $e['#attributes']['class'][] = "open";
            }
          }
        }
      }
      if ($e['#href'] == $tpath) {
        $e['#attributes']['class'][] = "active";
        $e['#attributes']['class'][] = "open";
      }
    }
  }
  else if ($nodetype == 3) {
    if ($e['#href'] == "blog") {
      $e['#attributes']['class'][] = "active";
    }
  }

  // add a span for main menu
  if ($e["#original_link"]["menu_name"] == "menu-menu") {
    $addspan = true;
  }

  // add standard drupal classes
  if (isset($e["#localized_options"]["attributes"]["title"])) {
    $class = $e["#localized_options"]["attributes"]["title"];
  }

  // Add icons - description is used for that
  if (isset($e["description"])) {
    $class .= $e["description"];
  }

  if ($class) {
    $class_add = " class=\"" . trim($class) . "\"";
  }

  // Render submenu
  $sub = "";
  if ($e["#below"]) {
    $sub = drupal_render($e["#below"]);
  }

  $out = '<li' . drupal_attributes($e["#attributes"]) . '>';

  // Render name
  $name = "";
  if ($addspan) {
    $name = "<span" . $class_add . ">";
  }
  $name .= $e["#title"];

  if ($addspan) {
    $name .= "</span>";
  }

  // Finalize link
  $out .= l($name, $e["#href"], array("html" => true));
  $out .= $sub;
  $out .= '</li>';

  return $out;
}

/**
 * Implements theme_menu_tree__MENU().
 * Wraps the menu in the "Bliv Business Partner" sections into a div with class navigation.
 *
 * @param type $variables
 * @return string
 */
function at_designbase_menu_tree__menu_bliv_business_partner($variables) {
  $out = '<div class="navigation">';
  $out .= '<ul>';
  $out .= $variables['tree'];
  $out .= '</ul>';
  $out .= '</div>';

  return $out;
}

/**
 * Implementation of hook_css_alter().
 *
 * Remove unneeded css files from frontend to speed up load
 */
//function at_designbase_css_alter(&$css) {
//
//  // Imagecrop uses frontend - bail out on impact
//  if (arg(0) == "imagecrop") {
//    return;
//  }
//
//  $paths = _at_designbase_get_allowed_modules();
//
//  // Loop through all css'es
//  foreach ($css as $key => $value) {
//    $unset = TRUE;
//    foreach ($paths as $path) {
//      if (strpos($key, $path) !== FALSE) {
//        $unset = FALSE;
//      }
//    }
//
//    if ($unset) {
//      unset($css[$key]);
//    }
//  }
//}

/**
 * Get the url of a styled image
 *
 * @param $field the field to get the file from
 * @param $style name of the style to use
 *
 * @return url of the styled field
 */
function image_cache_for_apsis($style, $field) {
  $add = "";

  if (is_object($field)) {
    $uri = $field->uri;
  }
  else {
    $uri = $field["uri"];
    $sql = "SELECT entity_id as nid FROM {field_data_field_image} WHERE field_image_fid = :fid";

    $result = db_query($sql, array(":fid" => $field["fid"]));
    foreach ($result as $r) {
      if (is_object($r) > 0) {
        $node = node_load($r->nid);
        if (isset($node->nid)) {
          $add = "?" . $node->changed;
        }
      }
      else {
        $add = "";
      }
    }
  }

  $out = image_style_url_for_apsis($style, $uri);
  $out .= $add;

  return $out;
}

function image_style_url_for_apsis($style_name, $path) {
  $uri = image_style_path($style_name, $path);

  // If not using clean URLs, the image derivative callback is only available
  // with the query string. If the file does not exist, use url() to ensure
  // that it is included. Once the file exists it's fine to fall back to the
  // actual file path, this avoids bootstrapping PHP once the files are built.
  if (!variable_get('clean_url') && file_uri_scheme($uri) == 'public' && !file_exists($uri)) {
    $directory_path = file_stream_wrapper_get_instance_by_uri($uri)->getDirectoryPath();
    //  return url($directory_path . '/' . file_uri_target($uri), array('absolute' => TRUE));
  }

  return file_create_url_apsis($uri);
}

function file_create_url_apsis($uri) {
  // Allow the URI to be altered, e.g. to serve a file from a CDN or static
  // file server.
  drupal_alter('file_url', $uri);

  $scheme = file_uri_scheme($uri);

  if (!$scheme) {

    // Allow for:
    // - root-relative URIs (e.g. /foo.jpg in http://example.com/foo.jpg)
    // - protocol-relative URIs (e.g. //bar.jpg, which is expanded to
    //   http://example.com/bar.jpg by the browser when viewing a page over
    //   HTTP and to https://example.com/bar.jpg when viewing a HTTPS page)
    // Both types of relative URIs are characterized by a leading slash, hence
    // we can use a single check.
    if (drupal_substr($uri, 0, 1) == '/') {
      return $uri;
    }
    else {
      // If this is not a properly formatted stream, then it is a shipped file.
      // Therefore, return the urlencoded URI with the base URL prepended.
      return $GLOBALS['base_url'] . '/' . drupal_encode_path($uri);
    }
  }
  elseif ($scheme == 'http' || $scheme == 'https') {
    // Check for http so that we don't have to implement getExternalUrl() for
    // the http wrapper.
    return $uri;
  }
  else {
    // Attempt to return an external URL using the appropriate wrapper.
    if ($wrapper = file_stream_wrapper_get_instance_by_uri($uri)) {
      list($scheme, $target) = explode('://', $uri, 2);
      $target = trim($target, '\/');
      $path = str_replace('\\', '/', $target);

      $host = $_SERVER['HTTP_HOST'];
      if (strpos($host, 'www.') !== 0) {
        $host = 'www.' . $host;
      }
      return "http://" . $host . "/" . variable_get('file_public_path', conf_path() . '/files') . '/' . drupal_encode_path($path);
    }
    else {
      return FALSE;
    }
  }
}

/**
 * Get the url of a styled image
 *
 * @param $field the field to get the file from
 * @param $style name of the style to use
 *
 * @return url of the styled field
 */
function image_cache($style, $field) {
  $add = "";

  if (is_object($field)) {
    $uri = $field->uri;
  }
  else {
    $uri = $field["uri"];
    $sql = "SELECT entity_id as nid FROM {field_data_field_image} WHERE field_image_fid = :fid";

    $result = db_query($sql, array(":fid" => $field["fid"]));
    foreach ($result as $r) {
      if (is_object($r) > 0) {
        $node = node_load($r->nid);
        if (isset($node->nid)) {
          $add = "?" . $node->changed;
        }
      }
      else {
        $add = "";
      }
    }
  }

  $out = image_style_url($style, $uri);
  $out .= $add;

  return $out;
}

/**
 * Get a list of all modules that can enable css and js files
 *
 * @return array containing paths to modules
 */
function _at_designbase_get_allowed_modules() {

  $a[] = "admin_menu";
  $a[] = "toolbar";

  // Find paths for modules
  foreach ($a as $module) {
    $paths[] = drupal_get_path("module", $module);
  }

  return $paths;
}

/**
 * Dateformat
 *
 * @param $timestamp
 *   Unix timestamp
 * @return
 *   Formatted date
 */
function get_date($timestamp) {

  // Check for MySQL timestamp
  if (substr($timestamp, 4, 1) == "-") {
    return get_date_mysql($timestamp);
  }

  $day = date("d", $timestamp);
  $month = date("m", $timestamp);
  $year = date("y", $timestamp);

  $out = $day . "." . $month . "." . $year;

  return $out;
}

/**
 * Dateformat
 *
 * @param $timestamp
 *   Mysql timestamp
 * @return
 *   Formatted date
 */
function get_date_mysql($timestamp) {
  $year = substr($timestamp, 2, 2);
  $month = substr($timestamp, 5, 2);
  $day = substr($timestamp, 8, 2);

  $out = $day . "." . $month . "." . $year;

  return $out;
}

function at_designbase_theme() {
  return array(
    "comment_form" => array(
      'template' => "comment-form",
      "render element" => "",
    ),
    "comment_wrapper" => array(
      "arguments" => array('form' => NULL),
      'template' => "comment-wrapper",
    ),
  );
}

function get_day($day) {
  $ret = "";
  switch ($day) {
    case "Monday":
      $ret = "Mandag";
      break;
    case "Tuesday":
      $ret = "Tirsdag";
      break;
    case "Wednesday":
      $ret = "Onsdag";
      break;
    case "Thursday":
      $ret = "Torsdag";
      break;
    case "Friday":
      $ret = "Fredag";
      break;
    case "Saturday":
      $ret = "L&oslash;rdag";
      break;
    case "Sunday":
      $ret = "S&oslash;ndag";
      break;
  }
  return $ret;
}

function at_designbase_user_register_form() {
  
}

function at_designbase_user_register() {
  
}

function at_designbase_select_as_radios($vars) {
  $element = &$vars['element'];

  if (!empty($element['#bef_nested'])) {
    return theme('select_as_tree', $vars);
  }
  $types = array(
    'edit-field-event-type-tid',
    'edit-field-products-tid',
    'edit-field-job-geo-tid'
  );
  if (in_array($element['#id'], $types)) {
    $output = ($vars['element']['#value'] == 'most_active') ? '<div class="date active">' : '<div class="date">';
  }
  else {
    $output = '';
  }
  $i = 1;
  if ($element['#id'] == 'edit-field-products-tid') {
    foreach (element_children($element) as $key) {
      $element[$key]['#default_value'] = NULL;
      $element[$key]['#children'] = theme('radio', array('element' => $element[$key]));
      $output .= theme('form_element', array('element' => $element[$key]));
      if ($i == 2) {
        $output .= ($vars['element']['#value'] == 'update_sort_showroom') ? '</div><div class="date active">' : '</div><div class="date">';
      }
      if ($i == 3) {
        $not_value = array($vars['element']['#default_value'], 'update_sort_showroom', 'most_active');
        if (!in_array($vars['element']['#value'], $not_value)) {
          $output .= '</div><div class="type-filter active"><div>Produkter</div><div class="options"><div class="options-wrap clearfix">';
        }
        else {
          $output .= '</div><div class="type-filter"><div>Produkter</div><div class="options"><div class="options-wrap clearfix">';
        }
      }
      $i++;
    }
  }
  else {    
    foreach (element_children($element) as $key) {
      $element[$key]['#default_value'] = NULL;
      $element[$key]['#children'] = theme('radio', array('element' => $element[$key]));
      $output .= theme('form_element', array('element' => $element[$key]));
      if ($i == 1 && in_array($element['#id'], $types)) {
        $tit = ($element['#id'] == 'edit-field-job-geo-tid') ? 'Landsdel' : 'Type';
        if ($vars['element']['#value'] != $vars['element']['#default_value']) {
          $output .= '</div><div class="type-filter active"><div>' . $tit . '</div><div class="options"><div class="options-wrap clearfix">';
        }
        else {
          $output .= '</div><div class="type-filter"><div>' . $tit . '</div><div class="options"><div class="options-wrap clearfix">';
        }
      }
      $i++;
    }
  }
  
  if (in_array($element['#id'], $types)) {
    $output .= '</div></div></div>';
  }

  return $output;
}

/**
 * Implements theme_menu_tree().
 */
function at_designbase_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

function at_designbase_views_load_more_pager($vars) {
  drupal_add_js(drupal_get_path('theme', variable_get('theme_default', NULL)) . '/scripts/load_less.js', 'file');
  global $pager_page_array, $pager_total;
  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];

  $pager_classes = array('pager', 'pager-load-more');

  $li_next = theme('pager_next',
    array(
      'text' => (isset($tags[3]) ? $tags[3] : t($vars['more_button_text'])),
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
    )
  );
  
  if (empty($li_next)) {
    $li_next = empty($vars['more_button_empty_text']) ? '&nbsp;' : t($vars['more_button_empty_text']);
    $pager_classes[] = 'pager-load-more-empty';
  }
  // Compatibility with tao theme's pager
  elseif (is_array($li_next) && isset($li_next['title'], $li_next['href'], $li_next['attributes'], $li_next['query'])) {
    $li_next = l($li_next['title'], $li_next['href'], array('attributes' => $li_next['attributes'], 'query' => $li_next['query']));
  }
  $li_next = preg_replace('#pager_prev=Y#', '', $li_next);

  $page_cur = (isset($_GET['page'])) ? $_GET['page'] : 0;
  if ($page_cur > 0) {
    $parameters['page'] = $page_cur-1;
    $parameters['pager_prev'] = 'Y';
    $li_prev = l('&ndash;', '<front>', array('query' => $parameters, 'html' => TRUE));
  }

  if ($pager_total[$element] > 1) {
    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );
    if (isset($li_prev)) {
      $items[] = array(
        'class' => array('pager-prev'),
        'data' => $li_prev,
      );
    }
    return theme('item_list',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => $pager_classes),
      )
    );
  }
}


function at_designbase_preprocess_views_view_unformatted(&$vars) {
  $view = $vars['view'];
  $rows = $vars['rows'];
  $style = $view->style_plugin;
  $options = $style->options;

  $vars['classes_array'] = array();
  $vars['classes'] = array();
  $default_row_class = isset($options['default_row_class']) ? $options['default_row_class'] : FALSE;
  $row_class_special = isset($options['row_class_special']) ? $options['row_class_special'] : FALSE;
  // Set up striping values.
  $count = 0;
  $max = count($rows);
  foreach ($rows as $id => $row) {
    $count++;
    $p = $vars['view']->query->pager->current_page;
    $vars['classes'][$id][] = 'page-' . $p;
    if ($default_row_class) {
      $vars['classes'][$id][] = 'views-row';
      $vars['classes'][$id][] = 'views-row-' . $count;
    }
    if ($row_class_special) {
      $vars['classes'][$id][] = 'views-row-' . ($count % 2 ? 'odd' : 'even');
      if ($count == 1) {
        $vars['classes'][$id][] = 'views-row-first';
      }
      if ($count == $max) {
        $vars['classes'][$id][] = 'views-row-last';
      }
    }

    if ($row_class = $view->style_plugin->get_row_class($id)) {
      $vars['classes'][$id][] = $row_class;
    }

    // Flatten the classes to a string for each row for the template file.
    $vars['classes_array'][$id] = isset($vars['classes'][$id]) ? implode(' ', $vars['classes'][$id]) : '';
  }
}

function at_designbase_get_preview_id() {
  $preview_id = NULL;
  $path = explode('/', request_uri());
  foreach($path as $k => $p) {
    if ($p == 'pagepreview') {
      $preview_id = $path[$k + 1];
      break;
    }    
  }
  return $preview_id;
}
