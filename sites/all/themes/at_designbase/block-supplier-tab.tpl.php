<h1>Min konto</h1>
<p class="editSectionText">
  <?php 
  global $user;
  if (array_key_exists(4, $user->roles)) {
    print variable_get('admin_myprofile_showroom_text');
  } 
  else {
    print variable_get('admin_myprofile_text');
  }
  ?>
</p>
<?php $subscription = (isset($supplier->field_subscription_type['und'][0]['value']))?$supplier->field_subscription_type['und'][0]['value']:1; ?>
<div class="editSection">
<?php if ($subscription != 2): ?>    
<div class="businessPartner"></div>
<?php else : ?>
<div class="eventPartner"></div>
<?php endif; ?>
<?php 
global $user;
$detect = mobile_detect_get_object();
$is_mobile = $detect->isMobile();
if (array_key_exists(4, $user->roles) && !$is_mobile) :
?>
  <ul class="tabs">
    <?php if ($context == 'basisinfo'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Virksomhedsprofil','user'); ?>
    </li>
    
    <?php if ($context == 'brands_profile'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Brands','brandoversigt'); ?>
    </li>
    
    
    <?php if ($subscription != 2): ?>    
    <?php if ($context == 'kontakter'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Personer','kontaktoversigt'); ?>
    </li>
    <?php endif; ?>

    <?php if ($context == 'events'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Events','eventoversigt'); ?>
    </li>

    <?php if ($subscription != 2): ?>
      <?php if ($context == 'nyhederoversigt'): ?>
      <li class="active">
      <?php else: ?> 
      <li>
      <?php endif; ?>
        <?php print l('Opslag','nyhederoversigt'); ?>
      </li>
    <?php endif; ?>
      
    
    <?php if ($subscription != 2) : ?>
    <li <?php if ($context == "joboversigt"): ?> class="active" <?php endif;?> >
      <?php print l('Jobopslag','joboversigt'); ?>
    </li>
    <?php endif; ?>

    <?php if ($subscription != 2): ?>
    <?php if ($context == 'statistik'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Statistik','statistik'); ?>
    </li>
    <?php endif; ?>
  </ul>
<?php endif; ?>
  <div class="tabContainer">
  <?php print $content; ?>
  </div>
</div>