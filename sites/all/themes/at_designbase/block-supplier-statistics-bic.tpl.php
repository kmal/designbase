<div class="container-group">
  <span class="headline"><?php print $type; ?></span>
  <span class="count"><?php print $content['supplier']; ?></span>
  <div class="bic">
    <div class="container-border">
      <div class="container-bar">
        <div class="bar" style="width: <?php print ($content['pct'] * 100); ?>%;"></div>
      </div>
    </div>
    <span class="pct"><?php print round($content['pct'] * 100); ?>%</span>
  </div>
</div>