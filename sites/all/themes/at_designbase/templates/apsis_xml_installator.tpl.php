<root>
<ANPNEWSLETTER mailinglist="Apsis Test" name="Designbase XML Template" subject="Apsis XML Template" templatename="[1503] Designbase XML" tracking="all" sendnow="false" fromemail="noreply@apsis.com" fromname="Apsis" usemainfilter="false">
<ANPPLACEHOLDER id="0">
<!--  Module: Header  -->
<ANPMODULEBLOCK moduleID="441185" filterName="">
<BLOCKITEM paramtag="@@WebversionText@@">
<![CDATA[ Problemmer med at se nyhedsbrevet? ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@WebversionLinkText@@">
<![CDATA[ klik her ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LogoImageURL@@">
<![CDATA[
http://customers.anpdm.com/horisontgruppen/1503_mknd/designbase_logo.png
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LogoLinkURL@@">
<![CDATA[ ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@GalleryImageURL@@">
<![CDATA[
http://customers.anpdm.com/horisontgruppen/1503_mknd/icon_gallery.png
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@GalleryURL@@">
<![CDATA[ http:// ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@CalendarImageURL@@">
<![CDATA[
http://customers.anpdm.com/horisontgruppen/1503_mknd/icon_calendar.png
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@CalendarURL@@">
<![CDATA[ http:// ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@Date@@">
<![CDATA[ <?php print date("d.m.y"); ?> ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<!--  Module: Full width image  -->
<ANPMODULEBLOCK moduleID="441186" filterName="">
<BLOCKITEM paramtag="@@ImageURL@@">
<![CDATA[ http://i.anpwidget.com/img/580/150 ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL@@">
<![CDATA[ http:// ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<!--  Module: F: Centered text  -->
<ANPMODULEBLOCK moduleID="441187" filterName="">
<BLOCKITEM paramtag="@@Headline@@">
<![CDATA[ K&aelig;re ##SubscriberName## ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@Text@@">
<![CDATA[
Du modtager dette nyhedsbrev, fordi du er tilmeldt www.designbase.dk's nyhedsbrev p&aring e-mail adressen: ##SubscriberEmail##. 
God forn&oslash;jelse!
]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<!--  Module: Menu  -->
<ANPMODULEBLOCK moduleID="441188" filterName="">
<BLOCKITEM paramtag="@@MenuLinkURL1@@">
<![CDATA[ http://designbase.dk ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@MenuLinkText1@@">
<![CDATA[ FORSIDE ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@MenuLinkURL2@@">
<![CDATA[ http://designbase.dk/showroom ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@MenuLinkText2@@">
<![CDATA[ SHOWROOMS ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@MenuLinkURL3@@">
<![CDATA[ http://designbase.dk/events ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@MenuLinkText3@@">
<![CDATA[ EVENTS ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@MenuLinkURL4@@">
<![CDATA[ http://designbase.dk/jobopslag ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@MenuLinkText4@@">
<![CDATA[ JOBOPSLAG ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<!--  Module: F: Title bar  -->
<ANPMODULEBLOCK moduleID="441189" filterName="">
<BLOCKITEM paramtag="@@Headline@@">
<![CDATA[ Seneste nyt ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
</ANPPLACEHOLDER>
<!--
 ANP_HOLDER_1 is identical to ANP_HOLDER_0 and can be used for full width modules. 
-->
<ANPPLACEHOLDER id="1"/>
<ANPPLACEHOLDER id="2">
<!--   Module: ⅔ Image, title & link  -->
<?php foreach ($content['news'] as $news): ?>
<?php if ($news->type_name == 'ARTICLE'): ?>
<ANPMODULEBLOCK moduleID="441191" filterName="">
<?php else : ?>
<ANPMODULEBLOCK moduleID="441192" filterName="">
<?php endif ?>
<BLOCKITEM paramtag="@@ImageURL@@">
 <?php if (!empty($news->field_image)) : ?>
<![CDATA[ <?php print(image_style_url('720px', $news->field_image['und'][0]['uri'])); ?> ]]>
<?php else : ?>
<![CDATA[ ]]>
<?php endif ?>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL@@">
<![CDATA[ http://www.designbase.dk<?php print url("node/".$news->nid); ?> ]]>
</BLOCKITEM>
<?php if ($news->type_name != 'ARTICLE'): ?>
<BLOCKITEM paramtag="@@Title@@">
<![CDATA[ <?php print $news->type_name; ?> ]]>
</BLOCKITEM>
<?php endif ?>
<BLOCKITEM paramtag="@@Headline@@">
<?php if (!empty($news->field_headline)) : ?>
<![CDATA[ <?php print $news->field_headline['und'][0]['value']; ?> ]]>
<?php else : ?>
<![CDATA[ ]]>
<?php endif ?>
</BLOCKITEM>
<BLOCKITEM paramtag="@@Text@@">
<?php if (!empty($news->field_teaser)) : ?>
<![CDATA[<?php print $news->field_teaser['und'][0]['value']; ?>]]>
<?php else : ?>
<![CDATA[ ]]>
<?php endif ?>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkText@@">
<![CDATA[  LÆS MERE ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<?php endforeach; ?>
<!--   Module: ⅔ Image (can be used for ads) -->
<ANPMODULEBLOCK moduleID="454775" filterName="">
<BLOCKITEM paramtag="@@ImageURL@@">
<![CDATA[ http://i.anpwidget.com/img2x/764/280 ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL@@">
<![CDATA[ http:// ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<!--  Module: ⅔: All news  -->
<ANPMODULEBLOCK moduleID="441193" filterName="">
<BLOCKITEM paramtag="@@SeeMoreURL@@">
<![CDATA[ http://designbase.dk/ ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@SeeMoreText@@">
<![CDATA[ SE ALLE NYHEDER ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<!--  Module: ⅔: 5x news with image -->
<ANPMODULEBLOCK moduleID="441196" filterName="">
<BLOCKITEM paramtag="@@Headline@@">
<![CDATA[ Nyheder fra Showroom ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@GreyText@@">
<![CDATA[ 
Sponseret indhold ]]>
</BLOCKITEM>
<?php $i = 1; ?>
<?php foreach ($content['bp_news'] as $bp_news): ?>
<BLOCKITEM paramtag="@@ImageURL<?php echo $i; ?>@@">
<?php if (!empty($bp_news->field_image)) : ?>
<![CDATA[ <?php print(image_style_url('379px', $bp_news->field_image['und'][0]['uri'])); ?> ]]>
<?php else : ?>
<![CDATA[ ]]>
<?php endif ?>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL<?php echo $i; ?>@@">
<![CDATA[ http://www.designbase.dk<?php print url("node/".$bp_news->nid); ?> ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@Title<?php echo $i; ?>@@">
<?php if (!empty($bp_news->type_name)) : ?>
<![CDATA[ <?php print $bp_news->type_name; ?> ]]>
<?php else : ?>
<![CDATA[ ]]>
<?php endif ?>
</BLOCKITEM>
<BLOCKITEM paramtag="@@Headline<?php echo $i; ?>@@">
<?php if (!empty($bp_news->field_headline)) : ?>
<![CDATA[ <?php print $bp_news->field_headline['und'][0]['value']; ?> ]]>
<?php else : ?>
<![CDATA[ ]]>
<?php endif ?>
</BLOCKITEM>
<BLOCKITEM paramtag="@@Text<?php echo $i; ?>@@">
<?php if (!empty($bp_news->field_teaser)) : ?>
<![CDATA[<?php print $bp_news->field_teaser['und'][0]['value']; ?>]]>
<?php else : ?>
<![CDATA[ ]]>
<?php endif ?>
</BLOCKITEM>
<?php $i++; ?>
<?php endforeach; ?>
</ANPMODULEBLOCK>
</ANPPLACEHOLDER>

<ANPPLACEHOLDER id="3">
<!--  Module: ⅓: News - headline  -->
<?php if (!empty($content['jobs'])) : ?>
<ANPMODULEBLOCK moduleID="454755" filterName="">
<BLOCKITEM paramtag="@@Headline@@">
<![CDATA[ Jobopslag ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<?php foreach ($content['jobs'] as $job): ?>
<!--  Module: ⅓: News - content  -->
<ANPMODULEBLOCK moduleID="454756" filterName="">
<BLOCKITEM paramtag="@@Title@@">
 <?php if (!empty($job->field_companyname)) : ?>
<![CDATA[ <?php print mb_strtoupper($job->field_companyname['und'][0]['value'] . (' søger')); ?> ]]>
<?php else: ?>
<![CDATA[ ]]>
<?php endif; ?>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkText@@">
<![CDATA[ <?php print $job->field_headline['und'][0]['value']; ?> ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL@@">
<![CDATA[ http://www.designbase.dk<?php print url("node/".$job->nid); ?> ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<?php endforeach; ?>
<?php endif; ?>
<!--  Module: Spacer  -->
<ANPMODULEBLOCK moduleID="454772" filterName="">
<BLOCKITEM paramtag="@@@@">
<![CDATA[ ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<!--   Module: ⅓Image (can be used for ads) -->
<ANPMODULEBLOCK moduleID="454776" filterName="">
<BLOCKITEM paramtag="@@ImageURL@@">
<![CDATA[ http://i.anpwidget.com/img2x/376/280 ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL@@">
<![CDATA[ http:// ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>


<!--  Module: ⅓: Most read - headline  -->
<ANPMODULEBLOCK moduleID="454755" filterName="">
<BLOCKITEM paramtag="@@Headline@@">
<![CDATA[ Mest læste ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>

<!--  Module: ⅓: Most read - content  -->
<?php foreach ($content['mostread'] as $key => $mostread) : ?>
<ANPMODULEBLOCK moduleID="454756" filterName="">
<BLOCKITEM paramtag="@@Title@@">
 <?php if (!empty($news->category)) : ?>
<![CDATA[ <?php print $news->category->name; ?> ]]>
<?php else : ?>
<![CDATA[ ]]>
<?php endif ?>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkText@@">
<![CDATA[
<?php print $mostread->field_headline['und'][0]['value']; ?>
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL@@">
<![CDATA[ http://www.designbase.dk<?php print url("node/". $mostread->nid); ?> ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<?php endforeach; ?>

<!--  Module: Spacer  -->
<ANPMODULEBLOCK moduleID="454772" filterName="">
<BLOCKITEM paramtag="@@@@">
<![CDATA[ ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>

<!--  Module: ⅓: Events - headline  -->
<ANPMODULEBLOCK moduleID="454755" filterName="">
<BLOCKITEM paramtag="@@Headline@@">
<![CDATA[ Events ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>

<!--  Module: ⅓: Events - content  -->
<?php foreach ($content['events'] as $event) : ?>
<ANPMODULEBLOCK moduleID="454756" filterName="">
<BLOCKITEM paramtag="@@Title@@">
 <?php if (!empty($event->type_name)) : ?>
<![CDATA[ <?php print mb_strtoupper($event->type_name); ?> ]]>
<?php else : ?>
<![CDATA[ ]]>
<?php endif ?>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkText@@">
<![CDATA[
<?php print $event->field_headline['und'][0]['value']; ?>
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL@@">
<![CDATA[ http://www.designbase.dk<?php print url("node/".$event->nid); ?> ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<?php endforeach; ?>

<!--  Module: ⅓: See more  -->
<ANPMODULEBLOCK moduleID="454758" filterName="">
<BLOCKITEM paramtag="@@SeeMoreURL@@">
<![CDATA[ http://designbase.dk/events ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@SeeMoreText@@">
<![CDATA[ SE ALLE EVENTS ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
</ANPPLACEHOLDER>
<ANPPLACEHOLDER id="4">
<!--  Module: Divider  -->
<ANPMODULEBLOCK moduleID="454780" filterName="">
<BLOCKITEM paramtag="@@@@">
<![CDATA[ ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<!--  Module: F: Small text  -->
<ANPMODULEBLOCK moduleID="441218" filterName="">
<BLOCKITEM paramtag="@@Text@@">
<![CDATA[
Designbase består af et ugentligt nyhedsbrev, websitet www.designbase.dk og et trykt magasin, der udkommer 8 gange årligt. Designbase berører emner som trends. ledelse, produktorientering samt tendenser og udvikling i detailhandlen. Formålet er at give dig inspiration og konkrete værktøjer til at kunne optimere din forretning og tage de rigtige beslutninger, som gør din virksomhed konkurrencedygtig. Den redaktionelle målsætning er gennem spændende og informative interviews, reportager, virksomhedscases, ekspertindlæg og produktnyheder at give læserne nye perspektiver, nyttig viden, faglig inspiration og redskaber til at drive forretning på professionel og fremadrettet vis. 
]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
<!--  Module: Divider  -->
<ANPMODULEBLOCK moduleID="454780" filterName="">
<BLOCKITEM paramtag="@@">
<![CDATA[ ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
</ANPPLACEHOLDER>
<ANPPLACEHOLDER id="5">
<!--  Module: Footer  -->
<ANPMODULEBLOCK moduleID="441219" filterName="">
<BLOCKITEM paramtag="@@LinkURL1@@">
<![CDATA[ http://horisontgruppen.dk/erhvervsmedier/#/designbase ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkText1@@">
<![CDATA[ Annoncer ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL2@@">
<![CDATA[ http://designbase.dk/abonnement-0 ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkText2@@">
<![CDATA[ Abonnement ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL3@@">
<![CDATA[ http://designbase.dk/nytjob ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkText3@@">
<![CDATA[ Opret joboslag ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkURL4@@">
<![CDATA[ http://designbase.dk/modal/showroom_create/nojs ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LinkText4@@">
<![CDATA[ Opret showroom ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@FbImageURL@@">
<![CDATA[
http://customers.anpdm.com/horisontgruppen/1503_mknd/icon_fb.png
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@FbURL@@">
<![CDATA[ https://www.facebook.com/designbasedk ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LiImageURL@@">
<![CDATA[
http://customers.anpdm.com/horisontgruppen/1503_mknd/icon_li.png
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LiURL@@">
<![CDATA[ https://www.linkedin.com/company/5151476 ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@InstaImageURL@@">
<![CDATA[
http://customers.anpdm.com/horisontgruppen/1503_mknd/icon_insta.png
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@InstaURL@@">
<![CDATA[ https://instagram.com/designbasedk/ ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@PostalAdress@@">
<![CDATA[
Center Boulevard 5<br>International House<br>2300 København S
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@Phone@@">
<![CDATA[ 32473230 ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@Fax@@">
<![CDATA[ 32473239 ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@Email@@">
<![CDATA[ info@horisontgruppen.dk ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LogoImageURL@@">
<![CDATA[
http://customers.anpdm.com/horisontgruppen/1503_mknd/hg_logo.png
]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@LogoLinkURL@@">
<![CDATA[ http://horisontgruppen.dk ]]>
</BLOCKITEM>
<BLOCKITEM paramtag="@@TagLine@@">
<![CDATA[ nichemedier der bringer vækst og værdi ]]>
</BLOCKITEM>
</ANPMODULEBLOCK>
</ANPPLACEHOLDER>
</ANPNEWSLETTER>
</root>
