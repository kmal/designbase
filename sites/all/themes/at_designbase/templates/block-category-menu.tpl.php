<div class=" menu-block-3">
  <ul class="category-menu clearfix">
    <li><?php print l('Forside', '<front>'); ?></li> 
    <li class="expanded"><?php print l('Kategorier <i class="fa fa-plus"></i>', 'category', array('html' => TRUE)); ?> 
      <ul class="menu">
        <?php foreach ($tree as $term) : ?>
        <li><?php print l($term->name, "taxonomy/term/" . $term->tid); ?></li>
        <?php endforeach; ?>
      </ul>
    </li>
  </ul>
</div>
