<div id="page-wrapper">
  <div class="preloader-page">
    <div class="img-cont"><img src="<?php print drupal_get_path('theme', variable_get('theme_default', NULL)) . '/images/preloader.gif';?>"></div>
  </div>  
  <div id="top_nav_bar" class="clearfix">
  <div class="section clearfix">     
  <!-- !Leaderboard Region -->
    <?php if ($site_logo || $site_name || $site_slogan): ?>
      <!-- !Branding -->
      <div<?php print $branding_attributes; ?>>

        <?php if ($site_logo): ?>
          <div id="logo">
            <?php print $site_logo; ?>
          </div>
        <?php endif; ?>

        <?php if ($site_name || $site_slogan): ?>
          <div<?php print $hgroup_attributes; ?>>

            <?php if ($site_name): ?>
              <h1<?php print $site_name_attributes; ?>><?php print $site_name; ?></h1>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <h2<?php print $site_slogan_attributes; ?>><?php print $site_slogan; ?></h2>
            <?php endif; ?>

          </div>
        <?php endif; ?>

      </div>
    <?php endif; ?>
    <?php print render($page['leaderboard']); ?>
  </div></div>
  <div id="main-navigation">
    <div class="close"><span>LUK</span> <i class="fa fa-times"></i></div>  
    <?php print render($page['main_navigation']); ?>
  </div>
  <div id="wrapper-white-opacity" class="hidden"></div>
  <div id="page" class="container <?php print $classes; ?>">
    
    <?php if ($page['header']) :?>
    <header<?php print $header_attributes; ?>>
      <!-- !Header Region -->
      <?php print render($page['header']); ?>
    </header>
    <?php endif; ?>

    <!-- !Navigation -->
    <?php print render($page['menu_bar']); ?>
    <?php if ($primary_navigation): print $primary_navigation; endif; ?>
    <?php if ($secondary_navigation): print $secondary_navigation; endif; ?>

    <!-- !Breadcrumbs -->
    <?php //if ($breadcrumb): print $breadcrumb; endif; ?>

    <!-- !Messages and Help -->
    <?php print $messages; ?>
    <?php print render($page['help']); ?>

    <!-- !Secondary Content Region -->
    <?php print render($page['secondary_content']); ?>

    <div id="columns" class="columns clearfix">
      <main id="content-column" class="content-column" role="main">
        <div class="content-inner">

          <!-- !Highlighted region -->
          <?php print render($page['highlighted']); ?>

          <<?php print $tag; ?> id="main-content">

            <?php print render($title_prefix); // Does nothing by default in D7 core ?>

            <!-- !Main Content Header -->
            <?php if ($title || $primary_local_tasks || $secondary_local_tasks || $action_links = render($action_links)): ?>
              <header<?php print $content_header_attributes; ?>>

                <?php if ($title): ?>
                  <h1 id="page-title">
                    <?php print $title; ?>
                  </h1>
                <?php endif; ?>

                <?php 
                global $user;
                
                if (array_key_exists(3, $user->roles)) {
                  
                  if ($primary_local_tasks || $secondary_local_tasks || $action_links): ?>
                    <div id="tasks">

                      <?php if ($primary_local_tasks): ?>
                        <ul class="tabs primary clearfix"><?php print render($primary_local_tasks); ?></ul>
                      <?php endif; ?>

                      <?php if ($secondary_local_tasks): ?>
                        <ul class="tabs secondary clearfix"><?php print render($secondary_local_tasks); ?></ul>
                      <?php endif; ?>

                      <?php if ($action_links = render($action_links)): ?>
                        <ul class="action-links clearfix"><?php print $action_links; ?></ul>
                      <?php endif; ?>

                    </div>
                  <?php endif; 
                
                }?>

              </header>
            <?php endif; ?>

            <!-- !Main Content -->
            <?php if ($content = render($page['content'])): ?>
              <div id="content" class="region">
                <?php print $content; ?>
              </div>
            <?php endif; ?>

            <!-- !Feed Icons -->
            <?php print $feed_icons; ?>

            <?php print render($title_suffix); // Prints page level contextual links ?>

          </<?php print $tag; ?>><!-- /end #main-content -->

          <!-- !Content Aside Region-->
          <?php print render($page['content_aside']); ?>

        </div><!-- /end .content-inner -->
      </main><!-- /end #content-column -->

      <!-- !Sidebar Regions -->
      <?php $sidebar_first = render($page['sidebar_first']); print $sidebar_first; ?>
      <?php $sidebar_second = render($page['sidebar_second']); print $sidebar_second; ?>

    </div><!-- /end #columns -->
    <div id="footer-wpapper">
    <!-- !Tertiary Content Region -->
    <?php print render($page['tertiary_content']); ?>
    <?php if ($page['pre_footer']): ?>
      <div id="pre-footer" class="clearfix">
        <?php print render($page['pre_footer']); ?>
      </div> <!-- /#pre-footer -->
    <?php endif; ?>
    <!-- !Footer -->
    <?php if ($page['footer'] || $attribution): ?>
      <footer<?php print $footer_attributes; ?>>
        <?php print render($page['footer']); ?>
        <?php print $attribution; ?>
      </footer>
    <?php endif; ?>
    </div>

  </div>
</div>
