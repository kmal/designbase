<?php 
hide($content['field_link']); 
hide($content['field_date']);
hide($content['field_location']);
hide($content['field_add_this']);
?>  
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <?php if (isset($share_block) && $share_block) :?>
    <div class="left-share-block clearfix">
      <?php if (isset($content['field_add_this'])) {?>  
        <div class="item add-this"><?php print render($content['field_add_this']);?></div>
      <?php }?>
      <?php if (isset($disqus)) { ?>  
        <div class="comment-count"><i class="fa fa-comment"></i><?php print $disqus;?></div>
      <?php }?>
    </div>
  <?php endif; ?> 
  
  <?php if ($title_prefix): ?>
    <div class="title-prefix"><?php print render($title_prefix); ?></div>
  <?php endif; ?>
  <?php if (!$page): ?>
    <h1 <?php print $title_attributes; ?>>
      <?php print $title; ?>
    </h1>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <div class="title-suffix">
    <?php if (isset($node->supplier_company['und'][0]['value'])): ?>
      <span class="author"><?php print $node->supplier_company['und'][0]['value']; ?></span>
      <span class="seperator">|</span>
      <span class="link"><?php print l('Se virksomhedsprofil', 'node/' . $node->supplier_id); ?></span>
    <?php endif; ?>  
  </div>
  <div class="eventWhenWhere" >
    <ul class="">
      <li>
      <?php
      $time =  strtotime($node->field_date['und'][0]['value']);
      $date = '<span class="date"><i class="fa fa-calendar"></i>' . date('d.m.Y', $time) . '</span>' . ' <span class="time"><i class="fa fa-clock-o"></i>' . date('H.i', $time) . '</span>';
      print $date;
      ?> 
      </li>
      <?php if (isset($node->field_location['und'])) : ?>
      <li><?php print '<i class="fa fa-map-marker"></i>' . $node->field_location['und'][0]['value']; ?> </li>
      <?php endif; ?>  
    </ul>
    <?php if ($node->field_link && !empty($node->field_link['und'][0]['value'])): ?>
        <?php if (strpos($node->field_link['und'][0]['value'], 'mailto:') > -1): ?>
          <a class="event-mailto" data-mail="<?php print $node->field_link['und'][0]['value']; ?>" class="gray-plus" data-eid="<?php print $node->nid; ?>"><span>Tilmeld</span></a>
        <?php else: ?>
          <a href="<?php echo $node->field_link['und'][0]['value']; ?>" target="_blank" class="gray-plus"><span>Tilmeld</span></a>
        <?php endif; ?>   
      <?php endif; ?>
      
  </div>  

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
      
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>
  
   

</div>
