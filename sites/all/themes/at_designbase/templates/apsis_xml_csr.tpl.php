<?php print '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<root>
<ANPNEWSLETTER mailinglist="Horisontgruppen" name="CSR Nyhedsbrev" subject="CSR XML Template" templatename="[1210-CSR] CSR" tracking="yes" sendnow="false" fromemail="info@horisontgruppen.com" fromname="Horisontgruppen" usemainfilter="false">
    <ANPPLACEHOLDER id="0">
      <ANPMODULEBLOCK moduleID="140937" filterName=""> <!-- Module: Webversion + annonce -->
        <BLOCKITEM paramtag="@@EnergiAvtale@@"><![CDATA[<?php print $content['news'][0]->field_headline['und'][0]['value']; ?>]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageHovedsponsor@@"><![CDATA[http://customers.anpdm.com/horisontgruppen/1207_knd/205.gif]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@"><![CDATA[#]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageAnnonce@@"><![CDATA[http://customers.anpdm.com/horisontgruppen/1207_knd/205.gif]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL2@@"><![CDATA[#]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
      <ANPMODULEBLOCK moduleID="140913" filterName=""> <!-- Module: Header -->
        <BLOCKITEM paramtag="@@Datum@@"><![CDATA[<?php print date("d.m.y"); ?>]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Logo@@"><![CDATA[http://www.csr.dk/images/csr_logo.jpg?a=1]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@"><![CDATA[http://www.csr.dk]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Slogan@@"><![CDATA[Bliv en del af netværket]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Logo2@@"><![CDATA[http://customers.anpdm.com/horisontgruppen/1207_knd/bp_logo.gif]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL2@@"><![CDATA[#]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
		  <ANPMODULEBLOCK moduleID="140926" filterName=""> <!-- Module: Menu -->
        <BLOCKITEM paramtag="@@LinkText1@@"><![CDATA[FORSIDE]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@LinkURL1@@"><![CDATA[http://www.csr.dk/]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@LinkText2@@"><![CDATA[BLOGGEN]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@LinkURL2@@"><![CDATA[http://www.csr.dk/blog]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@LinkText3@@"><![CDATA[KOMPETENCEGUIDE]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@LinkURL3@@"><![CDATA[http://www.csr.dk/supplierguide]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@LinkText4@@"><![CDATA[EVENTS]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@LinkURL4@@"><![CDATA[http://www.csr.dk/events]]></BLOCKITEM>
        
      </ANPMODULEBLOCK>
    </ANPPLACEHOLDER>
        
    <ANPPLACEHOLDER id="1">      
      <ANPMODULEBLOCK moduleID="140912" filterName="">	<!-- Module: Full width text -->
        <BLOCKITEM paramtag="@@Title1@@"><![CDATA[K&aelig;re ##SubscriberName##]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@BodyText1@@"><![CDATA[           
          Du modtager dette nyhedsbrev, fordi du er tilmeldt www.csr.dk&#39;s nyhedsbrev på e-mail adressen: ##SubscriberEmail##. God fornøjelse!
          ]]></BLOCKITEM> 
      </ANPMODULEBLOCK>
      <ANPMODULEBLOCK moduleID="140910" filterName="">	<!-- Module: Full width divider -->
        <BLOCKITEM paramtag="@@@@"><![CDATA[]]></BLOCKITEM>
      </ANPMODULEBLOCK>                  
    </ANPPLACEHOLDER>                    
    <ANPPLACEHOLDER id="2">      
      <?php foreach ($content['news'] as $news): ?>
      <?php if ($news->field_image && $news->field_image['und'][0]['filename']) : ?>
      <ANPMODULEBLOCK moduleID="140922" filterName="">	<!-- Module: L - image-->
        <BLOCKITEM paramtag="@@Image1@@"><![CDATA[<?php print image_cache_for_apsis("379px",$news->field_image['und'][0]); ?> ]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@"><![CDATA[http://www.csr.dk<?php print url("node/".$news->nid); ?>]]></BLOCKITEM>
      </ANPMODULEBLOCK>            
      <ANPMODULEBLOCK moduleID="140924" filterName="">	<!-- Module: L - main article -->
        <BLOCKITEM paramtag="@@Title1@@"><![CDATA[<?php print $news->field_headline['und'][0]['value']; ?>]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@LinkText1@@"><![CDATA[<?php print $news->category->name; ?>]]></BLOCKITEM>		
        <BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk/<?php print url("taxonomy/term/".$news->category->tid); ?>]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@Date@@"><![CDATA[<?php print date("d.m.Y", $news->created); ?>]]></BLOCKITEM>

        <BLOCKITEM paramtag="@@BodyText1@@"><![CDATA[<?php print $news->field_teaser['und'][0]['value']; ?>]]></BLOCKITEM>        
        <BLOCKITEM paramtag="@@LinkText2@@"><![CDATA[L&#230;s mere]]></BLOCKITEM>		
        <BLOCKITEM paramtag="@@URL2@@"><![CDATA[http://www.csr.dk<?php print url("node/".$news->nid); ?>]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      <?php else: ?>
      <ANPMODULEBLOCK moduleID="140924" filterName="">	<!-- Module: L - main article -->
        <BLOCKITEM paramtag="@@Title1@@"><![CDATA[<?php print $news->field_headline['und'][0]['value']; ?>]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@LinkText1@@"><![CDATA[<?php print $news->category->name; ?>]]></BLOCKITEM>		
<?php /* ?>        <BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk<?php print url("taxonomy/term/".$news->category->tid); ?>]]></BLOCKITEM><?php */ ?>
        <BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk<?php print url("node/".$news->nid); ?>]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Date@@"><![CDATA[<?php print date("d.m.Y", $news->created); ?>]]></BLOCKITEM>

        <BLOCKITEM paramtag="@@BodyText1@@"><![CDATA[<?php print $news->field_teaser['und'][0]['value']; ?>]]></BLOCKITEM>        
        <BLOCKITEM paramtag="@@LinkText2@@"><![CDATA[L&#230;s mere]]></BLOCKITEM>		
        <BLOCKITEM paramtag="@@URL2@@"><![CDATA[http://www.csr.dk<?php print url("node/".$news->nid); ?>]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      <?php endif; ?>
      
      <ANPMODULEBLOCK moduleID="140917" filterName="">	<!-- Module: L - divider -->
        <BLOCKITEM paramtag="@@@@"><![CDATA[]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      <?php endforeach; ?>                  
      <ANPMODULEBLOCK moduleID="140915" filterName="">	<!-- Module: L - annonce-->
        <BLOCKITEM paramtag="@@Image1@@"><![CDATA[http://customers.anpdm.com/horisontgruppen/1207_knd/365.gif]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@"><![CDATA[#]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
      <ANPMODULEBLOCK moduleID="140925" filterName="">	<!-- Module: L - tema -->
        <BLOCKITEM paramtag="@@Tema@@"><![CDATA[Tema]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Title1@@"><![CDATA[Lorem ipsum dolor sit amet, consectetuer elit.]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@BodyText1@@"><![CDATA[Lorem ipsum dolor sit amet, consectetuer elit.]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
      <ANPMODULEBLOCK moduleID="140920" filterName="">	<!-- Module: L - grey headline -->
          <BLOCKITEM paramtag="@@Title1@@"><![CDATA[NYHEDER FRA LEVERANDØRER, FORENINGER OG ORGANISATIONER]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
      <?php foreach ($content['bp_news'] as $key => $bp_news) : ?>

      <?php if ($bp_news->field_image && $bp_news->field_image['und'][0]['filename']) : ?>
      <ANPMODULEBLOCK moduleID="140923" filterName="">	<!-- Module: L - Image left, text  right -->
        <BLOCKITEM paramtag="@@Image1@@"><![CDATA[<?php print image_cache_for_apsis("70px", $bp_news->field_image['und'][0]); ?>]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@"><![CDATA[http://www.csr.dk<?php print url("node/".$bp_news->nid); ?>]]></BLOCKITEM>
                
        <BLOCKITEM paramtag="@@TitleLink1@@"><![CDATA[<?php print $bp_news->field_headline['und'][0]['value']; ?>]]></BLOCKITEM>                
        <BLOCKITEM paramtag="@@TitleURL1@@"><![CDATA[http://www.csr.dk<?php print url("node/".$bp_news->nid); ?>]]></BLOCKITEM>                        
        <BLOCKITEM paramtag="@@LinkText1@@"><![CDATA[<?php print $bp_news->category->name; ?>]]></BLOCKITEM>		
        <?php /* ?><BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk<?php print url("taxonomy/term/".$bp_news->category->tid); ?>]]></BLOCKITEM><?php */ ?>
        <BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk<?php print url("node/".$bp_news->nid); ?>]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Date@@"><![CDATA[<?php print date("d.m.Y", $bp_news->created); ?>]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@BodyText1@@"><![CDATA[<?php print htmlspecialchars($bp_news->field_teaser['und'][0]['value']); ?>]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
      <?php else: ?>
      <ANPMODULEBLOCK moduleID="140916" filterName="">	<!-- Module: L - article blue headline -->
        <BLOCKITEM paramtag="@@TitleLink1@@"><![CDATA[<?php print $bp_news->field_headline['und'][0]['value']; ?>]]></BLOCKITEM>                
        <BLOCKITEM paramtag="@@TitleURL1@@"><![CDATA[http://www.csr.dk<?php print url("node/".$bp_news->nid); ?>]]></BLOCKITEM>                        
        <BLOCKITEM paramtag="@@LinkText1@@"><![CDATA[<?php print $bp_news->category->name; ?>]]></BLOCKITEM>		
        <?php /* ?><BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk<?php print url("taxonomy/term/".$bp_news->category->tid); ?>]]></BLOCKITEM><?php */ ?>
        <BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk<?php print url("node/".$bp_news->nid); ?>]]></BLOCKITEM>        
        <BLOCKITEM paramtag="@@Date@@"><![CDATA[<?php print date("d.m.Y", $bp_news->created); ?>]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@BodyText1@@"><![CDATA[<?php print htmlspecialchars($bp_news->field_teaser['und'][0]['value']); ?>]]></BLOCKITEM>
      </ANPMODULEBLOCK>      
      <?php endif; ?>      
      <?php if ($key != count($content['bp_news'])-1) : ?>
      <ANPMODULEBLOCK moduleID="140918" filterName="">	<!-- Module: L - divider 1px -->
        <BLOCKITEM paramtag="@@@@"><![CDATA[]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      <?php endif; ?>
      <?php endforeach; ?>
      
      <ANPMODULEBLOCK moduleID="140940" filterName="">
      <!-- Module: L - flere leverandorer -->
      <BLOCKITEM paramtag="@@LinkText1@@">
      <![CDATA[ Flere nyheder fra leverandører ]]>
      </BLOCKITEM>
      <BLOCKITEM paramtag="@@URL1@@">
      <![CDATA[www.csr.dk/nyheder ]]>
      </BLOCKITEM>
      <BLOCKITEM paramtag="@@Image1@@">
      <![CDATA[http://customers.anpdm.com/horisontgruppen/1210_kndc/arrow.gif]]>
      </BLOCKITEM>
      <BLOCKITEM paramtag="@@ImageLinkURL1@@">
      <![CDATA[www.csr.dk/nyheder ]]>
      </BLOCKITEM>
      </ANPMODULEBLOCK>                 
      <ANPMODULEBLOCK moduleID="140919" filterName="">	<!-- Module: L - grey box, text + image -->
        <BLOCKITEM paramtag="@@Title1@@"><![CDATA[5 HURTIGE TIL Lorem Ipsum]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Title2@@"><![CDATA[Lorem Ipsum]]></BLOCKITEM>        
        <BLOCKITEM paramtag="@@Image1@@"><![CDATA[http://customers.anpdm.com/horisontgruppen/1207_knd/146_146.jpg]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@"><![CDATA[#]]></BLOCKITEM>        
        <BLOCKITEM paramtag="@@BodyText1@@"><![CDATA[Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.]]></BLOCKITEM>        
        <BLOCKITEM paramtag="@@Image2@@"><![CDATA[http://customers.anpdm.com/horisontgruppen/1210_kndc/read_more_2]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL2@@"><![CDATA[#]]></BLOCKITEM>
      </ANPMODULEBLOCK>       
    </ANPPLACEHOLDER>
    
    
    <ANPPLACEHOLDER id="3">
      <ANPMODULEBLOCK moduleID="140933" filterName="">	<!-- Module: R - mest laste -->
        <BLOCKITEM paramtag="@@Title1@@"><![CDATA[MEST LÆSTE]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      <?php $mr_count = count($content['mostread']); ?>
      <?php foreach ($content['mostread'] as $key => $mostread) : ?>
        <?php if ($mr_count-1 > $key) : ?>
      <ANPMODULEBLOCK moduleID="140931" filterName="">	<!-- Module: R - mest laste - content -->
        <BLOCKITEM paramtag="@@Date@@"><![CDATA[<?php print date("d.m.Y", $mostread->created); ?>]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@TitleLink1@@"><![CDATA[<?php print $mostread->field_headline['und'][0]['value']; ?>]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@TitleURL1@@"><![CDATA[http://www.csr.dk<?php print url("node/". $mostread->nid); ?>]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      <?php else: ?>
        <ANPMODULEBLOCK moduleID="140932" filterName="">	<!-- Module: R - mest laste - content -->
          <BLOCKITEM paramtag="@@Date@@"><![CDATA[<?php print date("d.m.Y", $mostread->created); ?>]]></BLOCKITEM>
          <BLOCKITEM paramtag="@@TitleLink1@@"><![CDATA[<?php print $mostread->field_headline['und'][0]['value']; ?>]]></BLOCKITEM>
          <BLOCKITEM paramtag="@@TitleURL1@@"><![CDATA[http://www.csr.dk<?php print url("node/". $mostread->nid); ?>]]></BLOCKITEM>
        </ANPMODULEBLOCK>
        <?php endif; ?>
      <?php endforeach; ?>
      
      <ANPMODULEBLOCK moduleID="140927" filterName="">	<!-- Module: R - annonce-->
        <BLOCKITEM paramtag="@@Image1@@"><![CDATA[http://customers.anpdm.com/horisontgruppen/1207_knd/160.gif]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@"><![CDATA[#]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
      <ANPMODULEBLOCK moduleID="140930" filterName="">	<!-- Module: R - events -->
        <BLOCKITEM paramtag="@@Title1@@"><![CDATA[EVENTS]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
      <?php foreach ($content['events'] as $event) : ?>
      <ANPMODULEBLOCK moduleID="140928" filterName="">	<!-- Module: R - events - content -->
        <BLOCKITEM paramtag="@@TitleLink1@@"><![CDATA[<?php print $event->field_headline['und'][0]['value']; ?>]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@TitleURL1@@"><![CDATA[http://www.csr.dk<?php print url("node/".$event->nid); ?>]]></BLOCKITEM>
       	<BLOCKITEM paramtag="@@BodyText1@@"><![CDATA[Afholdes: <?php print date("d.m.y", strtotime($event->field_date['und'][0]['value'])) . ", " . $event->comp_name . ". " . truncate_utf8($event->field_teaser['und'][0]['value'], 100, true, true); ?>]]></BLOCKITEM>	
      </ANPMODULEBLOCK>
      <?php endforeach; ?>      
      <ANPMODULEBLOCK moduleID="140929" filterName="">	<!-- Module: R - events - end -->
        <BLOCKITEM paramtag="@@LinkText1@@"><![CDATA[Flere events]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk/events]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Image1@@">
        <![CDATA[http://customers.anpdm.com/horisontgruppen/1210_kndc/arrow.gif]]>
        </BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@">
        <![CDATA[www.csr.dk/events]]>
        </BLOCKITEM>
      </ANPMODULEBLOCK>
      
      
      <ANPMODULEBLOCK moduleID="140936" filterName="">	<!-- Module: R - navnenyt -->
        <BLOCKITEM paramtag="@@Title1@@"><![CDATA[NAVNENYT]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
      <?php foreach ($content['namenews'] as $news) : ?>
      <ANPMODULEBLOCK moduleID="140934" filterName="">	<!-- Module: R - navnenyt - content -->
        <BLOCKITEM paramtag="@@Date@@"><![CDATA[<?php print date("d.m.Y", $news->created); ?>]]></BLOCKITEM>        
        <BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk<?php print url("node/".$news->nid); ?>]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@LinkText1@@"><![CDATA[<?php print $news->field_headline['und'][0]['value']; ?>]]></BLOCKITEM>
        
      </ANPMODULEBLOCK>
      <?php endforeach; ?>
      <ANPMODULEBLOCK moduleID="140935" filterName="">	<!-- Module: R - navnenyt - end -->
        <BLOCKITEM paramtag="@@LinkText1@@"><![CDATA[Mere navnenyt]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@URL1@@"><![CDATA[http://www.csr.dk/merenavnenyt]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Image1@@">
        <![CDATA[http://customers.anpdm.com/horisontgruppen/1210_kndc/arrow.gif]]>
        </BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@">
        <![CDATA[www.csr.dk/merenavnenyt]]>
        </BLOCKITEM>
      </ANPMODULEBLOCK>      
      
      
      <ANPMODULEBLOCK moduleID="141160" filterName="">
      <!-- Module: R - seneste kommentarer -->
      <BLOCKITEM paramtag="@@Title1@@">
      <![CDATA[ SENESTE KOMMENTARER ]]>
      </BLOCKITEM>
      </ANPMODULEBLOCK>
      <?php foreach ($content['comments'] as $comment) : ?>
      <ANPMODULEBLOCK moduleID="138774" filterName="">
      <!-- Module: R - seneste kommentarer - content -->
      <BLOCKITEM paramtag="@@Image1@@">
      <![CDATA[http://customers.anpdm.com/horisontgruppen/1207_knd/quote.gif]]>
      </BLOCKITEM>
      <BLOCKITEM paramtag="@@ImageLinkURL1@@">
        <![CDATA[www.csr.dk<?php print url('node/' . $comment['nid']) . "#comment-" . $comment['cid']; ?>]]>
      </BLOCKITEM>
      <BLOCKITEM paramtag="@@Title1@@">
      <![CDATA[ <?php print $comment['headline']; ?> ]]>
      </BLOCKITEM>
      <BLOCKITEM paramtag="@@TitleURL1@@">
      <![CDATA[www.csr.dk<?php print url('node/' . $comment['nid']) . "#comment-" . $comment['cid']; ?>]]>
      </BLOCKITEM>
      <BLOCKITEM paramtag="@@BodyText1@@">
        <![CDATA[ <?php print truncate_utf8($comment['body'], 75, true, true); ?> ]]>
      </BLOCKITEM>
      </ANPMODULEBLOCK>
      <?php endforeach; ?>
    </ANPPLACEHOLDER>  
              
    <ANPPLACEHOLDER id="4">
      <ANPMODULEBLOCK moduleID="140909" filterName=""> <!-- Module: Full width annonce-->
        <BLOCKITEM paramtag="@@FullWidthAnnonce@@"><![CDATA[http://customers.anpdm.com/horisontgruppen/1207_knd/580.gif]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@"><![CDATA[#]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      <ANPMODULEBLOCK moduleID="140912" filterName="">	<!-- Module: Full width text -->
        <BLOCKITEM paramtag="@@Title1@@"><![CDATA[ ]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@BodyText1@@"><![CDATA[
          Erhvervsmagasinet CSR udkommer seks gange årligt som magasin og to gange som tillæg i Børsen. Med magasinet CSR, csr.dk og e-nyhedsbrevet er vi Danmarks eneste medie for ledere, beslutningstagere og andre fagfolk med ansvar og interesse for Corporate Social Responsibility. Magasinet fokuserer på virksomheders sociale, økonomiske og miljømæssige udfordringer gennem interviews, ekspertindlæg, virksomhedscases og debatstof.]]></BLOCKITEM>
      </ANPMODULEBLOCK>
      
      <ANPMODULEBLOCK moduleID="140908" filterName=""> <!-- Module: Footer -->
        <BLOCKITEM paramtag="@@CompanyName@@"><![CDATA[Horisont Gruppen a/s]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Street@@"><![CDATA[Center boulevard 5]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@City@@"><![CDATA[DK-2300 K&oslash;benhavn S]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@Phone@@"><![CDATA[3247 3230]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Fax@@"><![CDATA[3247 3239]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@CVRnr@@"><![CDATA[8775 1619]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@Email@@"><![CDATA[info@horisontgruppen.dk]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@EmailURL@@"><![CDATA[mailto:info@horisontgruppen.dk]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@Homepage@@"><![CDATA[www.horisontgruppen.dk]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@HomepageURL@@"><![CDATA[http://www.horisontgruppen.dk/]]></BLOCKITEM>
        
        
        <BLOCKITEM paramtag="@@Logo@@"><![CDATA[http://customers.anpdm.com/horisontgruppen/1207_knd/hg_logo.gif]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@ImageLinkURL1@@"><![CDATA[http://www.horisontgruppen.dk/]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@Slogan@@"><![CDATA[erhvervsmedier der bringe vækst og værdi]]></BLOCKITEM>
        
        <BLOCKITEM paramtag="@@Usubscribe@@"><![CDATA[Afmeld nyhedsbrevet]]></BLOCKITEM>
        <BLOCKITEM paramtag="@@UnsubscribeLink1@@"><![CDATA[##OptOutList##]]></BLOCKITEM>        
      </ANPMODULEBLOCK>      
    </ANPPLACEHOLDER>        
  </ANPNEWSLETTER>
</root>
