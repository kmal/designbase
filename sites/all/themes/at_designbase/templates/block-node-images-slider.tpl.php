<?php 
$grid[5] = array(490, 270, 180, 250, 180);
$grid[4] = array(490, 270, 180, 250);
$grid[3] = array(490, 450, 250);
$grid[2] = array(600, 600);
$grid[1] = array(1200);
drupal_add_css(libraries_get_path('jquery-ui-carousel') . '/dist/css/jquery.rs.carousel.css');
drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/dist/js/jquery.rs.carousel.js');
drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/dist/js/jquery.rs.carousel-autoscroll.js');
drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/dist/js/jquery.rs.carousel-continuous.js');
drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/dist/js/jquery.rs.carousel-touch.js');
?>
<div class="block-node-images-slider1 block-mansory-slider hidden">
<div class="slider rs-carousel">
  <ul class="slides ">
    <?php
    $detect = mobile_detect_get_object();
    $is_mobile = $detect->isMobile();
    if ($is_mobile) {
    ?>
      <?php 
      foreach ($images as $k => $image) : ?>
    <li class="slide">
        <div class="image-cont1 slide-content">    
          <span class="image">  
            <?php $params = array(
              'style_name' => 'slider',
              'path' => $image['uri'],
              'alt' => $image['alt'],
              'title' => $image['title'],
              'width' => $image['width'],
              'height' => $image['height'],
              'attributes' => array('class' => array('image')),
              'getsize' => FALSE,
            );
            print theme('image_style', $params); ?>
          </span>
          <div class="image-description">
            <div class="show-title"><i class="fa fa-info"></i></div>  
            <div class="image-title hidden">
              <?php print $image['title']; ?>  
            </div>
          </div>
        </div> 
    </li>
    <?php endforeach; ?> 
    <?php } 
    else { ?>
    
    <li class="slide">
    <?php $i =0;
      $kolvo = (count($images) < 5) ? count($images) : 5;
      foreach ($images as $k => $image) : ?>
      <?php if ($k % 5 == 0 && $k != 0) {?>
        </li><li class="slide">
      <?php $i = 0; 
      $kolvo = (count($images) - $k + 1 < 5)? count($images) - $k : 5;   
      } ?>  
        <?php 
        $width = $grid[$kolvo][$i];
        $height = ($kolvo == 5 && $grid[$kolvo][$i] == 180) ? 184 : 370; 
        ?>    
        <div class="image-cont1 slide-content item-<?php print $i;?>qq" style="width: <?php print $width + 3;?>px; height: <?php print $height + 3;?>px">    
          <div class="image">  
            <?php $params = array(
              'style_name' => (($kolvo == 5 && $width == 180)) ? '180mini' : $width,
              'path' => $image['uri'],
              'alt' => $image['alt'],
              'title' => $image['title'],
              'width' => $image['width'],
              'height' => $image['height'],
              'attributes' => array('class' => array('image')),
              'getsize' => FALSE,
            );
            print theme('image_style', $params); ?>
          </div>
          <?php if (strlen($image['title']) > 0) { ?>
            <div class="image-description">
              <div class="show-title"><i class="fa fa-info"></i></div>  
              <div class="image-title hidden">
                <?php print $image['title']; ?>  
              </div>
            </div>
          <?php } ?>
        </div> 
    <?php $i++; endforeach; ?> 
            </li>
    <?php } ?>        
  </ul>
</div>
</div>  