<?php 
$detect = mobile_detect_get_object();
$is_mobile = $detect->isMobile();
// For tach
if ($is_mobile) {
  drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/vendor/jquery.ui.widget.js');
  drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/vendor/jquery.event.drag.js');
  drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/vendor/jquery.translate3d.js');
  drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/vendor/modernizr.3dtransforms.touch.js');
}
drupal_add_css(libraries_get_path('jquery-ui-carousel') . '/dist/css/jquery.rs.carousel.css');
drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/dist/js/jquery.rs.carousel.js');
drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/dist/js/jquery.rs.carousel-autoscroll.js');
drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/dist/js/jquery.rs.carousel-continuous.js');
drupal_add_js(libraries_get_path('jquery-ui-carousel') . '/dist/js/jquery.rs.carousel-touch.js');

// Add scripts for fancybox
drupal_add_js(libraries_get_path('fancybox') . '/jquery.fancybox.js');
drupal_add_css(libraries_get_path('fancybox') . '/jquery.fancybox.css');
drupal_add_js(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-thumbs.js');
drupal_add_css(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-thumbs.css');
drupal_add_js(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-buttons.js');
drupal_add_css(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-buttons.css');
drupal_add_js(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-media.js');

drupal_add_js(drupal_get_path('module', 'jquery_carousel') . '/js/jquery_carousel.init.js');
?>
<div class="for-fancy">
  <?php 
  $i = 1;
  foreach ($images as $k => $image) : ?>
    <a class="fancybox-thumb <?php print 'item-' . $k; ?>" rev="<?php print $k;?>" rel="fancybox-thumb" href="<?php print file_create_url($image['uri']); ?>" title="<?php print $image['title']; ?>">
    </a>
  <?php endforeach; ?> 
</div>
<div class="block-slider-carousel">
<div class="slider rs-carousel">
  <ul class="slides ">
      <?php 
      $i = 1;
      foreach ($images as $k => $image) : ?>
        <li class="slide " rel="<?php print 'item-' . $k; ?>">
        <div class="image-cont1 slide-content">    
          <span class="image">  
            <?php 
            $params = array(
              'style_name' => ($is_mobile) ? 'slider' : 'slider',
              'path' => $image['uri'],
              'alt' => (isset($image['alt'])) ? $image['alt'] : NULL,
              'title' => $image['title'],
              'width' => (isset($image['width'])) ? $image['width'] : NULL,
              'height' => (isset($image['height'])) ? $image['height'] : NULL,
              'attributes' => array('class' => array('image')),
              'getsize' => FALSE,
            );
            print theme('image_style', $params); ?>
          </span>
          <?php if (strlen($image['title']) > 0) {?>
            <div class="slider-image-description clearfix">
                <div class="num"><?php print $i;?></div>
                <div class="text"><?php print $image['title']; ?></div>
            </div>
          <?php 
            $i++;
          }?>
        </div> 
    </li>
    <?php endforeach; ?> 
</div>
</div>  