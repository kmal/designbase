<?php
    drupal_add_js(libraries_get_path('fancybox') . '/jquery.fancybox.js');
    drupal_add_css(libraries_get_path('fancybox') . '/jquery.fancybox.css');
    drupal_add_js(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-thumbs.js');
    drupal_add_css(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-thumbs.css');
    drupal_add_js(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-buttons.js');
    drupal_add_css(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-buttons.css');
    drupal_add_js(libraries_get_path('fancybox') . '/helpers/jquery.fancybox-media.js');
?>
<div class="image mod short <?php print (isset($node->field_addon_position['und'][0]['value']))?$node->field_addon_position['und'][0]['value']:''; ?> <?php //print get_edit_classes($node); ?>"><!-- Add classes 'short' and 'right'-->
  <div class="img-con">
  <a href='' class='fancybox-gallery-group' rel='addon-gallery'>
    <?php if (isset($node->field_image['und'][0])) : ?>
    <?php $params = array(
      'style_name' => '404px',
      'path' => $node->field_image['und'][0]['uri'],
      'alt' => $node->field_image['und'][0]['alt'],
      'title' => $node->field_image['und'][0]['title'],
      'attributes' => array('class' => array('image')),
      'getsize' => FALSE,
    );
    print theme('image_style', $params);?>
     
    <?php endif; ?>
    <?php if (isset($node->field_caption['und'][0]['value'])): ?>
    <p><?php print $node->field_caption['und'][0]['value']; ?></p>
    <?php endif; ?>
  </a>
  </div>
</div>