<div class="view-partner-profile <?php print $content['class']; ?>">
  <?php if (isset($content['profile_picture'])): ?>
    <div class="views-field-field-image"><?php print $content['profile_picture'];?></div>
  <?php endif; ?>
    
  <?php if (isset($content['logo'])): ?>
    <div class="views-field views-field-field-logo"><div class="field-content"><div class="image-cont"><?php print $content['logo'];?></div></div></div>
  <?php endif; ?>
</div>