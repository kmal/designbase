<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="left-share-block clearfix">
    <?php 
    global $user;
    if ($user->uid == 0) {
      ?><div class="item"><div class="flag-my-designbase">
        <?php print l('<i class="fa fa-heart-o"></i>', 'modal/user_create', array('html' => TRUE)); ?>
        </div></div><?php
    }
    else { ?>
      <div class="item"><?php print render($content['flag_my_designbase']); ?></div>
    <?php }?>
    <?php if (isset($content['field_add_this'])) {?>  
      <div class="item add-this"><?php print render($content['field_add_this']);?></div>
    <?php }?>
      <div class="comment-count">
	<?php if($disqus) {?>
	<i class="fa fa-comment"></i><?php print $disqus;?>
        <?php }?>
  </div>
  </div>
  <?php  $node_wrapper = entity_metadata_wrapper('node', $node); ?>    
  <?php 
    hide($content['field_image']);
    hide($content['field_headline']);
    hide($content['field_category']);
    hide($content['flag_my_designbase']);


    $date_create = date('d. ', $node->created);
    $date_create .= t(date('M', $node->created));
    $date_create .= date(' Y', $node->created);
    
  ?>
  <div class="title-prefix">
    <?php 
    $category = $node_wrapper->field_category->value();
    if ($category) {      
      print $node_wrapper->field_category->name->value(); 
    }
    ?>
  </div>  
  <h1 <?php print $title_attributes; ?>>
    <?php print $node_wrapper->field_headline->value(); ?>
  </h1>
  <?php
  $field_hide_teaser = $node_wrapper->field_hide_teaser->value();
  
  if (!$field_hide_teaser) {
    ?><div class="filed-teaser"><?php print $node_wrapper->field_teaser->value();?></div><?php
  }
  ?>
  
  <div class="title-suffix">
  <!-- BYLINE -->	  
  <?php if (!isset($node->field_show_byline['und'][0]) || $node->field_show_byline['und'][0]['value'] == 1) : ?>
    <span class="author">
      <?php if (isset($node->field_byline['und'])) :?>
        Af 
        <?php if ($node->field_byline['und'][0]['value'] != '') : ?>
          <?php print $node->field_byline['und'][0]['value']; ?>
        <?php else : ?>
          <?php if ($node->field_type['und'][0]['value'] != 2) : ?>
            <?php print $node->author->field_name['und'][0]['value']; ?>
          <?php endif; ?>
        <?php endif; ?>
      <?php else : ?>
        <?php if ($node->field_type['und'][0]['value'] != 2 && isset($node->author->field_name['und'][0]['value'])) : ?>
        Af <?php print $node->author->field_name['und'][0]['value']; ?>
        <?php endif; ?>
      <?php endif; ?>
      <?php if ($node->field_type['und'][0]['value'] != 1) : ?>
        <?php if (isset($node->company) && isset($node->company->name) && ($node->company->name != '')) : ?> 
          <?php print $node->company->name;?>
        <?php endif; ?>
      <?php endif; ?>
      <?php if ($node->field_type['und'][0]['value'] != 2 && isset($node->author->mail)) : ?> | <?php endif; ?>
      <?php print $date_create; ?>
    </span>
    <?php if ($node->field_type['und'][0]['value'] != 2) : ?>
       | <span class="email"><a href="mailto: <?php print $node->author->mail; ?>" title=""><?php print $node->author->mail; ?></a></span>
    <?php endif; ?>
  <?php endif; ?>
  <!-- END BYLINE -->	 
  </div>
  
    <?php 
    $type = $node_wrapper->field_top_carousel_template->value();
    $images = $node_wrapper->field_image->value();
    if ($type == 'top_image' || count($images) == 1) {
      $image = $images[0];
      $params = array(
        'style_name' => 'top_image',
        'path' => $image['uri'],
        'alt' => $image['alt'],
        'title' => $image['title'],
        'width' => $image['width'],
        'height' => $image['height'],
        'attributes' => array('class' => array('image')),
        'getsize' => FALSE,
      );
      $class = ($image['width'] < $image['height']) ? 'vertical' : 'horizont';
      $top_image = '<div class="top-image ' . $class . ' "><div class="top-image-content">';
      $top_image .= theme('image_style', $params);
      if (strlen($image['title']) > 0) {
        $top_image .= '<div class="top-image-description">
            ' . $image['title'] . '
        </div>';
      }
      $top_image .= '</div></div>';
      print $top_image;      
    }
    ?> 

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']); 
      
      print render($content);
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
//    if ($teaser || !empty($content['comments']['comment_form'])) {
//      unset($content['links']['comment']['#links']['comment-add']);
//    }
    // Only display the wrapper div if there are links. ?>
  <?php  
  $links = render($content['links']);
  if ($links): ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
  
  
</div>
