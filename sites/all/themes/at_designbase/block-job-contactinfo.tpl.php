<div class="mod box jobContact job-contacts ">
  <div class="inner special">
    <div class="bd">
      <div class="about views-fieldset">
        <div class="views-field views-field-field-companyname">
        <?php if (isset($node->field_company['und'][0]['tid'])) {
            $supplier = _get_supplier_by_company_tid($node->field_company['und'][0]['tid']);
            if ($supplier && $supplier->field_subscription_type['und'][0]['value'] == 1) {
              $isBusiness = 1;
              $bid = $supplier->nid;
            }
            if (isset($isBusiness) && $isBusiness) {
              print l($supplier->title, "node/" . $bid);
            }
          }
          else {
            print l($node->field_companyname['und'][0]['value'], 'http://' . $node->field_www['und'][0]['value'], array('absolute' => TRUE));
          }
        ?>
        </div>
        <?php if (isset($node->field_company_address['und'][0]['value'])): ?>
          <div class="views-field"><?php print $node->field_company_address['und'][0]['value']; ?></div>
        <?php endif; ?>
        <?php if (isset($node->field_postal_code['und'][0]['value'])): ?>
          <div class="views-field"><?php print $node->field_postal_code['und'][0]['value'] . " "; ?></div>
        <?php endif; ?> 
        <?php if (isset($node->field_city['und'][0]['value'])): ?>
          <div class="views-field views-field-field-city"><?php print $node->field_city['und'][0]['value'];?></div>
        <?php endif; ?>
      </div>
      
      <div class="contacts views-fieldset">
        <?php if (isset($node->field_phone['und'][0]['value'])): ?>
          <div class="views-field">Tlf.: <?php print $node->field_phone['und'][0]['value']; ?></div>
        <?php endif; ?>
        <?php if (isset($node->field_email['und'][0]['value'])): ?>
          <div class="views-field"><a href="mailto:<?php print $node->field_email['und'][0]['value']; ?>"><?php print $node->field_email['und'][0]['value']; ?></a></div>
        <?php endif; ?>
        <?php if (!empty( $node->field_www['und'][0]['value'])) : ?>
          <div class="views-field"><a href="http://<?php print $node->field_www['und'][0]['value']; ?>"><?php print $node->field_www['und'][0]['value']; ?></a></div>
        <?php endif; ?>
      </div>  
      
      <div class="anden views-fieldset">
        <?php if (isset($node->field_jobtype['und'][0]['value'])): ?>
          <div class="views-field views-field-field-jobtype">
            <span class="views-label">Ansættelsesforhold:  </span><div class="field-content"><?php print $node->field_jobtype['und'][0]['value']; ?></div>
          </div>
        <?php endif; ?>
        
        <?php if (isset($node->field_jobtitle['und'][0]['value'])): ?>
          <div class="views-field">
            <span class="views-label">Jobtitel:  </span><div class="field-content"><?php print $node->field_jobtitle['und'][0]['value']; ?></div>
          </div>
        <?php endif; ?>
        
        <?php if (isset($node->field_job_geo['und'][0]['value'])): ?>
          <div class="views-field ">
            <span class="views-label">Geografi:  </span><div class="field-content"><?php print $node->field_job_geo['und'][0]['value']; ?></div>
          </div>
        <?php endif; ?>
        
        <?php if ($node->field_applicationdeadline_soon['und'][0]['value'] == 1): ?>
          <div class="views-field">
            <span class="views-label">Ansøgningsfrist:  </span><div class="field-content">Hurtigst muligt</div>
          </div>
        <?php else: ?>     
          <?php if (isset($node->field_applicationdeadline['und'][0]['value'])): ?>
            <div class="views-field">
              <span class="views-label">Ansøgningsfrist:  </span><div class="field-content"><?php print date('d.m.Y', strtotime($node->field_applicationdeadline['und'][0]['value'])); ?></div>
            </div>
          <?php endif; ?>
        <?php endif; ?>
        
        <?php if ($node->field_startdate_soon['und'][0]['value'] == 1): ?>
          <div class="views-field">
            <span class="views-label">Tiltrædelsesdato:  </span><div class="field-content">Snarest muligt</div>
          </div>
        <?php else: ?>
          <?php if (isset($node->field_startdate['und'][0]['value'])): ?>
            <div class="views-field">
              <span class="views-label">Tiltrædelsesdato:  </span><div class="field-content"><?php print date('d.m.Y', strtotime($node->field_startdate['und'][0]['value'])); ?></div>
            </div>
          <?php endif; ?>
        <?php endif; ?>
        
        <?php if (isset($node->field_deadline_wish['und'][0]['value'])): ?>
          <div class="views-field">
            <span class="views-label">Ønsket publisering:  </span><div class="field-content"><?php print date('d.m.Y', strtotime($node->field_deadline_wish['und'][0]['value'])); ?></div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
