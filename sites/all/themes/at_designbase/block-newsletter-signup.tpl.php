<?php
$errors = form_get_errors();
if (!(isset($errors["name"]) || isset($errors["email"]) || isset($errors["newsletter_terms"]))) {
  $errors = array();
}
?>  

<?php if (!$page) {?>
<div id="block-newsletter-newsletter-signup-apsis" class="<?php if (_newsletter_get_cookie_flag('newsletter_subscribed') && _newsletter_get_cookie_flag('newsletter_receipt') || !empty($errors)) {print 'open';} ?>">
    <div class="open-newsletter <?php if (!empty($errors)) {print 'open'; } ?>"><span>Nyhedsbrev</span></div>
  <?php }?>
  <?php
    if ($page) {
      $class = 'page-newsletter';
    }
    elseif (!empty($errors)) {
      $class = 'open';
    }
    else {
      $class = 'hidden';
    }
  
  ?>  
  <div class="wrap-newsletter <?php print $class; ?>">
  <?php if (_newsletter_get_cookie_flag('newsletter_subscribed') && _newsletter_get_cookie_flag('newsletter_receipt')) : ?>
    <?php setcookie('newsletter_receipt'); ?>
    <div class="title">Tak for din tilmelding</div>
    <div class="bd dark">
      <p class="text"><?php print variable_get("admin_newsletter_receipt"); ?></p>
    </div>
  <?php else: ?>
    <div class="title"><?php print variable_get('admin_newsletter_header', 'TILMELD NYHEDSBREV'); ?></div>
    <div class="bd dark">
      <?php print $content; ?>
    </div>
  <?php endif; ?>

  </div>
<?php if (!$page) {?>    
</div>
<?php } ?>