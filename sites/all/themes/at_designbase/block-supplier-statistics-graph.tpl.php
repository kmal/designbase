<div class="container-graph">
  <div class="google-chart"></div>
  <div class="data-chart">
    <?php foreach ($content as $key => $data): ?>
    <span data-date="Dato" data-value="<?php print $type; ?>" data-key="<?php print $key; ?>" data-sum="<?php print $data['sum']; ?>"></span>
    <?php foreach ($data['dates'] as $date => $value): ?>
    <span data-date="<?php print date('d-m-y', strtotime($date)); ?>" data-value="<?php print $value; ?>" data-key="<?php print $key; ?>"></span>
    <?php endforeach; ?>
    <?php endforeach; ?>
  </div>
  <div class="description">
    <?php if (isset($entities)): ?>
    <select class="select-chart">
      <?php foreach ($entities as $key => $title): ?>
      <option value="<?php print $key; ?>"><?php print $title; ?></option>
      <?php endforeach; ?>
    </select>
    <?php endif; ?>
    <?php if (isset($clickrate)): ?>
    <span class="clickrate-chart">Clickrate for valgte periode (sidevisninger / eksponeringer):<span class="value">-</span></span>
    <?php endif; ?>
    <p><?php print $description; ?></p>
  </div>
</div>