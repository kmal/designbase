<?php
$term = taxonomy_term_load(arg(2));
if (arg(2)) {
  $attributes = array(
    'title' => 'Viser indhold om ' . mb_strtolower($term->name),
    'main' => TRUE
  );
  $tid = arg(2);
  print theme_custom_title($attributes);
}
else {
  $attributes = array(
    'title' => 'Kategorier',
    'main' => TRUE
  );
  print theme_custom_title($attributes);
}
?>

<ul class="category-menu clearfix">
  <?php if (arg(0) == 'category') : ?>
    <li class = "active"><?php print l('Alle', 'category'); ?></li>
  <?php else: ?>
    <li><?php print l('Alle', 'category'); ?></li>
  <?php endif; ?>
  <?php foreach ($tree as $term) : ?>
  <?php if ($term->tid == $tid) : ?>
  <li class = "active"><?php print l($term->name, "taxonomy/term/" . $term->tid); ?></li>
  <?php else: ?>
  <li><?php print l($term->name, "taxonomy/term/" . $term->tid); ?></li>
  <?php endif; ?>
  <?php endforeach; ?>
</ul>
