<?php // Shameful include of site specific stylesheet config.php file - HERE BE UGLY DRAGONS! ?>
<?php require_once(DRUPAL_ROOT . '/stylesheet/config.php'); ?>
<script type="text/javascript" src="//www.google.com/jsapi"></script>
<script type="text/javascript" src="<?php print drupal_get_path('module', 'supplier_statistics') . '/js/supplier_statistics.graphs.js'; ?>"></script>

<?php print theme_custom_title(array('title' => 'Statistik', 'main' => TRUE)); ?>
<div class="mod box statistics">
  <div class="inner special">
    <div class="bd">
      <div class="list">
        <p><?php print nl2br(variable_get('admin_statistics_description_first')); ?></p>
        <button class="input-button-general aButtonNew print">
          <span>Print</span>
        </button>
        <button class="input-button-general aButtonNew hidden">
          <?php print l('Download as pdf', 'printpdf/statistik'); ?>
        </button>
        <?php if (isset($content['activities'])): ?>
        <div class="container-activities">
          <h2 class='activity-header'>Aktiviteter</h2>
          <p class='activity-description'><?php print nl2br(variable_get('admin_statistics_description_second', '')); ?></p>
          <div class="container-group header">
            <span class="type">Type</span>
            <span class="count">Antal</span>
            <span class="pageview">Sidevisninger</span>
            <span class="exposure">Eksponeringer</span>
            <span class="clickrate">Clickrate</span>
          </div>
          <?php if (isset($content['activities']['partner_profile'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['partner_profile'], 'type' => 'Virksomhedsprofil/Brandprofil')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['article'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['article'], 'type' => 'Nyheder')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['job'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['job'], 'type' => 'Jobopslag')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['event_post'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['event_post'], 'type' => 'Events')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['name_article'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['name_article'], 'type' => 'Navnenyt')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['product_news'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['product_news'], 'type' => 'Produktomtale')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['industry_news'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['industry_news'], 'type' => 'Branchenyt')); ?>
          <?php endif; ?>
        </div>
        <?php endif; ?>
        <p class='entities-description'><?php print nl2br(variable_get('admin_statistics_description_third', '')); ?></p>
        <?php if (isset($content['entities'])): ?>
        <div class="container-entities">
          <div class="container-group header">
            <span class="type">Type</span>
            <span class="title">Titel</span>
            <span class="created">Oprettet</span>
            <span class="pageview">Sidevisninger</span>
            <span class="exposure">Eksponeringer</span>
            <span class="clickrate">Clickrate</span>
          </div>
          <?php foreach($content['entities'] as $nid => $entity): ?>
            <?php
            $type = (check_plain($entity['type']) == 'Virksomhedsprofil') ? 'Virksomhedsprofil/Brandprofil' : check_plain($entity['type']);
            $url = (check_plain($entity['type']) == 'Virksomhedsprofil') ? url('node/' . $supplier->nid) : url('node/' . $nid);
            ?>
            <span class="entity"
              data-nid="<?php print $nid; ?>"
              data-url="<?php print $url; ?>"
              data-type="<?php print $type; ?>"
              data-title="<?php print check_plain($entity['title']); ?>"
              data-timestamp="<?php print $entity['timestamp']; ?>"
              data-created="<?php print $entity['created']; ?>"
              data-pageview="<?php print $entity['pageview']; ?>"
              data-exposure="<?php print $entity['exposure']; ?>"
              data-clickrate="<?php print _sscc($entity['pageview'], $entity['exposure']); ?>"></span>
          <?php endforeach; ?>
        </div>
        <?php endif; ?>
        <?php print render($form); ?>
        <div class="container-graphs">
          <span class="chart-options" data-color="#526687"></span>
        <?php if (isset($content['partner_profile'])): ?>
          <h2>Virksomhedsprofil</h2>
          <div class="container-group">
          <?php if (isset($content['partner_profile']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['partner_profile']['pageview'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_supplier_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['partner_profile']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['partner_profile']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_supplier_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (isset($content['article'])): ?>
          <?php print theme_custom_title(array('title' => 'Nyheder')); ?>
          <div class="container-group">
          <?php if (isset($content['article']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['article']['pageview'], 'entities' => $content['article']['entities'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_article_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['article']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['article']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_article_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (isset($content['job'])): ?>
          <h2>Jobopslag</h2>
          <div class="container-group">
          <?php if (isset($content['job']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['job']['pageview'], 'entities' => $content['job']['entities'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_job_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['job']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['job']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_job_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (isset($content['event_post'])): ?>
          <h2>Events</h2>
          <div class="container-group">
          <?php if (isset($content['event_post']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['event_post']['pageview'], 'entities' => $content['event_post']['entities'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_event_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['event_post']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['event_post']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_event_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (isset($content['name_article'])): ?>
          <h2>Navnenyt</h2>
          <div class="container-group">
            <?php if (isset($content['name_article']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['name_article']['pageview'], 'entities' => $content['name_article']['entities'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_name_article_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['name_article']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['name_article']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_name_article_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
          
          
        <?php if (isset($content['product_news'])): ?>
          <h2>Produktomtale</h2>
          <div class="container-group">
            <?php if (isset($content['product_news']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['product_news']['pageview'], 'entities' => $content['product_news']['entities'], 'type' => 'Sidevisninger', 'description' =>  nl2br(variable_get('admin_statistics_product_news_pageview_description', '')))); ?>
          <?php endif; ?>
          <?php if (isset($content['product_news']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['product_news']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_product_news_exposure_description', '')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
          
        <?php if (isset($content['industry_news'])): ?>
          <h2>Branchenyt</h2>
          <div class="container-group">
            <?php if (isset($content['industry_news']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['industry_news']['pageview'], 'entities' => $content['industry_news']['entities'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_industry_news_pageview_description', '')))); ?>
          <?php endif; ?>
          <?php if (isset($content['industry_news']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['industry_news']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_industry_news_exposure_description', '')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
