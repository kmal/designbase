<table class="listing">
  <!-- Table header -->
    <thead>
      <tr>
        <th scope="col">Hvad</th>
        <th scope="col">Hvor</th>
        <th scope="col">Startdato</th>
        <th scope="col">Rediger</th>
        <th scope="col">Slet</th>
      </tr>
    </thead>
  <!-- Table footer -->
  <!-- Table body -->
    <tbody>
      <?php for ($i = 0; $i < count($content); $i++): ?>
      <tr class="<?php print (($i%2==1)?'odd':'even'); ?>">
        <td class="first"><span><?php print l($content[$i]['field_headline']['und'][0]['value'],'node/' . $content[$i]['nid']); ?></span></td>
        <td><span><?php print $content[$i]['field_location']['und'][0]['value']; ?></span></td>
        <td><span><?php print date('d.m.y k\l. H:i',strtotime($content[$i]['field_date']['und'][0]['value']));?></span></td>
        <td><span><?php print l('<i class="fa fa-pencil"></i>', 'redigerevent/' . $content[$i]['nid'], array('html' => TRUE)); ?></span></td>
        <td><span><a href="eventoversigt?delete=<?php print $content[$i]['nid'];?>" onclick="if(!confirm('Slet?')) return false;"><i class="fa fa-times"></i></a></span>
        </td>
      </tr>
      <?php endfor; ?>
    </tbody>
</table>
<?php print theme('pager'); ?>
<?php print l('<span>Opret event</span>', 'opretevent', array('attributes' => array('class' => array('aButtonNew')),'html' => TRUE)); ?>

