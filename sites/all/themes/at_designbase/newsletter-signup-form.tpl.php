
<?php $errors = form_get_errors(); 

if (isset($variables['newsletter-signup-form'])) {
$form = $variables['newsletter-signup-form'];
}
else {
$form = $variables[""];
}

?>

<input type="hidden" id="<?php print $form['form_build_id']['#id'] ?>" name="<?php print $form['form_build_id']['#name']?>" value="<?= $form['form_build_id']['#value'] ?>"  />
<input type="hidden" id="<?php print $form['form_id']['#id'] ?>" name="<?php print $form['form_id']['#name']?>" value="<?= $form['form_id']['#value'] ?>"  />
<?php if (isset($form['form_token'])): ?>
<input type="hidden" id="<?php print $form['form_token']['#id'] ?>" value="<?php print $form['form_token']['#default_value'] ?>" name="<?= $form['form_token']['#name']?>" />
<?php endif; ?>

<div class="form-item">
  <input class="clear-input" type="text" name="newsletter_name" placeholder="Dit navn" value="<?php if (isset($_POST['newsletter_name'])) { print $_POST['newsletter_name']; } else { ?><?php } ?>" />
  <input type="hidden" name="" value="Dit navn" />
  <?php if(isset($errors["name"])): ?>
  <p class="error">
    <?php print $errors["name"]; ?>
  </p>
  <?php endif; ?>
</div>
<div class="form-item">
  <input class="clear-input" type="text" name="newsletter_email" placeholder="Din Email" value="<?php if (isset($_POST['newsletter_email'])) { print $_POST['newsletter_email']; } else { ?><?php } ?>" />
  <input type="hidden" name="" value="" />
  <?php if(isset($errors["email"])): ?>
  <p class="error">
    <?php print $errors["email"]; ?>
  </p>
  <?php endif; ?>
</div>
 <div class="form-item clearfix">
    <input type="checkbox" name="newsletter_terms" checked="checked"   value="693447"  id="subscribe">
    <label for="subscribe">
      <?php 
      $def_value = 'Ved tilmelding af nyhedsbrev  accepterer jeg også at modtage  tilbud og markedsføringsmateriale  fra designbase.dk og designbase-partnere via elektronisk post.';
      print variable_get('admin_newsletter_confirm', $def_value);?>
    </label>
  </div>
<div class="form-item submit-item">
  <div class="input-button right">
    <input type="submit" name="submit" value="Tilmeld" />
  </div>
</div>


