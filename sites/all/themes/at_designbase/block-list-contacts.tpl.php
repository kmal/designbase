<table class="listing">
  <!-- Table header -->
    <thead>
      <tr>
        <th scope="col">Navn</th>
        <th scope="col">Dato</th>
        <th scope="col">Rediger</th>
        <th scope="col">Slet</th>
      </tr>
    </thead>
  <!-- Table footer -->
  <!-- Table body -->
    <tbody>
      <?php for ($i = 0; $i < count($content); $i++): ?>
      <tr class="<?php print (($i%2==1)?'odd':'even'); ?>">
        <td class="first"><span><?php print $content[$i]['field_name']['und'][0]['value']; ?></span></td>
        <td><span><?php print date('d.m.y k\l. H:i',$content[$i]['created']);?></span></td>
        <td><span><?php print l('<i class="fa fa-pencil"></i>', 'redigerkontakt/' . $content[$i]['nid'], array('html' => true)); ?></span></td>
        <td><span><a href="kontaktoversigt?delete=<?php print $content[$i]['nid'];?>" onclick="if(!confirm('Slet?')) return false;"><i class="fa fa-times"></i></a></span>
        </td>
      </tr>
      <?php endfor; ?>
    </tbody>
</table>
<?php print theme('pager'); ?>
<?php print l('<span>Opret kontaktperson</span>', 'opretkontakt', array('attributes' => array('class' => array('aButtonNew')),'html' => TRUE)); ?>

