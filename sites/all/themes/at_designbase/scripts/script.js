/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
  
  Drupal.behaviors.themeDesignbase = {
    attach: function () {
      addToHomescreen({
         lifespan: 15, 
         startDelay: 1,
         displayPace: 1440,
       });
      setTimeout(socialShare, 1200);
      fancyboxGallery();
      showroomFiltered();
      $('.preloader-page').hide();
      window.onbeforeunload = function(evt) {
        if ($('#page').find('.form-partmer-cms').length != 0) {
          curElement = document.activeElement;
          if (!$(curElement).hasClass('form-submit')) {
//            return "Har du husket at gemme dit indhold ";
          }
        }
      };

      $('.dsq-widget-item').each(function(){
        $(this).find('.dsq-widget-meta a:first').wrap('<div class="views-field-title"></div>')
        var widget_meta = $(this).find('.views-field-title');
        $(this).find('.dsq-widget-comment').before(widget_meta);
      });

      if ($('#page').find('.view-display-id-job_page').length > 0 || $('#page').find('.event-page-view').length > 0) {
        $('.views-row').each(function(){
          if ($(this).find('img').height() < $(this).find('.content-right').height()) {
            $(this).find('.views-field-field-logo').children().height($(this).find('.content-right').height());
          }
          else {
            $(this).find('.views-field-field-logo').children().height($(this).find('a').height());
          }
        });
      }

      $('.mobile-title, .field-group-div h2, .pane-views.pane-partner-profile h2').click(function(){
        $(this).parent().toggleClass('hover');
      });

      $('.pager-load-more li a').click(function(){
        $('body').removeClass('disqus-processed');
      });

      $('.form-item input[type="checkbox"], .form-item-job-geography select, #edit-category, .form-create-event select, .select-chart').styler({});

      $('.flag-my-designbase').click(function(){
        setTimeout(countMyDesignbase, 500);
      });

      $('.tabContainer .right-link, .tabContainer .create-links-cont').delay(500).animate({
        opacity: '0.3'
      }).delay(300).animate({
        opacity: '1'
      }).delay(300).animate({
        opacity: '0.3'
      }).delay(300).animate({
        opacity: '1'
      });

      $('.menu-block-3 li.expanded').hover(
        function(){
          $(this).find('.menu').show();
          $(this).addClass('hover');
        },
        function(){
//          $(this).find('.menu').hide();
//          $(this).removeClass('hover');
        }
      );

      $('.field-name-field-add-this').click(function(){
        $(this).toggleClass('open');
      });

      $('#edit-job-applicationdeadline-soon').change(function(){
        if ($(this).attr('checked') === 'checked') {
          $('#edit-job-applicationdeadline').val($(this).parents('.form-item').find('.description').text());
          $('#edit-job-applicationdeadline').attr('disabled', 'disabled');
        }
        else {
          $('#edit-job-applicationdeadline').val('');
          $('#edit-job-applicationdeadline').removeAttr('disabled');
        }
      });
      $('#edit-job-startdate-soon').change(function(){
        if ($(this).attr('checked') === 'checked') {
          $('#edit-job-startdate').val($(this).parents('.form-item').find('.description').text());
          $('#edit-job-startdate').attr('disabled', 'disabled');
        }
        else {
          $('#edit-job-startdate').val('');
          $('#edit-job-startdate').removeAttr('disabled');
        }
      });
      at_designbase_newslatter();


      $('.image.mod.left, .image.mod.right').each(function() {
        var widthImage = $(this).find('img').width();
        $(this).width(widthImage);
      });

      if($('#edit-teaser').length > 0){
        var actualCount = 150 - $.trim($("#edit-summary-print").val()).length;
        $('#edit-teaser').parent().parent().find('.description').append('<span id="counter">Du har <span>'+actualCount+'</span> tegn tilbage</span>');
        $('#edit-teaser').bind('keyup', function() {
          var maxchar = 150;
          var cnt =  $.trim($(this).val()).length;
          var remainingchar = maxchar - cnt;
          if(remainingchar < 0){
            $('#counter span').html('0');
            $(this).val($.trim($(this).val().slice(0, 150)));
          }else{
            $('#counter span').html(remainingchar);
          }
        });
      }
      $('.form-create-contact input[type="file"]').styler({
        filePlaceholder: '<i class="fa fa-picture-o"></i><div>Upload profilbilled</div>'
      });

      $('.files-fielset input[type="file"]').styler({
        filePlaceholder: '<i class="fa fa-folder-o"></i><div>Upload en fil</div>'
      });
      $('#edit-images input[type="file"]').styler({
        filePlaceholder: '<i class="fa fa-picture-o"></i><div>Upload et billed</div>'
      });
      $('.form-type-file input[type="file"]').styler({
        filePlaceholder: ''
      });

      if ($('#preview-block-open').length > 0) {
        $('.opacity-preview').height($('body').height());
        $('#preview-block-open .fa.fa-times').click(function(){
          $('.preview-block-cont').remove();
          $('.opacity-preview').remove();
        });
      }

      if ($('body').hasClass('node-type-product-news')) {
        if ($(window).width() < 671) {
          $('.node .content .field-name-field-content').append($('.mod.facts'));
        }
        $(window).resize(function(){
          if ($(window).width() < 671) {
            $('.node .content .field-name-field-content').append($('.mod.facts'));
          }
        });
      };

      $('.tabContainer .delete-image').click(function(){
        $(this).addClass('hidden');
        $(this).parent().addClass('hidden');
        $(this).parents('form').find('.' + $(this).attr('id')).val(1);
      });

      if ($('body').hasClass('node-type-event-post')) {
        $(window).resize(function(){
          if ($(window).width() > 581) {
            $('.node .content').before($('.eventWhenWhere'));
            $('.node .content').find('.field-name-field-logo').removeClass('mobile');
          }
          else {
            $('.node .field-name-field-logo').after($('.eventWhenWhere'));
            $('.node .content').find('.field-name-field-logo').addClass('mobile');
          }
        });
      }

      $('.view-latest-news').append('<div class="preloder"></div>');
      $('.view-latest-news').addClass('preloder');
      setTimeout(gridLatestNews, 1500);

      setTimeout(gridFactCompany, 500);

      socialCompanyEdit();

      customizeWebFormModal();

      at_designbase_topNavBar();

      gridGallery();
      $(window).resize(function(){
        gridGallery();
      });


      /* more content from comapny */
      $('.more-content-from-company .views-row').hover(
        function(){
          $(this).find('.views-field-field-headline').stop().animate({
            opacity: 1
          });
          $(this).find('.views-field-field-image').stop().animate({
            opacity: 0.5
          });
        }, function(){
          $('.more-content-from-company .views-field-field-headline').stop().animate({
            opacity: 0
          });
          $('.more-content-from-company .views-field-field-image').stop().animate({
            opacity: 1
          });
        }
      );
      /* end more content from comapny */
      $('.views-exposed-form .type-filter .options').once(function(){
        $(this).attr('rel', $(this).outerHeight());
        $(this).css({
          height: 0,
          'overflow' : 'hidden'
        });
      });

      $('.views-exposed-form .type-filter').hover(
        function(){
          $(this).find('.options').stop().animate({
            height: $(this).find('.options').attr('rel')
          });
        },
        function(){
          $(this).find('.options').stop().animate({
            height: 0
          });
        }
      );

      /* slider for articles */

//      if ($('#page').find('.block-mansory-slider').length > 0) {
////        $('.block-mansory-slider').addClass('hidden');
//        var $slider = $('.block-node-images-slider1 .slide').packery({
//          itemSelector: ".image-cont1"
//        });
//        if ($(window).width > 581) {
//          $slider.packery();
//        }
//        $('.rs-carousel').carousel({
//          pagination: false
//        });
//        $('.block-mansory-slider').removeClass('hidden');
//      }

      /* end slider */
      $('.block-mansory-slider .image-description, .top-image .image-description').hover(
        function(){
//          if ($(this).parent().hasClass('rs-carousel-item-active')) {
            $(this).find('.image-title').fadeIn();
//          }
        },
        function(){
          $(this).find('.image-title').fadeOut();
        }
      );

      $('.image-description').hover(
        function(){
          if ($(this).parent().hasClass('rs-carousel-item-active')) {
            $(this).find('.image-title').fadeIn();
          }
        },
        function(){
          $(this).find('.image-title').fadeOut();
        }
      );

      function at_designbase_topNavBar() {
        $('#top_nav_bar #block-db-common-login-button #login-form-button').click(function(){
          if ($(this).parents('#block-db-common-login-button').hasClass('hover')) {
            $(this).parents('#block-db-common-login-button').removeClass('hover');
            $('#top_nav_bar #login-form-button-content').css({'overflow' : 'hidden'});
            $('#top_nav_bar #login-form-button-content').stop().animate({
              height: 0
            });
          }
          else {
            var height = $('#login-form-button-content .content').outerHeight();
            height = 260;
            $('#top_nav_bar #login-form-button-content').stop().animate({
              height: height
            });
            $(this).parents('#block-db-common-login-button').addClass('hover');
          }
        });
        $('#top_nav_bar #block-db-common-custom-search-form .form-submit').click(function(){
          if ($(this).parents('#block-db-common-custom-search-form').hasClass('hover')) {
            $(this).parents('#block-db-common-custom-search-form').removeClass('hover');
            $('#top_nav_bar #block-db-common-custom-search-form .search-item').slideUp();
          }
          else {
            $(this).parents('#block-db-common-custom-search-form').addClass('hover');
            $(this).parents('#block-db-common-custom-search-form').find('.search-item').slideDown();
          }
        });
      }

      $('.field_indicate_sponsor').hover(
        function(){
          if ($(window).width() > 670) {
            $(this).find('.field_indicate_sponsor_content').fadeIn();
          }
        }, function(){
          if ($(window).width() > 670) {
            $('.field_indicate_sponsor_content').fadeOut();
          }
        }
      );
      $('.field_indicate_sponsor').click(function(){
        if ($(window).width() < 671) {
          $(this).find('.field_indicate_sponsor_content').toggleClass('hidden');
        }
      });

      if ($('body').find('.rs-carousel').length > 0) {
        setTimeout(regular_slider_show, '1000');
      }
      $(window).resize(function(){
        if ($('body').find('.rs-carousel').length > 0) {
          regular_slider_show();
        }
      });

      function regular_slider_show() {
        var winWidth = $(window).width();
        var mLeft = (winWidth - $('.block-slider-carousel').parent().width()) / 2;
        var $img = $('.block-slider-carousel img');
        $('.block-slider-carousel').width(winWidth);
        $('.block-slider-carousel').css({
          'margin-left' : -mLeft
        });
        $('.block-slider-carousel .slide-content').css({
            'width': 'auto',
            'min-width': 250
        });
        $('.block-slider-carousel .slide-content').each(function(){
          if ($(this).find('img').width() > winWidth) {
             $(this).width(winWidth);
          }
          else {
            $(this).width($(this).find('img').width());
          }
          $(this).find('.num').height($(this).find('.text').height());
        });
        $('.block-slider-carousel .slide-content').each(function(){
          $(this).find('.num').height($(this).find('.text').height());
        });
        $('.rs-carousel').carousel({
          pagination: false,
          touch: true,
          items: '.slide',
          translate3d: false,
          loop: true,
          continuous: true
        });
//        $('.rs-carousel-action-prev').html('<i class="fa fa-angle-left"></i>');
//        $('.rs-carousel-action-next').html('<i class="fa fa-angle-right"></i>');

//        $(window).resize(function(){
//          if ($(window).width() < 681) {
//            $('.block-slider-carousel .rs-carousel .rs-carousel-item').width($('.block-slider-carousel .rs-carousel').width());
//            $('.block-slider-carousel .rs-carousel .rs-carousel-item').height($('.block-slider-carousel .rs-carousel li img').height());
//          }
//          else {
//            $('.block-slider-carousel .rs-carousel .rs-carousel-item').width($('.block-slider-carousel .rs-carousel').width(1200));
//            $('.block-slider-carousel .rs-carousel .rs-carousel-item').height($('.block-slider-carousel .rs-carousel li img').height());
//          }
//          $(':rs-carousel').carousel('refresh');
//        });
      }

      $('.scroll_to_top').click(function () {
        $('body,html').animate({
          scrollTop: 0
        }, 800);
        return false;
      });


      $('#left-menu-link').click(function(){
        $('#wrapper-white-opacity').fadeIn('fast');
        if ($(window).width() < 350) {
          var width = '100%';
        }
        else {
          var width = '350';
        }
        $("#main-navigation").animate({width:width},350);

        if ($(window).width() < 671) {
          $('body,html').animate({
            scrollTop: 0
          }, 400);
        }
      });
      $('.ctools-use-modal').click(function(){
        if ($(window).width() < 671) {
          $('body,html').animate({
            scrollTop: 0
          }, 400);
        }
      });
      $('#wrapper-white-opacity, a.ctools-use-modal, #main-navigation .close').click(function(){
        $('#wrapper-white-opacity').fadeOut('fast');
        $("#main-navigation").animate({width:'0'},350);
      });
      $('#user-register-form .inline-box').click(function() {
        $('#terms').removeClass('hidden');
      });
      $('#terms .close-terms').click(function(){
        $('#terms').addClass('hidden');
      });

      function showroomFiltered() {
        if ($('#page').find('.form-item-combine')) {
          $('.showroom-filtered .views-exposed-form .form-item-combine').append($('.view-showroom .views-submit-button'));
          $('.views-exposed-form .form-item-combine').once(function(){
            $(this).append('<div class="search-button"><i class="fa fa-search"></i></div>');
          });
          $('.showroom-filtered .form-radio').change(function() {
            $('.showroom-filtered .form-submit').click();
          });
          $('.showroom-filtered .search-button').click(function() {
            $('.showroom-filtered .form-submit').click();
          });
        }
      }

      function socialShare() {
        $('.field-name-field-add-this').once(function(){
          $('.at_PinItButton').html('<i class="fa fa-pinterest"></i>');
          $('.aticon-linkedin').html('<i class="fa fa-linkedin"></i>');
          $('.aticon-twitter').html('<i class="fa fa-twitter"></i>');
          $('.aticon-facebook').html('<i class="fa fa-facebook"></i>');
          $('.aticon-email, .addthis_button_mail').html('<i class="fa fa-envelope-o"></i>');
          $(this).prepend('<div class="share-open"><i class="fa fa-share-alt"></i></div></div>');
  //        <i class="fa fa-share-alt"></i>

          $('.at_PinItButton, .at4-icon').css({
            'background' : '#ccc',
            'text-indent' : '0'
          });

          $('.node-full .at_PinItButton, .node-full .at4-icon').css({
            'overflow': 'visible'
          });
        });
      }

      function customizeWebFormModal() {
        var modalContent = $('#modalContent');
        var top = 86;
        $('#top_nav_bar').css({
          'z-index' : '100005'
        });
        modalContent.css({
          'top': top + 'px'
        });
        $('#modalContent .modal-header .close').html('<i class="fa fa-times"></i>');
      }

      function countMyDesignbase(){
        $.ajax({
          url: "ajax/flag_count",
          success: function(result){
            $('#block-db-common-right-top-navigation .my-designbase sup').text(result);
          }
        });
      }

      function gridLatestNews(){
        if ($('body').find('.view-latest-news').length > 0 ) {
          if ($('.view-latest-news .views-row-first').find('.node-article').length > 0) {
            $('.view-latest-news .views-row-first').addClass('view-row-article');
          }

          var $container = $('.view-latest-news .view-content').packery({
            itemSelector: ".views-row"
          });
          $container.packery();
          $container.packery('reloadItems');

          gridLatestNewsFlagPosition();
          $('.view-latest-news .views-row .comments-count').each(function(){
            if ($(this).text() === '0' || $(this).text() === 'Kommentarer') {
              if ($(this).parent().find('.field-name-field-category').length > 0) {
                $(this).remove();
              }
              else {
                $(this).parent().remove();
              }
            }
          });

          $container.packery();
          $container.packery('reloadItems');
          $('.view-latest-news .preloder').remove();
          $('.view-latest-news').removeClass('preloder');
//          console.log($('.view-latest-news .views-row-last').attr('class'));
//          $container.packery();
          $container.packery('reloadItems');

        }
      }
      $(window).resize(function(){
        gridLatestNewsFlagPosition();
      });
      function gridLatestNewsFlagPosition() {
        if ($('body').find('.view-latest-news').length > 0 ) {
          if ($(window).width() < 671) {
            $('.view-latest-news .node').each(function(){
              $(this).append($(this).find('.flag-outer-my-designbase'));
              $(this).append($(this).find('.field-name-field-add-this'));
            });
          }
          else {
            $('.view-latest-news .node').each(function(){
              if ($(this).hasClass('node-article')) {
                $(this).find('.group-image-cont').append($(this).find('.flag-outer-my-designbase'));
               $(this).find('.group-image-cont').append($(this).find('.field-name-field-add-this'));
              }
              else {
                $(this).find('.group-buttons').append($(this).find('.flag-outer-my-designbase'));
                $(this).find('.add-this-cont').prepend($(this).find('.field-name-field-add-this'));
              }
            });
          }
        }
      }
      function gridFactCompany() {
        if ($('body').find('.view-display-id-content_from_company').length > 0 ) {
          var $container = $('.view-display-id-content_from_company .view-content').packery({
            itemSelector: ".views-row"
          });
          $container.packery('destroy');
          $('.view-display-id-content_from_company .views-row').each(function(){
            $(this).css({
              height: $(this).height()
            });
          });
          $container.packery('destroy');
          $container.packery();
          $container.packery('reloadItems');
        }
      }
      function socialCompanyEdit() {
        if ($('body').find('#edit-social').length > 0) {
          $('#edit-social .form-text.active').parents('.form-item').addClass('active');
          $('#edit-social .social-head li').click(function(){
            $('#edit-social .social-head li').removeClass('active');
            $(this).addClass('active');
            $('#edit-social .form-item').removeClass('active');
            $('#edit-social .' + $(this).attr('id')).parents('.form-item').addClass('active');
          });
        }
      }
      function nodeGalleryGrid() {
        var $container1 = $('.gallery-up .content').packery({
          itemSelector: ".item"
        });
//        $container1.packery('destroy');
        $container1.packery();
        $container1.packery('reloadItems');
      }
      function gridGallery(){
        if ($('#page').find('.view-gallery').length > 0) {
          var $container = $('.view-gallery .view-content').packery({
            itemSelector: ".views-row",
            isResizeBound: true
          });
          $container.packery();
          $container.packery('reloadItems');

          var imgs = $('.view-gallery img');
          $('.view-gallery').find('.title').fadeOut();
          $('.view-gallery .view-content a').hover(
            function(){
              $(this).addClass('opacity');
              $(this).find('.title').fadeIn();
            },
            function(){
              $(this).removeClass('opacity');
              $(this).find('.title').fadeOut();
            }
          );
          $('.view-gallery .view-content .views-row').each(function(){
            $(this).css({
              height: $(this).find('img').height(),
//              width: $(this).width()
            });
          });
//          $container.packery('destroy');
          $container.packery();
          $container.packery('reloadItems');
        }
      }
      
      function fancyboxGallery() {
        if ($('#page').find('.rs-carousel, .img-con, .top-image-content').length > 0 && $(window).width() > 670) {
            
          $('a.fancybox-gallery-group').each(function(){
            var srcAddon = $(this).children().attr("src");
            $(this).attr("href", srcAddon);
          });

          $('li.slide').click(function(){
            $('a.' + $(this).attr('rel')).click();
          });
          
          
          $(".fancybox-thumb, a.fancybox-gallery-group").fancybox({
            prevEffect  : 'fade',
            nextEffect  : 'fade',
            openEffect : 'fade',
            padding: '0',
            onUpdate: fancyUpdate,
            tpl: {
              closeBtn : '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"><i class="fa fa-times"></i></a>',
              next     : '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
              prev     : '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
            }
          });
        }
      }
      
      function fancyUpdate() {
        var all = this.group.length;
        var current = this.index + 1;
        
        $('.fancybox-overlay').once(function(){
          $(this).append('<div class="count-images">123</div>');
          $('.fancybox-overlay').append($('.fancybox-prev'));
          $('.fancybox-overlay').append($('.fancybox-next'));
          $('.fancybox-overlay').append($('.fancybox-close'));
          var logo = $('#logo');

          $('.fancybox-overlay').append(logo.clone());
        });
        $('.fancybox-wrap .fancybox-prev').remove();
        $('.fancybox-wrap .fancybox-next').remove();
        $('.fancybox-wrap .fancybox-close').remove();
        $('.fancybox-title').width($('.fancybox-inner').width());
        $('.count-images').text(current + '/' + all);
        if (all == 1) {
            $('.count-images').hide();
        }
      }


      function at_designbase_newslatter(){
        $('.open-newsletter').click(function(){
          $('.wrap-newsletter').toggleClass('hidden');
          $(this).toggleClass('open');
          $(this).parent().toggleClass('open');
        });

        $('.open-newsletter').mouseover(function(){
          $('#block-newsletter-newsletter-signup-apsis').stop().animate({
            width: 270,
            queue: false
          });
        });
        $('.open-newsletter').mouseleave(function(){
          if (!$(this).hasClass('open')) {
            $('#block-newsletter-newsletter-signup-apsis').stop().animate({
              width: 175,
              queue: false
            });
          }
        });
      }


    /******************************************************
   * DATEPICKER
  ******************************************************/

    $( "#edit-date" ).datepicker({
      dateFormat: 'yy-mm-dd',
      constrainInput: false,
      minDate: 0
    });
    $('#unpublish_date, #publish_date, #date_event, #edit-job-applicationdeadline, #edit-job-startdate').datepicker({
      dateFormat: 'yy-mm-dd',
      constrainInput: true,
      minDate: 0
    });

    $( "#publish-date" ).datepicker({
      dateFormat: 'yy-mm-dd',
      minDate: 0
    });

    $( "#unpublish-date" ).datepicker({
      dateFormat: 'yy-mm-dd',
      minDate: 0
    });


/******************************************************
 * CAROUSEL
 ******************************************************/
    var  carousel = {

      init: function(sel, slideDis, noElem, styling) {
        $cs = this;
        $cs.selected = $(sel);
        $cs.numOfElem = $cs.selected.find('ul.ss>li').length;
        $cs.slideDistance = slideDis;
        $cs.currentPage = 1;
        $cs.numOfPages = 1;
        $cs.elemPerPage = noElem;

        if ($cs.numOfElem > noElem) {
          $('.box-slideshow-nav-right').css('display', 'block');

          $cs.numOfPages = Math.ceil($cs.numOfElem/$cs.elemPerPage);

          //If any custom styling needed
          if(styling) {$cs.customStyling();}

          $cs.navigationVisibility();
          $cs.leftNavigation();
          $cs.rightNavigation();
        }
      },

      //Custom styles/function
      customStyling: function() {

      },

      //Left navigation
      leftNavigation: function() {
        $('.box-slideshow-nav-left').click(function() {
          $cs.selected.find('ul.ss').animate({  marginLeft: -(($cs.currentPage-2)*$cs.slideDistance) }, 750);
          $cs.currentPage--;
          $cs.navigationVisibility();
        });
      },

      //Right navigation
      rightNavigation: function() {
        $('.box-slideshow-nav-right').click(function() {
          $cs.selected.find('ul.ss').animate({  marginLeft: -($cs.currentPage*$cs.slideDistance) }, 750);
          $cs.currentPage++;
          $cs.navigationVisibility();
        });
      },

      // Show or hide navigation
      navigationVisibility: function() {

        if ($cs.currentPage == 1) {
          $('.box-slideshow-nav-left').css('display', 'none');
          $('.box-slideshow-nav-right').css('display', 'block');
        } else if ($cs.currentPage == $cs.numOfPages) {
          $('.box-slideshow-nav-right').css('display', 'none');
          $('.box-slideshow-nav-left').css('display', 'block');
        } else {
          $('.box-slideshow-nav-left').css('display', 'block');
          $('.box-slideshow-nav-right').css('display', 'block');
        }
      }
    };
    //carousel.init(parent element, slide distance, number of elements visible at a time, custom styling)
    if ($('.article-carousel').length) { carousel.init($('.article-carousel'), 1022, 4, true); }


    }
  };

    Drupal.ajax.prototype.commands.viewsLoadMoreAppend = function (ajax, response, status) {
      Drupal.settings.disqusComments = 'designbase';

    // Get information from the response. If it is not there, default to
    // our presets.
    var wrapper = response.selector ? $(response.selector) : $(ajax.wrapper);
    var method = response.method || ajax.method;
    var targetList = response.targetList || '';
    var effect = ajax.getEffect(response);
    var pager_selector = response.options.pager_selector ? response.options.pager_selector : '.pager-load-more';

    // We don't know what response.data contains: it might be a string of text
    // without HTML, so don't rely on jQuery correctly iterpreting
    // $(response.data) as new HTML rather than a CSS selector. Also, if
    // response.data contains top-level text nodes, they get lost with either
    // $(response.data) or $('<div></div>').replaceWith(response.data).
    var new_content_wrapped = $('<div></div>').html(response.data);
    var new_content = new_content_wrapped.contents();

    // For legacy reasons, the effects processing code assumes that new_content
    // consists of a single top-level element. Also, it has not been
    // sufficiently tested whether attachBehaviors() can be successfully called
    // with a context object that includes top-level text nodes. However, to
    // give developers full control of the HTML appearing in the page, and to
    // enable Ajax content to be inserted in places where DIV elements are not
    // allowed (e.g., within TABLE, TR, and SPAN parents), we check if the new
    // content satisfies the requirement of a single top-level element, and
    // only use the container DIV created above when it doesn't. For more
    // information, please see http://drupal.org/node/736066.
    if (new_content.length != 1 || new_content.get(0).nodeType != 1) {
      new_content = new_content_wrapped;
    }
    // If removing content from the wrapper, detach behaviors first.
    var settings = response.settings || ajax.settings || Drupal.settings;
    Drupal.detachBehaviors(wrapper, settings);
    if ($.waypoints != undefined) {
      $.waypoints('refresh');
    }

    // Set up our default query options. This is for advance users that might
    // change there views layout classes. This allows them to write there own
    // jquery selector to replace the content with.
    // Provide sensible defaults for unordered list, ordered list and table
    // view styles.
    var content_query = targetList && !response.options.content ? '> .view-content ' + targetList : response.options.content || '> .view-content';

    // If we're using any effects. Hide the new content before adding it to the DOM.
    if (effect.showEffect != 'show') {
      new_content.find(content_query).children().hide();
    }

    // Update the pager
    // Find both for the wrapper as the newly loaded content the direct child
    // .item-list in case of nested pagers
    wrapper.find(pager_selector).replaceWith(new_content.find(pager_selector));

    // Add the new content to the page.
    wrapper.find(content_query)[method](new_content.find(content_query).children());

    // Re-class the loaded content.
    // @todo this is faulty in many ways.  first of which is that user may have configured view to not have these classes at all.
    wrapper.find(content_query).children()
      .removeClass('views-row-first views-row-last views-row-odd views-row-even')
      .filter(':first')
        .addClass('views-row-first')
        .end()
      .filter(':last')
        .addClass('views-row-last')
        .end()
      .filter(':even')
        .addClass('views-row-odd')
        .end()
      .filter(':odd')
        .addClass('views-row-even')
        .end();

    if (effect.showEffect != 'show') {
      wrapper.find(content_query).children(':not(:visible)')[effect.showEffect](effect.showSpeed);
    }

    // Additional processing over new content
    wrapper.trigger('views_load_more.new_content', new_content.clone());

    // Attach all JavaScript behaviors to the new content
    // Remove the Jquery once Class, TODO: There needs to be a better
    // way of doing this, look at .removeOnce() :-/
    var classes = wrapper.attr('class');
    var onceClass = classes.match(/jquery-once-[0-9]*-[a-z]*/);
    wrapper.removeClass(onceClass[0]);
    settings = response.settings || ajax.settings || Drupal.settings;
    Drupal.attachBehaviors(wrapper, settings);
    Drupal.settings.disqusComments = 'designbase';
//    console.log(Drupal.settings.disqusComments);
  };

})(jQuery);
