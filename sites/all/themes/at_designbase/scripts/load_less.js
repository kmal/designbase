(function ($) {

  Drupal.ajax.prototype.commands.LoadLess = function(ajax, response, status) {
    
    var wrapper = response.selector ? $(response.selector) : $(ajax.wrapper);
    wrapper.find('.views-row.page-' + response.npage).remove();
    wrapper.find('.view-content').children()
      .removeClass('views-row-first views-row-last views-row-odd views-row-even')
      .filter(':first')
        .addClass('views-row-first')
        .end()
      .filter(':last')
        .addClass('views-row-last')
        .end()
      .filter(':even')
        .addClass('views-row-odd')
        .end()
      .filter(':odd')
        .addClass('views-row-even')
        .end();
    wrapper.find('.item-list-pager').replaceWith(response.pager);
    var offsetLast = wrapper.find('.views-row-last').offset();
    $('body,html').animate({
      scrollTop: offsetLast.top
    }, 800);
      
    var classes = wrapper.attr('class');
    var onceClass = classes.match(/jquery-once-[0-9]*-[a-z]*/);
    wrapper.removeClass(onceClass[0]);
    settings = response.settings || ajax.settings || Drupal.settings;
    Drupal.attachBehaviors(wrapper, settings);
  };
  
})(jQuery);

