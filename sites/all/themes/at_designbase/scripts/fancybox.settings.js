/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
  Drupal.behaviors.fancybox = {
    attach: function () {   
      if ($('#page').find('.rs-carousel').length > 0 && $(window).width() > 670) {
        $('li.slide').live('click', function(){
          $('.for-fancy .' + $(this).attr('rel')).trigger('click');
        });

        function fancyUpdate() {
          var i = 1;
          $('.fancybox-thumbs-list li').each(function(){
            $(this).attr('rel', i);
            i++;
          });
          $('.fancybox-title').width($('.fancybox-inner').width());
          $('.fancybox-title').wrapInner('<div class="text"></div>');
          $('.fancybox-title').prepend('<div class="num">' + $('.fancybox-thumbs-list li.active').attr('rel') + '</div>');
          var offsetFancy = $('.fancybox-wrap').position();
          var bottom = offsetFancy.top + $('.fancybox-wrap').height() + 30; 
          $('#fancybox-thumbs').css({
            top: bottom,
            bottom: 'auto',
            left: offsetFancy.left,
            width: $('.fancybox-wrap').width()
          });
        }
                
        $(".fancybox-thumb").fancybox({
          prevEffect	: 'none',
          nextEffect	: 'none',
          maxHeight: 600,
          padding: '0',
          onUpdate: fancyUpdate,
          tpl: {
            closeBtn : '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"><i class="fa fa-times"></i></a>', 
            next     : '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span><i class="fa fa-angle-right"></i></span></a>',
            prev     : '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span><i class="fa fa-angle-left"></i></span></a>'
          },
          helpers	: {
            title	: {
              type: 'outside'
            },
            thumbs	: {
              width	: 140,
              height	: 130
            }
          }
        });
      }
    }
  }
});

