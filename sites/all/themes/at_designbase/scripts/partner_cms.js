/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
  Drupal.behaviors.partner_cms = {
    attach: function () {  
//      $("#edit-product-categories").chosen({disable_search_threshold: 10});
    $("#edit-tags").pqSelect({
        multiplePlaceholder: 'Vælg Tags', 
        displayText: 'Vælg Tags',
        maxDisplay: 0,
        checkbox: true //adds checkbox to options    
    });
    if ($('.images-edit-cont input[type="checkbox"]:first').attr('checked') !== 'checked') {
      $('.images-edit-cont input[type="checkbox"]:first').attr('checked', 'checked');
      $('.images-edit-cont input[type="checkbox"]:first').parents('.form-item').find('.jq-checkbox').addClass('checked');
    }
    
    $("#edit-barnds").pqSelect({
        multiplePlaceholder: 'Vælg Brand',  
        displayText: 'Vælg Brand',
        maxDisplay: 0,
        checkbox: true //adds checkbox to options    
    });
    
    
    $("#edit-product-categories").pqSelect({
        multiplePlaceholder: 'Vælg produktkategorier',   
        checkbox: true,
        width: 339,
        maxDisplay: 0,
        displayText: 'Vælg produktkategorier'
//        deselect: false,
    });
   
    $('.pq-select-search-input').attr('placeholder', 'søg');  
      
    $('#unpublish_time, #publish_time, #time_event').timepicker({
      showPeriod: false,
      amPmText: ['', '']
    });

    imageMultiple();
    
    function imageMultiple(){

        if($('.pane-usersystem-create-event').length > 0 || $('.pane-partner-cms-create-product-news').length > 0 || $('.partner-cms-create-industry-news').length > 0){  
          
          $('.fact-content.active').parents('.form-item').addClass('active');
          
          $('#edit-titles').append('<span class="add-factbox">+</span>');
          
          $('.fact-title').live('click', function(){
            $('.fact-title').removeClass('active');
            $('#edit-fact-box-cont .cke_reset').css({
              display: 'none'
            });
            $('#edit-fact-box-cont .form-item').removeClass('active');
            $(this).addClass('active');
            $('.fact-content').removeClass('active');
            $('#' + $(this).attr('id') + '-fact').addClass('active');
            $('#' + $(this).attr('id') + '-fact').parents('.form-item').addClass('active');
            $('#' + $(this).attr('id') + '-fact').parents('.form-item').find('.cke_reset').css({
              display: 'block'
            });
          });
                    
          
          $('.add-factbox').live('click',function(){
            $('.fact-content').removeClass('active');
            $('.form-item').removeClass('active');
            $('.fact-title').removeClass('active');
            $('#edit-fact-box-cont .cke_reset').css({
              display: 'none'
            });
             
            var factHtml = $('#edit-fact-box-cont .form-item:first').clone();

            var newfactHtml = factHtml.clone();
            var uploadFact = $('.fact-content').length + 1;
            newfactHtml.find('textarea').attr('name', 'fact_box[' + uploadFact + ']');
            newfactHtml.find('textarea').html('');  
            newfactHtml.find('textarea').addClass('active');  
            newfactHtml.find('textarea').addClass('new');
            newfactHtml.find('.cke_reset').remove();
            newfactHtml.find('textarea').attr('id', 'new-fact-' + uploadFact + '-fact');
            newfactHtml.addClass('active');  
            $('#edit-fact-box-cont .content-facts').append(newfactHtml);
            
            var titleHtml = $('#edit-titles .fieldset-wrapper .fact-title:first').clone();
            var newtitleHtml = titleHtml.clone();
            newtitleHtml.attr('id', 'new-fact-' + uploadFact);
            newtitleHtml.addClass('active');
            newtitleHtml.text('Fakta box ' + uploadFact);
            $('#edit-titles .fieldset-wrapper').append(newtitleHtml);

            CKEDITOR.replace('new-fact-' + uploadFact + '-fact');
          });
          
          
          //image upload
            
//          var imageHtml = $('#edit-images .fieldset-wrapper .form-item-images-, #edit-images .fieldset-wrapper .form-item-files-img').clone();
//          var imageCaptionHtml = $('#edit-images .fieldset-wrapper .form-item-captions-, #edit-images .fieldset-wrapper .form-item-caption').clone();

          var uploadIndex = 1;

          $('#edit-images').append('<div class="add-item-cont"><a class="add_image" href="#"><span>+</span>Tilføj nyt billede</a></div>');
          
          $('.remove_image').live('click',function(){
            $(this).parent().next().remove();
            $(this).parent().remove();
            return false;
          });

          $('.images-edit-cont input[type="checkbox"]').live('change', function(){
            $('.images-edit-cont input[type="checkbox"]').removeAttr('checked');
            $('#edit-images .jq-checkbox').removeClass('checked');
            $(this).parents('.form-item').find('.jq-checkbox').addClass('checked');
            $(this).parents('.form-item').find('input[type="checkbox"]').attr('checked', 'checked');
          });
            
          $('.add_image').live('click',function(){
            var imageItem = $('#edit-images .image-item:first');
            var newImageItem = imageItem.clone();
            newImageItem.find('.image-wrap').remove();
            newImageItem.find('textarea').text('');
            var numImage = parseInt($('#edit-images .image-item:last').find('textarea').attr('name').replace(/\D+/g,""), '') + 1;
            newImageItem.find('textarea').attr('name', 'captions[' + numImage + ']');
            newImageItem.find('.jq-checkbox').remove();
            newImageItem.attr('id', '');
            newImageItem.find('.jq-file').remove();
            newImageItem.find('.form-type-checkbox').prepend('<input size="31" type="checkbox" id="edit-preview-' + numImage + '" name="prew[' + numImage +']" class="form-checkbox">');
            newImageItem.find('.form-type-file').append('<input size="31" type="file" id="edit-img-' + numImage + '" name="images[' + numImage +']" class="form-file">');
            
            $('#edit-images .fieldset-wrapper:first').append(newImageItem);
            
            $('#edit-images input[type="file"], .form-checkbox').styler({
              filePlaceholder: '<i class="fa fa-picture-o"></i><div>Upload et billed</div>'
            }); 

            uploadIndex++;
            return false;
          });

          //fileupload

//          var fileHtml = $('#edit-files .fieldset-wrapper .form-item-files-').clone();

          var uploadIndex = 1;

          $('.files-fielset').append('<div class="add-item-cont"><a class="add_file" href="#"><span>+</span>Tilføj ny fil</a></div>');

          $('.remove_file').live('click',function(){
            $(this).parent().remove();
            return false;
          });

          $('.add_file').live('click',function(){

            var imageItem = $('.files-fielset .image-item:first');
            var newImageItem = imageItem.clone();
            newImageItem.find('.image-wrap').remove();
            newImageItem.find('textarea').text('');
            var numImage = parseInt($('.files-fielset .image-item:last').find('textarea').attr('name').replace(/\D+/g,""), '') + 1;
            newImageItem.find('textarea').attr('name', 'filedescription[' + numImage + ']');
            newImageItem.find('.jq-file').remove();
            newImageItem.find('.form-type-file').append('<input size="31" type="file" id="edit-img-' + numImage + '" name="files[]" class="form-file">');
//            newImageItem.find('input[type="file"]').attr('name', 'files[' + numImage + ']');
            
            
            $('.files-fielset .fieldset-wrapper:first').append(newImageItem);
            $('.files-fielset input[type="file"]').styler({
              filePlaceholder: '<i class="fa fa-folder-o"></i><div>Upload en fil</div>'
            }); 

            uploadIndex++;
            return false;
          });

        }
      }
    
    
    
    
    
    
    
    }
  };
})(jQuery);
