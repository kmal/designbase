<?php $form = $variables[""]; $variables['elements'] = $variables[""]; unset($variables[""]); $form['render'] = array(); ?>
<?php $form['mollom']['privacy']['#markup'] = ""; $form['mollom']['captcha']['#description'] = "";?>
<?php $errors = form_get_errors();  ?>
<form action="#">
  <input type="hidden" id="<?php print $form['form_build_id']['#id'] ?>" name="<?php print $form['form_build_id']['#name']?>" value="<?= $form['form_build_id']['#value'] ?>"  />
  <input type="hidden" id="<?php print $form['form_id']['#id'] ?>" name="<?php print $form['form_id']['#name']?>" value="<?= $form['form_id']['#value'] ?>"  />
  <?php if (isset($form['form_token'])): ?>
  <input type="hidden" id="<?php print $form['form_token']['#id'] ?>" value="<?php print $form['form_token']['#default_value'] ?>" name="<?= $form['form_token']['#name']?>" />
  <?php endif; ?>
  <div class="comments">
    <div class="comments-input">
      <h3>Bring en kommentar</h3>
      <p><?php print variable_get('admin_good_comment_behaviour'); ?></p>
      <div class="field-wrap">
        <label>Emne</label>
        <input   <?php if (isset($errors['subject'])) : ?>class="error" <?php endif; ?>  type="text" value="<?php if ($_POST) { print $_POST['subject']; } ?>" name="subject">
        <?php if (isset($errors['subject'])) : ?><p class="error"><?php print $errors['subject']; ?></p><?php endif; ?>
      </div>
      <div class="field-wrap">
        <label>Kommentar</label>
        <textarea name="comment_body[und][0][value]" rows="68" cols="3"  <?php if (isset($errors['comment_body][und][0][value'])) : ?>class="error" <?php endif; ?>   ><?php if ($_POST) { print $_POST['comment_body']['und'][0]['value']; } ?></textarea>
        <?php if (isset($errors['comment_body][und][0][value'])) : ?><p class="error"><?php print $errors['comment_body][und][0][value']; ?></p><?php endif; ?>        
      </div>
      <div class="field-wrap medium first">
        <label>Navn</label>
        <?php global $user; $user = user_load($user->uid);
          if ($user->uid > 0) : ?>
        <p><?php print $user->field_name['und'][0]['value']; ?></p>
        <?php else: ?>            
        <input <?php if (isset($errors['name'])) : ?>class="error" <?php endif; ?> type="text" value="<?php if ($_POST) { print $_POST['name']; } ?>" name="name">
        <?php if (isset($errors['name'])) : ?><p class="error"><?php print $errors['name']; ?></p><?php endif; ?>
        <?php endif; ?>
      </div>
      <div class="field-wrap medium">
        <label>Email (vises ikke p&aring; din kommentar)</label>
        <?php if ($user->uid > 0) : ?>
        <p><?php print $user->mail; ?></p>
        <input type="hidden" value="<?php print $user->mail; ?>" name="field_mail[und][0][value]">              
        <?php else: ?>            
        <input type="text" <?php if (isset($errors['field_mail][und][0][value'])) : ?>class="error" <?php endif; ?>  value="<?php if ($_POST) { print $_POST['field_mail']['und'][0]['value']; } ?>" name="field_mail[und][0][value]">              
        
        <?php if (isset($errors['field_mail][und][0][value'])) : ?><p class="error"><?php print $errors['field_mail][und][0][value']; ?></p><?php endif; ?>
        <?php endif; ?>
      </div>
      <?php print render($form['mollom']); ?>

      <?php print render($form['notify_settings']); ?>
      <div class="field-wrap">
        <div class="input-button left">
          <input type="submit" name="" value="Bring kommentar">
        </div>
      </div>
    </div>
  </div> 

