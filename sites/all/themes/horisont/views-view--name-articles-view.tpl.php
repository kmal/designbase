<div class="supplier-guide-content">
	<div class="inner">
    <!-- SOCIAL MENU -->
    <div class="social-menu">
    	<ul>
		<?php print theme("share_block"); ?>
    	</ul>
    </div>
    <!-- END SOCIAL MENU -->
		<h1>Mere navnestof</h1>
    <p><?php print variable_get('admin_name_articles_text'); ?></p>
	</div>
</div>

<?php if ($pager) : ?>
<div class="pager-footer">
  <div class="pagination">
  <div class="inner">
    <?php print $pager; ?>
  </div>
  </div>
</div>
<?php endif; ?>
<div class="supplier-guide-list name-list custom-list link-overlay">
    <?php if($rows): ?>
    <ul>
      <?php print $rows; ?>
    </ul>
    <?php endif; ?>
</div>
<?php if ($pager) : ?>
<div class="pager-footer">
  <div class="pagination">
    <div class="inner">
      <?php print $pager; ?>
    </div>
  </div>
</div>
<?php endif; ?>

