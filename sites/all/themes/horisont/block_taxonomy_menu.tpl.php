<ul class="category">
  <?php foreach ($tree as $term) : ?>
  <?php if ($term->tid == $tid) : ?>
  <li><?php print l($term->name, "taxonomy/term/" . $term->tid, array(
  "attributes" => array("class" => "active"),
)); ?></li>
  <?php else: ?>
  <li><?php print l($term->name, "taxonomy/term/" . $term->tid); ?></li>
  <?php endif; ?>
  <?php endforeach; ?>
</ul>
