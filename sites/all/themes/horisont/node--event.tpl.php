<div class="article-content">
  <div class="inner">
    <?php if (user_is_logged_in()) : ?>
    <div class="article-action">
      <?php print l('Opret event', "opretevent"); ?>
    </div>
    <?php else :  ?>
    <div class="article-action">
      <?php print l('Opret event', "bliv-business-partner"); ?>
    </div>
    <?php endif; ?>
    <!-- SOCIAL MENU -->
    <div class="social-menu">
    	<ul>
		<?php print theme("share_block"); ?>
    	</ul>
    </div>
    <!-- END SOCIAL MENU --> 
    
    <!-- LARGE IMAGE -->
    <?php if(isset($node->field_image["und"])): ?>
      <?php if(count($node->field_image['und'])==1): ?>
        <div class="img-con">
          <a class="fancy-trigger" href="<?php print image_cache("615px", $node->field_image['und'][0]); ?>" title="<?php print (isset($node->field_image["und"][0]['title']))? $node->field_image['und'][0]['title']:'' ?>"><img src="<?php print image_cache("615px", $node->field_image['und'][0]); ?>" title="<?php print (isset($node->field_image["und"][0]['title']))? $node->field_image['und'][0]['title']:'' ?>" alt="" /></a>
        </div>
      <?php else: ?> 
        <div class="galleryWrapper">
        <div class="img-con slider">
        <?php for ($i = 0; $i < count($node->field_image['und']) ; $i++) : ?> 
          <div class="slides"><img src="<?php print image_cache("615px", $node->field_image['und'][$i]); ?>" title="<?php print (isset($node->field_image["und"][$i]['title']))? $node->field_image['und'][$i]['title']:'' ?>" alt="" /> <?php  if(isset($node->field_image["und"][$i]['title']) && strlen($node->field_image["und"][$i]['title']) > 0) :?>   <p class="slidesText"><?php print (isset($node->field_image["und"][$i]['title']))? $node->field_image['und'][$i]['title']:'' ?></p><?php endif; ?></div>
         
        <?php endfor; ?>
        </div>
        </div>
       
	  <?php endif; ?>
    <?php endif; ?>

    <!-- END LARGE IMAGE -->    

    <h1 class="<?php print get_edit_classes($node); ?>"><?php print l($node->field_headline['und'][0]['value'], 'node/'. $node->nid); ?></h1>
    <div class="article-info">
    <?php if (isset($node->supplier_company['und'][0]['value'])): ?>
      <span class="seperator">|</span>
      <span class="author">Arrang&oslash;r: <?php print $node->supplier_company['und'][0]['value']; ?></span>
<?php endif; ?>
<?php if (isset($node->field_company['und'][0]['tid'])): ?>
      <span class="seperator">|</span>
      <span class="link">
      <?php $supplier = _get_supplier_by_company_tid($node->field_company['und'][0]['tid']); ?>
          <?php if (isset($supplier->nid)): ?>
            <?php print l('Se virksomhedsprofil', 'node/' . $supplier->nid); ?>
	  <?php endif; ?>
      </span>
<?php endif; ?>
    </div>
    <div class="eventWhenWhere">
      <ul>
      <?php if (isset($node->event_type)): ?>
        <li><strong>Hvad:</strong> <?php print $node->event_type; ?></li>
<?php endif; ?>
        <?php if (isset($node->field_location['und'])) : ?>
        <li><strong>Hvor:</strong> <?php print $node->field_location['und'][0]['value']; ?> </li>
        <?php endif; ?>
        <?php $time =  strtotime($node->field_date['und'][0]['value']); ?>
        <li><strong>Startdato:</strong> <?php print get_date($time) . " " . date("H:i",$time); ?></li>
      </ul>
			<?php if ($node->field_link && !empty($node->field_link['und'][0]['value'])): ?>
        <?php if (strpos($node->field_link['und'][0]['value'], 'mailto:') > -1): ?>
          <a class="event-mailto" data-mail="<?php print $node->field_link['und'][0]['value']; ?>" data-eid="<?php print $node->nid; ?>"><span>Tilmeld</span></a>
        <?php else: ?>
				  <a href="/event/tilmeld?eid=<?php echo $node->nid; ?>&&href=<?php echo $node->field_link['und'][0]['value']; ?>" target="_blank"><span>Tilmeld</span></a>
        <?php endif; ?>
			<?php endif; ?>
    </div>	
    <div class="text">
      <?php print $node->field_content['und'][0]['value']; ?>
    </div>
    <?php if (isset($node->field_file['und'])) : ?>
      <div class="files">
        <?php for ($i = 0; $i < count($node->field_file['und']) ; $i++) : ?>
        <div class="file">
        <?php print l($node->field_file['und'][$i]['filename'],file_create_url($node->field_file['und'][$i]['uri']),array('attributes' => array('target' => '_blank'))); ?> 
        <?php if ($node->field_file['und'][$i]['description'] != '') : ?>
          <p><?php print $node->field_file['und'][$i]['description']; ?></p>
        <?php endif; ?>
        </div>
        <?php endfor; ?>
      </div>
    <?php endif; ?>
  </div>
</div>

