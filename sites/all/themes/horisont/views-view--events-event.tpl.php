<?php if ($pager) : ?>
<div class="pagination">
  <div class="inner">
    <?php print $pager; ?>
  </div>
</div>
<?php endif; ?>
<div class="arrangements-list custom-list link-overlay">
    <?php if($rows): ?>
    <ul>
      <?php print $rows; ?>
    </ul>
    <?php endif; ?>
</div>
<?php if ($pager) : ?>
<div class="pagination">
  <div class="inner">
    <?php print $pager; ?>
  </div>
</div>
<?php endif; ?>
