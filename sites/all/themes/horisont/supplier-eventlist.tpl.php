<div class="mod box arrangements-and-courses">
  <div class="inner special">
    <div class="hd">
      <h4><?php print variable_get('admin_supplier_events_header', 'Events'); ?></h4>
    </div>
    <div class="bd">
      <div class="custom-list link-overlay">
        <ul>
          <?php foreach ($events as $node): ?>
            <li>
	      <h5 class="<?php if (function_exists('get_edit_classes')) : print get_edit_classes($node); endif; ?>"><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
	      <p><?php print $node->field_teaser["und"][0]["value"]; ?></p>
	      <span class="date">Startdato
		<?php if (function_exists('get_date')): ?>
  		  <?php print get_date($node->field_date["und"][0]["value"]); ?>
		<?php endif; ?>
		<?php if (isset($node->field_location["und"][0]["value"])) : ?>
		  , <?php print $node->field_location["und"][0]["value"]; ?>
		<?php endif; ?>
	      </span>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>
</div>
