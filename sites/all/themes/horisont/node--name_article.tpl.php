<?php $company = taxonomy_term_load($node->field_company['und'][0]['tid']); ?>
<?php $supplier = _supplier_get_profile($node->field_company['und'][0]['tid']); ?>
<div class="article-content">
  <div class="inner">
    <?php if (user_is_logged_in()) : ?>
    <div class="article-action">
      <?php print l('Opret navnenyt', 'opretnavnenyt'); ?>
    </div>
    <?php else :  ?>
    <div class="article-action">
      <?php print l('Opret navnenyt', 'bliv-business-partner'); ?>
    </div>
    <?php endif; ?>
    <!-- SOCIAL MENU -->
    <div class="social-menu">
      <ul><?php print theme('share_block'); ?></ul>
    </div>
    <!-- END SOCIAL MENU -->

    <h1 class="<?php print get_edit_classes($node); ?>"><?php print l($node->field_headline['und'][0]['value'], 'node/'. $node->nid); ?></h1>
    <div class="article-info">
      <span class="seperator">|</span>
      <span class="date"><?php print get_date($node->created); ?></span>
      <span class="seperator">|</span>
      <?php if (isset($company->name)): ?>
        <span class="author">Af <?php print $company->name; ?></span>
      <?php endif; ?>
      <?php if (is_object($supplier)) :?>
      <span class="seperator">|</span>
      <span class="link">
        <?php print l('Se virksomhedsprofil', 'node/' . $supplier->nid); ?>
      </span>
      <?php endif; ?>
    </div>
    <div class="text">
    <?php if (isset($node->field_image['und'][0])): ?>
      <img src="<?php print image_cache('144px', $node->field_image['und'][0]); ?>" alt="" />
    <?php endif; ?>
      <?php print $node->field_content['und'][0]['value']; ?>
    </div>
  </div>
</div>