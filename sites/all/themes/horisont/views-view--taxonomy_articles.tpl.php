<?php if ($pager) : ?>
<div class="pagination">  
  <div class="inner">
    <?php print $pager; ?>
  </div>
</div>
<?php endif; ?>
<div class="mod">
  <div class="bd">
		<div class="custom-list link-overlay">
      <?php if($rows): ?>
			<ul>
        <?php print $rows; ?>
      </ul>
      <?php endif; ?>
		</div>
	</div>
</div>
<?php if ($pager) : ?>
<div class="pager-footer">    
  <div class="pagination">
    <div class="inner">
      
      <?php print $pager; ?>
    </div>
  </div>
</div>
<?php endif; ?>    

