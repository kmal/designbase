<div class="main-container <?php if ($is_front) : ?>frontpage<?php endif; ?>"> 
  <div class="c100 header">  
		<div class="main-sponsor">
      <!-- adServing 7.1.1c www.installator.dk + Hovedsponsor 150x100 -->
      <script type="text/javascript">
//        var adspaceid = '<?php print variable_get('admin_top_sponsor_logo_id'); ?>';
//        var keywords = '';
//        var searchwords = ''; //insert searchwords seperated by semicolon
//        if(adServingLoad == undefined) var adServingLoad = '';
//        document.write('<scr'+'ipt type="text/javascript" src="http://horisontgruppen.adservinginternational.com/js.php?adspaceid='+adspaceid+'&adServingLoad='+adServingLoad+'&keywords='+keywords+'&sw='+searchwords+'&rnd='+Math.random()+'"></scr'+'ipt>');
      </script>
      <noscript>
        <iframe src="http://horisontgruppen.adservinginternational.com/iframe.php?adspaceid=93&keywords=" frameborder="0" marginheight="0" marginwidth="0" width="150" height="100" scrolling="no" style="width:150px; height:100px; margin:0px;"></iframe>
      </noscript>
    </div>      
      <?php print render($page['top_banner']); ?>
      <div class="login">
      	
        <div class="nav">
          <ul>
            <?php if($user->uid): ?>
            <li class="first <?php if (strstr($_GET['q'], "user/")) : ?> active <? endif; ?>"><a href="/user" title="Login" >Min konto</a></li>
            <li class="middle"><a href="/nytjob">Opret Jobopslag</a></li>
            <li class="last"><a href="/user/logout" title="Login" >Log ud</a></li>
            <?php else: ?>
            <li class="first"><a href="/nytjob">Opret Jobopslag</a></li>
            <li class="last <?php if ($_GET['q'] == "user/login") : ?> active <? endif; ?>"><a href="/user/login" title="Login" >Login</a></li>
            <?php endif; ?>
          </ul>
        </div>
        <div class="service-nav">
      		<a class="cookies" href="/persondata-politik"><img src="/images/iconsmall.png" alt="Cookies" /></a>
            <?php print render($page["main_menu"]); ?>
        </div>
      </div>      
      <div class="secondary-nav">
        <div class="logo">
            <a href="/" title="<?php print variable_get('site_name'); ?>"><img src="/images/logo.png" alt="<?php print variable_get('site_name'); ?>" /></a>
        </div>

        <p>
          <?php print variable_get('admin_logo_payoff'); ?>
        </p>
				<?php if (current_path() !== 'user/login' && current_path() !== 'nytjob'): ?>
					<div class="quickBecome">
						<a href="/bliv-partner" alt="Bliv Partner" class="signBtn">Bliv partner</a>
					</div>
				<?php endif; ?>
        <div class="searchWrap">
          <form action="/search_result" method="get">
            <input type="text" class="search-input clear-input" value="<?php if (isset($_GET['s'])) { print check_plain($_GET['s']); } else { print "Søg.."; } ?>" name="s" />
            <input type="hidden" name="" value="Søg.." />
            <input type="submit" class="search-submit" />
          </form>
        </div>
      </div>      
      <div class="primary-nav">
        <?php print render($page["menu"]); ?>
      </div>      
    </div>
    <?php $msg = _get_top_messages(); ?>
    <?php if (!empty($msg)) : ?>
    <div class="messageBoxTop">      
      <?php print $msg; ?>
    </div>
    <?php endif; ?>
    <?php $msg = _get_status_messages(); ?>
    <?php if (!empty($msg)) : ?>
    <div class="messageBox">      
      <?php print $msg; ?>
    </div>
    <?php endif; ?>
    <?php $error_msg = _get_error_messages(); ?>
    <?php if (!empty($error_msg)) : ?>
    <div class="messageBox">      
      <?php print $error_msg; ?>
    </div>
    <?php endif; ?>
    <?php $error_msg = _get_job_error_messages(); ?>
    <?php if (!empty($error_msg)) : ?>
    <div class="messageBox">      
      <?php print $error_msg; ?>
    </div>
    <?php endif; ?>
    <?php if (!empty($breadcrumb)) : ?>
    <div class="c100">
      <div class="breadcrumb">
        <?php print $breadcrumb; ?>
      </div>
    </div>
    <?php endif; ?>
<?php if (drupal_is_front_page() && (strpos($_SERVER['SERVER_NAME'], 'installator.dk') > -1 || strpos($_SERVER['SERVER_NAME'], 'scm.dk') > - 1)) : ?>
	
    <!-- adServing 7.1.1c www.installator.dk + forsidesponsor 930x40 -->
    <script type="text/javascript">
//      var adspaceid = '<?php print variable_get('admin_submenu_adservingspace_id', 0); ?>';
//      var keywords = '';
//      var searchwords = ''; //insert searchwords seperated by semicolon
//      if(adServingLoad == undefined) var adServingLoad = '';
//      document.write('<scr'+'ipt type="text/javascript" src="http://horisontgruppen.adservinginternational.com/js.php?adspaceid='+adspaceid+'&adServingLoad='+adServingLoad+'&keywords='+keywords+'&sw='+searchwords+'&rnd='+Math.random()+'"></scr'+'ipt>');
    </script>
    <noscript>
      <!--<iframe src="http://horisontgruppen.adservinginternational.com/iframe.php?adspaceid=90&keywords=" frameborder="0" marginheight="0" marginwidth="0" width="930" height="40" scrolling="no" style="width:930px; height:40px; margin:0px;"></iframe>-->
    </noscript>
<?php endif; ?>
    <?php print render($page['content']); ?>
    <?php print render($page['footer']); ?>
    <div class="c100 footer">
      <div class="inner">
        <a target="_blank" href="http://www.horisontgruppen.dk"><div class="logo"></div></a>
        <div class="footerContent">
        <div class="footerBlock"><?php print variable_get("admin_line1", ""); ?></div>
        <div class="footerBlock"><?php print variable_get("admin_line2", ""); ?></div>
        <div class="footerBlock"><?php print variable_get("admin_line3", ""); ?></div>
        </div>
        <div class="payoff">
        	nichemedier der bringer vækst og værdi
        </div>
      </div>
    </div>
  </div> 
</div> 
