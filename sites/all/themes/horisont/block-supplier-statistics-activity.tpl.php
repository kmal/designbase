<div class="container-group">
  <span class="type"><?php print $type; ?></span>
  <span class="count"><?php print $content['count']; ?></span>
  <span class="pageview"><?php print number_format($content['pageview'], 0, ',', '.'); ?></span>
  <span class="exposure"><?php print number_format($content['exposure'], 0, ',', '.'); ?></span>
  <span class="clickrate" title="<?php print _sscc($content['pageview'], $content['exposure'], 2); ?>"><?php print _sscc($content['pageview'], $content['exposure']); ?></span>
</div>