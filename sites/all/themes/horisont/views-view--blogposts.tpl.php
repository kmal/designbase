<div class="expert-blog-content">
<div class="inner">
    <!-- SOCIAL MENU -->
    <div class="social-menu">
    	<ul>
		<?php print theme("share_block"); ?>
    	</ul>
    </div>
    <!-- END SOCIAL MENU -->

  
  <h1><?php print variable_get("admin_expertblog_headline"); ?></h1>
  <p><?php print nl2br(variable_get("admin_expertblog_text")); ?></p>
</div>

  <?php if (!empty($pager)) : ?>
  <div class="pagination">
    <?php print $pager; ?>
  </div>
  <?php endif; ?>
  <div class="expert-blog-list custom-list link-overlay"> 
      <ul>
        <?php print $rows; ?>
      </ul>
  </div>
  <?php if (!empty($pager)) : ?>
  <div class="pager-footer">    
    <div class="pagination">
      <?php print $pager; ?>
    </div>
  </div>
  <?php endif; ?>
</div>

        


