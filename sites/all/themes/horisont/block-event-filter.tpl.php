<div class="arrangements-content">
	<div class="inner">
    <?php if (user_is_logged_in()) : ?>
    <div class="article-action">
      <?php print l('Opret event', "opretevent"); ?>
    </div>
    <?php else :  ?>
    <div class="article-action">
      <?php print l('Opret event', "bliv-business-partner"); ?>
    </div>
    <?php endif; ?>
    <!-- SOCIAL MENU -->
    <div class="social-menu">
    	<ul>
		<?php print theme("share_block"); ?>
    	</ul>
    </div>
    <!-- END SOCIAL MENU -->
		
		<h1>Events</h1>
    <?php print variable_get('admin_event_text'); ?>
	</div>
	<div class="find-box">
  <!--?php print render(drupal_get_form('_event_filter_form')); ?-->
    <form method="get">
      <label>V&aelig;lg kategori:</label>
      <select name="kategori">
        <?php foreach ($options as $tid => $term) : ?>
          <option value="<?php print $tid; ?>" <?php if ($selected == $tid) print 'selected="selected"'; ?> ><?php print $term; ?></option>
        <?php endforeach; ?>
      </select>
      <div class="input-button">
        <input type="submit" value="S&oslash;g" name="" />
      </div> 
    </form>
	</div>
</div>
