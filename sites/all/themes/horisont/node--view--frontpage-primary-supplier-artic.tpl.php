<li class="<?php print get_edit_classes($node);?>">
<h5><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
<div class="meta">
	<?php if($node->category): ?>
	<span class="tag"><?php print $node->category->name; ?></span>
	<?php endif; ?>
	<span class="date">| <?php print get_date($node->created); ?></span>
	<span class="madeby"><?php echo $node->field_company['und'][0]['taxonomy_term']->name; ?></span>
</div>
<div class="articleContent">
<?php if(isset($node->field_image["und"][0]["filename"])): ?>
<div class="img-con">
  <img src="<?php print image_cache("144px", $node->field_image["und"][0]); ?>" alt="" title="" />
</div>
<?php endif; ?>
<?php if (isset($node->field_teaser['und'])) : ?>
<p><?php print $node->field_teaser["und"][0]["value"]; ?></p>
</div>
<?php endif; ?>
</li>

