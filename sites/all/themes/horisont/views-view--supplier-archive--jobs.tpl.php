<div class="expert-blog-content">
  <div class="inner">
    <!-- SOCIAL MENU -->
    <div class="social-menu">
      <ul>
        <?php print theme("share_block"); ?>
      </ul>
    </div>
    <!-- END SOCIAL MENU -->    
    <h1><?php print variable_get('admin_archive_' . arg(2) . '_headline', ucfirst(arg(2))); ?></h1>
    <?php if (strlen(variable_get('admin_archive_' . arg(2) . '_body'))): ?>
      <p><?php print nl2br(variable_get('admin_archive_' . arg(2) . '_body')); ?></p>
    <?php endif; ?>
  </div>
</div>
<?php if ($pager) : ?>
<div class="pagination">
  <div class="inner">
    <?php print $pager; ?>
  </div>
</div>
<?php endif; ?>
<div class="archive-list custom-list link-overlay">
  <?php if($rows): ?>
  <ul>
    <?php print $rows; ?>
  </ul>
  <?php endif; ?>
</div>
<?php if ($pager) : ?>
<div class="pagination">
  <div class="inner">
    <?php print $pager; ?>
  </div>
</div>
<?php endif; ?>
