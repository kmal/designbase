<li class="<?php print get_edit_classes($node);?>">
  <?php if (isset($node->author->field_image['und'][0])) : ?>
  <div class="img-con">
    <img src="<?php print image_cache("54px", $node->author->field_image["und"][0]); ?>" alt="" title="" />
  </div>
  <?php endif; ?>
  <!--div class="text-con"-->
	<h5><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
	<a><span class="date">| <?php print date("d.m.y", $node->created); ?></span></a>
  <?php print l($node->field_teaser["und"][0]["value"], "node/". $node->nid); ?>
  <?php if (isset($node->author->field_name['und'][0]['value'])) : ?>
  <p class="name">- <?php print $node->author->field_name["und"][0]["value"]; ?></p>
  <?php endif; ?>
  <!--/div-->
</li>
