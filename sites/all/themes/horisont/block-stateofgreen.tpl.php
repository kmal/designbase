<div class="mod box rsslist">
	<div class="inner special">
		<div class="state-head">
			<a href="http://www.stateofgreen.com/" target="_blank"><img src="/images/stateofgreen_header.jpg" alt="State of Green" /></a>
		</div>
		<div class="bd">
      <?php if(isset($content['news'])): ?>
			<div class="custom-list mod">
        <div class="hd dark"><h4>News from State of Green</h4></div>
				<ul>
          <?php foreach ($content['news'] as $item) : ?>
            <li>
              <a href="<?php print $item['link']; ?>">
              <h5><?php print $item['title']; ?></h5>
              <span class="smallDate"><?php print date('d.m.y', strtotime($item['date'])); ?></span>
              <p><?php print $item['description']; ?></p>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <?php endif; ?>
      <?php if(isset($content['solutions'])): ?>
			<div class="custom-list">
        <div class="hd dark"><h4>Solutions from State of Green</h4></div>
				<ul>
          <?php foreach ($content['solutions'] as $item) : ?>
            <li>
              <a href="<?php print $item['link']; ?>">
              <h5><?php print $item['title']; ?></h5>
              <span class="smallDate"><?php print date('d.m.y', strtotime($item['date'])); ?></span>
              <p><?php print $item['description']; ?></p>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
			</div>
      <?php endif; ?>
		</div>
	</div>
  <div class="state-foot">
  	<a href="http://www.stateofgreen.com/join" target="_blank"><img src="/images/stateofgreen_footer.jpg" alt="State of Green" /></a>
  </div>
</div>
