<div class="signupContent step2">
	<div class="sorenPlaceholder"></div>
	<div class="colFull ">
		<h1 class="<?php print get_edit_classes($node);?>"><?php echo $node->field_headline['und'][0]['value']; ?></h1>
		<div class="threeCol">
          <div class="col">
            <?php echo $node->body['und'][0]['value']; ?>
            <a href="/user/register">
			<button class="signBtn">Start din gratis prøve nu</button>
            </a>
			</div>
			<div class="col">
				<div class="pricingTable event">
					<div class="hd">
						<h2>Event <span>Partner</span></h2>
					</div>
					<div class="bd">
						<ul>
                            <?php foreach($node->field_event_includes['und'] as $event_include): ?>
							<li><?php echo $event_include['value']; ?></li>
                            <?php endforeach; ?>
						</ul>
					</div>
					<div class="ft">
						<div class="pricewrap">
							<span class="currency">DKK</span>
							<span class="price"><?php echo $node->field_event_price['und'][0]['value']; ?></span>
						</div>
						<span class="pryear">/år</span>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="pricingTable business">
					<div class="hd">
						<h2>Business <span>Partner</span></h2>
					</div>
					<div class="bd">
						<ul>
                            <?php foreach($node->field_business_includes['und'] as $business_include): ?>
                              <li><?php echo $business_include['value']; ?></li>
                            <?php endforeach; ?>
						</ul>
					</div>
					<div class="ft">
						<div class="pricewrap">
							<span class="currency">DKK</span>
							<span class="price"><?php echo $node->field_business_price['und'][0]['value']; ?></span>
						</div>
						<span class="pryear">/år</span>
					</div>
				</div>
			</div>

			
		
		</div>

	</div>
</div>
