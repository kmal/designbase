<?php

// Plugin definition
$plugin = array(
  'title' => t('Single column'),
  'category' => t('Installator.dk'),
  'icon' => 'installator_onecol.png',
  'theme' => 'panels_installator_singlecol',
  'css' => 'installator_singlecol.css',
  'regions' => array(
    'main' => t('main'),
    ),
  );
