<?php

// Plugin definition
$plugin = array(
  'title' => t('Three column'),
  'category' => t('Installator.dk'),
  'icon' => 'installator_onecol.png',
  'theme' => 'panels_installator_threecol',
  'css' => 'installator_threecol.css',
  'regions' => array(
    'left' => t('left'),
    'center' => t('center'),
    'right' => t('right'),
    ),
  );
