<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column'),
  'category' => t('Installator.dk'),
  'icon' => 'installator_onecol.png',
  'theme' => 'panels_installator_twocol',
  'css' => 'installator_twocol.css',
  'regions' => array(
    'left' => t('left'),
    'right' => t('right'),
    ),
  );
