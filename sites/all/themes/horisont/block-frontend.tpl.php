<div class="signupSection">

<div class="navigation">

<ul>
	<li class="active">
		<a href="#">Start</a>
	</li>
	<li>
		<a href="#">Priser og fordele</a>
	</li>
	<li>
		<a href="#">Tilmelding</a>
	</li>
</ul>

</div>
<div class="signupContent step2">
	<div class="sorenPlaceholder">
		
			</div>
	<div class="colFull">
		<h1>Det tilbyder vi</h1>
		<div class="threeCol">
			<div class="col">
				<p>Suspendisse commodo nunc eu est porttitor eleifend. Praesent bibendum massa sed risus blandit viverra. Nullam sed malesuada odio. Vestibulum risus sem, luctus a ultrices nec, pellentesque non odio. </p>
			<button class="signBtn">Start din gratis prøve nu</button>
			</div>
			<div class="col">
				<div class="pricingTable event">
					<div class="hd">
						<h2>Event <span>Partner</span></h2>
					</div>
					<div class="bd">
						<ul>
							<li>Opret events</li>
							<li>Virksomhedsprofil i kompetenceguiden (online)</li>
							<li>Upload filer</li>
							<li>Support og service</li>
						</ul>
					</div>
					<div class="ft">
						<div class="pricewrap">
							<span class="currency">DKK</span>
							<span class="price">4.995</span>
						</div>
						<span class="pryear">/år</span>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="pricingTable business">
					<div class="hd">
						<h2>Business <span>Partner</span></h2>
					</div>
					<div class="bd">
						<ul>
							<li>Bring nyheder, artikler, cases m.m.</li>
							<li>Opret events</li>
							<li>Virksomhedsprofil i kompetenceguiden (online)</li>
							<li>Bring Navnenyt</li>
							<li>Opret personer</li>
							<li>Upload filer</li>
							<li>Support og service</li>
						</ul>
					</div>
					<div class="ft">
						<div class="pricewrap">
							<span class="currency">DKK</span>
							<span class="price">11.995</span>
						</div>
						<span class="pryear">/år</span>
					</div>
				</div>
			</div>

			
		
		</div>

	</div>
</div>

<div class="signupContent step1">
	<div class="colA">
		<h1>Bliv Business Partner</h1>
		<div class="text-item">
			<h2>Hvorfor?</h2>
			<p>Suspendisse commodo nunc eu est porttitor eleifend. Praesent bibendum massa sed risus blandit viverra. Nullam sed malesuada odio. Vestibulum risus sem, luctus a ultrices nec, pellentesque non odio.</p>
		</div>
		<button class="signBtn">Start din gratis prøve nu</button>
		<div class="text-item">
			<h2>Fordele?</h2>
			<p>Suspendisse commodo nunc eu est porttitor eleifend. Praesent bibendum massa sed risus blandit viverra.</p>
			<p>Nullam sed malesuada odio. Vestibulum risus sem, luctus a ultrices nec, pellentesque non odio.</p>
		</div>
		<div class="sorenPlaceholder">
		
		</div>
	</div>
	<div class="colB">
		<div class="item citat">
			<div class="bg"></div>
			<div class="citatKarusel">
				<div class="inner">
					<p>Gennem et professionelt setup, innovative løsninger og en hurtig og behjælpelig supportfunktion, 
har vi som <span>Business Partner</span> en god mulighed for at kommunikere relevante <strong>budskaber</strong>, <strong>kurser</strong> og <strong>produktnyheder</strong> ud til branchen.</p>
					<span class="meta">Sebastian Bøtcher | <a href="#">Schneider Electric</a></span>
				</div>
				<div class="inner">
					<p>Gennem et professionelt setup, innovative løsninger og en hurtig og behjælpelig supportfunktion</p>
					<span class="meta">Sebastian Huus | <a href="#">Whatever Electric</a></span>
				</div>
			</div>
		</div>
		<div class="item punchlines">
			<ul>
				<li>
					<strong>Gratis</strong> test i en måned. Vores service får mange pæne ord med på vejen, men <strong>prøv den selv i en måned</strong>. Opret dig nu og læg din første pressenyhed live på få minutter.
				</li>
				<li>
					Målrettet markedsføring: <strong>30.000 unikke</strong> brugere fra branchen i 2012.
				</li>
				<li>
					Hvad har du brug for? Skriv <strong>pressemeddelselser</strong>, invitér til <strong>produktpræsentationer</strong> og seminarer, skriv et <strong>jobopslag</strong>, navnenyt og læg dine data ind i <strong>kompetenceguiden</strong>.
				</li>
			</ul>
		</div>
	</div>
</div>


<div class="signupFooter">
	<div class="colA">
		<a href="#" class="chat signBtnInvert">Chat med os<span></span></button>
		<a href="http://player.vimeo.com/video/38428493?title=0&amp;byline=0&amp;portrait=0" id="signupVimeo" class="video signBtnInvert">se demovideo<span></span></a>
	</div>
	<div class="colB">
		<div class="signupForm">
			<span>prøv det gratis i dag</span>
			<div class="quickSignup">
				<div class="fieldWrap">
					<input class="signupText clear-input" type="text" value="FIRMA" />
					<input type="hidden" name="" value="FIRMA" />
				</div>
				<div class="fieldWrap">
					<input class="signupText clear-input" type="text" value="EMAIL" />
					<input type="hidden" name="" value="EMAIL" />
				</div>
				<button class="signBtn">Start din gratis prøve</button>
			</div>
		</div>
	</div>
</div>
	
</div>
