<li class="<?php print get_edit_classes($node);?>">
  <?php if(isset($node->field_image["und"][0]["filename"])): ?>
  <div class="img-con">
    <img src="<?php print image_cache("80px", $node->field_image["und"][0]); ?>" alt="" title="" width="80"/>
  </div>
  <?php endif; ?>
  <h5><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
  <span class="smallDate"><?php print date("d.m.y", $node->created); ?></span>
  <p><?php print truncate_utf8(strip_tags($node->field_content["und"][0]["value"]), 60, TRUE, TRUE); ?></p>
</li>
