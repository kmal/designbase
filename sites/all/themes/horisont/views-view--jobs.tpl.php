<?php if ($pager) : ?>
<div class="pager-footer">
  <div class="pagination">
  <div class="inner">
    <?php print $pager; ?>
  </div>
  </div>
</div>
<?php endif; ?>
<div class="supplier-guide-list custom-list">
    <?php if($rows): ?>
    <ul>
      <?php print $rows; ?>
    </ul>
    <?php endif; ?>
</div>
<?php if ($pager) : ?>
<div class="pager-footer">
  <div class="pagination">
    <div class="inner">
      <?php print $pager; ?>
    </div>
  </div>
</div>
<?php endif; ?>

