<div class="mod box jobPopular">
  <div class="inner special">
    <div class="hd">
      <h4>Jobopslag</h4>
    </div>
    <div class="bd">
      <ul>
	<?php foreach($jobs as $job) :  ?>
	<li>
	  <a href="<?php print url('node/'.$job->nid); ?>">
	    <h5><?php print $job->field_headline['und'][0]['value']; ?></h5>
	    <p>Ansøgningsfrist <?php print date("d-m-Y", strtotime($job->field_applicationdeadline['und'][0]['value'])); ?></p>
	  </a>
	</li>
	<?php endforeach; ?>
      </ul>      
    </div>
  </div>
</div>
