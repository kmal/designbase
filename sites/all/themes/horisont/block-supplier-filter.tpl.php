<div class="supplier-guide-content">
	<div class="inner">
    <!-- SOCIAL MENU -->
    <div class="social-menu">
    	<ul>
		<?php print theme("share_block"); ?>
    	</ul>
    </div>
    <!-- END SOCIAL MENU -->
		
		<h1>Kompetenceguide</h1>
    <?php print variable_get('admin_supplierguide_text'); ?>
	</div>
	<div class="find-box">
  <?php print $content; ?>
	</div>
</div>
