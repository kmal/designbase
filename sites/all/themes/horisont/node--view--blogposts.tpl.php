<li class="<?php print get_edit_classes($node);?>">
  <?php if (isset($node->author->field_image['und'][0])) :?>
  <div class="img-con">
    <img src="<?php print image_cache("54px", $node->author->field_image["und"][0]); ?>" alt="" title="" />
  </div>
  <?php endif; ?>
  <div class="info-block">
    <span class="date">| <?php print date("d.m.y", $node->created); ?></span>
    <?php if (isset($node->author->field_name['und'])) : ?>
      <span class="name"><?php print $node->author->field_name["und"][0]["value"]; ?></span>
    <?php endif; ?>
</div>
<div class="text-block">
  <h5><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
  <?php print l($node->field_teaser["und"][0]["value"], "node/". $node->nid); ?>
</div>
</li>
<!--
    <li>
  <div class="link-group">
    <div class="img-con">
      <img title="" alt="" src="images/dummy/54x70.jpg">
      </div>
      <div class="info-block">
        <span class="date">|  12.05.11</span>
        <span class="name">Lise-Lotte Harboe Trikker</span>
      </div>
      <div class="text-block">
        <h5><a title="" href="http://www.google.com">Sed nec dolor lorem, sit amet mollis ligula.</a></h5>
        <p>Vivamus blandit lacinia gravida. Sed lorem libero, volutpat eu pharetra eget, pellentesque et nibh. In non mauris risus. Aenean dictum, massa sit amet fermentum aliquet, massa libero fermentum magna</p>
        <p>Maecenas mattis mattis diam, nec molestie enim mattis in. Cras eu justo eros, at viverra mi. Aenean consectetur pellentesque justo ut malesuada.</p>
      </div>
    </div>
    <a class="comments-link" href="http://www.dr.dk">Kommentarer<span class="comments-bubble">78<span></span></span></a>
  </li>
-->

