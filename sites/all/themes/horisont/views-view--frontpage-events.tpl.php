<div class="mod box arrangements-and-courses">
	<div class="inner special">
		<div class="hd">
			<h4><?php print variable_get('admin_events_header', 'Events'); ?></h4>
		</div>
		<div class="bd">
			<div class="custom-list link-overlay">
        <?php if($rows): ?>
          <ul>
            <?php print $rows; ?>
          </ul>
        <?php endif; ?>
			</div>
		</div>
	</div>
	<div class="more">
		<a href="/events" title="">Flere events</a>
	</div>
</div>
