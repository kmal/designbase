<div class="mod box jobContact">
  <div class="inner special">
    <div class="hd">
      <h4>Kontakt</h4>
    </div>
    <div class="bd">
      <p>
        <?php if (isset($node->isBusiness) && $node->isBusiness) : ?>
          <a href="<?php print url("node/" . $node->bid); ?>"><?php print $node->field_companyname['und'][0]['value']; ?></a><br />
        <?php else: ?>
          <?php print $node->field_companyname['und'][0]['value']; ?><br>
        <?php endif; ?>
	<?php if (isset($node->field_company_address['und'][0]['value'])): ?>
          <?php print $node->field_company_address['und'][0]['value']; ?><br />
        <?php endif; ?>
	<?php if (isset($node->field_postal_code['und'][0]['value'])): ?>
	  <?php print $node->field_postal_code['und'][0]['value'] . " "; ?>
        <?php endif; ?>
	<?php if (isset($node->field_city['und'][0]['value'])): ?>
          <?php print $node->field_city['und'][0]['value']; ?>
        <?php endif; ?>
      </p>
      <p>
      <?php if(isset($node->field_contactperson['und'][0]['value'])): ?>
        <?php print $node->field_contactperson['und'][0]['value']; ?><br />
<?php endif; ?>
        <?php if (!empty( $node->field_www['und'][0]['value'])) : ?>
          Web: <a href="http://<?php print $node->field_www['und'][0]['value']; ?>"><?php print $node->field_www['und'][0]['value']; ?></a><br>
        <?php endif; ?>
	<?php if (isset($node->field_phone['und'][0]['value'])): ?>
          Tlf.: <?php print $node->field_phone['und'][0]['value']; ?><br />
        <?php endif; ?>
	<?php if (isset($node->field_email['und'][0]['value'])): ?>
          Email: <a href="mailto:<?php print $node->field_email['und'][0]['value']; ?>"><?php print $node->field_email['und'][0]['value']; ?></a>
        <?php endif; ?>
      </p>
    </div>
  </div>
</div>
