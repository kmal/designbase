<?php if (count($news) > 0) : ?>
<div class="mod box supplier-news">
	<div class="inner special">
		<div class="hd">
			<h4><?php print variable_get('admin_supplier_news_header', 'Nyt fra levererand&oslash;ren'); ?></h4>
		</div>
		<div class="bd">
			<div class="custom-list link-overlay">
        <ul>
          <?php foreach ($news as $node) : ?>
          <li class="<?php if (function_exists('get_edit_classes')): print get_edit_classes($node); endif; ?>">
          <?php if(isset($node->field_image["und"][0]["filename"])): ?>
          <div class="img-con">
	  <?php if (function_exists('image_cache')): ?>
            <img src="<?php print image_cache("70px", $node->field_image["und"][0]); ?>" alt="" title="" />
<?php endif; ?>
          </div>
          <?php endif; ?>
          <h5><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
	  <?php if (isset($node->field_category['und'][0]['tid'])): ?>
          <span class="tag"><?php print _get_term_by_tid($node->field_category['und'][0]['tid']); ?></span>
<?php endif; ?>
	  <span class="date">| <?php if (function_exists('get_date')): print get_date($node->created); endif; ?></span>
	  <?php if (isset($node->field_teaser["und"][0]["value"])): ?>
            <p><?php print $node->field_teaser["und"][0]["value"]; ?></p>
          <?php endif; ?>
        </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

