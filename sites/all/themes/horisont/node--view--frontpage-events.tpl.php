<li>
  <h5 class="<?php print get_edit_classes($node); ?>"><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
  <p><?php print $node->field_teaser["und"][0]["value"]; ?></p>
  <span class="date">Startdato 
  <?php print get_date($node->field_date["und"][0]["value"]); ?>
  <?php if (isset($node->field_location["und"][0]["value"])) : ?>
  , <?php print $node->field_location["und"][0]["value"]; ?>
  <?php endif; ?>  
  </span>
</li>
