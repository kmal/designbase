<div class="supplier-guide-content">
  <div class="inner">
  	<div class="article-action">
      <a href="/nytjob">Opret jobopslag</a>      
    </div>
    <!-- SOCIAL MENU -->
    <div class="social-menu">
      <ul>
	<?php print theme("share_block"); ?>
      </ul>
    </div>
    <!-- END SOCIAL MENU -->
    <h1>Jobopslag</h1>
    <?php print variable_get('admin_joblist_text'); ?>
  </div>
  <div class="find-box">
    <?php print $content; ?>
  </div>
</div>
