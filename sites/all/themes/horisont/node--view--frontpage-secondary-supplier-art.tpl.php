<li class="<?php print get_edit_classes($node);?>">
<h5><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
<span class="tag"><?php print $node->category->name; ?></span><a><span class="date">| <?php print get_date($node->created); ?></span></a>
<p><?php print $node->field_teaser["und"][0]["value"]; ?></p>
</li>
