<li class="<?php print get_edit_classes($node);?>">
	<h5><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h5>
	<?php if (isset($node->field_category['und'][0]['tid'])): ?><span class="tag"><?php print $node->field_category['und'][0]['taxonomy_term']->field_headline['und'][0]['value']; ?></span><?php endif; ?>
	<a><span class="date">| <?php print date("d.m.y", $node->created); ?></span></a>
	<?php print l($node->field_teaser["und"][0]["value"], $node->field_link["und"][0]["value"]); ?>
</li>
