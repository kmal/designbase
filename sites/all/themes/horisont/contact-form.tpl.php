<?php $errors = form_get_errors(); 
if (isset($variables['contact-form'])) {
$form = $variables['contact-form'];
}
else {
$form = $variables[""];
}
?>

<input type="hidden" id="<?php print $form['form_build_id']['#id'] ?>" name="<?php print $form['form_build_id']['#name']?>" value="<?= $form['form_build_id']['#value'] ?>"  />
<input type="hidden" id="<?php print $form['form_id']['#id'] ?>" name="<?php print $form['form_id']['#name']?>" value="<?= $form['form_id']['#value'] ?>"  />
<?php if (isset($form['form_token'])): ?>
<input type="hidden" id="<?php print $form['form_token']['#id'] ?>" value="<?php print $form['form_token']['#default_value'] ?>" name="<?= $form['form_token']['#name']?>" />
<?php endif; ?>

<div class="field-wrap">
  <input class="clear-input" type="text" name="requester_name" value="Dit navn" />
  <input type="hidden" name="" value="Dit navn" />
  <?php if(isset($errors["requester_name"])): ?>
  <p class="error">
    <?php print $errors["requester_name"]; ?>
  </p>
  <?php endif; ?>
</div>
<div class="field-wrap">
  <input class="clear-input" type="text" name="requester_company" value="Dit firma" />
  <input type="hidden" name="" value="Dit firma" />
</div>
<div class="field-wrap">
  <input class="clear-input" type="text" name="requester_email" value="Din email" />
  <input type="hidden" name="" value="Din email" />
</div>
<div class="field-wrap">
  <input class="clear-input" type="text" name="requester_phone" value="Dit telefonnummer" />
  <input type="hidden" name="" value="Dit telefonnummer" />
  <?php if(isset($errors["requester_phone"])): ?>
  <p class="error">
    <?php print $errors["requester_phone"]; ?>
  </p>
  <?php endif; ?>
</div>
<div class="field-wrap">
  <div class="input-button right">
    <input type="submit" name="submit" value="Send">
  </div>
</div>
