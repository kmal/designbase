<div class="mod box most-read">
  <div class="inner">
    <div class="hd">
      <h4 class="active">Mest l&aelig;ste</h4>
      <h4>Seneste kommentarer</h4>
    </div>
    <div class="bd">
      <div class="custom-list link-overlay hide">
        <ul>
          <?php foreach ($content['most_read'] as $node) : ?>
          <li>
          	<span class="smallDate"><?php print date("d.m.y", $node->created); ?></span>
            <?php print l($node->field_headline['und'][0]['value'], "node/".$node->nid); ?>
          </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class="custom-list link-overlay hide mostRecent">
        <ul>
          <?php foreach ($content['most_recent_comments'] as $comment): ?>
          <li>
            <a href="<?php print url('node/' . $comment['nid']) . '#comment-' . $comment['cid']; ?>">
              <span class="commentHeader"><?php print $comment['headline']; ?></span>
 <?php print truncate_utf8(strip_tags($comment['body']), 75, true, true); ?>
              <span></span>
            </a>
          </li>
          <?php endforeach; ?>
        </ul>
      </div>       
    </div>
  </div>
</div>
