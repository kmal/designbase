<!DOCTYPE html>
	<!--[if IE 7 ]><html lang="da" xmlns:fb="http://ogp.me/ns/fb#" class="ie7 <?php print $html_class; ?>"><![endif]-->
	<!--[if IE 8 ]><html lang="da" xmlns:fb="http://ogp.me/ns/fb#" class="ie8 <?php print $html_class; ?>"><![endif]-->
	<!--[if IE 9 ]><html lang="da" xmlns:fb="http://ogp.me/ns/fb#" class="ie9 <?php print $html_class; ?>"><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--><html lang="da" xmlns:fb="http://ogp.me/ns/fb#" class="<?php print $html_class; ?>"><!--<![endif]-->
  <head>
  	<meta charset="utf-8">

    <!-- <link rel="stylesheet" media="screen" type="text/css" href="/stylesheet/screen.css?v=1"> -->
    <link rel="stylesheet" media="print" type="text/css" href="/stylesheet/print.css">
    <link rel="icon" href="http://www.installator.dk/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="http://www.installator.dk/favicon.ico" type="image/x-icon"> 
    <link rel="stylesheet" type="text/css" href="/stylesheet/smoothness/jquery-ui-1.8.17.custom.css">
      <script type="text/javascript" src="/javascript/jquery-1.7.1.min.js"></script>
  	  <script type="text/javascript" src="/javascript/jquery.fancybox-1.3.4.pack.js"></script>
	    <script type="text/javascript" src="/javascript/jquery-ui-1.8.17.custom.min.js"></script>
      <script type="text/javascript" src="/javascript/jquery.cookie.js"></script>
      <script type="text/javascript" src="/javascript/site.js?v=2"></script>    
      <script type="text/javascript" src="/javascript/ckeditor/ckeditor.js?v=1"></script>
      <title><?php print $head_title ?></title>
			<?php if (isset($_SERVER['REDIRECT_URL']) && strpos($_SERVER['REDIRECT_URL'], 'bliv-partner') > -1): ?>
				<!--Start of Zopim Live Chat Script-->
				<script type="text/javascript">
				function openChat() { window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//cdn.zopim.com/?13njoyxlYz84ZDsBSSo0yUItQh0aJIy8';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
 $zopim.livechat.window.show(); return 1;}
				</script>
				<!--End of Zopim Live Chat Script-->
			<?php endif; ?>
      <?php if (user_access("access administration pages")): ?>
        <?php print $scripts; ?>
        <?php print $styles; ?>
        <!-- Start TimePicker additions -->
        <link rel="stylesheet" href="/sites/all/libraries/jquery-ui-timepicker/jquery-ui-timepicker-addon.css">
        <script type="text/javascript" src="/sites/all/libraries/jquery-ui-timepicker/i18n/jquery-ui-timepicker-da.js"></script>
        <!-- End TimePicker additions -->
        <link rel="stylesheet" href="/edit.css" />
        <script src="/edit.js"></script>
      <?php endif; ?>
      <?php print $head; ?>
    </head>
    <body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/da_DK/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<!--- page -->	
      <?php print $page; ?>
	  <!-- page -->
  <!-- Addthis -->
  <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>
  <!-- GA -->
	<?php print variable_get('admin_ga'); ?>
  </body>  
</html>
