<li>
  <div class="info-block">
    <span class="date">|  <?php print date("d.m.y", $comment->created); ?></span>
    <span class="name"><?php print $comment->name; ?></span>
  </div>
  <div class="text-block ">
    <strong><?php print $comment->subject; ?></strong>
    <p><?php print nl2br($comment->comment_body['und'][0]['value']); ?></p>
  </div>
</li>

