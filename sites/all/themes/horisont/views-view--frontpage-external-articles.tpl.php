<div class="mod box others-write-about">
	<div class="inner special">
		<div class="hd">
			<h4><?php print variable_get('admin_others_write_about_header', 'andre skriver om'); ?></h4>
		</div>
		<div class="bd">
			<div class="custom-list link-overlay">
        <?php if($rows): ?>
  				<ul>
            <?php print $rows; ?>
          </ul>
        <?php endif; ?>
			</div>
		</div>
	</div>
</div>
