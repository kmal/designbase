<?php if (!empty($contacts)) : ?>
  <div class="mod box supplier-contacts">
    <div class="inner special">
      <div class="hd">
	<h4><?php print variable_get('admin_supplier_contacts_header', 'Kontaktpersoner'); ?></h4>
      </div>
      <div class="bd">
	<div class="custom-list">
	  <ul>
            <?php foreach ($contacts as $contact) : ?>
              <li class="<?php print get_edit_classes($contact);?>">
		<?php if(isset($contact->field_image["und"][0]["filename"])): ?>
		  <div class="img-con">
		    <img src="<?php print image_cache("63px", $contact->field_image["und"][0]); ?>" alt="" title="" />
		  </div>
		<?php else: ?>
		  <div class="company-contact-img"> <img src="/images/dummy/dummy.jpg" alt="" /> </div>
		<?php endif; ?>
		<div class="company-contact-description">
		  <h5><?php print $contact->field_name['und'][0]['value']; ?></h5>
		  <ul>
		  <?php if (isset($contact->field_title['und'][0]['value'])): ?>
                    <li><span>Titel:</span> <?php print $contact->field_title['und'][0]['value']; ?></li>
		    <?php endif; ?>
		    <?php if (isset($contact->field_phone['und'][0]['value'])): ?>
                    <li><span>TLF:</span> <?php print $contact->field_phone['und'][0]['value']; ?></li>
		    <?php endif; ?>
		    <?php if (isset($contact->field_email['und'][0]['value'])): ?>
                    <li><span>Email:</span> <a href="mailto: <?php print $contact->field_email['und'][0]['value']; ?>"><?php print $contact->field_email['und'][0]['value']; ?></a></li>
		    <?php endif; ?>
		  </ul>
		</div>
              </li>
            <?php endforeach; ?>
          </ul>
	</div>
      </div>
    </div>
  </div>
<?php endif; ?>
