<div class="article-content">
  <div class="inner">
    <!-- SOCIAL MENU -->
    <div class="social-menu">
    	<ul>
		<?php print theme("share_block"); ?>
    	</ul>
    </div>
    <!-- END SOCIAL MENU -->  
    <!-- LARGE IMAGE -->
    <?php if(isset($node->field_image["und"][0]["filename"])): ?>
    <div class="img-con">
      <a class="fancy-trigger" href="<?php print image_cache("615px", $node->field_image['und'][0]); ?>" title="Title"><img src="<?php print image_cache("615px", $node->field_image['und'][0]); ?>" title="" alt="" /></a>
      <?php if (isset($node->field_caption["und"][0]["value"])): ?>
      <p><?php print $node->field_caption['und'][0]['value']; ?></p>
      <?php endif; ?>
    </div> 
    <?php endif; ?>
    <!-- END LARGE IMAGE -->		

    <h1 class="<?php print get_edit_classes($node); ?>"><?php print $node->field_headline['und'][0]['value']; ?></h1>		

    <div class="text">
      <?php print $node->field_content['und'][0]['value']; ?>
    </div>
  </div>
</div>


