<!-- SOCIAL MENU -->
<div class="article-content">
  <div class="inner">
    <div class="article-action">
      <a href="/nytjob">Opret jobopslag</a>      
    </div>
    <div class="social-menu"> 
      <ul>
        <?php print theme("share_block"); ?>
  </ul>
</div>
</div>
</div>
<!-- END SOCIAL MENU -->
<div class="<?php print get_edit_classes($node); ?> jobArticle">
     <?php if (isset($node->field_logo['und'][0])): ?>
  <img class="photo" src="<?php print image_cache('144px', $node->field_logo['und'][0]); ?>">
<?php endif; ?>
  <h1><?php print $node->field_headline['und'][0]['value']; ?></h1>
  <?php print $node->field_jobdescription['und'][0]['value']; ?>
  <p>Skriv venligst i din ans&oslash;gning, at du så annoncen p&aring; <?php print variable_get('site_name'); ?></p>
</div>
