<?php if (is_object($supplier)): ?>
<div class="supplierAdWrap">
  <div class="supplierBottomAd">
    <span class="adText">Annonce</span>
    <div class="logoContainer">
      <?php if (isset($supplier->field_logo['und'][0]['uri'])): ?>
        <img src="<?php print image_cache('144px', $supplier->field_logo['und'][0]); ?>" />
      <?php endif; ?>
      <?php echo l('SE PROFIL »', 'node/' . $supplier->nid); ?>
    </div>
    <div class="description">
      <span class="companyHead"><strong><?php echo $supplier->field_supplier_company['und'][0]['value']; ?></strong> <?php echo variable_get('admin_company_info_heading', 'er partner på'); ?> <?php echo variable_get('site_name', 'installator.dk'); ?></span>
      <?php echo truncate_utf8(strip_tags($supplier->field_summary['und'][0]['safe_value']), 200, TRUE, TRUE); ?>   
    </div>
    <div class="activity">
      <strong class="activityHead">Aktivitet:</strong>
      <?php if ($supplier->articles_count > 0): ?>
        <span class="item">
          Artikler:
          <strong><?php echo $supplier->articles_count; ?></strong> 
        </span>
      <?php endif; ?>
      <?php if ($supplier->namenews_count > 0): ?>
        <span class="item">
          <span class="sep">|</span>
          Navnenyt:
          <strong><?php echo $supplier->namenews_count; ?></strong>
        </span>
      <?php endif; ?>
      <?php if ($supplier->events_count > 0): ?>
        <span class="item">
          <span class="sep">|</span>
          Events:
          <strong><?php echo $supplier->events_count; ?></strong>
        </span>
      <?php endif; ?>
      <?php if ($supplier->competences > 0): ?>
        <span class="item">
          <span class="sep">|</span>
          Kompetenceområder:
          <strong><?php echo $supplier->competences; ?></strong>
        </span>
      <?php endif; ?>
     </div>
     <p class="becomeBusiness"><?php echo variable_get('admin_company_info_readmore_text', 'Er du også en del af branchen, så læs om mulighederne for at blive partner.'); ?><br />
     <a href="/bliv-partner"><?php echo variable_get('admin_company_info_readmore_link', 'Læs mere her'); ?></a></p>
  </div>
</div>
<?php endif; ?>