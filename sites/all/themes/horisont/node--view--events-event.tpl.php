<li class="item--event">
  <div class="background <?php print get_edit_classes($node); ?>">
    <div class="white-box<?php print (isset($node->supplier_logo['und'][0]['filename']))?'':' noimage'; ?>">				
      <?php if(isset($node->supplier_logo["und"][0]["filename"])): ?>
        <div class="img-con ">
          <?php print l('<img src="'.image_cache('144px', $node->supplier_logo['und'][0]).'" alt="' . ((isset($node->supplier_company['und'][0]['value']))?$node->supplier_company['und'][0]['value']:'') . '" />', 'node/'. $node->nid, array('html' => TRUE)); ?>
        </div>
      <?php endif; ?>

      <div class="text-block">
        <h5><a href="<?php print url("node/".$node->nid); ?>"><?php print $node->field_headline['und'][0]['value']; ?></a></h5>
        <p><?php print $node->field_teaser['und'][0]['value']; ?></p>
        <span class="date">Startdato <?php print date("d.m.y H:i", strtotime($node->field_date['und'][0]['value'])); ?><?php if (isset($node->field_location['und'])) :?>, <?php print $node->field_location['und'][0]['value']; ?><?php endif; ?></span>
      </div>
    </div>
  </div>
</li>
