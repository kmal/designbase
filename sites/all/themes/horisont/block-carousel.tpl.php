<div class="c100 article-carousel">

	<div class="box-slideshow">
		<div class="box-slideshow-nav-left"></div>
		<div class="box-slideshow-nav-right"></div>
		<!--div class="box-slideshow-fade-out-left"></div>
		<div class="box-slideshow-fade-out-right"></div-->
		<div class="box-slideshow-content">
			<ul class="ss">
				<? for ( $i=0; $i < count($content); $i++) { ?> 
				<li class="box">
					<div class="inner special">
						<div class="hd">
							<h4><?php print $content[$i]['term']; ?></h4>
						</div>
						<div class="bd">
							<div class="custom-list link-overlay">
								<ul>
                  <? $nodes = $content[$i]['nodes']; 
                    for ( $j=0; $j < count($nodes); $j++) { ?>
									<li>
										<h5><span class="date"><?php print get_date($nodes[$j]['date']); ?></span><a href="<? print $nodes[$j]['path']; ?>" title="<? print $nodes[$j]['title']; ?>"><? print $nodes[$j]['title']; ?></a></h5>
									</li>
                  <? } ?>
								</ul>
							</div>
						</div>
					</div>
				</li>
				<? } ?> 
			</ul>
		</div>
	</div>
</div>
