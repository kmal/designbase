<div class="mod box jobFacts">
  <div class="inner special">
    <div class="hd">
      <h4>Jobfakta</h4>
    </div>
    <div class="bd">
      <table>
	<tbody>
	  <?php if (isset($node->field_jobtype['und'][0]['value'])): ?>
	    <tr>
	      <td>Ansættelsesforhold:</td>
	      <td><?php print $node->field_jobtype['und'][0]['value']; ?></td>
	    </tr>
	  <?php endif; ?>
	  <?php if (isset($node->field_jobtitle['und'][0]['value'])): ?>
	    <tr>
	      <td>Jobtitel:</td>
	      <td><?php print $node->field_jobtitle['und'][0]['value']; ?></td>
	    </tr>
	  <?php endif; ?>
	  <tr>
	    <td>Geografi:</td>
	    <td><?php print $node->region->name; ?></td>
	  </tr>
	  <tr>
	    <td>Ansøgningsfrist:</td>
	    <?php if (isset($node->field_deadline['und'][0]['value']) && $node->field_deadline['und'][0]['value'] == 1): ?>
	      <td>Hurtigst muligt</td>
	      <?php elseif (isset($node->field_deadline['und'][0]['value']) && $node->field_deadline['und'][0]['value'] == 2): ?>
		<td>L&oslash;bende</td>
	      <?php else: ?>
		<td><?php print date("d.m.Y", strtotime($node->field_applicationdeadline['und'][0]['value'])); ?></td>
	      <?php endif; ?>
	  </tr>
	  <tr>
	    <td>Tiltrædelsesdato:</td>
	    <?php if (isset($node->field_job_startsoon['und'][0]['value']) && $node->field_job_startsoon['und'][0]['value']): ?>
	      <td>Snarest muligt</td>
	    <?php else: ?>
	      <td><?php print date("d.m.Y", strtotime($node->field_startdate['und'][0]['value'])); ?></td></td>
	    <?php endif; ?>
	  </tr>
	  <tr>
	    <td>Oprettelsesdato:</td>
	    <td><?php print date("d.m.Y", $node->created); ?></td>
	  </tr>
	</tbody>
      </table>
    </div>
  </div>
</div>
