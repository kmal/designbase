$<div class="mod box arrangements-and-courses">
<div class="inner special">
  <div class="hd">
    <h4>Mine nyheder</h4>
  </div>
  <div class="bd">
    <?php if ($content !== FALSE) : ?>
      <div class="custom-list link-overlay">
	<ul>
          <?php foreach ($content as $row) : ?>
	    <li>
	      <h5><?php print l($row->field_headline['und'][0]['value'],'redigernyhed/' . $row->nid ); ?></h5>
	      <p><?php print truncate_utf8($row->field_teaser['und'][0]['value'], 80, TRUE, TRUE ); ?></p>
	      <span class="date">Indrykket <?php print format_date($row->created); ?></span>
	    </li>
          <?php endforeach; ?>
	</ul>
      </div>
    <?php else: ?>
      <p><?php print variable_get('admin_suppliernews_text'); ?></p>  
    <?php endif; ?>
  </div>
</div>
<?php if ($content !== FALSE) : ?>
  <div class="more">
    <?php print l('Opret nyhed', 'optretnyhed'); ?>
  </div>
<?php endif; ?>
</div>
