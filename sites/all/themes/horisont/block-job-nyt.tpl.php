<h1>Opret jobopslag</h1>
 <p class="editSectionText"><?php print variable_get('admin_newjob_nobusiness_text'); ?></p>
 <div class="editSection">
   <ul class="tabs">
     <li>
       <?php print l('Jobopslag','nytjob'); ?>
     </li>
   </ul>
   <div class="tabContainer">
     <?php print $content; ?>
   </div>
 </div>
 
