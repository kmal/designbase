<?php // Shameful include of site specific stylesheet config.php file - HERE BE UGLY DRAGONS! ?>
<?php require_once(DRUPAL_ROOT . '/stylesheet/config.php'); ?>
<script type="text/javascript" src="//www.google.com/jsapi"></script>
<script type="text/javascript" src="<?php print drupal_get_path('module', 'supplier_statistics') . '/js/supplier_statistics.graphs.js'; ?>"></script>

<div class="mod box statistics">
  <div class="inner special">
    <div class="bd">
      <div class="list">
        <p><?php print nl2br(variable_get('admin_statistics_description_first')); ?></p>
        <button class="input-button-general aButtonNew print">
          <span>Print</span>
        </button>
        <?php if (isset($content['activities'])): ?>
        <div class="container-activities">
          <h5>Aktiviteter</h5>
          <div class="container-group header">
            <span class="type">Type</span>
            <span class="count">Antal</span>
            <span class="pageview">Sidevisninger</span>
            <span class="exposure">Eksponeringer</span>
            <span class="clickrate">Clickrate</span>
          </div>
          <?php if (isset($content['activities']['supplier'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['supplier'], 'type' => 'Virksomhedsprofil')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['article'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['article'], 'type' => 'Nyheder')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['job'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['job'], 'type' => 'Jobopslag')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['event'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['event'], 'type' => 'Events')); ?>
          <?php endif; ?>
          <?php if (isset($content['activities']['name_article'])): ?>
          <?php print theme('company_statistics_activity', array('content' => $content['activities']['name_article'], 'type' => 'Navnenyt')); ?>
          <?php endif; ?>
        </div>
        <?php endif; ?>
        <p><?php print nl2br(variable_get('admin_statistics_description_second')); ?></p>
        <?php if (isset($content['entities'])): ?>
        <div class="container-entities">
          <h5>Indhold i detaljer</h5>
          <div class="container-group header">
            <span class="type">Type</span>
            <span class="title">Titel</span>
            <span class="created">Oprettet</span>
            <span class="pageview">Sidevisninger</span>
            <span class="exposure">Eksponeringer</span>
            <span class="clickrate">Clickrate</span>
          </div>
          <?php foreach($content['entities'] as $nid => $entity): ?>
          <span class="entity"
                data-nid="<?php print $nid; ?>"
                data-url="<?php print url('node/' . $nid); ?>"
                data-type="<?php print check_plain($entity['type']); ?>"
                data-title="<?php print check_plain($entity['title']); ?>"
                data-timestamp="<?php print $entity['timestamp']; ?>"
                data-created="<?php print $entity['created']; ?>"
                data-pageview="<?php print $entity['pageview']; ?>"
                data-exposure="<?php print $entity['exposure']; ?>"
                data-clickrate="<?php print _sscc($entity['pageview'], $entity['exposure']); ?>"></span>
        <?php endforeach; ?>
        </div>
        <?php endif; ?>
        <?php print render($form); ?>
        <div class="container-graphs">
          <span class="chart-options" data-color="<?php print $editBorderColor; ?>"></span>
        <?php if (isset($content['supplier'])): ?>
          <h5>Virksomhedsprofil</h5>
          <div class="container-group">
          <?php if (isset($content['supplier']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['supplier']['pageview'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_supplier_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['supplier']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['supplier']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_supplier_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (isset($content['article'])): ?>
          <h5>Nyheder</h5>
          <div class="container-group">
          <?php if (isset($content['article']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['article']['pageview'], 'entities' => $content['article']['entities'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_article_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['article']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['article']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_article_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (isset($content['job'])): ?>
          <h5>Jobopslag</h5>
          <div class="container-group">
          <?php if (isset($content['job']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['job']['pageview'], 'entities' => $content['job']['entities'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_job_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['job']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['job']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_job_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (isset($content['event'])): ?>
          <h5>Events</h5>
          <div class="container-group">
          <?php if (isset($content['event']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['event']['pageview'], 'entities' => $content['event']['entities'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_event_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['event']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['event']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_event_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (isset($content['name_article'])): ?>
          <h5>Navnenyt</h5>
          <div class="container-group">
            <?php if (isset($content['name_article']['pageview'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['name_article']['pageview'], 'entities' => $content['name_article']['entities'], 'type' => 'Sidevisninger', 'description' => nl2br(variable_get('admin_statistics_name_article_pageview_description')))); ?>
          <?php endif; ?>
          <?php if (isset($content['name_article']['exposure'])): ?>
            <?php print theme('company_statistics_graph', array('content' => $content['name_article']['exposure'], 'clickrate' => TRUE, 'type' => 'Eksponeringer', 'description' => nl2br(variable_get('admin_statistics_name_article_exposure_description')))); ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
