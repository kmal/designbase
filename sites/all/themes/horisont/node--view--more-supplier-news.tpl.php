<li class="<?php print get_edit_classes($node);?>">
  <div class="content-wrap ">
    <div class="link-group ">
      <?php if(isset($node->field_image["und"][0]["filename"])): ?>
        <div class="img-con ">
          <?php print l("<img src=\"".image_cache("144px", $node->field_image['und'][0])."\" alt=\"\" title=\"\" />", "node/". $node->nid, array('html' => TRUE)); ?>
        </div>
      <?php endif; ?>
      <h3><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></h3>
      <?php if (isset($node->category->name)): ?>
      <span class="tag"><?php print $node->category->name; ?></span>
      <span class="seperator">|</span>
    <?php endif; ?>
      <span class="date"><?php print get_date($node->created); ?></span>
      <?php if (!isset($node->field_show_byline['und'][0]) || $node->field_show_byline['und'][0]['value'] == 1) : ?>
        <span class="seperator">|</span>
        <span class="author">Af 
        <?php if (isset($node->company) && ($node->company->name != '')) : ?> 
          <?php print $node->company->name;?>
        <?php endif; ?>
        </span>
      <?php endif; ?>
<?php if(isset($node->field_teaser["und"][0]["value"])): ?>
      <p><?php print $node->field_teaser["und"][0]["value"]; ?></p>
<?php endif; ?>
    </div>
    <?php if ($node->comment_count > 0 ) : ?> 
    <a href="<?php print url("node/".$node->nid); ?>#comments" title="" class="comments-link">Kommentarer<span class="comments-bubble"><?php print $node->comment_count; ?><span></span></span></a>
    <?php endif; ?>
  </div>
</li>
