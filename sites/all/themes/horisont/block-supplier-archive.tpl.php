<div class="mod box archive">
  <div class="inner special">
    <div class="hd">
      <h4><?php print $content['headline']; ?></h4>
    </div>
    <div class="bd">
      <ul>
      <?php foreach($content['links'] as $link): ?>
        <li><?php print $link; ?></li>
      <?php endforeach; ?>
      </ul>      
    </div>
  </div>
</div>