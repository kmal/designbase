<li>
  <a class="listBlockLink" href="<?php print url("node/".$node->nid); ?>">
    <div class="background">
      <div class="white-box">
        <div class="img-con ">
          <?php if (isset($node->field_logo["und"][0]["filename"])): ?>
          <?php print '<img src="' . image_cache('144px', $node->field_logo['und'][0]) . '" alt="' . $node->field_supplier_company['und'][0]['value']. '" />'; ?>
          <?php else: ?>
          &nbsp;
          <?php endif; ?>
        </div>
        
        <div class="text-block">
          <?php if (isset($node->field_summary['und'][0]['safe_value'])): ?>
            <p><?php print truncate_utf8(strip_tags($node->field_summary['und'][0]['safe_value']), 120, FALSE, TRUE); ?></p>
          <?php else : ?>
            <?php if (isset($node->field_summary['und'][0]['value'])): ?>
              <?php if (strstr($node->field_summary['und'][0]['value'], '&amp;') !== FALSE): ?>
              <p><?php print truncate_utf8(utf8_encode(html_entity_decode(strip_tags($node->field_summary['und'][0]['value'], '<strng><p>'))), 120, FALSE, TRUE); ?></p>
              <?php else: ?>
              <p><?php print truncate_utf8(strip_tags($node->field_summary['und'][0]['value'], '<strong><p>'), 120, FALSE, TRUE); ?></p>
              <?php endif; ?>
            <?php endif; ?>
          <?php endif; ?>
        </div>
        <div class="info-block">
          <?php if (isset($node->field_supplier_company['und'][0]['value'])): ?>
          <p><?php print $node->field_supplier_company['und'][0]['value']; ?></p>
          <?php endif; ?>
          <?php if (isset($node->field_address['und'][0]['value'])): ?>
          <?php if ($node->field_address['und'][0]['value'] != ''): ?>
          <p><?php print $node->field_address['und'][0]['value']; ?></p>
          <?php endif; ?>
          <?php endif; ?>
          <?php if (isset($node->field_postal_code['und'][0]['value'])): ?>
          <?php if ($node->field_postal_code['und'][0]['value'] != ''): ?>
          <p><?php print $node->field_postal_code['und'][0]['value']; ?> <?php print $node->field_city['und'][0]['value']; ?></p>
          <?php endif; ?>
          <?php endif; ?>
          <?php if (isset($node->field_phone['und'][0]['value'])): ?>
          <?php if ($node->field_phone['und'][0]['value'] != ''): ?>
          <p>Telefon <?php print $node->field_phone['und'][0]['value']; ?></p>
          <?php endif; ?>
          <?php endif; ?>
          <?php if (isset($node->field_www['und'][0]['value'])): ?>
          <?php if ($node->field_www['und'][0]['value'] != ''): ?>
          <p><?php print $node->field_www['und'][0]['value']; ?></p>
          <?php endif; ?>
          <?php endif; ?>
          <?php if (isset($node->field_email['und'][0]['value'])): ?>
          <?php if ($node->field_email['und'][0]['value'] != ''): ?>
          <p><?php print $node->field_email['und'][0]['value']; ?></p>
          <?php endif; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </a>
</li>

