<div class="mod box name-news">
	<div class="inner special">
		<div class="hd">
			<h4><?php print variable_get('admin_namenews_header','Nyt om navne'); ?></h4>
		</div>
		<div class="bd">
			<div class="custom-list link-overlay">
				<ul>
          <?php print $rows; ?>
        </ul>
			</div>
		</div>
	</div>
  <div class="more">
    <?php 
      print l('Har du navnenyt?', variable_get('admin_about_name_articles_redirect')); 
    ?>
    <?php 
      print l('Mere navnenyt', 'merenavnenyt'); 
    ?>
  </div>
</div>
