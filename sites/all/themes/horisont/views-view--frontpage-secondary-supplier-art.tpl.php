<div class="mod box supplier-news supplier-secondary">
	<div class="inner special">
		<div class="hd dark">
			<h4><?php print variable_get('admin_secondary_supplier_news', 'Nyt fra leverand&oslash;rene'); ?></h4>
		</div>
		<div class="bd">
			<div class="custom-list link-overlay">
        <?php if($rows): ?>
        <ul>
          <?php print $rows; ?>
        </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
