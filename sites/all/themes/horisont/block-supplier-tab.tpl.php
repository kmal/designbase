<h1>Min konto</h1>
<p class="editSectionText"><?php print variable_get('admin_myprofile_text'); ?></p>
<?php $subscription = (isset($supplier->field_subscription_type['und'][0]['value']))?$supplier->field_subscription_type['und'][0]['value']:1; ?>
<div class="editSection">
<?php if ($subscription != 2): ?>    
<div class="businessPartner"></div>
<?php else : ?>
<div class="eventPartner"></div>
<?php endif; ?>
  <ul class="tabs">
    <?php if ($context == 'basisinfo'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Virksomhedsprofil','user'); ?>
    </li>
    <?php if ($subscription != 2): ?>    
    <?php if ($context == 'kontakter'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Personer','kontaktoversigt'); ?>
    </li>
    <?php endif; ?>

    <?php if ($subscription != 2): ?>    
    <?php if ($context == 'nyheder'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Nyheder','nyhedsoversigt'); ?>
    </li>
    <?php endif; ?>

    <?php if ($context == 'events'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Events','eventoversigt'); ?>
    </li>

    <?php if ($subscription != 2): ?>    
    <?php if ($context == 'navnenyt'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Navnenyt','navnenytoversigt'); ?>
    </li>
    <?php endif; ?>
    <?php if ($subscription != 2) : ?>
    <li <?php if ($context == "joboversigt"): ?> class="active" <?php endif;?> >
      <?php print l('Jobopslag','joboversigt'); ?>
    </li>
    <?php endif; ?>

    <?php if ($subscription != 2): ?> 
    <?php if ($context == 'statistik'): ?>
    <li class="active">
    <?php else: ?> 
    <li>
    <?php endif; ?>
      <?php print l('Statistik','statistik'); ?>
    </li>
    <?php endif; ?>
  </ul>
  <div class="tabContainer">
  <?php print $content; ?>
  </div>
</div>
