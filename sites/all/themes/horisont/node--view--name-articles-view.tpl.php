<li>
    <div class="background">
      <div class="white-box">
        <div class="img-con ">
          <?php if (isset($node->field_image["und"][0]["filename"])): ?>
          <?php print '<img src="' . image_cache('144px', $node->field_image['und'][0]) . '" alt="' . $node->field_headline['und'][0]['value']. '" />'; ?>
          <?php else: ?>
          &nbsp;
          <?php endif; ?>
        </div>
        
        <div class="text-block">
          <?php if (isset($node->field_headline['und'][0]['value'])): ?>
          <p><?php print l($node->field_headline["und"][0]["value"], "node/". $node->nid); ?></p>
          <?php endif; ?>
        </div>
        <span class="smallDate"><?php print date("d.m.y", $node->created); ?></span>
        <div class="info-block">
          <?php if (isset($node->field_content['und'][0]['value'])): ?>
          <p><?php print truncate_utf8(strip_tags($node->field_content["und"][0]["value"]), 160, TRUE, TRUE); ?></p>
          <?php endif; ?>
        </div>
      </div>
    </div>
</li>

