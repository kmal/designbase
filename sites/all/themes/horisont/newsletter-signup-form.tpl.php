<?php $errors = form_get_errors(); 

if (isset($variables['newsletter-signup-form'])) {
$form = $variables['newsletter-signup-form'];
}
else {
$form = $variables[""];
}

?>


<input type="hidden" id="<?php print $form['form_build_id']['#id'] ?>" name="<?php print $form['form_build_id']['#name']?>" value="<?= $form['form_build_id']['#value'] ?>"  />
<input type="hidden" id="<?php print $form['form_id']['#id'] ?>" name="<?php print $form['form_id']['#name']?>" value="<?= $form['form_id']['#value'] ?>"  />
<?php if (isset($form['form_token'])): ?>
<input type="hidden" id="<?php print $form['form_token']['#id'] ?>" value="<?php print $form['form_token']['#default_value'] ?>" name="<?= $form['form_token']['#name']?>" />
<?php endif; ?>

<div class="field-wrap">
  <input class="clear-input" type="text" name="newsletter_name" value="<?php if (isset($_POST['newsletter_name'])) { print $_POST['newsletter_name']; } else { ?>Dit navn<?php } ?>" />
  <input type="hidden" name="" value="Dit navn" />
  <?php if(isset($errors["name"])): ?>
  <p class="error">
    <?php print $errors["name"]; ?>
  </p>
  <?php endif; ?>
</div>
<div class="field-wrap">
  <input class="clear-input" type="text" name="newsletter_email" value="<?php if (isset($_POST['newsletter_email'])) { print $_POST['newsletter_email']; } else { ?>Din email<?php } ?>" />
  <input type="hidden" name="" value="Din email" />
  <?php if(isset($errors["email"])): ?>
  <p class="error">
    <?php print $errors["email"]; ?>
  </p>
  <?php endif; ?>
</div>
<div class="field-wrap">
  <div class="input-button right">
    <input type="submit" name="submit" value="Tilmeld" />
  </div>
</div>

