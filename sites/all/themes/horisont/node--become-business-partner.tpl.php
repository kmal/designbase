<div class="signupContent step1 ">
	<div class="colA <?php print get_edit_classes($node);?>">
		<h1><?php echo $node->field_headline['und'][0]['value']; ?></h1>
		<div class="text-item">
			<?php echo $node->body['und'][0]['value']; ?>
		</div>
		<a href="/user/register">
			<button class="signBtn">Start din gratis prøve nu</button>
		</a>
		<div class="text-item">
			<?php echo $node->field_body_after['und'][0]['value']; ?>
		</div>
		<div class="sorenPlaceholder"></div>
	</div>
	<div class="colB">
		<div class="item citat">
			<div class="bg"></div>
			<div class="citatKarusel">
                <?php foreach($node->testimonial as $testimonial): ?>
				<div class="inner">
                      <p><?php echo $testimonial->body['und'][0]['value']; ?></p>
                      <span class="meta"><?php echo $testimonial->field_source['und'][0]['value'];  ?> | <a href="<?php echo $testimonial->link; ?>"><?php echo $testimonial->company;  ?></a></span>
				</div>
                  <?php endforeach; ?>
			</div>
		</div>
		<div class="item punchlines">
			<ul>
                <?php foreach($node->field_advantages['und'] as $advantage): ?>
                  <li><? echo $advantage['value']; ?></li>
                <?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>
