<?php if (count($news) > 0) : ?>
<div class="mod box name-news">
  <div class="inner special">
    <div class="hd">
      <h4><?php print variable_get('admin_supplier_namenews_header', 'Nyt om navne'); ?></h4>
    </div>
    <div class="bd">
      <div class="custom-list">
        <ul>
          <?php foreach ($news as $node) : ?>
          <li class="<?php print get_edit_classes($node);?>">
          <?php if(isset($node->field_image["und"][0]["filename"])): ?>
          <div class="img-con">
            <img src="<?php print image_cache("80px", $node->field_image["und"][0]); ?>" alt="" title="" width="80"/>
          </div>
          <?php endif; ?>
          <h5><?php print $node->field_headline["und"][0]["value"]; ?></h5>
          <span class="smallDate"><?php print date("d.m.y", $node->created); ?></span>
          <p><?php print $node->field_content["und"][0]["value"]; ?></p>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>
</div>
<?php endif;  ?>
