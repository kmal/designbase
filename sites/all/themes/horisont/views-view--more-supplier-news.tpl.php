<div class="expert-blog-content">
  <div class="inner">
    <?php if (user_is_logged_in()) : ?>
    <div class="article-action">
      <?php print l('Opret nyhed', "opretnyhed"); ?>
    </div>
    <?php else :  ?>
    <div class="article-action">
      <?php print l('Opret nyhed', "bliv-business-partner"); ?>
    </div>
    <?php endif; ?>
    <!-- SOCIAL MENU -->
    <div class="social-menu">
    	<ul>
		<?php print theme("share_block"); ?>
    	</ul>
    </div>
    <!-- END SOCIAL MENU -->    
    <h1><?php print variable_get('admin_more_supplier_news_header', 'Flere nyheder fra leverand&oslash;rer' ); ?></h1>
    <?php if (variable_get('admin_more_supplier_news_body') != ''): ?>
      <p><?php print variable_get('admin_more_supplier_news_body'); ?></p>
    <?php endif; ?>
  </div>
</div>
<?php if ($pager) : ?>
<div class="pagination">  
  <div class="inner">
    <?php print $pager; ?>
  </div>
</div>
<?php endif; ?>
<div class="mod">
  <div class="bd">
		<div class="custom-list link-overlay">
      <?php if($rows): ?>
			<ul>
        <?php print $rows; ?>
      </ul>
      <?php endif; ?>
		</div>
	</div>
</div>
<?php if ($pager) : ?>
<div class="pager-footer">    
  <div class="pagination">
    <div class="inner">
      
      <?php print $pager; ?>
    </div>
  </div>
</div>
<?php endif; ?>    

