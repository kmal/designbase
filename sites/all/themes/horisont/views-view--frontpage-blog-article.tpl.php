<div class="mod box weekly-blog">
	<div class="inner">
		<div class="hd">
			<h4><?php print variable_get('admin_blog_header', 'ugens blogindl&aelig;g'); ?></h4>
		</div>
    <div class="bd link-overlay">
      <?php if($rows): ?>
        <ul>
          <?php print $rows; ?>
        </ul>
      <?php endif; ?>
		</div>
	</div>
</div>
