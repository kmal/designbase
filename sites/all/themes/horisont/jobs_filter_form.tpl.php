<?php 
if (isset($variables['supplier-filter-form'])) {
  $form = $variables['supplier-filter-form'];
}
else {
  $form = $variables[""];
}
?>

<label class="hidden-input" for="virksomhed">Find virksomhed</label>
<input id="virksomhed" type="text" name="virksomhed" value="<?php print $form['supplier']['#default_value']; ?>" class="clear-input" />
<label>V&aelig;lg region:</label>
<select name="industry">
  <?php foreach ($form['industry']['#options'] as $tid => $term) : ?>
    <option value="<?php print $tid; ?>" <?php if ($form['industry']['#default_value'] == $tid) print 'selected="selected"'; ?> ><?php print $term; ?></option>
  <?php endforeach; ?>
</select>
<div class="input-button">
  <input type="submit" value="S&oslash;g" name="" />
</div>
