<?php 
if (isset($variables['jobs-filter-form'])) {
  $form = $variables['jobs-filter-form'];
}
else {
  $form = $variables[""];
}

?>

<label class="hidden-input" for="virksomhed">Find jobopslag</label>
<input id="virksomhed" type="text" name="freetext" value="<?php print $form['freetext']['#default_value']; ?>" class="clear-input" />
<label>V&aelig;lg region:</label>
<select name="regions">
  <?php foreach ($form['regions']['#options'] as $tid => $term) : ?>
    <option value="<?php print $tid; ?>" <?php if ($form['regions']['#default_value'] == $tid) print 'selected="selected"'; ?> ><?php print $term; ?></option>
  <?php endforeach; ?>
</select>
<div class="input-button">
  <input type="submit" value="S&oslash;g" name="" />
</div>
<?php print render($form['form_token']); ?>
<?php print render($form['form_id']); ?>
<?php print render($form['form_build_id']); ?>
