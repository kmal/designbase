<div class="article-content">
  <div class="inner">
    <?php if ($node->nid != 1718) : ?>
      <?php if (user_is_logged_in()) : ?>
	<div class="article-action">
          <?php print l('Opret nyhed', "opretnyhed"); ?>
	</div>
      <?php else :  ?>
	<div class="article-action">
          <?php print l('Opret nyhed', "bliv-partner"); ?>
	</div>
      <?php endif; ?>
    <?php endif; ?>
    <!-- SOCIAL MENU -->
    <div class="social-menu">
      <ul>
	<?php print theme("share_block"); ?>
	<li class="socialComments"><a href="#comments" class="comments-link">Kommentarer<span class="comments-bubble"><?php print $node->comment_count; ?><span></span></span></a></li>
      </ul>
    </div>
    <!-- END SOCIAL MENU -->
    <!-- LARGE IMAGE -->
    <?php if(isset($node->field_image["und"])): ?>
      <?php if(count($node->field_image['und'])==1): ?>
	<div class="img-con">
          <a class="fancy-trigger" href="<?php print image_cache("615px", $node->field_image['und'][0]); ?>" title=""><img src="<?php print image_cache("615px", $node->field_image['und'][0]); ?>" title="" alt="" /></a>
          <?php if (isset($node->field_image["und"][0]['title'])) : ?>
            <p><?php print $node->field_image['und'][0]['title']; ?></p>
          <?php endif; ?>
	</div>
      <?php else: ?> 
	<div class="galleryWrapper">
          <div class="img-con slider">
	    <?php if (isset($node->field_image['und'])): ?>
              <?php for ($i = 0; $i < count($node->field_image['und']) ; $i++) : ?> 
                <div class="slides"><img src="<?php print image_cache("615px", $node->field_image['und'][$i]); ?>" title="<?php print (isset($node->field_image["und"][$i]['title'])) ? $node->field_image['und'][$i]['title']:'' ?>" alt="" />
                  <?php if (isset($node->field_image["und"][$i]['title']) && strlen($node->field_image["und"][$i]['title']) > 0): ?>
                    <p class="slidesText"><?php print (isset($node->field_image["und"][$i]['title'])) ? $node->field_image['und'][$i]['title']:'' ?></p>
                  <?php endif; ?>
                </div>
              <?php endfor; ?>
            <?php endif; ?>
          </div>
	</div>
      <?php endif; ?>
    <?php endif; ?>
    <!-- END LARGE IMAGE -->	
    <h1 class="<?php print get_edit_classes($node); ?>"><?php print $node->field_headline['und'][0]['value']; ?></h1>		
    <?php if (isset($node->field_teaser['und'][0]['value'])): ?>
      <p class="intro-text"><?php print $node->field_teaser['und'][0]['value']; ?></p>		
    <?php endif; ?>

    <?php if ($node->field_type["und"][0]["value"] != "4"): ?>
      <div class="article-info">
	<span class="seperator">|</span>
	<span class="date"><?php print get_date($node->created); ?></span>
	<?php if (!isset($node->field_show_byline['und'][0]) || $node->field_show_byline['und'][0]['value'] == 1) : ?>
          <span class="seperator">|</span>
          <span class="author">Af 
            <?php if (isset($node->field_byline['und'])) :?>
              <?php if ($node->field_byline['und'][0]['value'] != '') : ?>
		<?php print $node->field_byline['und'][0]['value']; ?>
              <?php else : ?>
		<?php if ($node->field_type['und'][0]['value'] != 2) : ?>
		  <?php print $node->author->field_name['und'][0]['value']; ?>
		<?php endif; ?>
              <?php endif; ?>
            <?php else : ?>
              <?php if ($node->field_type['und'][0]['value'] != 2 && isset($node->author->field_name['und'][0]['value'])) : ?>
		<?php print $node->author->field_name['und'][0]['value']; ?>
              <?php endif; ?>
            <?php endif; ?>
            <?php if ($node->field_type['und'][0]['value'] != 1) : ?>
              <?php if (isset($node->company) && isset($node->company->name) && ($node->company->name != '')) : ?> 
		<?php print $node->company->name;?>
              <?php endif; ?>
            <?php endif; ?>
          </span>
          <?php if ($node->field_type['und'][0]['value'] != 2) : ?>
            <span class="seperator">|</span>
            <span class="email"><a href="mailto: <?php print $node->author->mail; ?>" title=""><?php print $node->author->mail; ?></a></span>
          <?php endif; ?>
	<?php endif; ?>
	<?php if ($node->field_type['und'][0]['value'] == 2 && isset($node->field_company['und'][0]['tid'])) : ?>
          <span class="seperator">|</span>
          <span class="link">
	  <?php $supplier = _get_supplier_by_company_tid($node->field_company['und'][0]['tid']); ?>
	    <?php if (isset($supplier->nid)): ?>
	      <?php print l('Se virksomhedsprofil', 'node/' . $supplier->nid); ?>
            <?php endif; ?>
          </span>
	<?php endif; ?>
      </div>
    <?php endif; ?>
    <div class="text">
      <?php if (isset($node->category->name)): ?>
	<span class="tag"><?php print l($node->category->name, "taxonomy/term/". $node->category->tid); ?></span>
      <?php endif; ?>
      <?php print $node->field_content['und'][0]['value']; ?>
    </div>
    <?php if (isset($node->field_file['und'])) : ?>
      <div class="files">
     	<ul>
          <?php for ($i = 0; $i < count($node->field_file['und']) ; $i++) : ?>
            <li><span></span>
	      <?php print l($node->field_file['und'][$i]['filename'],file_create_url($node->field_file['und'][$i]['uri']),array('attributes' => array('target' => '_blank'))); ?> 
              <?php if ($node->field_file['und'][$i]['description'] != '') : ?>
		<p><?php print $node->field_file['und'][$i]['description']; ?></p>
              <?php endif; ?>
            </li>
          <?php endfor; ?>
        </ul>
      </div>
    <?php endif; ?>
  </div>
</div>
