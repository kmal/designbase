<div class="mod box jobsFront">
  <div class="inner special">
    <div class="hd">
      <h4>Nye Jobopslag</h4>
    </div>
    <div class="bd">
      <ul class="jobSlide">

        <?php foreach ($jobs as $key => $node) : ?>
          <?php if (($key+1) % 3 == 1) : ?>
            <li>
          <?php endif; ?>
          <a href="<?php print url('node/'.$node->nid); ?>">
            <div class="item">
              <img src="<?php if (function_exists('image_cache')): print image_cache('144px', $node->field_logo['und'][0]); endif; ?>" alt="">
              <div class="description">
                <h5><?php print $node->field_headline['und'][0]['value']; ?></h5>
                <p><?php print $node->field_jobteaser['und'][0]['value']; ?></p>

              </div>
              <span class="date">Ans&oslash;gningsfrist:
		<?php if (isset($node->field_deadline['und'][0]['value']) && $node->field_deadline['und'][0]['value']): ?>
                  Hurtigst muligt
                <?php else: ?>
                  <?php print date("d.m.Y", strtotime($node->field_applicationdeadline['und'][0]['value']));?>, <?php print $node->geo->name; ?>
                <?php endif; ?>
              </span>
            </div>	  
          </a>
          <?php if (($key+1) % 3 == 0) : ?>
            </li>
          <?php endif; ?>
        <?php endforeach; ?>
      </ul>
      <div class="slideNav"></div>
    </div>
  </div>
  <div class="more">
    <a href="/jobopslag" title="">Flere Jobopslag</a>
    <a class="newjob" href="/nytjob" title="">Opret Jobopslag</a>
  </div>
</div>
