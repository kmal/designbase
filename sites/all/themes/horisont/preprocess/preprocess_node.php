<?php
/**
 * Implemebts hook_preprocess_node().
 */
function horisont_preprocess_node(&$variables) {
  $node = $variables['node'];
  //Preprocess function
  $preprocess_function = "_preprocess_" . $node->type;
  //If preprocess function exists, call it
  if (function_exists($preprocess_function)) {
    $preprocess_function($node);
  }
}

/**
 * Preprocess a business partner.
 */
function _preprocess_become_business_partner(&$node) {
  $result = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', 'testimonial')
    ->orderRandom()
    ->range(0, 5)
    ->execute();
  foreach ($result as $row) {
    $testimonial = node_load($row->nid);
    $company = node_load($testimonial->field_sourcecompany['und'][0]['nid']);
    $testimonial->company = $company->field_supplier_company['und'][0]['value'];
    $testimonial->link = 'node/' . $company->nid;
    $node->testimonial[] = $testimonial;
  }
}

/**
 * Preprocess of article
 * - loads author data
 * - load tag-data
 */
function _preprocess_article(&$node) {
  $node->author = user_load($node->uid);
  $node->category = NULL;
  if ($node->field_type['und'][0]['value'] == 3) {
    if ($_SERVER['HTTP_HOST'] == "www.csr.dk" || $_SERVER['HTTP_HOST'] == "csr.dk") {
      $domain = "csr.dk";
    }
    if ($_SERVER['HTTP_HOST'] == "www.installator.dk" || $_SERVER['HTTP_HOST'] == "installator.dk") {
      $domain = "installator.dk";
    }
    if ($_SERVER['HTTP_HOST'] == "www.scm.dk" || $_SERVER['HTTP_HOST'] == "scm.dk") {
      $domain = "scm.dk";
    }
    if (isset($domain)) {
      $node->author->mail = "redaktion@" . $domain;
    }
  }
  if (isset($node->field_category["und"][0]["tid"])) {
    $node->category = taxonomy_term_load($node->field_category["und"][0]["tid"]);
  }
  $node->company = NULL;
  if (isset($node->field_company["und"][0]["tid"])) {
    $node->company = taxonomy_term_load($node->field_company["und"][0]["tid"]);
  }
  // Create an array of company to display info about in the bottom of the page.
  if (isset($node->field_company['und'][0]['tid'])) {
    $supplier = _get_supplier_by_company_tid($node->field_company['und'][0]['tid']);
    // Check if the supplier is a business partner.
    if ($supplier && isset($supplier->field_subscription_type['und'][0]['value']) && $supplier->field_subscription_type['und'][0]['value'] === '1') {
      $display_supplier = TRUE;
      // If the stop on expiration is set, check the expiration date.
      if ($supplier->field_stop_on_expiration['und'][0]['value']) {
        $expiration = strtotime($supplier->field_expiration_date['und'][0]['value']);
        $display_supplier = time() < $expiration;
      }
      if ($display_supplier) {
        $node->supplier = $supplier;
      }
    }
  }
}

/**
 * Preprocess a job.
 */
function _preprocess_job(&$node) {
  if (isset($node->field_company['und'][0]['tid'])) {
    $supplier = _get_supplier_by_company_tid($node->field_company['und'][0]['tid']);
    if (!isset($node->field_logo['und'][0])) {
      if (isset($supplier->field_logo['und'][0]['fid'])) {
        $node->field_logo = $supplier->field_logo;
      }
    }
  }
  $node->geo = taxonomy_term_load($node->field_job_geo['und'][0]['tid']);
}

/**
 * Preprocess of event
 * - loads supplier logo via uid
 */
function _preprocess_event_post(&$node) {
  $supplier = db_select('node', 'n');
  $supplier->join("field_data_field_company", "com", "com.entity_id = n.nid");
  if (isset($node->field_company['und'][0]['tid'])) {
    $supplier = $supplier
      ->condition("com.field_company_tid", $node->field_company['und'][0]['tid']);
  }
  $supplier = $supplier
    ->fields('n')
    ->condition('n.type', 'supplier')
    ->execute()
    ->fetchAssoc();
  if ($supplier) {
    $supplier = node_load($supplier['nid']);
    $node->supplier_logo = $supplier->field_logo;
    $node->supplier_company = $supplier->field_supplier_company;
    $node->supplier_id = $supplier->nid;
    if (isset($node->field_event_type['und'][0]['tid'])) {
      $event_type = taxonomy_term_load($node->field_event_type['und'][0]['tid']);
      $node->event_type = $event_type->name;
    }
  }
}
