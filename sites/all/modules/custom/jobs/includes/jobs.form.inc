<?php
/**
 * Implementation of hook_form
 */
function _jobs_create_job_form($form, &$form_state, $node = NULL) {
  $supplier = _get_supplier();
  $companyname = $addr = $contact = $www = $email = $phone = $publish = $start_date = "";
  $reminder = 1;
  $applicationdeadline_soon = $startdate_soon = 0;
  $regions = taxonomy_get_tree(10);
  $reg = array();
  foreach ($regions as $region) {
    $reg[$region->tid] = $region->name;
  }
  $publish = $start_date = FALSE;
  if ($node !== NULL && $node->nid) {
    $publish = (!empty($node->field_applicationdeadline['und'])) ? date('Y-m-d', strtotime($node->field_applicationdeadline['und'][0]['value'])) : '';
    $start_date = isset($node->field_startdate['und'][0]['value']) ? date('Y-m-d', strtotime($node->field_startdate['und'][0]['value'])) : '';
//    $tmp_logo_path = (isset($node->field_logo['und'][0])) ? image_cache('280px', $node->field_logo['und'][0]) : '';

    $applicationdeadline_soon = (isset($node->field_applicationdeadline_soon['und'][0]['value'])) ? $node->field_applicationdeadline_soon['und'][0]['value'] : 0;
    $startdate_soon = (isset($node->field_startdate_soon['und'][0]['value'])) ? $node->field_startdate_soon['und'][0]['value'] : 0;

    $geo = $node->field_job_geo['und'][0]['tid'];
    $reminder = $node->field_reminder['und'][0]['value'];
  }
  elseif ($supplier) {
    $addr = (!empty($supplier->field_address['und'])) ? $supplier->field_address['und'][0]['value'] : '';
    $www = (!empty($supplier->field_www['und'])) ? $supplier->field_www['und'][0]['value'] : '';
    $email = (!empty($supplier->field_email['und'])) ? $supplier->field_email['und'][0]['value'] : '';
    $companyname = (!empty($supplier->field_supplier_company['und'])) ? $supplier->field_supplier_company['und'][0]['value'] : '';
    $phone = (!empty($supplier->field_phone['und'])) ? $supplier->field_phone['und'][0]['value'] : '';
    $zip = (!empty($supplier->field_postal_code['und'])) ? $supplier->field_postal_code['und'][0]['value'] : '';
    $city = (!empty($supplier->field_city['und'])) ? $supplier->field_city['und'][0]['value'] : '';
  }

  if (!isset($geo) || !$geo) {
    $geo = 1390;
  }
  if ($supplier) {
    $image_def = $supplier->field_logo['und'];
  }
  else {
    $image_def = $node->field_logo['und'];
  }
  if (!empty($image_def)) {
    $logo = theme_image_style(
      array(
        'style_name' => '225x135px',
        'path' => $image_def[0]['uri'],
        'width' => $image_def[0]['width'],
        'height' => $image_def[0]['height'],
      )
    );
  }
  $top_jobs = array(
    1 => 'Basis-job: <span class="job-description">' . variable_get('admin_job_basis_job_text') . '</span>',
    2 => 'Top-job: <span class="job-description">' . variable_get('admin_job_top_job_text') . '</span>',
  );

  $form = array();
  if ($node !== NULL && $node->nid) {
    $form['node_id'] = array(
      "#type" => "hidden",
      '#default_value' => $node->nid,
    );
  }
  $form['logo_cont'] = array(
    "#type" => "fieldset",
    '#attributes' => array('class' => array('wrapper-fielset')),
  );

  // if ($supplier->type == 'partner_profile')
  if (empty($supplier)) {
    $form['logo_cont']['job_top'] = array(
      '#title' => 'Jobtype',
      '#type' => 'radios',
      '#required' => true,
      '#options' => $top_jobs,
      '#default_value' => 1,
    );
  }
  elseif ($supplier->type == 'partner_profile') {
    $form['logo_cont']['job_top'] = array(
      '#type' => 'hidden',
      '#default_value' => 2,
    );
  }
  $form['logo_cont']['job_logo'] = array(
    "#title" => "Firmalogo *",
    "#type" => "file",
    "#description" => variable_get("admin_job_logo_text"),
  );
  if (isset($logo) && $logo != '') {
    $form['logo_cont']['img_thumb'] = array(
      '#type' => 'markup',
      '#markup' => $logo,
    );
  }

  $form['job_headline'] = array(
    "#title" => "Overskrift",
    "#type" => "textfield",
    "#required" => true,
    "#description" => variable_get("admin_job_headline_text"),
  );

  $form['job_teaser'] = array(
    "#title" => "Teaser tekst",
    "#type" => "textarea",
    "#description" => variable_get("admin_job_teaser_text"),
    "#required" => true,
  );

  $form['job_description'] = array(
    "#title" => "Stillingsbeskrivelse",
    "#type" => "textarea",
    "#required" => true,
    '#attributes' => array('class' => array('ckeditor')),
    "#description" => variable_get("admin_job_jobdescription_text"),
  );
  if ($supplier) {
    $form['publish_reminder'] = array(
      '#type' => 'checkbox',
      '#title' => '',
      '#default_value' => $reminder,
    );
    $notify_help = variable_get('admin_notify_help', '');
    $form['publish_reminder']['#description'] = '<label>' . t('Vil du notificeres ifm. publicering') . '</label>'. $notify_help;
  }
  $form['job_title'] = array(
    "#title" => "Jobtitel",
    "#type" => "textfield",
    "#required" => true,
    "#description" => variable_get("admin_job_jobtitle_text"),
  );

  $form['job_type'] = array(
    "#title" => "Ans&aelig;ttelsesforhold",
    "#type" => "textfield",
    "#required" => true,
    "#description" => variable_get("admin_job_jobtype_text"),
  );

  $form['job_applicationdeadline'] = array(
    "#title" => "Ans&oslash;gningsfrist*",
    "#type" => "textfield",
    "#description" => variable_get('admin_job_deadline_text', 'V&aelig;lg dato'),
    '#default_value' => $publish,
  );
  if ($applicationdeadline_soon) {
    $form['job_applicationdeadline']['#disabled'] = TRUE;
    $form['job_applicationdeadline']['#default_value'] = 'Hurtigst muligt';
  }
  $form['job_applicationdeadline_soon'] = array(
    "#type" => "checkbox",
    '#description' => 'Hurtigst muligt',
    '#default_value' => $applicationdeadline_soon,
  );

  $form['job_startdate'] = array(
    "#title" => "Tiltr&aelig;delsesdato*",
    "#type" => "textfield",
    "#description" => variable_get('admin_job_start_text', 'V&aelig;lg dato'),
    '#default_value' => $start_date,
  );
  if ($startdate_soon) {
    $form['job_startdate']['#disabled'] = TRUE;
    $form['job_startdate']['#default_value'] = 'Snarest muligt';
  }

  $form['job_startdate_soon'] = array(
    "#type" => "checkbox",
    '#description' => 'Snarest muligt',
    '#default_value' => $startdate_soon,
  );

  $form['job_companyname'] = array(
    "#title" => "Firmanavn",
    "#type" => "textfield",
    "#required" => true,
    "#default_value" => $companyname,
    "#description" => variable_get("admin_job_company_text"),
  );

  $form['job_contactperson'] = array(
    "#title" => "Kontaktperson",
    "#type" => "textfield",
    "#required" => true,
    "#description" => variable_get("admin_job_contactperson_text"),
  );

  $form['job_address'] = array(
    "#title" => "Adresse",
    "#type" => "textfield",
    "#required" => true,
    "#default_value" => $addr,
    "#description" => variable_get("admin_job_address_text"),
  );

  $form['job_zipcode'] = array(
    "#title" => "Postnummer",
    "#type" => "textfield",
    "#required" => true,
    "#default_value" => isset($zip) ? $zip : '',
    "#description" => variable_get("admin_job_zipcode_text"),
  );

  $form['job_city'] = array(
    "#title" => "By",
    "#type" => "textfield",
    "#required" => true,
    "#default_value" => isset($city) ? $city : '',
    "#description" => variable_get("admin_job_city_text"),
  );

  $form['job_website'] = array(
    "#title" => "Hjemmeside",
    "#type" => "textfield",
    "#required" => true,
    "#default_value" => $www,
    "#description" => variable_get("admin_job_website_text"),
  );

  $form['job_email'] = array(
    "#title" => "E-mail",
    "#type" => "textfield",
    "#required" => true,
    "#default_value" => $email,
    "#description" => variable_get("admin_job_email_text"),
  );

  $form['job_phone'] = array(
    "#title" => "Telefonnummer",
    "#type" => "textfield",
    "#required" => true,
    "#default_value" => $phone,
    "#description" => variable_get("admin_job_phone_text"),
  );

  $form['job_geography'] = array(
    "#type" => "select",
    "#options" => $reg,
    "#title" => "Geografi",
    "#default_value" => $geo,
    "#description" => variable_get("admin_job_geography_text"),
  );
  $form['subs'] = array(
    '#type' => 'fieldset',
    '#attributes' => array('class' => array('wrapper-fielset')),
  );
  $form['subs']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Opret jobopslag',
  );
  $form['subs']['preview'] = array(
    '#type' => 'button',
    '#value' => 'Preview',
  );

  if (isset($node->nid) && $node->nid > 0) {
    $fields = _get_field_array();

    foreach ($fields as $key => $field) {
      if (isset($node->$field)) {
        $f = $node->$field;
      }
      if (isset($f['und'][0]['value']) && isset($form[$key])) {
        $form[$key]['#default_value'] =  $f['und'][0]['value'];
      }
      else if (isset($f['und'][0]['value']) && isset($form['fieldset'][$key])) {
        $form['fieldset'][$key]['#default_value'] =  $f['und'][0]['value'];
      }
    }
    $form['submit']['#value'] = "Gem jobopslag";
  }
  $form['#attributes']['enctype'] = 'multipart/form-data';
  $form['#attributes']['class'] = array('job-create-form');
  $form['#attributes']['class'][] = 'form-partmer-cms';

  return $form;
}


function _jobs_create_job_form_validate(&$form, &$form_state) {
  global $user;
  $valid_images = array('image/jpg', 'image/png', 'image/gif', 'image/jpeg');
  $i = "job_logo";
  if ((isset($_FILES['files']['size']['job_logo']) && $_FILES['files']['size']['job_logo'] > 0)) {
    $file = new stdClass();
    $file->uid = $user->uid;
    $file->filename = trim($_FILES['files']['name'][$i]);
    $file->uri = $_FILES['files']['tmp_name'][$i];
    $file->filemime = file_get_mimetype($file->filename);
    $file->filesize = $_FILES['files']['size'][$i];

    if (in_array($file->filemime, $valid_images)) {
      $form_state['values']['job_logo'] = $file;
    }
    else {
      form_set_error('img', 'Billede kan ikke uploades.');
    }
  }

  if (empty($form_state['values']['job_applicationdeadline']) && $form_state['values']['job_applicationdeadline_soon'] != 1) {
    form_set_error("job_applicationdeadline","Ans&oslash;gningsfrist skal s&aelig;ttes");
  }

  if (empty($form_state['values']['job_startdate']) && $form_state['values']['job_startdate_soon'] != 1) {
    form_set_error("job_startdate", "Tiltr&aelig;delsesdatoen skal v&aelig;re senere end ans&oslash;gningsfristen");
  }

  if (!empty($form_state['values']['job_applicationdeadline']) &&
    (!empty($form_state['values']['job_startdate'])) &&
    $form_state['values']['job_applicationdeadline_soon'] != 1 &&
    $form_state['values']['job_startdate_soon'] != 1
  ) {
    if (strtotime($form_state['input']['job_applicationdeadline']) > strtotime($form_state['input']['job_startdate'])) {
      form_set_error("job_startdate", "Tiltr&aelig;delsesdatoen skal v&aelig;re senere end ans&oslash;gningsfristen");
    }
  }

  $errors = form_get_errors();

  $form ['#prefix'] = '';
  $form_state['complete form']['#prefix'] = '';
  if ($errors) {
    foreach ($errors as $er) {
      drupal_set_message($er, 'job_error');
    }
  }

}

function _jobs_create_nobusiness_job_form_submit($form, &$form_state) {
  $preview = FALSE;
  if ($form_state['values']['op'] == 'Preview') {
    $preview = TRUE;
  }

  $a = _get_field_array();

  $node = _get_node("job");
  $sendmail = false;
  if (!$node) {
    $node = new stdClass;
    $node->type = "job";
    $node->title = $form_state['values']['job_headline'];
    $node->language = "da";
    $node->uid = 0;
    $node->status = 1;
    $sendmail = true;
  }
  if (isset($form_state['values']['job_top'])) {
    $node->field_jobopslag_job_type['und'][0]['value'] = $form_state['values']['job_top'];
  }

  foreach ($a as $key => $field) {
    if ($key === 'job_applicationdeadline') {
      if (isset($form_state['input']['job_applicationdeadline'])) {
        $form_state['values']['job_applicationdeadline'] = $form_state['input']['job_applicationdeadline'];
      }
      // Checks if "Snarest muligt" or "Løbende" is selected. If so, it sets the deadline to one month from current date.
      if (intval($form_state['input']['job_applicationdeadline_soon']) === 1) {
        $form_state['values']['job_applicationdeadline'] = 0;
      }
    }
    if ($key === 'job_startdate') {
      if (isset($form_state['input']['job_startdate'])) {
        $form_state['values']['job_startdate'] = $form_state['input']['job_startdate'];
      }
      // Checks if "Snarest muligt" or "Løbende" is selected. If so, it sets the deadline to one month from current date.
      if (intval($form_state['input']['job_startdate_soon']) === 1) {
        $form_state['values']['job_startdate'] = 0;
      }
    }

    if (!empty( $form_state['values'][$key])) {
      $field_array = array("und" => array(0 => array("value" => $form_state['values'][$key])));
      $node->$field  = $field_array;
    }
  }
  $node->field_startdate_soon['und'][0]['value'] = $form_state['input']['job_startdate_soon'];
  $node->field_applicationdeadline_soon['und'][0]['value'] = $form_state['input']['job_applicationdeadline_soon'];

  $node->field_jobdescription['und'][0]['format']  = 'filtered_html';

  if (!empty($_SESSION[$form_state['values']['images_session']])) {
    $node->field_logo[LANGUAGE_NONE][0] = $_SESSION[$form_state['values']['images_session']];
  }

  $image_file = $form_state['values']['job_logo'];
  $node->field_job_geo['und'][0]['tid'] = $form_state['values']['job_geography'];
  if ($image_file) {
    $directory = 'public://jobs/';
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    $fileinfo = image_get_info($image_file->uri);
    $destination = file_stream_wrapper_uri_normalize($directory . $image_file->filename . $fileinfo['extension']);
    if ($final_file = file_move($image_file, $destination, FILE_EXISTS_REPLACE)) {
      $saved_file = file_save($final_file);
      $node->field_logo[LANGUAGE_NONE][0] = (array)$saved_file;
    }
  }
  if (!$preview) {
    node_save($node);
    if ($sendmail) {
      $server_name = $_SERVER['SERVER_NAME'];

      if (strpos($server_name, 'www') > -1) {
        $server_name = substr($server_name, strpos($server_name, 'www') + 4);
      }

      $headers = "From: " . $server_name . "<no-reply@" . $server_name . ">\r\n";
      $headers.= "reply-to: no-reply@" . $server_name . "\r\n";

      $mailtext = variable_get("admin_job_nobusiness_mail");
      $mailtext = str_replace("#LINK#", url("node/" . $node->nid, array("absolute" => true)), $mailtext);
      //    mail($node->field_email['und'][0]['value'], "Kvittering for oprettelse af jobopslag", $mailtext, $headers);

      $params = array(0 => $mailtext);
      drupal_mail("jobs", "jobs_receipt_nopartner", $node->field_email['und'][0]['value'] , "DA_dk", $params, variable_get("site_mail") , TRUE );
      $mailtext = variable_get("admin_job_administrator_mail_text_nobusiness");
      $mailtext = str_replace("#LINK#", url("node/" . $node->nid, array("absolute" => true)), $mailtext);
      $params = array(0 => $mailtext);
      drupal_mail("jobs", "jobs_admin_notify_nopartner", "kth@dwarf.dk" , "DA_dk", $params, variable_get("site_mail") , TRUE );
    }

    _schedule_job($node);


    drupal_goto("nytjob", array("query" => array("receipt" => 1)));
  }
  else {
    return $node;
  }
}

function _jobs_create_job_form_submit($form, &$form_state) {
  $preview = FALSE;
  if ($form_state['values']['op'] == 'Preview') {
    $preview = TRUE;
  }
  global $user;
  $a = _get_field_array();
  $node = _get_node("job");
  $sendmail = false;
  $new_node = false;
  if (!$node) {
    $new_node = TRUE;
    $node = new stdClass;
    $node->type = "job";
    $node->title = $form_state['values']['job_headline'];
    $node->language = "da";
    $node->uid = $user->uid;
    $sendmail = true;
  }
  global $user;
  $node->field_company['und'][0]['tid'] = _get_user_company_tid();
  if (isset($form_state['values']['job_top'])) {
    $node->field_jobopslag_job_type['und'][0]['value'] = $form_state['values']['job_top'];
  }
  foreach ($a as $key => $field) {
    if ($key === 'job_applicationdeadline') {
      if (isset($form_state['input']['job_applicationdeadline'])) {
        $form_state['values']['job_applicationdeadline'] = $form_state['input']['job_applicationdeadline'];
      }
      // Checks if "Snarest muligt" or "Løbende" is selected. If so, it sets the deadline to one month from current date.
      if (intval($form_state['input']['job_applicationdeadline_soon']) === 1) {
        $form_state['values']['job_applicationdeadline'] = 0;
      }
    }
    if ($key === 'job_startdate') {
      if (isset($form_state['input']['job_startdate'])) {
        $form_state['values']['job_startdate'] = $form_state['input']['job_startdate'];
      }
      // Checks if "Snarest muligt" or "Løbende" is selected. If so, it sets the deadline to one month from current date.
      if (intval($form_state['input']['job_startdate_soon']) === 1) {
        $form_state['values']['job_startdate'] = 0;
      }
    }

    if (!empty($form_state['values'][$key])) {
      $field_array = array("und" => array(0 => array("value" => $form_state['values'][$key])));
      $node->$field  = $field_array;
    }
    else {
      $node->$field  = array();
    }
  }
  $node->field_startdate_soon['und'][0]['value'] = $form_state['input']['job_startdate_soon'];
  $node->field_applicationdeadline_soon['und'][0]['value'] = $form_state['input']['job_applicationdeadline_soon'];

  $node->field_job_geo['und'][0]['tid'] = $form_state['values']['job_geography'];

  $node->field_jobdescription['und'][0]['value'] = $form_state['values']['job_description'];
  $node->field_jobdescription['und'][0]['format']  = 'filtered_html';

  $node->field_reminder['und'][0]['value'] = $form_state['values']['publish_reminder'];

  $image_file = $form_state['values']['job_logo'];
  if (!$preview) {
    node_save($node);
    if ($new_node) {
      partner_cms_publish_reminder_submit($node);
    }
  }

  $supplier = _get_supplier();
  if (is_object($supplier)) {
    $supplier_wrap = entity_metadata_wrapper('node', $supplier);
    $node->field_logo[LANGUAGE_NONE][0] = $supplier_wrap->field_logo->value();
  }

  if (isset($form_state['values']['images_session']) && !empty($_SESSION[$form_state['values']['images_session']])) {
    $node->field_logo[LANGUAGE_NONE][0] = $_SESSION[$form_state['values']['images_session']];
  }
  if ($image_file) {
    $directory = 'public://suppliers/jobs/';
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    $fileinfo = image_get_info($image_file->uri);
    $destination = file_stream_wrapper_uri_normalize($directory . $image_file->filename . '.' . $fileinfo['extension']);
    if ($final_file = file_move($image_file, $destination, FILE_EXISTS_REPLACE)) {
      $saved_file = file_save($final_file);
      $node->field_logo[LANGUAGE_NONE][0] = (array)$saved_file;
    }
  }
  if (!$preview) {
    if (is_object($supplier)) {
      $supplier_wrap->field_logo->set($node->field_logo[LANGUAGE_NONE][0]);
      $supplier_wrap->save();
    }

    node_save($node);
    if ($sendmail) {
      $server_name = $_SERVER['SERVER_NAME'];

      if (strpos($server_name, 'www') > -1) {
        $server_name = substr($server_name, strpos($server_name, 'www') + 4);
      }

      $headers = "From: " . $server_name . "<no-reply@" . $server_name . ">\r\n";
      $headers.= "reply-to: no-reply@" . $server_name . "\r\n";

      $mailtext = variable_get("admin_job_business_mail");
      $mailtext = str_replace("#LINK#", url("node/" . $node->nid, array("absolute" => true)), $mailtext);

      $params = array(0 => $mailtext);
      drupal_mail("jobs", "jobs_receipt_partner", $node->field_email['und'][0]['value'] , "DA_dk", $params, variable_get("site_mail") , TRUE );

      $mailtext = variable_get("admin_job_administrator_mail_text_business");
      $mailtext = str_replace("#LINK#", url("node/" . $node->nid, array("absolute" => true)), $mailtext);
      $params = array(0 => $mailtext);
      //  mail(variable_get("admin_job_administrator_mail"), "Jobopslag oprettet - Businesspartner", $mailtext, $headers);
      drupal_mail("jobs", "jobs_admin_notify_partner", variable_get("admin_job_administrator_mail") , "DA_dk", $params, variable_get("site_mail") , TRUE );
    }

    _schedule_job($node);

    drupal_goto('joboversigt');
//    header("Location: /joboversigt");
    exit;
  }
  else {
    return $node;
  }
}

function _jobs_create_nobusiness_job_form_validate(&$form, &$form_state) {
  $valid_images = array('image/jpg', 'image/png', 'image/gif', 'image/jpeg');

  $i = "job_logo";
  if ((isset($_FILES['files']['size']['job_logo']) && $_FILES['files']['size']['job_logo'] > 0)) {
    $file = new stdClass();
    $file->uid = $user->uid;
    $file->filename = trim($_FILES['files']['name'][$i]);
    $file->uri = $_FILES['files']['tmp_name'][$i];
    $file->filemime = file_get_mimetype($file->filename);
    $file->filesize = $_FILES['files']['size'][$i];

    if (in_array($file->filemime, $valid_images)) {
      $form_state['values']['job_logo'] = $file;
    }
    else {
      form_set_error('img', 'Billede kan ikke uploades.');
    }
  }

  if (empty($form_state['values']['job_applicationdeadline']) && $form_state['values']['job_applicationdeadline_soon'] != 1) {
    form_set_error("job_applicationdeadline","Ans&oslash;gningsfrist skal s&aelig;ttes");
  }
  if (!empty($form_state['values']['job_applicationdeadline']) &&
    (!empty($form_state['values']['job_startdate'])) &&
    $form_state['values']['job_applicationdeadline_soon'] != 1 &&
    $form_state['values']['job_startdate_soon'] != 1
  ) {
    if (strtotime($form_state['input']['job_applicationdeadline']) > strtotime($form_state['input']['job_startdate'])) {
      form_set_error("job_startdate", "Tiltr&aelig;delsesdatoen skal v&aelig;re senere end ans&oslash;gningsfristen");
    }
  }


  if (empty($form_state['values']['job_startdate']) && $form_state['values']['job_startdate_soon'] != 1) {
    form_set_error("job_startdate", "Tiltr&aelig;delsesdatoen skal v&aelig;re senere end ans&oslash;gningsfristen");
  }



  $errors = form_get_errors();

  $form ['#prefix'] = '';
  $form_state['complete form']['#prefix'] = '';
  if ($errors) {
    foreach ($errors as $er) {
      drupal_set_message($er, 'job_error');
    }
  }
//  return $form;

}