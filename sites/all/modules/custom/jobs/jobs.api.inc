<?php

/**
 * Returns the number of jobs from a supplier.
 *
 * @param int $tid The taxonomy term ID of the supplier.
 * @return int The number of jobs from the given supplier.
 */
function jobs_get_jobs_count($tid) {
  return db_select('field_data_field_company', 'c')
    ->fields('c', array('entity_id'))
    ->condition('c.field_company_tid', $tid)
    ->condition('c.bundle', 'job')
    ->execute()
    ->rowCount();
}

function jobs_get_nodes_for_unpublish() { 
  $nodes = array();
  $date_format = variable_get('scheduler_date_format', 'Y-m-d H:i:s');
  $now = format_date(time(), 'custom', $date_format);
  $res = db_select("node", "n");
  $res->join("field_data_field_applicationdeadline", "jd", "n.nid = jd.entity_id");
  $res->condition("n.type", 'job');
  $res->condition("n.status", 1);
  $res->condition("jd.field_applicationdeadline_value", $now, '<');
  $res->fields('n', array('nid', 'title'));
  $ress = $res->execute()->fetchAll();
  foreach ($ress as $item) {
    $nodes[] = $item->nid;
  }
  return $nodes;
}

function jobs_get_types_job() { 
  $res = db_select("field_revision_field_jobtype", "jt");
  $res->join("node", "n", "n.nid = jt.entity_id");
  $res = $res
    ->condition("n.type", 'job')  
    ->fields('jt', array('field_jobtype_value'));
  $ress = $res->execute()->fetchAll();
  
  $types['All'] = 'All';
  foreach ($ress as $item) {
    $types[$item->field_jobtype_value] = truncate_utf8($item->field_jobtype_value, 25, TRUE, TRUE);
//    $types[$item->field_jobtype_value] = $item->field_jobtype_value;
  }
  return $types;
}



/**
 * Content function for the Job page
 */
function _jobs_get_jobs() {
  _delete_node("job");
//  $data = _get_supplier_data("job");
//  $content = theme("jobs_overview", array("content" =>$data));
  $attributes = array(
    'title' => 'Mine jobopslag',
    'path' => 'opretjob',
    'text_link' => 'Opret jobopslag'
  );
  $content = theme_custom_title($attributes);
  $content .= '<div class="overall-description">' . variable_get('admin_job_posts_description', '') . '</div>';
  $content .= views_embed_view('lists_user_content', 'partner_cms_job');
  return theme("supplier_tab", array("content" => $content, "context" => "joboversigt"));
}