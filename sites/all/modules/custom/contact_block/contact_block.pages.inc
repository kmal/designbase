<?php
// $Id$

/**
 * @file
 * Page callbacks for voucher pages
 */

/**
 * Dashboard callback function
 */
function _contact_block_callback() {
  $out = '<h1>Contact Requests</h1>';
  $out .= '<p>Listing of contact requests via contact block module.</p>';
  $headers = array('Id', 'Name', 'Company', 'Email', 'Phone', 'Date');
  $rows = _contact_block_get_contact_requests(); 
  $out .= theme('table', array('header' => $headers, 'rows' => $rows));
  $out .= theme('pager');
  return $out;
}

function _contact_block_get_contact_requests() {
  $query = "SELECT * FROM {c_contact_request} ORDER by date DESC"; 
  $res = db_select('c_contact_request', 'c')
          ->fields('c')
          ->orderBy('date', 'DESC')
          ->extend('PagerDefault')
            ->limit(25)
          ->execute();
  
  $result = array();
  foreach ($res as $o) {
    $tmp['id'] = $o->id; 
    $tmp['name'] = $o->name; 
    $tmp['company'] = $o->company; 
    $tmp['email'] = $o->email; 
    $tmp['phone'] = $o->phone; 
    $tmp['date'] = $o->date; 
    $result[] =  $tmp;
  }
  return $result; 
}
