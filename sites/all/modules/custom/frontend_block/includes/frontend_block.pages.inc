<?php

function frontend_block_create_user() {
  $data = '<div class="page-user-create page-create">';
  $node = node_load(11945);
  if (is_object($node)) {
    $node->title = $node->field_headline['und'][0]['value'];
    $node_view = node_view($node,  'teaser');
    $data .= drupal_render($node_view);
  }
  
  $form = drupal_get_form('user_register_form');
  $form['type_user'] = array(
    '#type' => 'hidden',
    '#name' => 'type_user',
    '#value' => 'user',
  );
  if (arg(0) != 'admin') {
    $form['account']['field_terms_and_conditions'] = array(
      '#type' => 'checkbox',
      '#title' => variable_get('admin_newsletter_subscription', ''),
      '#weight' => 10,
      '#checked' => TRUE
    );
  }
  $form['actions']['submit']['#value'] = 'Opret bruger';
  $form['field_company']['#access'] = FALSE;  
  $data .= drupal_render($form);
  
  $data .= '</div>';
  
  return $data;
}


function frontend_block_showroom_create(){
  $data = '<div class="page-showroom-create page-create" id="page-showroom-create">';
  $node = node_load(11946);
  if (is_object($node)) {
    $node->title = $node->field_headline['und'][0]['value'];
    $node_view = node_view($node,  'teaser');
    $data .= drupal_render($node_view);
  }
  
  $detect = mobile_detect_get_object();
  $is_mobile = $detect->isMobile();
  if ($is_mobile) {
    $form_contact = module_invoke('contact_block', 'block_view', 'contact_form');
    $data .= '<div id="user-register-form">' . render($form_contact['content']) . '</div>';
  }
  else {
    $form = drupal_get_form('user_register_form');
    $form['type_user'] = array(
      '#type' => 'hidden',
      '#name' => 'type_user',
      '#value' => 'partner',
    );
    if (arg(0) != 'admin') {
      $form['account']['field_terms_and_conditions'] = array(
        '#type' => 'checkbox',
        '#weight' => 10,
        '#checked' => TRUE
      );
      $form['account']['terms_and_conditions'] = array(
        '#type' => 'markup',
        '#weight' => 11,
        '#markup' => '<a href="#terms" class="inline-box">Jeg har l&aelig;st og accepterer abonnements og forretningsvilk&aring;r</a>',
      );          
    }
    $form['actions']['submit']['#value'] = 'Opret showroom';
    $form['field_company']['#required'] = FALSE;
    
    $form['#suffix'] = '<div id="terms" class="hidden">' . variable_get('admin_terms_and_conditions', '') .  '<div class="close-terms" ><i class="fa fa-times"></i></div></div>';
    $data .= drupal_render($form);
  }
  
  
  $data .= '</div>';
  
  return $data;
}

function frontend_block_user_login_page(){
  $output = '';
  if ($user->uid == 0) {
    $output .= '<div id="login-form-button-content" class=""><div class="content">';
    $form = drupal_get_form('user_login_block');
    
    $output .= drupal_render($form);
    $output .= '</div>';
  }
  
  return $output;
}

function frontend_block_node_gallery_page($node) {
  $node_wrap = entity_metadata_wrapper('node', $node);
  $images = $node_wrap->field_image->value();
  $a = '';
  foreach ($images as $image) {
    $params = array(
      'style_name' => 'test',
      'path' => $image['uri'],
      'alt' => (isset($image['alt'])) ? $image['alt'] : NULL,
      'title' => $image['title'],
      'width' => (isset($image['width'])) ? $image['width'] : NULL,
      'height' => (isset($image['height'])) ? $image['height'] : NULL,
      'attributes' => array('class' => array('image')),
      'getsize' => FALSE,
    );
    $a .= '<div class="item">'. theme('image_style', $params) . '</div>';
  }
  return drupal_json_output($a);
}


/**
 * Provides ajax callback for form submit
 */
function frontend_block_user_create_form() {
  module_load_include('pages.inc', 'user');
 
  list($form, $form_state) = ajax_get_form();
 
  drupal_process_form($form['#form_id'], $form, $form_state);
 
  $commands = array();
  
  if (isset($form_state['uid']) && $form_state['uid'] > 0) {
    $user = user_load($form_state['uid']);
    if (array_key_exists(4, $user->roles)) {
      $node = node_load(11522);
    }
    else {
      $node = node_load(11521); 
    }
    $node->title = $node->field_headline['und'][0]['value'];
    $node_view = node_view($node,  'teaser');
    $content = '<div class="page-user-create">';
    $content .= drupal_render($node_view);
    $content .= l('Gå til min konto', 'user', array('attributes' => array('class' => 'back'))); 
    $content .= '</div>';
    $commands[] = ajax_command_replace('.page-create', $content);
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  else {
    $commands[] = ajax_command_html('.page-create #ajax-forms-messages', theme('status_messages'));
    return array('#type' => 'ajax', '#commands' => $commands);
  }
}


function frontend_block_user_login_page_form(){
  module_load_include('pages.inc', 'user');
  list($form, $form_state) = ajax_get_form();
  drupal_process_form($form['#form_id'], $form, $form_state);
  $commands = array();
  if ($form_state['executed'] == TRUE) {
    global $user;
    $user = user_load($form_state['uid']);
    
    // user_login_finalize
    watchdog('user', 'Session opened for %name.', array('%name' => $user->name));
    $user->login = REQUEST_TIME;
    db_update('users')->fields(array('login' => $user->login))->condition('uid', $user->uid)->execute();
    drupal_session_regenerate();

    ctools_include('ajax');
    ctools_add_js('ajax-responder');
    global $base_url;
    $commands[] = ctools_ajax_command_redirect($base_url . '/user');
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  else {
    $commands[] = ajax_command_html('#login-form-button-content #ajax-forms-messages', theme('status_messages'));

    return array('#type' => 'ajax', '#commands' => $commands);
  }
}

function frontend_block_preview_callback() {
  list($form, $form_state) = ajax_get_form();
  drupal_process_form($form['#form_id'], $form, $form_state);
  ctools_include('ajax');
  ctools_add_js('ajax-responder');
  global $base_url;
  $commands[] = ctools_ajax_command_redirect($base_url . '/user');
  return array('#type' => 'ajax', '#commands' => $commands);
}
