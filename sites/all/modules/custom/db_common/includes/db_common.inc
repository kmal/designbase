<?php

function showroom_create_link(){
  $link = '';
  global $user;
  $attributes = array(
			'attributes' => array(
        'class' => array('link-plus', 'ctools-use-modal'), 
        'target' => '_blank'
      )
    );
  if ($user->uid == 0) {
    $link = l('Opret showroom', 'modal/showroom_create/nojs', $attributes);
  }
  return $link;
}

function db_common_disqus_comments_count($node) {
  if (is_object($node)) {  
    if (user_access('view disqus comments') && isset($node->disqus)) {
      $disqus = $node->disqus;
      $content = array(
        '#theme' => 'link',
        '#text' => t('Comments'),
        '#path' => $disqus['identifier'],
        '#options' => array(
          'fragment' => 'disqus_thread',
          'attributes' => array(
            'data-disqus-identifier' => $disqus['identifier'],
          ),
          'html' => FALSE,
        ),
      );

      $content['#attached'] = array(
        'js' => array(
          array('data' => drupal_get_path('module', 'disqus') . '/disqus.js'),
          array(
            'data' => array('disqusComments' => $disqus['domain']),
            'type' => 'setting',
          ),
        ),
      );
      
      $result = drupal_render($content);
      return $result;
    } 
  }
}