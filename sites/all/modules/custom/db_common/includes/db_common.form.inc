<?php


function custom_search_form($form, &$form_state){
  $form['submit_sub'] = array(
    '#type' => 'markup', 
    '#markup' => '<div class="form-submit"><i class="fa fa-search"></i></div>',
  );
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => 'search',
    '#attributes' => array(
      'class' => array('hidden'),
    ),
  );
  $form['s'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#attributes' => array(
      'class' => array(
        'form-item', 
        'form-item-search-block-form', 
      )
    ),
    '#prefix' => '<div class="search-item dddd">',
    '#suffix' => '</div>',
    '#title' => 'Søg'
  );
  return $form;
}

function custom_search_form_submit(&$form, &$form_state){
  if ($form_state['values']['s']) {
    $options = array(
      'query' => array('s' => $form_state['values']['s']),
    );
    drupal_goto('search_result', $options);
  }
}