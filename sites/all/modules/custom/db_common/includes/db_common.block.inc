<?php

function _db_common_login_button_content(){
  global $user;
  $output = '<div id="login-form-button">';

  if ($user->uid == 0) {
    $output .= t('Login');
  }
  else {
    $output .= l(t('Min Konto'), 'user');
  }
  $output .= '<i class="fa fa-user"></i></div>';
  if ($user->uid == 0) {
    $output .= '<div id="login-form-button-content" class=""><div class="content">';
    $form = drupal_get_form('user_login_block');
    $output .= drupal_render($form);
    $output .= '</div></div>';
  }
  return $output;
}

function _db_common_left_menu_link_content(){
  $output = '<div id="left-menu-link">INDHOLD</div>';
  return $output;
}

function _db_common_right_top_navigation_content() {
  $output = '';
  $items[] = l("<i class='fa fa-th'></i>", 'gallery', array('attributes' => array('class' => array('gallery')), 'html' => TRUE));
  
  global $user;
  $url = ($user->uid > 0) ? 'my_desigbase' : 'modal/user_create';
  $options['attributes']['class'][] = 'my-designbase';
  $options['html'] = TRUE;
  $count_my_desigbase = count_my_desigbase();
  $item = l("<i class='fa fa-heart-o'></i><sup>$count_my_desigbase</sup>", $url, $options);
  if ($user->uid == 0) {
    $item .= '<div class="description hidden">' . variable_get('admin_mydesigbase_info_text') . '</div>';
  }
  $items[] = $item;   
  
  return theme('item_list', array('items' => $items));
}

function _db_common_social_media_content() {
  if ($link = variable_get("admin_facebook", "")) {
    $items[] = "<a href='$link' class='facebook'><i class='fa fa-facebook'></i></a>";
  }
  if ($link = variable_get("admin_linkedin", "")) {
    $items[] = "<a href='$link' class='linkedin'><i class='fa fa-linkedin'></i></a>";
  }
  if ($link = variable_get("admin_instagram", "")) {
    $items[] = "<a href='$link' class='instagram'><i class='fa fa-instagram'></i></a>";
  }
  
  return theme('item_list', array('items' => $items));
}

function _db_common_main_navigation(){
  global $user;
  $output = '';
  $result['search'] = module_invoke('db_common', 'block_view', 'custom_search_form');
//  $result['login'] = module_invoke('db_common', 'block_view', 'login_button');
  $result['category_menu'] = module_invoke('taxonomy_menu', 'block_view', 'category_menu');
  $result['menu'] = module_invoke('menu_block', 'block_view', '3');
  $result['bottom_menu'] = module_invoke('menu_block', 'block_view', '2');
  $result['social_media'] = module_invoke('db_common', 'block_view', 'social_media');
  
  foreach ($result as $k => $res) {
    if ($k == 'category_menu') {
      $url = ($user->uid > 0) ? 'user' : 'modal/user_login';
      $text_link = ($user->uid > 0) ? 'Min Konto' : 'Log ind';
      if ($user->uid > 0) {
        $output .= '<div class="two-blocks login clearfix">';
      }
      else {
        $output .= '<div class="two-blocks clearfix">';
      }
      $output .= '<div class="block-content login">' . 
          l('<div class="icon"><i class="fa fa-user"></i></div>' . $text_link . '</a>', $url, array('html' => TRUE)  ) . 
          '</div>';
      
      if ($user->uid == 0) {
        $output .= 
          '<div class="block-content opret-bruger">' . 
          l('<div class="icon"><i class="fa fa-plus"></i></div>Opret bruger', 'modal/user_create', array('html' => TRUE)) . 
          '</div>';
      }
      else {
        $output .= 
          '<div class="block-content opret-bruger">' . 
          l('<div class="icon"><i class="fa fa-sign-out"></i></div>Log ud', 'user/logout', array('html' => TRUE)) . 
          '</div>';
      }
      $url = ($user->uid > 0) ? 'my_desigbase' : 'modal/user_create';
      $output .= '<div class="block-content my_designbase">' . 
          l('<div class="icon"><i class="fa fa-heart-o"></i></div>Min designbase', $url, array('html' => TRUE)) .
          '</div>';
      $output .= '</div>';
    }
    $output .= '<div class="block-content ' . $k . '">' . render($res['content']) . '</div>';
  }
  
  return $output;
}
