<?php

function _db_common_get_partner_profile($uid = NULL) {
  if ($uid) {
    $query = new EntityFieldQuery();
    $result = $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'partner_profile')
      ->propertyCondition('uid', $uid)
      ->execute();
    foreach ($result['node'] as $node) {
      $nid = $node->nid;
    }
    return $nid;
  }
}

function count_my_desigbase(){
  global $user;
  $res = db_select("flagging", "f");
  $res->join("node", "n", "n.nid = f.entity_id");
  $res = $res
//    ->condition("n.status", 1)  
    ->condition("f.fid", 1)
    ->condition("f.uid", $user->uid)
    ->countQuery()
    ->execute()
    ->fetchField();
  return $res;
}

function db_common_get_latest_company_update($company = NULL) {
  if ($company) {
    $query = db_select('node', 'n');
    $query->innerJoin('field_data_field_company', 'c', 'n.nid = c.entity_id');
    $query->condition('c.field_company_tid', $company);
    $query->fields('n', array('changed', 'nid', 'title'));
    $query->orderBy('n.changed', 'DESC');
    $query->range(0, 1);
    $result = $query->execute();
    
    foreach($result as $r) {
      $update = $r->changed;
    } 
    return $update;
  }
}

function db_common_delete(){
  $query = db_select('node', 'n');
  $query->condition('n.created', '1428575402', '>');
  $query->fields('n', array('changed', 'nid', 'title'));
  $query->orderBy('n.changed', 'DESC');
  $result = $query->execute();

  foreach($result as $r) {
    node_delete($r->nid);
  } 
}

function db_common_node_sticky_update($nid_current = NULL){
  if ($nid_current) {    
    $nodes = db_select('node', 'n')
      ->fields('n', array('nid',))
      ->condition('n.type', 'article')
      ->condition('n.sticky', '1')
      ->execute()
      ->fetchAll();
    foreach ($nodes as $item) {
      $nids[$item->nid] = $item->nid;
    }
    unset($nids[$nid_current]);
    
    if (!empty($nids)) {      
      db_update('node')
        ->fields(array('sticky' => '0'))
        ->condition('nid', $nids, 'IN')
        ->execute();

      db_update('node_revision')
        ->fields(array('sticky' => '0'))
        ->condition('nid', $nids, 'IN')
        ->execute();
    }
  }
}
