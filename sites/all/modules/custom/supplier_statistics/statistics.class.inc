<?php

// Field constants
define('SUPPLIER_TYPE', '2');
define('BUSINESS_PARTNER', '1');
define('EVENT_PARTNER', '2');
// Tables
define('TBL_STATISTICS_CACHE', 'statistics_cache');
define('TBL_STATISTICS_ENTRY', 'statistics_entry');
define('TBL_STATISTICS_AGGREGATE', 'statistics_aggregate');

/**
 * @file
 * Statistics class
 */

class Statistics {

  /**
   * List of exposure views.
   * @var array
   */
  protected $exposure_views = array(
    'latest_news',
    'showroom',
    'indsustry_news_block',
    'jobs',
    'events_event',
    'related_content',
    'frontpage_events',
    'brandprofiles'
    // Supplier Articles
//    'latest_news-latest_news'                   => 'article',
//    'frontpage_secondary_supplier_art-default'  => 'article',
//    'more_supplier_news-default'                => 'article',
//    // Events
//    'events_event-default'                      => 'event_post',
//    'frontpage_events-default'                  => 'event_post',
//    'supplier_archive-events'                   => 'event_post',

    // Jobs
//    'jobs-block'                                => 'job',
    
//    'supplier_archive-jobs'                     => 'job',
//    // Name Articles
//    'frontpage_name_articles-default'           => 'name_article',
//    'name_articles_view-default'                => 'name_article',
//    'supplier_name_articles-default'            => 'name_article',
//    // Suppliers
//    'supplierguide_suppliers-default'           => 'supplier',
  );

  /**
   * Returns the list of exposure views.
   * @return array Views
   */
  public function getExposureViews() {
    return $this->exposure_views;
  } // function getExposureViews

  /**
   * Checks if the specified path is a node view.
   * @param  string  $path Content path
   * @return boolean       TRUE if path is a node view
   */
  public function isNodeViewPath($path) {
    $valid = FALSE;

    // Sanity check
    if (is_string($path)) {
      $path_parts = arg(NULL, $path);
      $valid = count($path_parts) == 2 && $path_parts[0] == 'node';
    } // if

    return $valid;
  } // function isNodeViewPath

  /**
   * Checks if a node is valid for tracking
   * @param  object  $node Node to check for tracking
   * @return boolean       TRUE if node is valid for tracking
   */
  public function isNodeValid($node) {
    // Validate node
    $valid = is_object($node) && $node->status;
    $valid = $valid && isset($node->field_company['und'][0]['tid']);
    // Validate type
    if ($valid) {
      switch ($node->type) {
        // Supplier Articles
        case 'article':
//          $valid = isset($node->field_type['und'][0]['value']);
//          if ($valid) {
//            $valid = $node->field_type['und'][0]['value'] == SUPPLIER_TYPE;
//          } // if
          break;

        // Events
        case 'event_post':
          break;

        // Jobs
        case 'job':
          break;
        
        // Product news
        case 'product_news':
          break;
        
        // Industry news
        case 'industry_news':
          break;

        // Name Articles
        case 'name_article':
          break;

        // Suppliers
        case 'partner_profile':
          $valid = isset($node->field_subscription_type['und'][0]['value']);
          if ($valid) {
            $valid = $node->field_subscription_type['und'][0]['value'] != 2;
          } // if
          break;

        default:
          $valid = FALSE;
          break;
      } // switch
    } // if

    return $valid;
  } // function isNodeValid

  /**
   * Adds tracking to cache.
   * @param string $uri      URI for tracking
   * @param string $tracking Tracking type
   * @param array  $values   Values to cache
   */
  public function addCache($uri, $tracking, array $values) {
    // Check values
    if (is_string($uri) && is_string($tracking) && !empty($values)) {
      $data = serialize($values);
      // Insert query.
      $query = db_insert(TBL_STATISTICS_CACHE)
        ->fields(array('uri' => $uri, 'tracking' => $tracking, 'data' => $data))
        ->execute();
    } // if
  } // function addCache

  /**
   * Fetches cache for a specified URI.
   * @param  string $uri      URI for tracking
   * @param  string $tracking (optional) Tracking type
   * @return array            Cache content
   */
  public function fetchCache($uri, $tracking = NULL) {
    // Cache
    $cache = array();
    
    if (is_string($uri)) {
      // Cache query
      $query = db_select(TBL_STATISTICS_CACHE, 'c')
        ->fields('c')
        ->condition('uri', $uri);

      // Limit tracking if specified
      if (is_string($tracking)) {
        $query->condition('tracking', $tracking);
      } // if

      // Execute query
      $results = $query->execute();
      // Unserialize values
      foreach ($results as $row) {
        $cache[] = array(
          'tracking' => $row->tracking,
          'data' => unserialize($row->data),
        );
      } // foreach
    } // if

    return $cache;
  } // function fetchCache

  /**
   * Deletes cache for a specified URI.
   * @param  string $uri      URI for tracking
   * @param  string $tracking (optional) Tracking type
   */
  public function deleteCache($uri, $tracking = NULL) {
    if (is_string($uri)) {
      // Delete query
      $query = db_delete(TBL_STATISTICS_CACHE)
        ->condition('uri', $uri);

      // Limit tracking if specified
      if (is_string($tracking)) {
        $query->condition('tracking', $tracking);
      } // if

      // Execute query
      $query->execute();
    } // if
  } // function deleteCache

  /**
   * Adds tracking entries.
   * @param string $tracking Tracking type
   * @param array  $values   Values
   */
  public function addEntries($tracking, array $values, $useragent = NULL) {
    // If no user agent is supplied process the one from the server vars
    if (!isset($useragent) && isset($_SERVER['HTTP_USER_AGENT'])) {
      $useragent = $_SERVER['HTTP_USER_AGENT'];
    } // if
    // Add defaults just in case
    $values += array(
      'values' => array(),
//      'timestamp' => '1427925600',
      'timestamp' => REQUEST_TIME,
    );

    // Check entries count
    if (is_string($tracking) && !empty($values['values'])) {
      // Insert query.
      $query = db_insert(TBL_STATISTICS_ENTRY)
        ->fields(array('nid', 'type', 'tracking', 'timestamp', 'useragent'));

      // Add values
      foreach ($values['values'] as $nid => $type) {
        $query->values(array($nid, $type, $tracking, $values['timestamp'], $useragent));
      } // foreach

      // Execute insertion
      $query->execute();
    } // if
  } // function addEntries

  /**
   * Aggregates tracking entries.
   */
  public function aggregateEntries($limit = NULL) {
    // Fetch entries
    $entries = $this->fetchEntries($limit);
    $last_id = 0;

    $aggregate = array();
    foreach ($entries as $entry) {
      $id = (int)$entry->id;
      $date = date('Y-m-d', $entry->timestamp);
      // Ensure arrays exist
      if (!isset($aggregate[$date][$entry->tracking][$entry->type][$entry->nid])) {
        if (!isset($aggregate[$date][$entry->tracking][$entry->type])) {
          if (!isset($aggregate[$date][$entry->tracking])) {
            if (!isset($aggregate[$date])) {
              $aggregate[$date] = array();
            } // if
            $aggregate[$date][$entry->tracking] = array();
          } // if
          $aggregate[$date][$entry->tracking][$entry->type] = array();
        } // if
        $aggregate[$date][$entry->tracking][$entry->type][$entry->nid] = 0;
      } // if
      // Increment entry count
      $aggregate[$date][$entry->tracking][$entry->type][$entry->nid]++;
      // Check last id
      if ($id > $last_id) {
        $last_id = $id;
      } // if
    } // foreach

    // Make sure there's aggregate data
    if (count($aggregate)) {
      foreach ($aggregate as $date => &$trackings) {
        foreach ($trackings as $tracking => &$types) {
          foreach ($types as $type => &$nodes) {
            foreach ($nodes as $nid => $count) {
              $values = array(
                'nid' => $nid,
                'type' => $type,
                'tracking' => $tracking,
                'count' => $count,
                'date' => $date,
              );
              // Update aggregate data
              $this->updateAggregate($values);
            } // foreach
          } // foreach
        } // foreach
      } // foreach
    } // if
    // Cleanup old entries
    $this->deleteEntries($last_id);
  } // function aggregateEntries

  protected function fetchEntries($limit) {
    // Select query
    $query = db_select(TBL_STATISTICS_ENTRY, 'e')
      ->fields('e')
      ->condition('is_processed', 0);
    if (!empty($limit)) {
      $query->range(0, $limit);
    }

    // Execute query
    $results = $query->execute();

    // Entries
    $entries = array();
    foreach ($results as $row) {
      $entries[] = $row;
    } // foreach

    return $entries;
  } // function fetchEntries

  protected function deleteEntries($id) {
    if ($id) {
      // Delete query
      db_update(TBL_STATISTICS_ENTRY)
        ->fields(array('is_processed' => 1))
        ->condition('id', $id, '<=')
        ->condition('is_processed', 0)
        ->execute();
    } // if
  } // function deleteEntries

  protected function updateAggregate(array $values) {
    // Update query
    $num_updated = db_update(TBL_STATISTICS_AGGREGATE)
      ->expression('count', 'count + :count', array(':count' => $values['count']))
      ->condition('nid', $values['nid'])
      ->condition('type', $values['type'])
      ->condition('tracking', $values['tracking'])
      ->condition('date', $values['date'])
      ->execute();

    // Update failed - new entry
    if (!$num_updated) {
      db_insert(TBL_STATISTICS_AGGREGATE)
        ->fields($values)
        ->execute();
    } // if
  } // function updateAggregate

  /**
   * Returns the related supplier for the specified user id.
   * @param  int $uid User ID
   * @return object   Supplier node object or NULL if none were found
   */
  public function getRelatedSupplier($uid) {
    $supplier = NULL;

    $user = user_load($uid);
    // User checks
    if (is_object($user) && isset($user->field_company['und'][0]['tid'])) {
      $company_tid = $user->field_company['und'][0]['tid'];
      // Supplier query
      $row = db_select('field_data_field_company', 'c')
        ->fields('c', array('entity_id'))
        ->condition('entity_type', 'node')
        ->condition('bundle', 'partner_profile')
        ->condition('field_company_tid', $company_tid)
        ->execute()
        ->fetchAssoc();

      // Sanity check
      if (is_array($row)) {
        $node = node_load($row['entity_id']);
        // Ensure node is valid
        if (is_object($node)) {
          $supplier = $node;
        } // if
      } // if
    } // if

    return $supplier;
  } // function getRelatedSupplier

  /**
   * Fetches various statistics for a given supplier in a specified timeframe.
   * @param  object $supplier  Suppler object
   * @param  string $date_from Date from
   * @param  string $date_to   Date to
   * @return array             Statistics array
   */
  public function fetch($supplier, $date_from, $date_to) {
    $company_tid = 0;
    if (is_object($supplier)) {
      $company_tid = (int)$supplier->field_company['und'][0]['tid'];
    } // if
    $statistics = array();
    // Generate dates array
    $dates = array();
    $date_loop = $date_from;
    while ($date_loop <= $date_to) {
      $dates[$date_loop] = 0;
      $date_loop = date('Y-m-d', strtotime($date_loop . ' +1 day'));
    } // while
    // Generate aggregates arrays
    $this->generateAggregatesArrays($statistics, $dates);
    // Fetch aggregates
    $aggregates = $this->fetchAggregates($company_tid, $date_from, $date_to);
    if (count($aggregates)) {
      // Add aggregates
      foreach ($aggregates as &$row) {
        $nid = (int)$row->nid;
        $type = $row->type;
        $tracking = $row->tracking;
        $count = (int)$row->count;
        $date = $row->date;

        // Ensure arrays exist
        if (!isset($statistics[$type]['entities'][$nid])) {
          $statistics[$type]['entities'][$nid] = date('(d-m-Y) ', $row->created) . $row->title;
        } // if
        if (!isset($statistics[$type][$tracking][$nid])) {
          $statistics[$type][$tracking][$nid] = array(
            'dates' => $dates,
            'sum' => 0,
          );
        } // if

        $statistics[$type][$tracking]['all']['dates'][$date] += $count;
        $statistics[$type][$tracking]['all']['sum'] += $count;
        $statistics[$type][$tracking][$nid]['dates'][$date] += $count;
        $statistics[$type][$tracking][$nid]['sum'] += $count;
      } // foreach
    } // if

    // Generate activities arrays
    $this->generateActivitiesArrays($statistics);
    // Fetch activities
    $activities = $this->fetchAggregates($company_tid);
    if (count($activities)) {
      $types = array(
        'article' => 'Nyhed',
        'product_news' => 'Produktomtale',
        'partner_profile' => 'Virksomhedsprofil',
        'job' => 'Jobopslag',
        'event_post' => 'Event',
        'name_article' => 'Navnenyt',
        'industry_news' => 'Branchenyt',
      );
      // Add activities
      foreach ($activities as &$row) {
        $nid = (int)$row->nid;
        $type = $row->type;
        $tracking = $row->tracking;
        $count = (int)$row->count;
        $date = $row->date;

        // Check entity
        if (!isset($statistics['entities'][$nid])) {
          $statistics['entities'][$nid] = array(
            'type' => $types[$type],
            'title' => $row->title,
            'timestamp' => $row->created,
            'created' => date('d-m-Y', $row->created),
            'pageview' => 0,
            'exposure' => 0,
          );
          // Add type count
          $statistics['activities'][$type]['count']++;
        } // if

        // Increment activities
        $statistics['activities'][$type][$tracking] += $count;
        $statistics['entities'][$nid][$tracking] += $count;
      } // foreach
    } // if
    // $activities = $this->fetchActivities($date_from, $date_to);
    // if (count($activities)) {
    //   $activities_summary = array();
    //   foreach ($activities as $row) {
    //     $type = $row->type;
    //     $field_company_tid = (int)$row->field_company_tid;
    //     // Ensure arrays exist
    //     if (!isset($activities_summary[$type][$field_company_tid])) {
    //       if (!isset($activities_summary[$type])) {
    //         $activities_summary[$type] = array();
    //       } // if
    //       $activities_summary[$type][$field_company_tid] = 0;
    //     } // if
    //     $activities_summary[$type][$field_company_tid]++;
    //   } // foreach
    //   $statistics['activities'] = array();
    //   // Sort summaries and add to stats
    //   foreach ($activities_summary as $type => $summary) {
    //     arsort($summary);
    //     $supplier_count = isset($summary[$company_tid]) ? $summary[$company_tid] : 0;
    //     $bic_count = reset($summary);
    //     $statistics['activities'][$type] = array(
    //       'supplier' => $supplier_count,
    //       'bic' => $bic_count,
    //       'pct' => $supplier_count / $bic_count,
    //     );
    //   } // foreach
    // } // if

    return $statistics;
  } // function fetch

  protected function fetchAggregates($company_tid, $date_from = NULL, $date_to = NULL) {
    // Select query
    $query = db_select(TBL_STATISTICS_AGGREGATE, 'a');
    $query->innerJoin('node', 'n', 'a.nid = n.nid');
    $query->innerJoin('field_data_field_company', 'c', "n.nid = c.entity_id AND c.entity_type = 'node'");
    $query->fields('a');
    $query->fields('n', array('title', 'created'));
    if ($date_from && $date_to) {
      $query->condition('date', array($date_from, $date_to), 'BETWEEN');
    } // if 
    else {
      $query->condition('date', date('Y-m-d'), '<');
    }
    $query->condition('c.field_company_tid', $company_tid);
    $query->orderBy('n.created', 'DESC');

    // Execute query
    $results = $query->execute();

    // Aggregates
    $aggregates = array();
    foreach ($results as $row) {
      $aggregates[] = $row;
    } // foreach
    return $aggregates;
  } // function fetchAggregates

  protected function fetchActivities($date_from, $date_to) {
    // Make timestamps
    $ts_from = strtotime($date_from . ' 00:00:00');
    $ts_to = strtotime($date_to . ' 23:59:59');
    // Select query
    $query = db_select('node', 'n');
    $query->innerJoin('field_data_field_company', 'c', "n.nid = c.entity_id AND c.entity_type = 'node' AND c.bundle IN ('article','event_post','job','name_article','partner_profile')");
    $query->leftJoin('field_data_field_type', 't', "n.nid = t.entity_id AND t.entity_type = 'node' AND t.bundle = 'article'");
    $query->leftJoin('field_data_field_subscription_type', 'st', "n.nid = st.entity_id AND st.entity_type = 'node' AND st.bundle = 'partner_profile'");
    $query->fields('n', array('nid', 'type'));
    $query->addField('c', 'field_company_tid');
    $query->addField('t', 'field_type_value');
    $query->addField('st', 'field_subscription_type_value');
    $query->condition('n.created', array($ts_from, $ts_to), 'BETWEEN');

    // Execute query
    $results = $query->execute();

    // Aggregates
    $activities = array();
    foreach ($results as $row) {
      if ($this->isActivityValid($row)) {
        $activities[] = $row;
      } // if
    } // foreach

    return $activities;
  } // function fetchActivities

  protected function isActivityValid($activity) {
    // Validate activity
    $valid = !empty($activity->field_company_tid);
    // Validate type
    if ($valid) {
      switch ($activity->type) {
        // Supplier Articles
        case 'article':
          $valid = !empty($activity->field_type_value);
          if ($valid) {
            $valid = $activity->field_type_value == SUPPLIER_TYPE;
          } // if
          break;

        // Events
        case 'event_post':
          break;

        // Jobs
        case 'job':
          break;

        // Name Articles
        case 'name_article':
          break;

        // Suppliers
        case 'partner_profile':
          $valid = !empty($activity->field_subscription_type_value);
          if ($valid) {
            $valid = $activity->field_subscription_type_value != 2;
          } // if
          break;

        default:
          $valid = FALSE;
          break;
      } // switch
    } // if

    return $valid;
  } // isActivityValid

  protected function generateAggregatesArrays(array &$statistics, array $dates) {
    $aggregates = array(
      'article' => array('pageview', 'exposure'),
      'partner_profile' => array('pageview', 'exposure'),
      'job' => array('pageview', 'exposure'),
      'event_post' => array('pageview', 'exposure'),
      'name_article' => array('pageview', 'exposure'),
      'industry_news' => array('pageview', 'exposure'),
      'product_news' => array('pageview', 'exposure'),
    );

    foreach ($aggregates as $type => $trackings) {
      $statistics[$type] = array(
        'entities' => array(
          'all' => 'Alle',
        ),
      );
      foreach ($trackings as $tracking) {
        $statistics[$type][$tracking] = array(
          'flatline' => array(
            'dates' => $dates,
            'sum' => 0,
          ),
          'all' => array(
            'dates' => $dates,
            'sum' => 0,
          ),
        );
      } // foreach
    } // foreach
  } // function generateAggregatesArrays

  protected function generateActivitiesArrays(array &$statistics) {
    $statistics['activities'] = array();
    $statistics['entities'] = array();

    // Add types
    $types = array('article', 'partner_profile', 'job', 'event_post', 'name_article', 'industry_news', 'product_news');
    foreach ($types as $type) {
      $statistics['activities'][$type] = array(
        'count' => 0,
        'pageview' => 0,
        'exposure' => 0,
      );
    } // foreach
  } // function generateActivitiesArrays

} // class Statistics
