(function($,w,d,g,cookie) {
	// Custom round function - more precise
	var round = function(number, places) {
			if (!places) {
				places = 0;
			}
			return +(Math.round(number + 'e+' + places) + 'e-' + places);
		},
		format = function(number) {
			return number.toFixed(0).split(/(?=(?:\d{3})+$)/).join('.');
		};
	// Load the Visualization API library
	g.load('visualization', '1', {'packages': ['corechart'], 'language': 'da'});

	SS_GRAPH = function($chart, options) {
		var chart = new g.visualization.LineChart($chart[0]),
			$data = $chart.siblings('.data-chart').children().remove(),
			dataSet = {},
			dataTable = null,
			dataKey = 'flatline';

		$data.each(function() {
			var data = $(this).data();
			if ($.type(dataSet[data.key]) === 'undefined') {
				dataSet[data.key] = {
					title: data.value,
					sum: data.sum,
					values: []
				};
			}
			dataSet[data.key].values.push([data.date, data.value]);
		});

		var change = function(key) {
			dataKey = 'flatline';
			if ($.type(dataSet[key]) !== 'undefined') {
				dataKey = key;
			}
			var data = dataSet[dataKey];
			dataTable = g.visualization.arrayToDataTable(data.values);
			// Set options
			options.hAxis = {
				showTextEvery: Math.ceil((data.values.length - 1) / 7)
			};
			options.title = data.title + ': ' + format(data.sum) + ' i angivet periode';

			chart.draw(dataTable, options);
		};

		var getSum = function(type) {
			if (dataSet[dataKey].title == type) {
				return dataSet[dataKey].sum;
			}
			return 0;
		};

		return {
			change: change,
			getSum: getSum
		};
	};

	// Custom logic
	SS_GRAPHS = function() {
		var load = function() {
			var chart_options = $('.chart-options').remove().data();

                        // Default options
			var options = {
				animation: {
					duration: 1000,
					easing: 'out'
				},
				chartArea: {
					left: '5%',
					width: '90%'
				},
				colors: [chart_options.color],
				hAxis: {},
				vAxis: {
					maxValue: 20,
					textPosition: 'in',
					viewWindow: {
						min: 0
					}
				},
				legend: {
					position: 'none'
				},
				titleTextStyle: {
					bold: false
				}
			};
			$('.container-graphs .container-group').each(function() {
				var graphs = [],
					$clickrate = $(this).find('.clickrate-chart .value'),
					updateClickrate = function() {
						if ($clickrate.length > 0) {
							var pageview = 0,
								exposure = 0;
							$.each(graphs, function(i, graph) {
								pageview += graph.getSum('Sidevisninger');
								exposure += graph.getSum('Eksponeringer');
							});

							if (exposure > 0) {
								var clickrate = pageview * 100 / exposure;
								$clickrate.text(round(clickrate) + '%').attr('title', round(clickrate, 2) + '%');
							} else {
								$clickrate.text('-').attr('title', '-');
							}
						}
					};
				$(this).find('.google-chart').each(function() {
					var graph = new SS_GRAPH($(this), options);
					graph.change('all');
					graphs.push(graph);
				});
				updateClickrate();
        $(this).find('.select-chart').change(function() {
          var key = $(this).parent().find('select.select-chart').val();
					$.each(graphs, function(i, graph) {
						graph.change(key);
					});
					updateClickrate();
				});
			});
		};

		var init = function() {
			// Initialize charts
			g.setOnLoadCallback(load);
		};

		return {
			init: init
		}
	};

	SS_ENTITIES = function() {
		var $container_entities = null,
			$sorts = null,
			$entities = $(),
			entities = [],
			sorts = {
				type: {
					asc: function(a, b) {
						return a.type.localeCompare(b.type);
					},
					desc: function(a, b) {
						return b.type.localeCompare(a.type);
					}
				},
				title: {
					asc: function(a, b) {
						return a.title.localeCompare(b.title);
					},
					desc: function(a, b) {
						return b.title.localeCompare(a.title);
					}
				},
				created: {
					asc: function(a, b) {
						return a.timestamp - b.timestamp;
					},
					desc: function(a, b) {
						return b.timestamp - a.timestamp;
					}
				},
				pageview: {
					asc: function(a, b) {
						return a.pageview - b.pageview;
					},
					desc: function(a, b) {
						return b.pageview - a.pageview;
					}
				},
				exposure: {
					asc: function(a, b) {
						return a.exposure - b.exposure;
					},
					desc: function(a, b) {
						return b.exposure - a.exposure;
					}
				},
				clickrate: {
					asc: function(a, b) {
						return a.ctr - b.ctr;
					},
					desc: function(a, b) {
						return b.ctr - a.ctr;
					}
				}
			},
			settings = {
				sort_type: 'created',
				sort_order: 'desc',
				row_limit: 5
			};

		// Set cookie settings
//		cookie.json = true;

		var saveSettings = function() {
//			cookie('statistics', settings, {expires: 7});
		};

		var loadSettings = function() {
//			var tmpSettings = cookie('statistics');
//			if (tmpSettings) {
//				settings = tmpSettings;
//			}
		};

		var setupLimit = function() {
			var el = entities.length,
				limits = [10, 25, 50, 100, 500],
				$limit = $('<span>', {'class': 'limit'});

			// Treat case where a previous saved row limit exceeds current number of entitites
			if (settings.row_limit > el) {
				settings.row_limit = 5;
				saveSettings();
			}

			// Add limits
			for (var i = 0; i < limits.length; i++) {
				if (el > limits[i]) {
					var option_class = 'option';
					// Initial selected row limit
					if (limits[i] == settings.row_limit) {
						option_class += ' selected';
					}
					$limit.append($('<span>', {'class': option_class, 'data': {'limit': limits[i]}, 'text': limits[i]}));
					$limit.append($('<span>', {'class': 'sep', 'text': '/'}));
				}
			};

			// No point in show 'all' option if there are fewer than 5 entities
			if (el > 5) {
				var option_class = 'option';
				// Initial selected row limit
				if (settings.row_limit == 0) {
					option_class += ' selected';
				}
				$limit.append($('<span>', {'class': option_class, 'data': {'limit': 0}, 'text': 'Alle'}));
			}
			// Add click events
			$('.option', $limit).click(function() {
				settings.row_limit = $(this).data('limit');
				saveSettings();

				$(this).addClass('selected').siblings('.option').removeClass('selected');

				render();
			});
                        
                        

			// Add to headline
			$('p.entities-description').append($limit);
		};

		var setupSort = function() {
			$sorts = $();
			$('.header > span', $container_entities)
				.each(function() {
					// Use header class as typefor easier sorting
					$(this).data('type', $(this).attr('class'));
					var $sort = $('<span>', {'class': 'sort'});
					$sorts = $sorts.add($sort);
					$(this).append($sort);
				})
				.click(function() {
					var sort = $(this).data('type');
					if (settings.sort_type != sort) {
						settings.sort_order = 'desc';
					} else {
						settings.sort_order = settings.sort_order == 'asc' ? 'desc' : 'asc';
					}
					settings.sort_type = sort;

					saveSettings();
					sortData();
					render();

					$sorts.removeClass('asc desc');
					$('.sort', this).addClass(settings.sort_order);
				});

			// Add default sort
			$('.header > span.' + settings.sort_type + ' .sort').addClass(settings.sort_order);
		};

		var buildData = function() {
			var entity, row;
			for (var i = 0; i < entities.length; i++) {
				entity = entities[i];

				row = $('<div>', {'class': 'container-group'})
					.append($('<span>', {'class': 'type', 'text': entity.type}))
					.append($('<span>', {'class': 'title'})
						.append($('<a>', {'href': entity.url, 'title': entity.title, 'text': entity.title})))
					.append($('<span>', {'class': 'created', 'text': entity.created}))
					.append($('<span>', {'class': 'pageview', 'text': format(entity.pageview)}))
					.append($('<span>', {'class': 'exposure', 'text': format(entity.exposure)}))
					.append($('<span>', {'class': 'clickrate', 'text': entity.clickrate, 'title': round(entity.ctr, 2) + '%'}));
				entity._row = row;
			};
		};

		var sortData = function() {
			entities.sort(sorts[settings.sort_type][settings.sort_order]);
		};

		var render = function() {
			$entities.remove();
			$entities = $();
			var limit = entities.length;
			if (settings.row_limit > 0) {
				limit = Math.min(limit, settings.row_limit);
			}

			for (var i = 0; i < limit; i++) {
				$entities = $entities.add(entities[i]._row);
			};
			$container_entities.append($entities);
		};

		var init = function() {
			// Load container
			$container_entities = $('.container-entities');
			$container_entities.children('.entity').remove().each(function() {
				var data = $(this).data();
				data.ctr = 0;
				// Used for sorting clickrate
				if (data.exposure > 0) {
					data.ctr = data.pageview * 100 / data.exposure;
				}
				entities.push(data);
			});
			loadSettings();
			setupLimit();
			setupSort();
			buildData();
			sortData();
			render();
		};

		return {
			init: init
		}
	};

	$(function() {
		$('button.print').click(function(e) {
			e.preventDefault();
			$(this).blur();
			window.print();
			return false;
		});
		(new SS_GRAPHS).init();
		(new SS_ENTITIES).init();
	});
//})(jQuery,window,document,google,$.cookie);
})(jQuery,window,document,google);