<?php

/**
 * @file
 * Module file for supplier statistics
 */

/// ---------- FORMS ---------- ///

/**
 * Supplier statistics timeframe form.
 */
function supplier_statistics_selection_form($form, &$form_state) {
  // Set fieldset
  $form['dates'] = array(
    '#type'     => 'fieldset',
  );
  // Setup date fields
  $date_format = 'd-m-Y';
  $yesterday = strtotime('-1 day');
  $last_month = strtotime('-1 month', $yesterday);
  $selection_from_value = date('Y-m-d', $last_month);
  $selection_to_value = date('Y-m-d', $yesterday);
  // Check if values have been posted
  if (isset($form_state['values'])) {
    $selection_from_value = $form_state['values']['selection_from'];
    $selection_to_value = $form_state['values']['selection_to'];
  } // if
  $form['dates']['selection_from'] = array(
    '#type'                 => 'date_popup',
    '#required'             => TRUE,
    '#default_value'        => $selection_from_value,
    '#date_format'          => $date_format,
    '#date_label_position'  => 'none',
    '#date_year_range'      => '-3:0',
    '#datepicker_options'   => array(
      'maxDate'             => date('Y-m-d', $yesterday),
    ),
  );
  $form['dates']['selection_to'] = array(
    '#type'                 => 'date_popup',
    '#required'             => TRUE,
    '#default_value'        => $selection_to_value,
    '#date_format'          => $date_format,
    '#date_label_position'  => 'none',
    '#date_year_range'      => '-3:0',
    '#datepicker_options'   => array(
      'maxDate'             => date('Y-m-d', $yesterday),
    ),
  );
  // Add submit button
  $form['actions'] = array(
    '#type'                 => 'fieldset',
  );
  $form['actions']['submit'] = array(
    '#type'                 => 'submit',
    '#value'                => 'Vis',
    '#prefix'               => '<div class="input-button-general actionButton">',
    '#suffix'               => '</div>', 
  );

  return $form;
} // function supplier_statistics_selection_form

/**
 * Supplier statistics timeframe form validation.
 */
function supplier_statistics_selection_form_validate($form, &$form_state) {
  // Don't custom-validate if previous validation errors (still) exist
  if (form_get_errors()) return;
  // .. Otherwise, process custom form validation goes here
  $today = date('Y-m-d');
  $values = $form_state['values'];

  if ($values['selection_to'] >= $today) {
    form_set_error('selection_to', t('Date must be earlier than today.'));
  } // if
  if ($values['selection_from'] > $values['selection_to']) {
    form_set_error('selection_from', t('Date must be earlier or equal to end date.'));
  } // if
} // function supplier_statistics_selection_form_validate

/**
 * Supplier statistics timeframe form submission.
 */
function supplier_statistics_selection_form_submit($form, &$form_state) {
  // Rebuild the form
  $form_state['rebuild'] = TRUE;
} // function supplier_statistics_selection_form_submit

/// ---------- BLOCKS --------- ///

/**
 *  Implements hook_block_info().
 */
function supplier_statistics_block_info() {
  $blocks = array();
  $blocks['company_statistics'] = array(
    'info' => 'Company Statistics',
  );

  return $blocks;
} // function supplier_statistics_block_info

/**
 * Implements hook_block_view().
 */
function supplier_statistics_block_view($delta = '') {
  global $user;
  $statistics = new Statistics;
  $block = array();
  switch ($delta) {
    case 'company_statistics':
      // Get supplier
      $supplier = $statistics->getRelatedSupplier($user->uid);
      // Fetch form and extract dates
      $form = drupal_get_form('supplier_statistics_selection_form');
      $selection_from = $form['dates']['selection_from']['#default_value'];
      $selection_to = $form['dates']['selection_to']['#default_value'];
      // Fetch statistics
      $stats = $statistics->fetch($supplier, $selection_from, $selection_to);
      // Resolve block contents
      $content = theme('company_statistics', array(
        'content' => $stats,
        'form' => $form,
      ));
      $block = array(
        'subject' => t('Company Statistics'),
        'content' => theme('supplier_tab', array(
          'content' => $content,
          'context' => 'statistik',
          'supplier' => $supplier
        )),
      );
      break;
  }
  return $block;
} // function supplier_statistics_block_view

/**
 * Implements hook_theme().
 */
function supplier_statistics_theme() {
  $path = drupal_get_path('theme', variable_get('theme_default', NULL));
  return array(
    'company_statistics' => array(
      'template' => 'block-supplier-statistics',
      'path' => $path,
      'variables' => array(
        'content' => NULL,
        'form' => NULL,
      ),
    ),
    'company_statistics_graph' => array(
      'template' => 'block-supplier-statistics-graph',
      'path' => $path,
      'variables' => array(
        'content' => NULL,
        'entities' => NULL,
        'clickrate' => NULL,
        'type' => NULL,
        'description' => NULL,
      ),
    ),
    'company_statistics_bic' => array(
      'template' => 'block-supplier-statistics-bic',
      'path' => $path,
      'variables' => array(
        'content' => NULL,
        'type' => NULL,
      ),
    ),
    'company_statistics_activity' => array(
      'template' => 'block-supplier-statistics-activity',
      'path' => $path,
      'variables' => array(
        'content' => NULL,
        'type' => NULL,
      ),
    ),
  );  
} // function supplier_statistics_theme

/**
 * Calculates clickrate.
 * @param  int $pageviews Pageviews
 * @param  int $exposures Exposures
 * @param  int $precision Precision (default: 0)
 * @return string         Clickrate percentage
 */
function _sscc($pageviews, $exposures, $precision = 0) {
  if ($exposures > 0) {
    return round($pageviews * 100 / $exposures, $precision) . '%';
    // return sprintf("%0.{$precision}f%%", round($pageviews * 100 / $exposures, $precision));
  }
  return '-';
}

/// --------- TRACKING -------- ///

/**
 * Implements hook_views_pre_render().
 * Tracks exposures for views.
 */
function supplier_statistics_views_pre_render(&$view) {
  $uri = request_uri();
  $statistics = new Statistics;
  $views = $statistics->getExposureViews();
  // Check if view data should be processed
  $view_display_name = $view->name . '-' . $view->current_display;
  if (isset($views[$view_display_name])) {
    if (count($view->result)) {
      $values = array('values' => array());
      foreach ($view->result as $result) {
        $nid = 0;
        // Node ID for contextual based views :-/
        if (isset($result->field_company_taxonomy_term_data_nid)) {
          $nid = $result->field_company_taxonomy_term_data_nid;
        } // if
        // Node ID for normal views
        if (isset($result->nid)) {
          $nid = $result->nid;
        } // if
        // Load and validate node
        $node = node_load($nid);
        if ($statistics->isNodeValid($node)) {
          $values['values'][$node->nid] = $node->type;
        } // if
      } // foreach
      if (count($values['values'])) {
        $statistics->addCache($uri, 'exposure', $values);
      } // if
    } // if
  } // if
} // function supplier_statistics_views_pre_render

/**
 * Implements hook_exposure_track().
 * Custom exposure tracking hook invoked by custom modules.
 */
function supplier_statistics_exposure_track(array $nodes) {
  $uri = request_uri();
  // Loop through nodes and check for supplier articles
  if (count($nodes)) {
    $statistics = new Statistics;
    $values = array('values' => array());
    foreach ($nodes as $node) {
      if ($statistics->isNodeValid($node)) {
        $values['values'][$node->nid] = $node->type;
      } // if
    } // foreach
    if (count($values['values'])) {
      $statistics->addCache($uri, 'exposure', $values);
    } // if
  } // if
} // function supplier_statistics_exposure_track

/**
 * Implements hook_init().
 * Deletes existing cache for the current URI and tracks pageviews.
 */
function supplier_statistics_init() {
  $uri = request_uri();
  $statistics = new Statistics;
  // Delete cache
  $statistics->deleteCache($uri);

  // Check pageview tracking
  $path = current_path();
  if ($statistics->isNodeViewPath($path)) {
    $node = node_load(arg(1, $path));
    if ($statistics->isNodeValid($node)) {
      $values = array('values' => array($node->nid => $node->type));
      $statistics->addCache($uri, 'pageview', $values);
    } // if
  } // if
} // function supplier_statistics_init

/**
 * Implements hook_exit().
 * Adds tracking for the current URI based on cache.
 */
function supplier_statistics_exit() {
  // Ensure the tracking isn't done for robots
  $uas_parser = new UASParser;
  if (!$uas_parser->isRobot()) {
    $uri = request_uri();
    $statistics = new Statistics;
    // Fetch exposure cache and update entries
    $cache = $statistics->fetchCache($uri);
    if (count($cache)) {
      foreach ($cache as $entry) {
        if (is_array($entry['data'])) {
          $statistics->addEntries($entry['tracking'], $entry['data']);
        } // if
      } // foreach
    } // if
  } // if
} // function supplier_statistics_exit

/**
 * Implements hook_cron().
 * Aggregates accumulated tracking for each day.
 */
function supplier_statistics_cron() {
  $statistics = new Statistics;
  $statistics->aggregateEntries();
} // function supplier_statistics_cron

