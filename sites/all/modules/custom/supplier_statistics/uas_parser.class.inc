<?php

/**
 * @file
 * UASParser class
 * Extends the functionality from \UAS\Parser
 */

require_once dirname(__FILE__) . '/UASParser/UAS/Parser.php';

class UASParser extends Parser {

	/**
	 * Initiates the parser with a cache pointing to Drupal's files folder.
	 */
	public function __construct() {
		$path = variable_get('file_public_path', conf_path() . '/files') . '/uas_cache';
		$cache_dir = DRUPAL_ROOT . '/' . $path;
		parent::__construct($cache_dir);
		$this->doDownloads = false;
	} // function __construct

	/**
	 * Checks if user agent string belongs to a crawler bot.
	 * @param  string  $useragent User Agent
	 * @return boolean            TRUE if user agent belongs to a crawler bot
	 */
	public function isRobot($useragent = NULL) {
		$result = parent::parse($useragent);
		return $result['typ'] == 'Robot';
	} // function isRobot
} // class UASParser
