<?php
// $Id$

/**
 * Dwarf page settings module for Drupal 7.
 *
 * @file
 * @author Christoffer Kjeldgaard Petersen - Dwarf A/S <cpt@dwarf.dk>
 */

/**
 * Implementation of hook_schema.
 */
function page_settings_schema() {

  $schema = array();

  $schema['page_settings_groups'] = array(
    'description' => 'Table for storing page setting groups in fieldsets.',
    'fields' => array(
      'id' => array(
        'description' => 'Group ID field',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'Name of group.',
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );
  $schema['page_settings_types'] = array(
    'description' => 'Table for storing page settings types (text fields, text areas, etc.)',
    'fields' => array(
      'id' => array(
        'description' => 'Type ID field',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'type_name' => array(
        'description' => 'Name of type',
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );
  $schema['page_settings_variables'] = array(
    'description' => 'Table for storing page setting variable names.',
    'fields' => array(
      'id' => array(
        'description' => 'Variable ID field',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'group_id' => array(
        'description' => 'ID of the associated group',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'type_id' => array(
        'description' => 'ID of the associated type',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'name' => array(
        'description' => 'Name of the variable.',
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
      ),
      'help_text' => array(
        'description' => 'Help text for the variable.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'unique_variable' => array('name')
    ),
    'foreign keys' => array(
      'var_group' => array(
        'table' => 'page_settings_groups',
        'columns' => array('group_id' => 'id'),
      ),
      'var_type' => array(
        'table' => 'page_settings_types',
        'columns' => array('type_id' => 'id'),
      ),
    ),
    'indexes' => array(
      'var_groups' => array('group_id'),
      'var_types' => array('type_id'),
    ),
  );
  return $schema;
}

function page_settings_install() {

  // Create default types
  $type = new stdClass();
  $type->name = 'textfield';
  drupal_write_record('page_settings_types', $type);
  
  $type = new stdClass();
  $type->name = 'textarea';
  drupal_write_record('page_settings_types', $type);
  
  // Add foreign keys:
  db_query('ALTER TABLE page_settings_variables ADD FOREIGN KEY (group_id) REFERENCES page_settings_groups (id) ON DELETE CASCADE ON UPDATE CASCADE');
  db_query('ALTER TABLE page_settings_variables ADD FOREIGN KEY (type_id) REFERENCES page_settings_types (id) ON DELETE CASCADE ON UPDATE CASCADE');
}

function page_settings_uninstall() {
}
