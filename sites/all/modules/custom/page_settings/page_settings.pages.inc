<?php
// $Id$

/**
 * Pages for the page settings module
 *
 * @file
 * $author Christoffer Kjeldgaard Petersen - Dwarf A/S <cpt@dwarf.dk>
 */

/**
 * Calling the get_form function that uses the implemented hook_form 
 *
 * @return array The form.
 */
function _page_settings_form() {
  $form = drupal_get_form("page_settings_form");
  return $form;
}

/**
 * Calling the get_form function that uses the implemented hook_form for the page settings variables generation page for administrators.
 *
 * @return array The form.
 */
function _page_settings_admin() {
  $form = drupal_get_form("page_settings_admin_form");
  return $form;
}

/**
 * Gets the variable edit form based on page argument.
 */
function _page_settings_admin_edit($edit_id) {
  
  if (is_numeric($edit_id)) {
    $form = drupal_get_form("page_settings_admin_edit_form", $edit_id);
    return $form;
  }
}

function _page_settings_admin_add_group() {
  $form = drupal_get_form("page_settings_group_form");
  return $form;
}

/**
 * Confirm page for deleting variables.
 */
function page_settings_variable_delete_confirm($form, &$form_state, $id) {

  $sql_var = "SELECT * FROM {page_settings_variables} WHERE id = :id";
  $rs = db_query($sql_var, array(':id' => $id));
  $obj = $rs->fetch();

  return confirm_form($form,
    t('Are you sure you want to delete the variable %title?', array('%title' => $obj->name)),
    'admin/config/system/page_settings_admin',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Deletes a variable.
 */
function page_settings_variable_delete_confirm_submit($form, &$form_state) {
  $delete_id = $form_state['build_info']['args'][0];
  if (is_numeric($delete_id)) {

    $sql_var = "SELECT * FROM {page_settings_variables} WHERE id = :id";
    $rs = db_query($sql_var, array(':id' => $delete_id));
    $obj = $rs->fetch();

    variable_del($obj->name);
    $num_deleted = db_delete('page_settings_variables')->condition('id', $delete_id)->execute();
  
    drupal_set_message(filter_xss("Variable '" . $obj->name . "' deleted."));
    drupal_goto("admin/config/system/page_settings_admin");
  }
}

/**
 * Gets the group edit form based on page argument.
 */
function _page_settings_admin_group_edit($edit_id) {
  if (is_numeric($edit_id)) {
    $form = drupal_get_form("page_settings_admin_group_edit_form", $edit_id);
    return $form;
  }
}

/**
 * Confirm page for deleting groups.
 */
function page_settings_group_delete_confirm($form, &$form_state, $id) {
  $sql_var = "SELECT * FROM {page_settings_groups} WHERE id = :id";
  $rs = db_query($sql_var, array(':id' => $id));
  $obj = $rs->fetch();

  $sql_variables = "SELECT id, name FROM {page_settings_variables} WHERE group_id = :id";
  $rs_vars = db_query($sql_variables, array(':id' => $id));
  $var_list = "";
  if ($rs_vars->rowCount() > 0) {
    $var_list = "The following variables associated with this group will also be deleted.<ul>\n";
    foreach ($rs_vars as $var) {
      $var_list .= "<li>" . l($var->name, 'admin/config/system/page_settings_admin/edit/' . $var->id) . "</li>\n";
    }
    $var_list .= "</ul>\n";
  }
  
  return confirm_form($form,
    t('Are you sure you want to delete the group %title?', array('%title' => $obj->name)),
    'admin/config/system/page_settings_admin',
    t($var_list . 'This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Deletes a group.
 */
function page_settings_group_delete_confirm_submit($form, &$form_state) {
  $delete_id = $form_state['build_info']['args'][0];
  if (is_numeric($delete_id)) {

    $sql_var = "SELECT * FROM {page_settings_groups} WHERE id = :id";
    $rs = db_query($sql_var, array(':id' => $delete_id));
    $obj = $rs->fetch();

    // Delete associated variables:
    $sql_variables = "SELECT name FROM {page_settings_variables} WHERE group_id = :id";
    $rs_vars = db_query($sql_variables, array(':id' => $delete_id));
    foreach ($rs_vars as $var) {
      variable_del($var->name);
    }
    
    $num_deleted = db_delete('page_settings_groups')->condition('id', $delete_id)->execute();
  
    drupal_set_message(filter_xss("Group '" . $obj->name . "' deleted."));
    drupal_goto("admin/config/system/page_settings_admin");
  }
}
