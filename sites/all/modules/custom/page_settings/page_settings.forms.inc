<?php
// $Id$

/**
 * Forms for the page settings module
 *
 * @file
 * @author Christoffer Kjeldgaard Petersen - Dwarf A/S <cpt@dwarf.dk>
 */
 
/**
 * Implementation of hook_form. Defines the page settings form for editors.
 *
 * @return array The form.
 */
function page_settings_form() {
  $form = array();

  $sql_groups = "SELECT * FROM {page_settings_groups} ORDER BY name";
  $rs_groups = db_query($sql_groups);
  
  foreach ($rs_groups as $fieldset) {
    // First create fieldset
    $form[$fieldset->name] = array(
      "#type" => "fieldset",
      "#title" => filter_xss($fieldset->name),
      "#collapsed" => TRUE,
      "#collapsible" => TRUE,
      );
    
    $sql_vars = "SELECT v.*, t.type_name FROM {page_settings_variables} v INNER JOIN {page_settings_types} t ON v.type_id = t.id WHERE v.group_id = :group_id ORDER BY v.name";
    $rs_vars = db_query($sql_vars, array(':group_id' => $fieldset->id));
    foreach ($rs_vars as $var) {
      if ($var->type_name == "text_format") {
        $v = variable_get($var->name);
        $v = $v['value'];
      }
      else {
        $v = variable_get($var->name);
      }
    $form[$fieldset->name][$var->name] = array(
        "#type" => $var->type_name,
        "#title" => filter_xss(_page_settings_human_readable($var->name)),
        "#description" => filter_xss($var->help_text),
        "#default_value" => $v,
      );
      
    }
  }
  
  // Add submit button
  $form['submit_button'] = array(
    "#type" => "submit",
    "#submit" => array("_page_settings_save"),
    "#value" => t("Save Settings" ),
  );
  
  return $form;
}

/**
 * Saving all the data to the variable table.
 */
function _page_settings_save($form, &$form_state) {

  foreach ($form_state['values'] as $key => $form_elm) {
    if (strpos($key, "admin_") !== FALSE) {
      variable_set($key, $form_elm);
    }
  }
  
  drupal_set_message(t('The page settings has been submitted.'));
}

/**
 * Implementation of hook_form. The administration of page settings variables for administrators. Add, edit and delete variables and groups.
 *
 * @return array The form.
 */
function page_settings_admin_form() {
  $form = array();

  $form['vars'] = array(
    "#type" => "fieldset",
    "#title" => "Variables",
    "#collapsed" => FALSE,
    "#collapsible" => TRUE,
  );
  $form['vars']['vars_table'] = array(
    '#type' => 'markup',
    '#markup' => page_settings_admin_table(),
  );
  
  $form['new_var'] = array(
    "#type" => "fieldset",
    "#title" => "New variable",
    "#collapsed" => FALSE,
    "#collapsible" => TRUE,
  );
  // Set the variable form in a fieldset:
  $form['new_var']['var_name'] = array(
    "#type" => 'textfield',
    "#title" => t('Variable name'),
    "#description" => 'Machine-readable variable name. Only lowercase letters, numbers and underscores are accepted. This name must be unique and start with "admin_".',
    "#default_value" => 'admin_',
    "#maxlength" => 64,
    "#required" => TRUE,
  );
  $form['new_var']['var_help'] = array(
    "#type" => 'textfield',
    "#title" => t('Variable help text'),
    "#description" => 'The variable help text displayed under the field.',
    "#default_value" => '',
    "#maxlength" => 256,
    "#required" => TRUE,
  );
  $form['new_var']['var_group'] = array(
    "#type" => 'select',
    "#title" => t('Variable group'),
    "#options" => get_groups(),
    "#required" => TRUE,
  );  
  $form['new_var']['var_type'] = array(
    "#type" => 'select',
    "#title" => t('Variable type'),
    "#options" => get_types(),
    "#required" => TRUE,
    "#default_value" => 1,
  );
  $form['new_var']['var_submit_button'] = array(
    "#type" => "submit",
    "#validate" => array("_variable_validate"),
    "#submit" => array("_variable_submit"),
    "#value" => t("Add new variable" ),
  ); 
  
  $form['groups'] = array(
    "#type" => "fieldset",
    "#title" => "Groups",
    "#collapsed" => TRUE,
    "#collapsible" => TRUE,
  );
  $form['groups']['groups_table'] = array(
    '#type' => 'markup',
    '#markup' => page_settings_admin_groups_table(),
  );
  $form['groups']['new_group'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Add new group'), 'admin/config/system/page_settings_admin/add_group'),
  );
  
  return $form;
}

/**
 * Defines the variable form.
 */
function page_settings_variable_formular() {
  $form = array();
  
  $form['var_name'] = array(
    "#type" => 'textfield',
    "#title" => t('Variable name'),
    "#description" => 'Machine-readable variable name. Only lowercase letters, numbers and underscores are accepted. This name must be unique and start with "admin_".',
    "#default_value" => 'admin_',
    "#maxlength" => 64,
    "#required" => TRUE,
  );
  $form['var_help'] = array(
    "#type" => 'textfield',
    "#title" => t('Variable help text'),
    "#description" => 'The variable help text displayed under the field.',
    "#default_value" => '',
    "#maxlength" => 256,
    "#required" => TRUE,
  );
  $form['var_group'] = array(
    "#type" => 'select',
    "#title" => t('Variable group'),
    "#options" => get_groups(),
    "#required" => TRUE,
  );  
  $form['var_type'] = array(
    "#type" => 'select',
    "#title" => t('Variable type'),
    "#options" => get_types(),
    "#required" => TRUE,
    "#default_value" => 1,
  );
  $form['var_submit_button'] = array(
    "#type" => "submit",
    "#validate" => array("_variable_validate"),
    "#submit" => array("_variable_submit"),
    "#value" => t("Add new variable" ),
  ); 
  
  return $form;
}

/**
 * Defines the group form.
 */
function page_settings_group_form() {
  $form = array();
  
  $form['group_name'] = array(
    "#type" => 'textfield',
    "#title" => t('Group name'),
    "#description" => 'The group name.',
    "#default_value" => '',
    "#maxlength" => 256,
    "#required" => TRUE,
  );
  $form['group_submit_button'] = array(
    "#type" => "submit",
    "#validate" => array("_group_validate"),
    "#submit" => array("_group_submit"),
    "#value" => t("Add new group" ),
  ); 
  
  return $form;
}

/**
 * Displays the variable edit form. Gets the values from page arguments and adds variable ID as a hidden field to the form.
 *
 * @param $args The arguments.
 * @param array $id Contains the variable ID.
 */
function page_settings_admin_edit_form($args, $id) {

  $sql_var = "SELECT * FROM {page_settings_variables} WHERE id = :id";
  $rs = db_query($sql_var, array(':id' => $id['build_info']['args'][0]));
  $var_obj = $rs->fetch();

  $form = drupal_get_form("page_settings_variable_formular", $var_obj->id);
  
  // Set current values in form fields:
  $form['var_name']['#value'] = $var_obj->name;
  $form['var_help']['#value'] = $var_obj->help_text;
  $form['var_group']['#value'] = $var_obj->group_id;
  $form['var_type']['#value'] = $var_obj->type_id;
  
  $form['var_submit_button']['#value'] = "Edit variable";
  
  // Add variable ID in hidden field for later use:
  $form['var_id'] = array(
    "#type" => 'hidden',
    "#default_value" => $var_obj->id,
  );
  
  return $form;
}

/**
 * Displays the group edit form. Gets the values from page arguments and adds group ID as a hidden field to the form.
 *
 * @param $args The arguments.
 * @param array $id Contains the group ID.
 */
function page_settings_admin_group_edit_form($args, $id) {
  $sql_var = "SELECT * FROM {page_settings_groups} WHERE id = :id";
  $rs = db_query($sql_var, array(':id' => $id['build_info']['args'][0]));
  $obj = $rs->fetch();
  
  $form = drupal_get_form("page_settings_group_form", $obj->id);
  
  // Set current values in form fields:
  $form['group_name']['#value'] = $obj->name;
  $form['group_submit_button']['#value'] = "Edit group";
  
  // Add group ID in hidden field for later use:
  $form['group_id'] = array(
    "#type" => 'hidden',
    "#default_value" => $obj->id,
  );
  
  return $form;
}

/**
 * Validates variables. Only accepts lowercase letters, numbers and underscores in variable name.
 */
function _variable_validate($form, &$form_state) {

  $var_name = $form_state['input']['var_name'];

  if (substr($var_name, 0, 6) !== "admin_") {
    form_set_error('var_name', t('Variable name must start with "admin_"'));
  }
  if (strlen($var_name) <= 6) {
    form_set_error('var_name', t('Variable name is too short.'));
  }
  if (preg_match("/^[a-z0-9_]*$/", $var_name) == FALSE) {
    form_set_error('var_name', t('Variable name can only contain lowercase letters, numbers and underscores.'));
  }
  // Only check for existing variable in adding new variable:
  if (!isset($form_state['input']['var_id'])) {
    $sql_check = "SELECT COUNT(*) AS cnt FROM {page_settings_variables} WHERE name = :name";
    $rs = db_query($sql_check, array(':name' => $var_name));
    $obj = $rs->fetch();
    if ($obj->cnt > 0) {
      form_set_error('var_name', t('A variable with the specified name already exists.'));
    }
  }
}

/**
 * Validates groups.
 */
function _group_validate($form, &$form_state) {
  if (!isset($form_state['input']['group_id'])) {
    $group_name = $form_state['input']['group_name'];
  
    $sql_check = "SELECT COUNT(*) AS cnt FROM {page_settings_groups} WHERE name = :name";
    $rs = db_query($sql_check, array(':name' => $group_name));
    $obj = $rs->fetch();
    if ($obj->cnt > 0) {
      form_set_error('group_name', t('A group with the specified name already exists.'));
    }
  }
}

/**
 * Saves variables
 */
function _variable_submit($form, &$form_state) {

  $var = new stdClass();
  $var->group_id = $form_state['input']['var_group'];
  $var->type_id = $form_state['input']['var_type'];
  $var->name = $form_state['input']['var_name'];
  $var->help_text = $form_state['input']['var_help'];
  
  if (isset($form_state['input']['var_id'])) {
    // Edit form:
    $var->id = $form_state['input']['var_id'];
  
    // Update variable if new variable name is different from the old one:
    $sql = "SELECT name FROM {page_settings_variables} WHERE id = :id";
    $rs = db_query($sql, array(':id' => $var->id));
    $old_obj = $rs->fetch();
    if ($old_obj->name != $var->name) {
      $old_value = variable_get($old_obj->name);
      variable_del($old_obj->name);
      variable_set($var->name, $old_value);
    }
  
    drupal_write_record('page_settings_variables', $var, 'id');
    drupal_set_message(filter_xss("Variable '" . $var->name . "' edited."));
    drupal_goto("admin/config/system/page_settings_admin");
  }
  else {
    // Add new variable:
    drupal_write_record('page_settings_variables', $var);
    drupal_set_message(filter_xss("New variable '" . $var->name . "' added."));
  }
}

/**
 * Saves groups
 */
function _group_submit($form, &$form_state) {
  $group = new stdClass();
  $group->name = $form_state['input']['group_name'];
  
  if (isset($form_state['input']['group_id'])) {
    // Edit form
    $group->id = $form_state['input']['group_id'];
  
    drupal_write_record('page_settings_groups', $group, 'id');
    drupal_set_message(filter_xss("Group '" . $group->name . "' edited."));
    drupal_goto("admin/config/system/page_settings_admin");
  }
  else {
    // Add new group:
    drupal_write_record('page_settings_groups', $group);
    drupal_set_message(filter_xss("New group '" . $group->name . "' added."));
  }
}
