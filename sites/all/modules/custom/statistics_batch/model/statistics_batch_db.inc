<?php
/**
 * Get count of unhandling statistics
 */
function statistics_batch_get_count_unhandling_statistics_db() {
  $count = db_select(TBL_STATISTICS_ENTRY, 'e')
    ->condition('is_processed', 0)
    ->countQuery()
    ->execute()
    ->fetchField();
  return $count;
}