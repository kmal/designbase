<?php
/**
 * @file
 */

/**
 * Control point for checking Drupal modules' versions.
 */
class ClientControlPointModuleVersions implements ClientControlPointInterface {
  /**
   * Get the versions of all installed modules.
   *
   * @return Array Array of module versions on the form $module => $version.
   */
  public function poll() {
    $modules = system_rebuild_module_data();
    $data = array();
    foreach ($modules as $identifier => $module) {
      if ($module->status && !in_array(strtolower($module->info['package']), array('dwarf', 'core'))) {
        $data[$identifier] = $module->info['version'];
      }
    }
    return $data;
  }
}
