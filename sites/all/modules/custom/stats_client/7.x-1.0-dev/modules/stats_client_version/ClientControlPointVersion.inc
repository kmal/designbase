<?php
/**
 * @file
 */

/**
 * Class for implementing the version control point.
 */
class ClientControlPointVersion implements ClientControlPointInterface {
  /**
   * Implements ClientControlPointInterface.poll().
   *
   * @return float Drupal core version.
   */
  public function poll() {
    return VERSION;
  }
}
