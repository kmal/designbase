<?php
/**
 * @file
 */

/**
 * Alive control point class.
 */
class ClientControlPointAlive implements ClientControlPointInterface {
  /**
   * Poll to see if site is alive.
   *
   * @return String Returns the string alive if Drupal is running. If not, Drupal is not running and this return value should not be your main concern.
   */
  public function poll() {
    return 'alive';
  }
}
