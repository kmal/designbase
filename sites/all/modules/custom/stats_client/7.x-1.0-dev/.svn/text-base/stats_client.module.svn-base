<?php
/**
 * Stats admin
 *
 * Module for monitoring the health of a number of Drupal sites.
 *
 * @author Jesper Jarlskov <jja@dwarf.dk> - 5/12 - 2013
 */

/**
 * Implements hook_menu().
 */
function stats_client_menu() {
  $items = array(
    'stats_client/get_control_points' => array(
      'page callback' => 'stats_client_get_control_points',
      'access callback' => TRUE,
    ),
    'stats_client/poll' => array(
      'page callback' => 'stats_client_poll',
      'page arguments' => array(FALSE),
      'access callback' => TRUE,
    ),
    'stats_client/poll/%' => array(
      'page callback' => 'stats_client_poll',
      'page arguments' => array(2),
      'access callback' => TRUE,
    ),
  );
  return $items;
}

/**
 * Return list of control points available at the client.
 */
function _stats_client_get_control_points() {
  $modules = db_select('stats_client_modules', 'm')
    ->fields('m')
    ->execute();

  $control_points = array();
  while ($control_point = $modules->fetchAssoc()) {
    $control_points[$control_point['mid']] = $control_point['class'];
  }

  return $control_points;
}

/**
 * Return JSON array with a list of available control points.
 *
 * @return JSON-array on the form $mid => result.
 */
function stats_client_get_control_points() {
  $data = array(
    'controlpoints' => array_keys(_stats_client_get_control_points()),
  );
  return drupal_json_output($data);
}

/**
 * Return JSON array of values of control points' return values.
 *
 * @param String $control_point Optional. The function returns the value of the selected control point. If no control point is specified an array with data from all available control points are returned.
 * @return JSON array of control point data.
 */
function stats_client_poll($mid) {
  $control_points = _stats_client_get_control_points();
  $output = array();
  if (!$mid) {
    foreach ($control_points as $id => $class) {
      $control_point = new $class;
      $output[$id] = $control_point->poll();
    }
  }
  else {
    if (array_key_exists($mid, $control_points)) {
      $class = $control_points[$mid];
      $control_point = new $class;
      $output[$mid] = $control_point->poll();
    }
    else {
      return _stats_client_return_error('Control point not supported by client.');
    }
  }
  return drupal_json_output($output);
}

/**
 * Helper function. Provides common functionality for returning error arrays.
 *
 * @param String $message Error message.
 * @return JSON array with the specified error message.
 */
function _stats_client_return_error($message) {
  $output = array(
    'error' => $message,
  );
  return drupal_json_output($output);
}
