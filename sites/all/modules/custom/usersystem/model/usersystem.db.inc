<?php

/**
 * Get the company (supplier) node from the Taxonomy ID.
 *
 * $return stdClass if found, FALSE if not found.
 */
function _get_supplier_by_company_tid($tid) {
  $result = db_select('node', 'n')->fields('n');
  $result->join('field_data_field_company', 'c', 'n.nid = c.entity_id');
  $result = $result
    ->condition("c.entity_type", 'node')
    ->condition('c.bundle', 'partner_profile')
    ->condition("n.type", 'partner_profile')
    ->condition("c.field_company_tid", $tid)
    ->execute()
    ->fetchAssoc();
  if ($result['nid']) {
    $supplier = node_load($result['nid']);
    $supplier->articles_count = _get_supplier_count($tid, 'article', TRUE);
    $supplier->namenews_count = _get_supplier_count($tid, 'name_article');
    $supplier->events_count = _get_supplier_count($tid, 'event_post');
    $supplier->competences = isset($supplier->field_industry['und']) ? count($supplier->field_industry['und']) : 0;
    return $supplier;
  }
  return FALSE;
}

/**
 * Returns how many articels a supplier has created.
 *
 * @param type $tid the taxonomy ID of the supplier.
 * @param type $type the type to count. Allowed values are article, name_article, event.
 */
function _get_supplier_count($tid, $type = 'article', $only_supplier_news = FALSE) {
  $query = db_select('field_data_field_company', 'c');
  $query->leftJoin('node', 'n', 'c.entity_id = n.nid');

  $query->fields('c')
    ->condition('c.entity_type', 'node')
    ->condition('c.bundle', $type)
    ->condition('c.field_company_tid', $tid)
	  ->condition('n.status', 1);

  if ($only_supplier_news) {
	  $query->leftJoin('field_data_field_type', 't', 't.entity_id = n.nid');
	  $query->condition('t.field_type_value', '2');
  }

  return $query->execute()
	  ->rowCount();
}


/**
 * _check_if_supplier_may_be_created
 * we introduce this function in reply to a bugfix #287 where company taxonomy tid was created manually and therefore conflicts 2nd user check, where supplier is not created yet
 *
 * @param mixed $tid
 *
 * @return boolean (whether supplier may be created or not, if not it is because user is 2nd and should be authorized by admin)
 */
function _check_if_supplier_may_be_created($tid) {
  $result = db_select('node', 'n')->fields('n');
  $result->join('field_data_field_company', 'c', 'n.nid = c.entity_id');
  $result = $result->condition("c.entity_type", 'node')->condition('c.bundle', 'supplier')->condition("n.type", 'supplier')->condition("c.field_company_tid", $tid)->execute()->fetchAssoc();
  if (!empty($result)) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Fetch supplier node
 */
function _get_supplier_node() {
  $tid = _get_user_company_tid();
  $n = db_select('field_data_field_company', 'c')
    ->fields('c', array('entity_id'))
    ->condition('bundle', 'partner_profile')
    ->condition('field_company_tid', $tid)
    ->execute()
    ->fetchAssoc();
  if ($n) {
    return node_load($n['entity_id']);
  }
  else {
    return FALSE;
  }
}

/**
 * If a supplier or other supplier entities have been added through backend, the uid is set to an admin users uid, that gives a redirect issue where a backend url is attempted to be reached upon node_save, this function sets the right (frontend user).
 */
function _set_supplier_user($user, &$node) {
  if (isset($node->uid)) {
    if($user->uid != $node->uid) {
      $new_user = user_load($node->uid);
      if (isset($new_user->roles[3])) {
        $node->uid = $user->uid;
      }
    }
  }
}

function _get_supplier_node_by_node($nid = NULL) {
  if ($nid) {
    if (arg(0) == 'pagepreview') {
      $preview_id = arg(1);
      $node = pagepreview_cache_get($preview_id);      
    }
    else {
      $node = node_load($nid);
    }
    if ($node && !empty($node->field_company)) {
      $user = _get_supplier_by_company_tid($node->field_company['und'][0]['tid']);
      if (is_object($user) && $user->type == 'partner_profile') {
        return $user->nid;
      }
      else {
        if (is_object($user)) {     
          $u = user_load($user->uid);
          if (isset($u->field_company['und'][0]['tid'])) {
            $tid = $u->field_company['und'][0]['tid'];
            $n = db_select('field_data_field_company', 'c')
            ->fields('c', array('entity_id'))
            ->condition('bundle', 'partner_profile')
            ->condition('field_company_tid', $tid)
            ->execute()
            ->fetchAssoc();
            if ($n) {
              return $n['entity_id'];
            }
            else
              return NULL;
          }
        }
      }
          return NULL;
    } 
  }
}

/**
 * Determine wheter company name term from vocabulary Company exists
 */
function _company_term_exists($company_name) {

  $sql = "SELECT * FROM {taxonomy_term_data} WHERE vid=4 AND name = :n ";

  $result = db_query($sql, array(':n' => $company_name));
  $result = $result->fetchAssoc();
  return !(_check_if_supplier_may_be_created($result['tid']));
}

/**
 * Retrieve the company tid for specified bundle
 */
function _get_node_company_tid($nid, $bundle) {
  $result = db_select('field_data_field_company', 'c')
    ->fields('c', array('field_company_tid'))
    ->condition('entity_type', 'node')
    ->condition('bundle', $bundle)
    ->condition('entity_id', $nid)
    ->execute()
    ->fetchAssoc();
  return $result['field_company_tid'];
}

function usersystem_get_terms_by_vid($vid) {
  $terms = db_query("
    SELECT tid, name, (
      SELECT COUNT(*) FROM {taxonomy_index} ti
      LEFT JOIN {node} n ON ti.nid = n.nid
      WHERE ti.tid = td.tid AND n.status = 1
    ) node_count FROM {taxonomy_term_data} td
    WHERE vid = :vid
    ORDER BY weight
  ", array(':vid' => $vid));
  $items = array();
  foreach ($terms as $term) {
    $items[] = $term;
  }
  return $items;
}

/**
 * Retrieve all kinds of supplier data for listings
 */
function _get_supplier_data($type) {
  $tid = _get_user_company_tid();
  $result = db_select('node', 'n')->fields('n');
  $result->join('field_data_field_company', 'c', 'n.nid = c.entity_id');
  if ($type == 'article') {
    $result->join('field_data_field_type', 'at', 'n.nid = at.entity_id');
    $result = $result->condition('at.field_type_value', 2);
  }
  if ($type == 'event_post') {
    $result = $result->condition('n.status', 1);
  }
  $result = $result->condition("c.entity_type", 'node')->condition("n.type", $type)->condition("c.field_company_tid", $tid);
  $result = $result->extend('PagerDefault')->limit(10)->orderBy('n.created', 'DESC')->execute();
  $nodes = array();
  foreach ($result as $record) {
    $nodes[] = (array)node_load($record->nid);
  }
  return $nodes;
}