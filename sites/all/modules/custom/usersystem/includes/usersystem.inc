<?php


/**
 * Fetch otherwise create supplier node and return it
 */
function _get_supplier() {
  global $user;
  $n = _get_supplier_node();
  if ($n) {
    return $n;
  }
  else {
    $usr = user_load($user->uid);

    if (isset($usr->field_company['und'][0]['tid'])) {
      $company = taxonomy_term_load($usr->field_company['und'][0]['tid']);
      $node = new stdClass;
      $node->type = 'partner_profile';
      $node->uid = $usr->uid;
      $node->title = $company->name;
      $node->field_supplier_company['und'][0]['value'] = $company->name;
      $node->language = 'da';
      $node->field_company = $usr->field_company;
      node_save($node);
      return $node;
    }
  }
  return FALSE;
}

/**
 * Retrieve the users company tid
 */
function _get_user_company_tid() {
  global $user;
  $u = user_load($user->uid);
  if (isset($u->field_company['und'][0]['tid']))
    return $u->field_company['und'][0]['tid'];
  else
    return NULL;
}


/**
 * Retrieve terms from taxonomy Category as options array
 */
function _get_category_term_options() {
  //@TODO make it more general
  $terms = taxonomy_get_tree(3);
  $options = array();
  if (isset($terms)) {
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }
  }
  return $options;
}

/**
 * Retrieve terms from taxonomy Eventtype as options array
 */
function _get_eventtype_term_options() {
  //@TODO make it more general
  $terms = taxonomy_get_tree(9);
  $options = array();
  if (isset($terms)) {
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }
  }
  return $options;
}

function _set_company_data_flag(&$node, $form_state) {
  if (
    ($node->field_address['und'][0]['value'] != $form_state['values']['address']) ||
    ($node->field_postal_code['und'][0]['value'] != $form_state['values']['postal_code']) ||
    ($node->field_city['und'][0]['value'] != $form_state['values']['city']) ||
    ($node->field_phone['und'][0]['value'] != $form_state['values']['phone']) ||
    ($node->field_www['und'][0]['value'] != $form_state['values']['www']) ||
    ($node->field_email['und'][0]['value'] != $form_state['values']['email'])
    ) {
      $node->field_company_data_last_edited['und'][0]['value'] = date('Y-m-d H:i:s', time());
    }
}

/**
 * Retrieve a supplier node by bundle
 */
function _get_node($bundle) {
  $nid = arg(1);
  if (isset($nid)) {
    $user_tid = _get_user_company_tid();
    $node_tid = _get_node_company_tid($nid, $bundle);
    if ($user_tid == $node_tid) {
      return node_load($nid);
    }
    else {
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}


/**
 * Deletes a supplier image for specified bundle if supplied nid by $_GET
 */
function _delete_file() {
  $nid = arg(1);
  if ( isset($nid) && isset($_GET['delete_file']) ) {
    $node = node_load($nid);
    $fid = $_GET['delete_file'];
    if (isset($node->field_file['und'])) {
      for ($i = 0 ; $i < count($node->field_file['und']) ; $i++) {
        if ($node->field_file['und'][$i]['fid'] == $fid) {
          unset($node->field_file['und'][$i]);
          node_save($node);
        }
      }
    }
  }
}

/**
 * Deletes a supplier image for specified bundle if supplied nid by $_GET
 */
function _delete_image() {
  $nid = arg(1);
  if ( isset($nid) && isset($_GET['delete_image']) ) {
    $node = node_load($nid);
    $fid = $_GET['delete_image'];
    if (isset($node->field_image['und'])) {
      for ($i = 0 ; $i < count($node->field_image['und']) ; $i++) {
        if ($node->field_image['und'][$i]['fid'] == $fid) {
          unset($node->field_image['und'][$i]);
          node_save($node);
        }
      }
    }
  }
}


/**
 * Deletes a supplier node for specified bundle if supplied nid by $_GET
 */
function _delete_node($bundle) {
  if (isset($_GET['delete'])) {
    $nid = $_GET['delete'];
    $user_tid = _get_user_company_tid();
    $node_tid = _get_node_company_tid($nid, $bundle);
    if ($user_tid == $node_tid) {
      return node_delete($nid);
    }
  }
}
function _delete_node_by_nid() {
  if (isset($_GET['delete'])) {
    $nid = $_GET['delete'];
    $node = node_load($nid);
    if (is_object($node)) {
      $user_tid = _get_user_company_tid();
      $node_tid = _get_node_company_tid($nid, $node->type);
      if ($user_tid == $node_tid) {
        return node_delete($nid);
      }      
    }
  }
}

/**
 * Deletes a suppliers logo
 */
function _delete_logo() {
  $node = _get_supplier();
  if ( isset($_GET['delete_logo']) ) {
    $fid = $_GET['delete_logo'];
    if ($node->field_logo['und'][0]['fid'] == $fid) {
      unset($node->field_logo['und'][0]);
      node_save($node);
    }
  }
  if ( isset($_GET['delete_logo_print'])) {
    $fid = $_GET['delete_logo_print'];
    if ($node->field_file_print['und'][0]['fid'] == $fid) {
      unset($node->field_file_print['und'][0]);
      node_save($node);
    }
  }
}

/**
 * Set company print flag if neccesary
 */
function _set_print_flag(&$node, $form_state) {
  if (
    ($node->field_summary_print['und'][0]['value'] != $form_state['values']['summary_print']) ||
    ($node->field_industry_print['und'][0]['tid'] != $form_state['values']['industry_print'])
  ) {
    $node->field_print_last_edited['und'][0]['value'] = date('Y-m-d H:i:s', time());
#    die($node->field_print_last_edited['und'][0]['value']);
  }
}

/**
 * _set_logo_print_flag
 *
 * @param mixed $node
 * @param mixed $form_state
 * @access protected
 * @return void
 */
function _set_logo_print_flag(&$node, $form_state) {
  if (isset($form_state['values']['img_print_file'])) {
    $node->field_print_logo_last_edited['und'][0]['value'] = date('Y-m-d H:i:s', time());
  }
}

function usersystem_validate_file($key_name = NULL, $field_name = NULL, $image = TRUE) {
  if ($field_name && $key_name) {
    if ($image) {
      $valid_files = array('image/jpg', 'image/jpeg', 'image/png', 'image/gif');
    }
    else {
      $valid_files = array('application/pdf', 'application/msword', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' );
    }
    if ($_FILES[$key_name]['size'][$field_name] > 0) {
      global $user;
      $file = new stdClass();
      $file->uid = $user->uid;
      $file->filename = trim($_FILES[$key_name]['name'][$field_name]);
      $file->uri = $_FILES[$key_name]['tmp_name'][$field_name];
      $file->filemime = file_get_mimetype($file->filename);
      $file->filesize = $_FILES['files']['size']['img_print'];
      if (in_array($file->filemime, $valid_files)) {
        return $file;
      }
      else {
        form_set_error('img', 'Billede kan ikke uploades.');
      }
    }
  }
} 

