<?php
/**
 * @file
 * jobs_content.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function jobs_content_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_right|node|job|teaser';
  $field_group->group_name = 'group_right';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'job';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Roght part',
    'weight' => '2',
    'children' => array(
      0 => 'field_jobteaser',
      1 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Roght part',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-right field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_right|node|job|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_teaser|node|job|full';
  $field_group->group_name = 'group_teaser';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'job';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Teaser',
    'weight' => '3',
    'children' => array(
      0 => 'field_jobteaser',
      1 => 'field_logo',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Teaser',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-teaser field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_teaser|node|job|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_top_line|node|job|full';
  $field_group->group_name = 'group_top_line';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'job';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Top line',
    'weight' => '0',
    'children' => array(
      0 => 'field_applicationdeadline',
      1 => 'field_jobtitle',
      2 => 'field_www',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Top line',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-top-line field-group-div eventWhenWhere',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_top_line|node|job|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_top|node|job|teaser';
  $field_group->group_name = 'group_top';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'job';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Top information',
    'weight' => '0',
    'children' => array(
      0 => 'field_applicationdeadline',
      1 => 'field_job_geo',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Top information',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-top field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_top|node|job|teaser'] = $field_group;

  return $export;
}
