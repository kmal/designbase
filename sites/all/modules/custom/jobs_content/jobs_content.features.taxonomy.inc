<?php
/**
 * @file
 * jobs_content.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function jobs_content_taxonomy_default_vocabularies() {
  return array(
    'company' => array(
      'name' => 'Company',
      'machine_name' => 'company',
      'description' => 'Company taxonomy of article',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'job_geography' => array(
      'name' => 'Landsdel',
      'machine_name' => 'job_geography',
      'description' => 'Taksonomi til landsdele',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
