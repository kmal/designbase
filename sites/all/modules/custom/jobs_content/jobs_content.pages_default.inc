<?php
/**
 * @file
 * jobs_content.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function jobs_content_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_5';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 5;
  $handler->conf = array(
    'title' => 'Job',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'node-type-job',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'job' => 'job',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'two_66_33';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
      'two_66_33_top' => NULL,
      'two_66_33_first' => NULL,
      'two_66_33_second' => NULL,
      'two_66_33_bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:field-headline';
  $display->uuid = '73550657-0815-485b-89c8-28aef0c43098';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-3ad4eee5-fb56-4897-8ce6-c5c4f98e795f';
    $pane->panel = 'two_66_33_first';
    $pane->type = 'node_content';
    $pane->subtype = 'node_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'links' => 1,
      'no_extras' => 1,
      'override_title' => 1,
      'override_title_text' => '',
      'identifier' => '',
      'link' => 1,
      'leave_node_title' => 1,
      'build_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3ad4eee5-fb56-4897-8ce6-c5c4f98e795f';
    $display->content['new-3ad4eee5-fb56-4897-8ce6-c5c4f98e795f'] = $pane;
    $display->panels['two_66_33_first'][0] = 'new-3ad4eee5-fb56-4897-8ce6-c5c4f98e795f';
    $pane = new stdClass();
    $pane->pid = 'new-aa0d2fc4-d3b0-4fcb-9b9f-d40192cff5b8';
    $pane->panel = 'two_66_33_second';
    $pane->type = 'block';
    $pane->subtype = 'jobs-job_contactinfo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aa0d2fc4-d3b0-4fcb-9b9f-d40192cff5b8';
    $display->content['new-aa0d2fc4-d3b0-4fcb-9b9f-d40192cff5b8'] = $pane;
    $display->panels['two_66_33_second'][0] = 'new-aa0d2fc4-d3b0-4fcb-9b9f-d40192cff5b8';
    $pane = new stdClass();
    $pane->pid = 'new-aea8d445-a3c6-4356-ab2f-6c11baa23414';
    $pane->panel = 'two_66_33_second';
    $pane->type = 'views';
    $pane->subtype = 'jobs';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '4',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'job_contacts',
      'context' => array(
        0 => '',
      ),
      'override_title' => 0,
      'override_title_text' => '111111111111111111111111',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'aea8d445-a3c6-4356-ab2f-6c11baa23414';
    $display->content['new-aea8d445-a3c6-4356-ab2f-6c11baa23414'] = $pane;
    $display->panels['two_66_33_second'][1] = 'new-aea8d445-a3c6-4356-ab2f-6c11baa23414';
    $pane = new stdClass();
    $pane->pid = 'new-e6aed45e-0267-4c7a-a2c5-3c15559f4251';
    $pane->panel = 'two_66_33_second';
    $pane->type = 'block';
    $pane->subtype = 'jobs-job_googlemaps_block';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'e6aed45e-0267-4c7a-a2c5-3c15559f4251';
    $display->content['new-e6aed45e-0267-4c7a-a2c5-3c15559f4251'] = $pane;
    $display->panels['two_66_33_second'][2] = 'new-e6aed45e-0267-4c7a-a2c5-3c15559f4251';
    $pane = new stdClass();
    $pane->pid = 'new-d96b1b9d-821e-4653-a3c2-c1e78a83855a';
    $pane->panel = 'two_66_33_second';
    $pane->type = 'views';
    $pane->subtype = 'jobs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '3',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 1,
      'args' => '',
      'url' => '',
      'display' => 'newest_jobs',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'd96b1b9d-821e-4653-a3c2-c1e78a83855a';
    $display->content['new-d96b1b9d-821e-4653-a3c2-c1e78a83855a'] = $pane;
    $display->panels['two_66_33_second'][3] = 'new-d96b1b9d-821e-4653-a3c2-c1e78a83855a';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-3ad4eee5-fb56-4897-8ce6-c5c4f98e795f';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_5'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function jobs_content_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'jobs';
  $page->task = 'page';
  $page->admin_title = 'Jobs';
  $page->admin_description = '';
  $page->path = 'jobopslag';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_jobs_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'jobs';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '08ab8590-5fd2-4362-a2f6-d7f455ee46f0';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9c318029-2789-4172-94a4-c61e3a7741ac';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'jobs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '4',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'job_page',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9c318029-2789-4172-94a4-c61e3a7741ac';
    $display->content['new-9c318029-2789-4172-94a4-c61e3a7741ac'] = $pane;
    $display->panels['middle'][0] = 'new-9c318029-2789-4172-94a4-c61e3a7741ac';
    $pane = new stdClass();
    $pane->pid = 'new-edd1764a-0826-406b-b483-d8d97e5a94e5';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'showroom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'php',
          'settings' => array(
            'description' => 'Mobile',
            'php' => '$detect = mobile_detect_get_object();
$is_mobile = $detect->isMobile();
if ($is_mobile) {
return FALSE;
}
else {
  return TRUE;
}',
          ),
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '3',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 1,
      'args' => '',
      'url' => '',
      'display' => 'showroom',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'edd1764a-0826-406b-b483-d8d97e5a94e5';
    $display->content['new-edd1764a-0826-406b-b483-d8d97e5a94e5'] = $pane;
    $display->panels['middle'][1] = 'new-edd1764a-0826-406b-b483-d8d97e5a94e5';
    $pane = new stdClass();
    $pane->pid = 'new-9d70df43-bf28-49a5-bb66-781c0b05f49a';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'quicktabs-popular';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'php',
          'settings' => array(
            'description' => 'Mobile',
            'php' => '$detect = mobile_detect_get_object();
$is_mobile = $detect->isMobile();
if ($is_mobile) {
return FALSE;
}
else {
  return TRUE;
}',
          ),
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '9d70df43-bf28-49a5-bb66-781c0b05f49a';
    $display->content['new-9d70df43-bf28-49a5-bb66-781c0b05f49a'] = $pane;
    $display->panels['middle'][2] = 'new-9d70df43-bf28-49a5-bb66-781c0b05f49a';
    $pane = new stdClass();
    $pane->pid = 'new-04529603-2737-4fed-a518-9769afb66393';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'frontpage_events';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'mobile_detect_ctools',
          'settings' => array(
            'type' => 'isMobile',
          ),
          'context' => array(),
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '4',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '04529603-2737-4fed-a518-9769afb66393';
    $display->content['new-04529603-2737-4fed-a518-9769afb66393'] = $pane;
    $display->panels['middle'][3] = 'new-04529603-2737-4fed-a518-9769afb66393';
    $pane = new stdClass();
    $pane->pid = 'new-532353b1-c55b-42fe-a5eb-dc2de0accad9';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'indsustry_news_block';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'mobile_detect_ctools',
          'settings' => array(
            'type' => 'isMobile',
          ),
          'context' => array(),
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '532353b1-c55b-42fe-a5eb-dc2de0accad9';
    $display->content['new-532353b1-c55b-42fe-a5eb-dc2de0accad9'] = $pane;
    $display->panels['middle'][4] = 'new-532353b1-c55b-42fe-a5eb-dc2de0accad9';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['jobs'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'opret_jobs';
  $page->task = 'page';
  $page->admin_title = 'opret jobs';
  $page->admin_description = '';
  $page->path = 'nytjob';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_opret_jobs_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'opret_jobs';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array();
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'cbb0c86e-02f5-4df1-8efe-8aee620dc423';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-49ffb02e-ed8c-4055-bb65-688d894fc037';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'jobs-job_createjobs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '49ffb02e-ed8c-4055-bb65-688d894fc037';
    $display->content['new-49ffb02e-ed8c-4055-bb65-688d894fc037'] = $pane;
    $display->panels['middle'][0] = 'new-49ffb02e-ed8c-4055-bb65-688d894fc037';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-49ffb02e-ed8c-4055-bb65-688d894fc037';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['opret_jobs'] = $page;

  return $pages;

}
