<?php foreach ($most_read as $node) : ?>
  <div class="views-row clearfix">
    <?php if (isset($node['image_url'])) {?>
      <div class="views-field-field-image">
        <?php print theme('image_style', 
          array(
            'style_name' => '75x70px', 
            'path' => $node['image_url'], 
            'width' => $node['width'],
            'height' => $node['height']
          )
        );?>
      </div>   
    <?php }?>
    <div class=" views-field-timestamp">
    <?php if (isset($node['byline'])) :?>
      <?php print $node['byline']; ?> | 
    <?php endif; ?>  
    <?php print $node['create']; ?></div>
    <div class="views-field-field-headline"><?php print $node['headline']; ?></div>      
    <div class="views-field-field-teaser"><?php print l($node['teaser'], 'node/' . $node['nid']); ?></div>
  </div>     
<?php endforeach; ?>
          