<?php

function newsletter_page_content(){
//      if (!_newsletter_get_cookie_flag('newsletter_subscribed') || _newsletter_get_cookie_flag('newsletter_receipt')) {
//  $content = theme("newsletter_signup_apsis", array('page' => TRUE));
  $content = _newsletter_get_content();
  $return = theme("newsletter_signup", array('content' => $content, 'page' => TRUE));
//      }
  return $return;
}

function newsletter_page_content_access(){
  $detect = mobile_detect_get_object();
  $is_mobile = $detect->isMobile();
  if ($is_mobile) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}