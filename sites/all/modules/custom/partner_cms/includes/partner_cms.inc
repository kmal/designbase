<?php

function partner_cms_get_supplier_brands() {
  $supplier = _get_supplier();
  $company_tid = $supplier->field_company['und'][0]['tid'];
  $terms = array();
  
  $query = db_select('node', 'n');
  $query->innerJoin('field_data_field_company', 'c', 'n.nid = c.entity_id');
  $query->innerJoin('field_data_field_brands', 'b', 'n.nid = b.entity_id');
  $query->condition('n.type', 'brands_profile');
  $query->condition('c.field_company_tid', $company_tid);
  $query->fields('b', array('field_brands_tid'));
  $result = $query->execute()->fetchAll();
  foreach ($result as $item) {
    $tids[] = $item->field_brands_tid;
  }
  if (!empty($tids)) {
    $terms = taxonomy_term_load_multiple($tids);
  }
  return $terms;
}

function partner_cms_term_name_exists($term_name_val = NULL) {
  $return = NULL;
  $term_name = trim($term_name_val);
  if ($term_name) {
    $q = db_select('taxonomy_term_data', 'td')
      ->fields('td', array('tid', 'vid'))
      ->condition('td.name', $term_name)
      ->condition('td.vid', 11);
    $tid = $q->execute()->fetchField();
    if ($tid) {
      $return = $tid;
    }
  }
  
  return $return;
}
