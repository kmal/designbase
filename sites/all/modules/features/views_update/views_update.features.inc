<?php
/**
 * @file
 * views_update.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_update_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
