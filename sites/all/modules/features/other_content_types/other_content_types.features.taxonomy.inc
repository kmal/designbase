<?php
/**
 * @file
 * other_content_types.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function other_content_types_taxonomy_default_vocabularies() {
  return array(
    'eventtype' => array(
      'name' => 'Eventtype',
      'machine_name' => 'eventtype',
      'description' => 'Type of events',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
