<?php
/**
 * @file
 * other_content_types.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function other_content_types_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event_post|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event_post';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
        1 => 'group_right',
        2 => 'field_headline',
        3 => 'field_event_type',
        4 => 'field_teaser',
        5 => 'field_date',
        6 => 'field_location',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
      'group_right' => 'ds_content',
      'field_headline' => 'ds_content',
      'field_event_type' => 'ds_content',
      'field_teaser' => 'ds_content',
      'field_date' => 'ds_content',
      'field_location' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '0',
    'page_option_title' => '',
  );
  $export['node|event_post|teaser'] = $ds_layout;

  return $export;
}
