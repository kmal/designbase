<?php
/**
 * @file
 * news_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function news_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function news_content_types_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'industry_news' => array(
      'name' => t('Branchenyt'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'name_article' => array(
      'name' => t('Navnenyt'),
      'base' => 'node_content',
      'description' => t('"Navnenyt" article'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_news' => array(
      'name' => t('Produktomtale'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
