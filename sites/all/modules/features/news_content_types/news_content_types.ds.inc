<?php
/**
 * @file
 * news_content_types.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function news_content_types_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|article|latest_news';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'article';
  $ds_fieldsetting->view_mode = 'latest_news';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'ds_post_date_medium_ds',
    ),
  );
  $export['node|article|latest_news'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|industry_news|latest_news';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'industry_news';
  $ds_fieldsetting->view_mode = 'latest_news';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'ds_post_date_medium_ds',
    ),
  );
  $export['node|industry_news|latest_news'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|name_article|latest_news';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'name_article';
  $ds_fieldsetting->view_mode = 'latest_news';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'ds_post_date_medium_ds',
    ),
  );
  $export['node|name_article|latest_news'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product_news|latest_news';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product_news';
  $ds_fieldsetting->view_mode = 'latest_news';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'ds_post_date_medium_ds',
    ),
  );
  $export['node|product_news|latest_news'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function news_content_types_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|article|latest_news';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'article';
  $ds_layout->view_mode = 'latest_news';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_image_cont',
        1 => 'group_byline',
        2 => 'field_image',
        3 => 'field_add_this',
        4 => 'field_byline',
        5 => 'flag_my_designbase',
        6 => 'field_headline',
        7 => 'field_teaser',
        8 => 'field_category',
      ),
    ),
    'fields' => array(
      'group_image_cont' => 'ds_content',
      'group_byline' => 'ds_content',
      'field_image' => 'ds_content',
      'field_add_this' => 'ds_content',
      'field_byline' => 'ds_content',
      'flag_my_designbase' => 'ds_content',
      'field_headline' => 'ds_content',
      'field_teaser' => 'ds_content',
      'field_category' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|article|latest_news'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|industry_news|latest_news';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'industry_news';
  $ds_layout->view_mode = 'latest_news';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
        1 => 'field_add_this',
        2 => 'flag_my_designbase',
        3 => 'group_buttons',
        4 => 'group_byline',
        5 => 'field_company',
        6 => 'field_headline',
        7 => 'field_teaser',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
      'field_add_this' => 'ds_content',
      'flag_my_designbase' => 'ds_content',
      'group_buttons' => 'ds_content',
      'group_byline' => 'ds_content',
      'field_company' => 'ds_content',
      'field_headline' => 'ds_content',
      'field_teaser' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '1',
    'page_option_title' => '%node:content-type',
  );
  $export['node|industry_news|latest_news'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|name_article|latest_news';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'name_article';
  $ds_layout->view_mode = 'latest_news';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_add_this',
        1 => 'field_image',
        2 => 'group_buttons',
        3 => 'flag_my_designbase',
        4 => 'group_byline',
        5 => 'field_company',
        6 => 'field_headline',
        7 => 'field_teaser',
      ),
    ),
    'fields' => array(
      'field_add_this' => 'ds_content',
      'field_image' => 'ds_content',
      'group_buttons' => 'ds_content',
      'flag_my_designbase' => 'ds_content',
      'group_byline' => 'ds_content',
      'field_company' => 'ds_content',
      'field_headline' => 'ds_content',
      'field_teaser' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|name_article|latest_news'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product_news|latest_news';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product_news';
  $ds_layout->view_mode = 'latest_news';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_add_this',
        1 => 'field_image',
        2 => 'flag_my_designbase',
        3 => 'group_buttons',
        4 => 'group_byline',
        5 => 'field_company',
        6 => 'field_headline',
        7 => 'field_teaser',
        8 => 'field_category',
      ),
    ),
    'fields' => array(
      'field_add_this' => 'ds_content',
      'field_image' => 'ds_content',
      'flag_my_designbase' => 'ds_content',
      'group_buttons' => 'ds_content',
      'group_byline' => 'ds_content',
      'field_company' => 'ds_content',
      'field_headline' => 'ds_content',
      'field_teaser' => 'ds_content',
      'field_category' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '0',
    'page_option_title' => '',
  );
  $export['node|product_news|latest_news'] = $ds_layout;

  return $export;
}
