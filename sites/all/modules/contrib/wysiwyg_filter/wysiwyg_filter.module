<?php

/**
 * Run filters on nodes presave instead of on output
 *
 * @param $node the node that is currently being saved
 *
 * Implements hook_node_presave($node)
 */
function wysiwyg_filter_node_presave($node) {
  wysiwyg_filter_filter_node($node);
}

/**
 * Filter all fields of a node that has a filter attached
 *
 * @param $node the node to filter fields on
 */
function wysiwyg_filter_filter_node($node) {
  $filterable_fields = wysiwyg_filter_get_filterable_field_names();
  foreach ($node as $name => &$field) {
    if (is_array($field) && drupal_substr($name, 0, 6) == 'field_') {
      foreach ($field as $lang => &$value) {
        // If field has a filter, use it on the input
        if (isset($value[0]['format'])) {
          $value[0]['value'] = check_markup($value[0]['value'], $value[0]['format'], $lang);
        }
        // If field does not have filter, check if it should have. Necessary for compatibility with already existing nodes.
        elseif (in_array($name, $filterable_fields)) {
          $value[0]['value'] = check_markup($value[0]['value'], "filtered_html", $lang);
        }
      }
    }
  }
}

function wysiwyg_filter_preprocess_node(&$variables) {
  wysiwyg_filter_filter_node($variables['node']);
}

function wysiwyg_filter_theme_registry_alter(&$reg) {
  $key = "preprocess functions";
  $wysiwyg_pos = 0;
  foreach ($reg['node'][$key] as $pos => $preprocess) {
    if (strstr($preprocess, "wysiwyg")) {
      $wysiwyg_pos = $pos;
      break;
    }
  }
  $tmp = $reg['node'][$key][2];
  $reg['node'][$key][2] = $reg['node'][$key][$wysiwyg_pos];
  $reg['node'][$key][$wysiwyg_pos] = $tmp;
}


/**
 * Retrieves all field names to check for missing filters (replaces wysiwyg_filter_field_has_format when finished)
 */
function wysiwyg_filter_get_filterable_field_names() {
  return variable_get('wysiwyg_filter_filtered_field_names');
}

/**
 * Define admin menu link
 *
 * Implements hook_admin
 */
function wysiwyg_filter_menu() {
  $items = array();
  $items['admin/config/wysiwygfilter'] = array(
    'title'             => 'Wysiwyg filter',
    'description'       => 'Configuration for the Wysiwyg filter',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('wysiwyg_filter_admin_form'),
    'access arguments'  => array('administer users'),
    'type'              => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Get a list of all defined field names
 * @return sorted array containing names of all fields defined for the site
 */
function wysiwyg_filter_get_available_field_names() {
  $field_names = array();
  $fields = field_info_fields();
  $filterable_types = array('text_long', 'text_with_summary');

  foreach($fields as $name => $field) {
    if (in_array($field['type'], $filterable_types)) {
      $field_names[] = $name;
    }
  }
  sort($field_names);

  return $field_names;
}

/**
 * Define admin form
 */
function wysiwyg_filter_admin_form() {
  $form = array();
  $fieldnames = wysiwyg_filter_get_available_field_names();
  $filtered_fields = wysiwyg_filter_get_filterable_field_names();
  $default_selection = array();
  foreach ($filtered_fields as $field => $name) {
    $default_selection[] = array_search($name, $fieldnames);
  }
  
  $form['fields'] = array(
    '#type'           => 'select',
    '#title'          => 'Fields to filter',
    '#options'        => $fieldnames,
    '#default_value'  => $default_selection,
    '#description'    => t('Select the names of all fields that might be required to be run through a wysiwyg filter.'),
    '#multiple'       => TRUE,
    '#size'           => 20,
  );

  $form['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save settings'),
  );

  return $form;
}

/**
 * Hook into admin form's submit process
 */
function wysiwyg_filter_admin_form_submit($form, $form_state) {
  $available_fields = wysiwyg_filter_get_available_field_names();
  $selected_fields = array();
  foreach ($form_state['values']['fields'] as $selected) {
    $selected_fields[] = $available_fields[$selected];
  }

  variable_set('wysiwyg_filter_filtered_field_names', $selected_fields);
  drupal_set_message(t('The settings have been saved!'));
}
