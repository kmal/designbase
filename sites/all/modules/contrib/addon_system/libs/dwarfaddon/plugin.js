(function()
{
    var dwarfAddonCmd =
    {
        canUndo : false,    // The undo snapshot will be handled by 'insertElement'.
        exec : function( editor )
        {
            var element = editor.document.createElement( 'hr' );
            element.addClass('addon');
            editor.insertElement( element );
            editor.insertElement( editor.document.createElement( 'p' ) );
        }
    };

    var pluginName = 'dwarfaddon';

    CKEDITOR.plugins.add( pluginName,
    {
        
        init : function( editor )
        {
            
            editor.addCss(
                '.addon' +
                '{' +
                    'display: block;' +
                    'width: 106px !important;' +
                    'height: 40px !important;' +
                    'margin: 1.25em 0 1em;' +
                    'background: url("'+ this.path + 'images/addon.png") no-repeat;' +
                    'border-width: 0;' +
                    'text-align:left;' +
                '} ' +
                '.addonActive' +
                '{' +
                    'background-position: 0 -40px;' +
                '}'
            );
            
            editor.on( 'selectionChange', function() {
                var selection = editor.getSelection().getSelectedElement();
                    
                $('iframe').contents().find('.cke_show_borders').find('hr').removeClass('addonActive');
                if(selection.hasClass('addon')) {;
                    selection.addClass('addonActive');
                }
            });
            
            editor.addCommand( pluginName, dwarfAddonCmd );
            editor.ui.addButton( 'DwarfAddon',
                {
                    label : Drupal.t('Insert Dwarf Addon'),
                    icon : this.path + 'images/dwarfaddon.gif',
                    command : pluginName
                });
        }
    });
})();
