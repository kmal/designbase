<?php
// $Id$

/**
 * Dwarf addon system module for Drupal 7.
 *
 * @file
 * @author Christoffer Kjeldgaard Petersen - Dwarf A/S <cpt@dwarf.dk>
 */

/**
 * Implements hook_form_alter(). Adds the addon system form on enabled addon types.
 */
function addon_system_form_alter(&$form, $form_state, $form_id) {
  $enabled_types = _get_enabled_types();

  // Edit form:
  if (array_key_exists('type', $form) && !empty($form['type']['#value']) && in_array($form['type']['#value'], $enabled_types)) {

    if (isset($_GET['addon_del']) && $_GET['addon_del'] > 0) {
      node_delete($_GET['addon_del']);
      db_query("DELETE FROM {dwarf_addon_relations} WHERE addon_nid = :addon_nid", array(":addon_nid" => $_GET['addon_del']));
      drupal_set_message(t("Add-on deleted"));
      drupal_goto("/node/" . $form['nid']['#value'] . "/edit");
    }

    $sql = "SELECT nt.type as type, nt.name FROM {dwarf_addon_types} at, {node_type} nt WHERE at.type = nt.type";
    $rs = db_query($sql);
    $options = array();

    foreach ($rs as $obj) {
      $options[$obj->type] = $obj->name;
    }
    $elements = count($options);
    if ($elements == 0) {
      $options[] = "No addon types";
    }

    // Insert the addon fieldset below the content fieldset (if it exists):
    $field_weight = NULL;
    if (array_key_exists("#fieldgroups", $form)) {
      if (array_key_exists("group_content", $form["#fieldgroups"])) {
        $field = _get_type_associated_field($form["#fieldgroups"]["group_content"]->bundle);
        if (in_array($field[1], $form["#fieldgroups"]["group_content"]->children)) {
          $weight_temp = $form["#fieldgroups"]["group_content"]->weight;
          foreach ($form["#fieldgroups"] as $key => $group) {
            if ($form["#fieldgroups"][$key]->weight > $weight_temp) {
              $form["#fieldgroups"][$key]->weight++;
            }
          }
          $form["#fieldgroups"]["group_content"]->weight = $weight_temp;
          $field_weight = $weight_temp;
        }
      }
    }

    $form['addons'] = array(
      "#title" => t("Addons"),
      "#type" => "fieldset",
      "#collapsible" => TRUE,
    );
    if ($field_weight > 0) {
      $form['addons']['#weight'] = $field_weight+1;
    }

    $form['addons']['new_addon'] = array(
      "#title" => t("New Addon"),
      "#default_value" => "image",
      "#type" => "select",
      "#weight" => 1,
      "#options" => $options,
    );

    $form['addons']['current_addons'] = array(
      "#title" => t("Current Addons"),
      "#type" => "fieldset",
      "#collapsible" => FALSE,
    );

    if ($elements  > 0) {
      $form['addons']['add_button'] = array(
        "#weight" => 2,
        "#value" => t("Add Addon"),
        "#type" => "submit",
        "#name" => "add_addon",
        "#attributes" => array(
          "onclick" => "javascript: return confirm('" . variable_get("addon_system_alert", "Remembered to save the node?") . "')",
        )
      );
    }
    $rs = db_query("SELECT * FROM {dwarf_addon_relations} ar, {node} n WHERE n.nid = ar.addon_nid AND  ar.art_nid = :art_nid ORDER BY ar.sort ASC", array(':art_nid' => $form['nid']['#value']));
    $data = array();
    foreach ($rs as $obj) {
      $data[] = $obj;
    }
    $header = array("Title", "weight");
    if (count($data) > 0) {
      foreach ($data as $d) {
        $node = node_load($d->nid);

        $edit_link = l($node->title, "node/" . $d->addon_nid . "/edit", array("query" => array("addon_nid" => $form['nid']['#value'])));
        $delete_link_options = array(
          "attributes" => array(
            "onclick" => "javascript: return confirm('Are you sure you want to delete this addon?')",
          ),
          "query" => array(
            "addon_del" => $d->addon_nid,
          ),
        );
        $delete_link = l(t('Delete'), drupal_get_path_alias($_GET["q"]), $delete_link_options);

        $sql_type_name = "SELECT name from {node_type} WHERE type = :type";
        $rs_type_name = db_query($sql_type_name, array(':type' => $d->type));
        $type_name = $rs_type_name->fetch();

        $form_element[$d->id]['data'] = array(
          "#type" => "value",
          "#value" => array($edit_link . " - (<b>" . $type_name->name . "</b>) - " . $delete_link),
          "#attributes" => array("class" => "weight "),
        );

        $form_element[$d->id]['weight-' . $d->id] = array(
          "#name" => "weight-" . $d->id,
          "#type" => "hidden",
          "#default-value" => $d->sort,
        );
      }

      drupal_add_js('misc/tableheader.js');
      if (count($form_element) > 0) {
        foreach ($form_element as $id => $row) {
          $form_element[$id]['weight-' . $id]['#attributes']['class'] = 'weight';
          $this_row = $row['data']['#value'];

          //Add the weight field to the row
          $this_row[] = drupal_render($form_element[$id]['weight-' . $id]);
          //Add the row to the array of rows
          $table_rows[] = array('data' => $this_row, 'class' => array('draggable'));
        }

        $output = theme('table', array('header' => $header, 'rows' => $table_rows, 'attributes' => array('id' => 'my-module-table')));
      }
      $form['addons']['current_addons']['list'] = array(
        "#type" => "markup",
        "#markup" => $output
      );
    }
    drupal_add_tabledrag('my-module-table', 'order', 'sibling', 'weight');

    if (count($form['addons']['current_addons']) == 3) {
      $form['addons']['current_addons']['err'] = array(
        "#type" => "markup",
        "#markup" => t("No Addons on this article"),
      );
    }
    $form['#submit'][] = "addon_system_edit_form_submit";
  }
}

/**
 * Implements hook_menu().
 */
function addon_system_menu() {
  $items['admin/config/system/addon_system'] = array(
    "title" => "Addon system configuration",
    "description" => "Configuration page for the addon system",
    "access arguments" => array("administer addon system"),
    "page callback" => "_conf_page",
    "type" => MENU_NORMAL_ITEM
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function addon_system_permission() {
  return array(
    'administer addon system' => array(
      'title' => t('Administer addon system'),
      'description' => t('Access the configuration page for the addon system.'),
    ),
  );
}

/**
 * Implements hook_wysiwyg_plugin().
 *
 * @see wysiwyg.api.php
 * @param array $editor Editor name
 * @param string $version Editor version.
 * @return array Settings for Dwarf Addon button in CK editor.
 */
function addon_system_wysiwyg_plugin($editor, $version) {
  switch ($editor) {
    case 'ckeditor':
      return array(
        'dwarfaddon' => array(
          'path' => drupal_get_path('module', 'addon_system') . '/libs/dwarfaddon',
          'buttons' => array(
            'DwarfAddon' => t('Insert Dwarf Addon'),
          ),
          'load' => TRUE,
          'internal' => FALSE,
        ),
      );
    break;
  }
}

function _conf_page() {
  return drupal_get_form("_conf_page_form");
}

/**
 * Creates the addon configuration form on the configuration page.
 *
 * @return array The configuration form.
 */
function _conf_page_form($form, &$form_state) {

  $sql = "SELECT * FROM {dwarf_addon_enabled_types}";
  $rs = db_query($sql);
  $enabled = array();

  foreach ($rs as $obj) {
    $enabled[$obj->type] = $obj->type;
  }

  $sql = "SELECT * FROM {dwarf_addon_types}";
  $rs = db_query($sql);
  $addons_listed = array();

  foreach ($rs as $obj) {
    $addons_listed[$obj->type] = $obj->type;
  }

  $form['alert_message'] = array(
    "#name" => "Alert message",
    "#type" => "textfield",
    "#title" => t("Alert message"),
    "#value" => variable_get("addon_system_alert", "Remembered to save the node?"),
  );

  $sql_types = "SELECT * FROM {node_type}";
  $rs_types = db_query($sql_types);
  $types = array();
  foreach ($rs_types as $obj) {
    $types[$obj->type] = $obj->name;
  }

  $form['types'] = array(
    "#type" => "checkboxes",
    "#title" => t("Types"),
    "#options" => $types,
    "#default_value" => $enabled,
    '#ajax' => array(
      'callback' => 'addon_system_autoselectfields_callback',
      'wrapper' => 'selectboxes_div',
      'effect' => 'fade',
    )
  );
  $form['selectboxes'] = array(
    '#title' => t("Fields"),
    '#prefix' => '<div id="selectboxes_div">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
    '#description' => t('Select fields for each type'),
  );

  // Default enabled select fields:
  foreach ($enabled as $type) {
    $field_name = "field_" . $type;

    $sql_name = "SELECT name FROM {node_type} WHERE type = :type";
    $rs_name = db_query($sql_name, array(':type' => $type));
    $n = $rs_name->fetch();

    $f = _get_type_associated_field($type);

    $form['selectboxes'][$field_name] = array(
      '#type' => 'select',
      '#title' => check_plain($n->name),
      '#options' => _get_content_type_fields($type),
      '#default_value' => $f[0],
    );
  }

  // Dynamically add new select fields:
  foreach ($types as $type => $name) {

    $field_name = "field_" . $type;

    if (!empty($form_state['input']['types'])) {
      if (!empty($form_state['input']['types'][$type]) && $form_state['input']['types'][$type]) {

        $f = _get_type_associated_field($type);
        $form['selectboxes'][$field_name] = array(
          '#type' => 'select',
          '#title' => check_plain($name),
          '#options' => _get_content_type_fields($type),
          '#default_value' => $f[0],
        );

      }
      else {
        unset($form['selectboxes'][$field_name]);
      }
    }
  }

  $form['addons'] = array(
    "#type" => "checkboxes",
    "#title" => t("Addons"),
    "#options" => $types,
    "#default_value" => $addons_listed
  );

  $form['submit'] = array(
    "#type" => "submit",
    "#value" => t("Save"),
  );

  return $form;
}

/**
 * AJAX callback function for replacing field select boxes into the replaceable selectboxes_div.
 *
 * @return array $form variable holding select boxes.
 */
function addon_system_autoselectfields_callback($form, $form_state) {
  return $form['selectboxes'];
}

/**
 * Get content fields for a specific content type.
 *
 * @param string $type Content type name.
 * @return array Returns an array of field IDs and field names.
 */
function _get_content_type_fields($type) {
  $sql_fields = "SELECT field_id, field_name FROM {field_config_instance} WHERE bundle = :bundle";
  $rs = db_query($sql_fields, array(':bundle' => $type));
  $fields = array();
  foreach ($rs as $f) {
    $fields[$f->field_id] = $f->field_name;
  }
  return $fields;
}

/**
 * Get the associated field for a given content type (if any).
 *
 * @param string $type Content type name.
 * @return int Returns ID of the associated field, NULL otherwise.
 */
function _get_type_associated_field($type) {
  $sql_type_id = "SELECT id FROM {dwarf_addon_enabled_types} WHERE type = :type";
  $rs_type = db_query($sql_type_id, array(':type' => $type));
  $type = $rs_type->fetch();

  $sql_field = "SELECT c.id, c.field_name FROM {field_config} c ";
  $sql_field .= "INNER JOIN {dwarf_addon_enabled_fields} f ON c.id = f.field_id ";
  $sql_field .= "WHERE f.enabled_type_id = :enabled_type_id";

  $rs = db_query($sql_field, array(':enabled_type_id' => $type->id));
  $obj = $rs->fetch();
  return array($obj->id, $obj->field_name);
}

/**
 * Submits the addon configuration form. Inserts and deletes types and addons and updates the enabled fields.
 */
function _conf_page_form_submit($form, $form_state) {

  // Insert and delete types:
  foreach ($form_state['values']['types'] as $type => $status) {
    $sql = "SELECT count(*) as c FROM {dwarf_addon_enabled_types} WHERE type = :type";
    $rs = db_query($sql, array(':type' => $type));
    $obj = $rs->fetch();
    $count = $obj->c;

    if ($status === $type && $count == 0) {
      $sql_insert = "INSERT INTO {dwarf_addon_enabled_types} (type) VALUES(:type)";
      db_query($sql_insert, array(':type' => $type));
    }
    elseif ($count != 0 && $status === 0) {
      $sql_delete = "DELETE FROM {dwarf_addon_enabled_types} WHERE type = :type";
      db_query($sql_delete, array(':type' => $type));
    }
  }

  // Insert and delete addons:
  foreach ($form_state['values']['addons'] as $type => $status) {
    $sql_check = "SELECT count(*) as c FROM {dwarf_addon_enabled_types} WHERE type = :type";
    $rs_check = db_query($sql_check, array(':type' => $type));
    $obj = $rs_check->fetch();
    if ($obj->c == 0) {
      $sql = "SELECT count(*) as c FROM {dwarf_addon_types} WHERE type = :type";
      $rs = db_query($sql, array(':type' => $type));
      $obj = $rs->fetch();
      $count = $obj->c;
      if ($status === $type && $count == 0) {
        $sql_insert = "INSERT INTO {dwarf_addon_types} (type) VALUES(:type)";
        db_query($sql_insert, array(':type' => $type));
      }
      elseif ($count != 0 && $status === 0) {
        $sql_delete = "DELETE FROM {dwarf_addon_types} WHERE type = :type";
        db_query($sql_delete, array(':type' => $type));
      }
    }
  }

  // Set and delete enabled fields:
  db_query("UPDATE {dwarf_addon_enabled_fields} SET delete_me = 1");
  foreach ($form_state['values'] as $key => $value) {

    if (drupal_substr($key, 0, 6) == "field_") {
      $parts = explode("_", $key);

      $sql_type_id = "SELECT id FROM {dwarf_addon_enabled_types} WHERE type = :type";
      $rs_type = db_query($sql_type_id, array(':type' => $parts[1]));
      $type = $rs_type->fetch();

      $sql_save = "REPLACE INTO {dwarf_addon_enabled_fields} (field_id, enabled_type_id, delete_me) VALUES (:field_id, :enabled_type_id, 0)";

      db_query($sql_save, array(':field_id' => $value, ':enabled_type_id' => $type->id));
    }
  }
  $sql_delete = "DELETE FROM {dwarf_addon_enabled_fields} WHERE delete_me = 1";
  db_query($sql_delete);
}

/**
 * Implements hook_form_submit().
 */
function addon_system_edit_form_submit($form, &$form_state) {

  // Update addon relations if Save button was clicked:
  if ($form_state['clicked_button']['#value'] == t('Save')) {
    $i = 1;
    if (!is_array($form_state['input'])) {
      $form_state['input'] = array();
    }
    foreach ($form_state['input'] as  $key => $values) {
      if (strpos($key, "weight-") !== FALSE) {
        list($string, $id) = explode("-", $key);
        $sql = "UPDATE {dwarf_addon_relations} SET sort = :sort WHERE id = :id";
        db_query($sql, array(':sort' => $i, ':id' => $id));
        $i++;
      }
    }
  }

  // If Add addon button clicked, then redirect to addon creation page:
  if ($form_state['values']['new_addon'] && $form_state['clicked_button']['#name'] == "add_addon") {
    header("location: /node/add/" . str_replace("_", "-", $form_state['values']['new_addon']) . "?addon_nid=" . $form_state['values']['nid']);
    exit();
  }
}

/**
 * Get enabled addon types.
 *
 * @return array An array of enabled types.
 */
function _get_enabled_types() {
  $sql = "SELECT * FROM {dwarf_addon_enabled_types}";
  $rs = db_query($sql);
  $types = array();
  foreach ($rs as $obj) {
    $types[$obj->type] = $obj->type;
  }
  return $types;
}

/**
 * Implements hook_node_insert(). To insert the addon into the relation table and redirect to the article.
 */
function addon_system_node_insert($node) {
  global $user;
  $enabled_types = _get_enabled_types();

  if (isset($_GET['addon_nid']) && $_GET['addon_nid'] > 0) {
    $sql_max = "SELECT MAX(sort) as next_sort FROM {dwarf_addon_relations} WHERE art_nid = :art_nid";
    $rs = db_query($sql_max, array(':art_nid' => $_GET['addon_nid']));
    $obj_max = $rs->fetch();
    $sql_insert = "INSERT INTO {dwarf_addon_relations} (name, art_nid, addon_nid, sort) VALUES (:name, :art_nid, :addon_nid, :sort)";
    $values = array(
      ':name' => $node->title,
      ':art_nid' => $_GET['addon_nid'],
      ':addon_nid' => $node->nid,
      ':sort' => $obj_max->next_sort+1,
    );
    db_query($sql_insert, $values);

    drupal_set_message(t("Addon Saved"));
    drupal_goto("node/" . $_GET['addon_nid'] . "/edit");
  }
}

function addon_system_node_update($node) {
  if (isset($_GET['addon_nid']) && $_GET['addon_nid'] > 0) {
    drupal_goto("node/" . $_GET['addon_nid'] . "/edit");
  }
}

/**
 * Implements hook_node_presave(). Performs Word cleamup if the current node is an addon enabled type.
 */
function addon_system_node_presave($node) {
  $enabled_types = _get_enabled_types();
  if (in_array($node->type, $enabled_types)) {
    _cleanup_word($node);
  }
}

/**
 * Implements hook_node_prepare(). Performs Word cleamup if the current node is an addon enabled type.
 */
function addon_system_node_prepare($node) {
  $enabled_types = _get_enabled_types();
  if (in_array($node->type, $enabled_types)) {
    _cleanup_word($node);
  }
}

/**
 * Removes HTML generated by Word from addon enabled content fields.
 *
 * @param object $node Node to clean.
 */
function _cleanup_word($node) {

  // Replacement array:
  $rpl = array();
  $rpl["<span>"] = "";
  $rpl['<span lang="EN-US">'] = "";
  $rpl['<span lang="EN">'] = "";
  $rpl['<span lang="DA">'] = "";
  $rpl['<o p="">'] = "";
  $rpl['</o>'] = "";
  $rpl["</span>"] = "";
  $rpl["</font>"] = "";
  $rpl["<div>"] = "<p>";
  $rpl["</div>"] = "</p>";
  $rpl["<p>&nbsp;</p>"] = "";
  $rpl["<p></p>"] = "";
  $rpl[' class="MsoNormal"'] = "";
  $rpl[' addonActive'] = "";

  $field = _get_type_associated_field($node->type);
  $a = &$node->$field[1];

  // Replacement of tags
  foreach ($rpl as $key => $val) {
    $a['und'][0]['value'] = str_replace($key, $val, $a['und'][0]['value']);
  }

  // Remove all font tags
  $a['und'][0]['value'] = preg_replace("/<font[^>]*>/", "", $a['und'][0]['value']);
}

/**
 * Implements hook_preprocess_node(). Checks if the current node is addon enabled and replaces themed addons into the corresponding fields.
 */
function addon_system_preprocess_node(&$variables) {

  $n = $variables["node"];

  if (is_object($n)) {
    // Check if current node can have addons:
    $sql_type = "SELECT type FROM {dwarf_addon_enabled_types} WHERE type = :type";
    $rs_type = db_query_range($sql_type, 0, 1, array(':type' => $n->type));
    if ($rs_type->rowCount() == 1) {

      $ids = _get_addon_ids($n->nid);

      $field = _get_type_associated_field($n->type);
      $field_name = $field[1];
      $node_field = &$n->$field_name;

      foreach ($ids as $nid) {
        $addon = node_load($nid);
        if ($addon) {
          $rpl = "";

          $render_array = node_show($addon);
          $rpl = drupal_render($render_array);

          $node_field['und'][0]['value'] = preg_replace('[(<p>)?<hr class="addon" \/>(</p>)?]', $rpl, $node_field['und'][0]['value'], 1);
        }
      }

      // Cleanup:
      $node_field['und'][0]['value'] = str_replace('<hr class="addon" />', "", $node_field['und'][0]['value']);
      $node_field['und'][0]['value'] = str_replace("<p></p>", "", $node_field['und'][0]['value']);
    }
  }
}

/**
 * Get addon ID's related to the current addon enabled type.
 *
 * @param int $id ID of the addon enabled type to get addon ID's from.
 * @return array Array containing the addons related to the current addon enabled type.
 */
function _get_addon_ids($id) {
  $sql = "SELECT addon_nid as nid FROM {dwarf_addon_relations} WHERE art_nid = :art_nid ORDER by sort";
  $result = db_query($sql, array(':art_nid' => $id));
  $a = array();
  foreach ($result as $o) {
    $a[] = $o->nid;
  }
  return $a;
}
